<?php

use Illuminate\Http\Request;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
 */
/*

 */
Route::middleware('auth:api')->get('/user', function (Request $request) {
    return $request->user();
});

Route::prefix('v2')->namespace('Api')->group(function () {
    Route::get('testing', function () {

        $product = "18-6135989";
        $analysis = "12238";

        $missing_data_samples = \App\Models\Task::select(['tasks.*'])->with('subtasks')->join('subtasks', 'subtasks.sample_number', '=', 'tasks.sample_number')
            ->where(function ($query) use ($product, $analysis) {
                $query->where('tasks.product', $product)
                    ->where('tasks.analysis', $analysis);
            })->orWhere(function ($query) use ($product, $analysis) {
            $query->where('tasks.product', $product)
                ->where('subtasks.analysis', $analysis);
        })->where('duration', 0)
            ->pluck('tasks.sample_number')->toArray();

        dd(array_unique($missing_data_samples));
        // event(new \App\Events\NewImportEvent);
    });

    Route::any('testing/SNS', function () {
        try {
            // Retrieve the message
            $message = \Aws\Sns\Message::fromRawPostData();

            // make validator instance
            $validator = new \Aws\Sns\MessageValidator();

            // Validate the message
            if ($validator->isValid($message)) {
                if ($message['Type'] == 'SubscriptionConfirmation') {
                    // if it's subscription or unsubscribe event then call SubscribeURL
                    file_get_contents($message['SubscribeURL']);
                } elseif ($message['Type'] === 'Notification') {
                    $subject = $message['Subject'];
                    $messageData = json_decode($message['Message']);
                    // use $subject and $messageData and take relevant action
                }
            }
        } catch (Exception $e) {
            dd($e);
        }
    });

    Route::post('/task/import', 'TasksController@importTasks');
    Route::post('/duration/import', 'TasksController@importDuratio');
    Route::post('/task/xml/import', 'TasksController@importTaskFromXml');
    Route::get('/task/week/{index}', 'TasksController@getTasksByWeekIndex');
    Route::put('/task/reset/duration/{id}', 'TasksController@updateTaskDuration');
    Route::put('/task/reorder/{id}', 'TasksController@updateTasksOrder');

    Route::get('/backlog/daily', 'TasksController@getDailyBacklog');

    Route::get('/task/missed', 'TasksController@getMissedTasks');

    Route::get('/task/adjust', 'TasksController@adjustTasks');

    Route::get('/team/states', 'TeamsController@getStates');
    Route::get('/team/leaderships', 'TeamsController@getLeaderships');
    Route::get('/team/number/{date}', 'TeamsController@getTeamByDate');
    Route::get('/team/state/{date}', 'TeamsController@getStatesInfoByDate');
    Route::get('/team/leader-presence/state/{date}', 'TeamsController@getLeaderPresenceStatesInfoByDate');
    Route::post('/team/state', 'TeamsController@modifyTeamState');
    Route::post('/team/leadership', 'TeamsController@modifyLeadershipState');
    Route::get('/team/leadership/chart', 'TeamsController@leadershipChart');
    Route::get('/team/mood-leadership/chart', 'TeamsController@moodVsLeadershipChart');
    Route::get('/team/trend', 'TeamsController@getTrend');
    Route::get('/team/trend/weekly', 'TeamsController@getTrendWeekly');
    Route::get('/team/trend/simple', 'TeamsController@getSimpleTrend');
    Route::post('/team/mood', 'TeamsController@changeMood');
    Route::post('/team/avatar/add', 'TeamsController@addAvatar');
    Route::get('/team/avatars', 'TeamsController@avatars');
    Route::get('/team/members', 'TeamsController@teamMembers');
    // Route::get('/state/trend/{date}', 'TeamsController@getTrendState');
    Route::put('/capacity/modify', 'TeamsController@updateCapacityByDate');
    Route::put('/demandrate/modify', 'TeamsController@updateDemandRateByDate');
    // Route::get('/state/trend/month/{month}', 'TeamsController@getTrendByMonth');

    Route::get('/team/calendar', 'TeamsController@getTeamCalendar');

    Route::get('/guage/analytic', 'CharsController@getGuageAnalytic');
    Route::get('/guage/sap', 'CharsController@getGuageSap');
    Route::get('/Schedule/Adherence', 'CharsController@getScheduleAdherence');
    Route::get('/Schedule/Adherence/week', 'CharsController@getScheduleAdherenceInWeeks');
    Route::get('/demand/rate', 'CharsController@getDemandRate');
    Route::get('/productivity', 'CharsController@getProductivityChart');

    // Route::get('/demand/rate/week', 'CharsController@getDemandRatePerWeek');
    // Route::get('/demand/rate/month', 'CharsController@getDemandRatePerMonth');
    Route::get('/demand/rate/year', 'CharsController@getDemandRatePerYear');
    Route::get('/productivity/year', 'CharsController@getProductivityChartPerYear');

    Route::get('/xml/new', 'TasksController@getNewXml');

    Route::PUT('/adminTasks/update', 'AdminTasksController@update');

    Route::get('/backlog/alltime', 'TasksController@getAllTimeBackLog');
    Route::get('/samples/missed', 'TasksController@allMissingDataSample');
    Route::post('/task/move', 'TasksController@moveTask');
    Route::post('/backlog/task/move', 'TasksController@moveAllTimeBacklogTask');
    Route::post('/task/move/backlog', 'TasksController@moveTaskToBacklog');

    //CRUD on List
    Route::post('/list/task/add', 'ListController@addTask');
    Route::get('/list/task/{index}', 'ListController@index');
    Route::delete('/list/remove/task/{id}', 'ListController@removeTask');
    Route::put('/list/update/task/{id}', 'ListController@updateTask');
    Route::get('/list/all/task', 'ListController@getAllTime');

    Route::POST('/sixs/modify', 'SixSController@modifyValues');

    ROUTE::GET('/sixs/month', 'SixSController@listValuesOfCurrentMonth');

    Route::GET('sixs/year', 'SixSController@listValuesPerYear');
    Route::GET('sixs/quarters/{year1}/{qu1}/{year2}/{qu2}', 'SixSController@listPerQuarter');

    Route::POST('/types/modify', 'TypesController@modifyValue');
    Route::POST('/types/month', 'TypesController@listTypeValuesPerMonth');
    Route::POST('/types/week', 'TypesController@listTypeValuesPerWeek');

    Route::GET('/ehs/daily', 'TypesController@getEhsData');
    Route::GET('/quality/daily', 'TypesController@getQualityDataDaily');
    Route::GET('/quality/trend', 'TasksController@getQualityTrend');

    Route::GET('/service/daily', 'ServiceController@getServicesDataDaily');
    Route::GET('/service/duedate/daily', 'ServiceController@getDueDatesDaily');

    Route::GET('/service/routine/month', 'ServiceController@getRoutineSamplesOfMonth');
    Route::GET('/service/special/month', 'ServiceController@getSpecialSamplesOfMonth');
    Route::GET('/service/routine/week', 'ServiceController@getRoutineSamplesOfWeek');
    Route::GET('/service/special/week', 'ServiceController@getSpecialSamplesOfWeek');

    Route::GET('/duedate/month', 'ServiceController@getDueDatesIntervalForCurrentMonth');
    Route::GET('/duedate/week', 'ServiceController@getDueDatesIntervalForCurrentWeek');
    Route::GET('/rescheduled/month', 'ServiceController@getRescheduledOfMonth');
    Route::GET('/rescheduled/week', 'ServiceController@getRescheduledSamplesOfWeek');
    Route::GET('/service/samples', 'ServiceController@samples');

    Route::get('/dashboard', 'CharsController@getDashboard');

    Route::POST('/sixs/modify', 'SixSController@modifyValues');

    ROUTE::GET('/sixs/month', 'SixSController@listValuesOfCurrentMonth');

    Route::GET('sixs/year', 'SixSController@listValuesPerYear');

    Route::POST('/types/modify', 'TypesController@modifyValue');
    Route::POST('/types/month', 'TypesController@listTypeValuesPerMonth');

    Route::get('/dashboard', 'CharsController@getDashboard');

    Route::GET('/problems', 'ProblemsController@index');
    Route::GET('/problems/statuses', 'ProblemStatusesController@index');
    Route::GET('/problems/categories', 'ProblemCategoriesController@index');
    Route::POST('/problem', 'ProblemsController@store');
    Route::PUT('/problem/{id}', 'ProblemsController@update');
    Route::DELETE('/problem/{problem}', 'ProblemsController@destroy');

    Route::POST('/lock/code', 'ListController@checkLockCode');

    Route::RESOURCE('highlights', 'HighlightsController');
    Route::GET('team/highlights', 'HighlightsController@teamHighlights');
    Route::GET('team/highlights/latest', 'HighlightsController@teamLatestHighlight');
    Route::POST('team/highlights/emoji', 'HighlightsController@emoji');
    Route::DELETE('/highlights/{id}', 'HighlightsController@destroy');

    Route::GET('/meeting-time', 'ListController@meetingTime');
    Route::POST('/config', 'ConfigurationsController@configure');
    Route::POST('/configurations/update', 'ConfigurationsController@updateAllConfigurations');
    Route::get('/configurations', 'ConfigurationsController@getAllConfigurations');

    Route::POST('/configurations/section/{id}/update', 'ConfigurationsController@setConfigurations');
    Route::get('/configurations/section/{id}', 'ConfigurationsController@listConfigurationsBySection');
    Route::get('/sections', 'ConfigurationsController@listSections');

    ROUTE::GET('/backlog/list/{date}', 'TasksController@listDailyBacklog');

    /**
     * Custom configuration apis
     */
    Route::POST('/configurations/team/update', 'ConfigurationsController@setTeamConfigurations');
    Route::GET('/configurations/team', 'ConfigurationsController@getTeamConfigurations');
    Route::POST('/configurations/general/update', 'ConfigurationsController@setGeneralConfigurations');
    Route::GET('/configurations/general', 'ConfigurationsController@getGeneralConfigurations');
    Route::POST('/configurations/productivity/update', 'ConfigurationsController@setProductivityConfigurations');
    Route::GET('/configurations/productivity', 'ConfigurationsController@getProductivityConfigurations');
    Route::POST('/configurations/quality/update', 'ConfigurationsController@setQualityConfigurations');
    Route::GET('/configurations/quality', 'ConfigurationsController@getQualityConfigurations');
    Route::POST('/configurations/service/update', 'ConfigurationsController@setServiceConfigurations');
    Route::GET('/configurations/service', 'ConfigurationsController@getServiceConfigurations');
    Route::POST('/configurations/challenges/update', 'ConfigurationsController@setChallengesConfigurations');
    Route::GET('/configurations/challenges', 'ConfigurationsController@getChallengesConfigurations');

    Route::get('/durations', 'DurationController@index');
    Route::get('/durations/export', 'DurationController@export');
    Route::put('/durations/{duration}/update', 'DurationController@update');

    Route::get('/productivity/forecast-hours', 'ProductivityController@forecastHours');

    /*
    Recent Activity
     */
    Route::GET('/logs', 'LogsController@index');
    Route::GET('/uploads/history', 'LogsController@getUploadsHistory');
    Route::GET('/details', 'LogsController@getBoardDetails');
    Route::GET('/import/unsolved/latest', 'LogsController@getLatestUnsolvedImport');
    Route::GET('/errors/check', 'LogsController@hasUnsolvedImport');
    Route::DELETE('/files/{id}', 'LogsController@removeUnsolvedImport');

    Route::GET('/files/{id}', 'DownloadController@getDownloadUrl');
    Route::GET('/files/download/{name}', 'DownloadController@download');
    Route::GET('/holidays', 'LogsController@getHolidays');

});

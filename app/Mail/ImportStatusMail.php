<?php

namespace App\Mail;

use Illuminate\Bus\Queueable;
use Illuminate\Mail\Mailable;
use Illuminate\Queue\SerializesModels;

class ImportStatusMail extends Mailable
{
    use Queueable, SerializesModels;

    public $errors;
    public $date;
    public $general_status;

    /**
     * Create a new message instance.
     *
     * @return void
     */
    public function __construct($errors, $date, $general_status = false)
    {
        $this->errors = $errors;
        $this->date = $date;
        $this->general_status = $general_status;
    }

    /**
     * Build the message.
     *
     * @return $this
     */
    public function build()
    {
        $lines = [];
        if (!$this->general_status) {
            $subject = 'Import failure notification.';
            $lines[] = 'Something went wrong with products importing';
        } else {
            $subject = 'Import success notification.';
            $lines[] = 'File imported successfully with few exceptions';
        }

        $lines[] = '    Here is the error logs : ';
        $mail = $this->subject($subject)
            ->markdown('vendor.notifications.markdown', [
                'introLines' => $lines,
                'errors' => $this->errors,
                'date' => $this->date,
            ]);

        return $mail;
    }
}

<?php

namespace App\Console\Commands;

use Illuminate\Console\Command;
use App\Helpers\TaskHelper;

class CloneTask extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'task:clone';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Clone Not completed Tasks';

    /**
     * task service
     */
    protected $task;

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct(TaskHelper $task)
    {
        parent::__construct();

        $this->task = $task;
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        $this->task->getMissedTasks();

         $this->info('Tasks Cloned Successfully.');
    }
}

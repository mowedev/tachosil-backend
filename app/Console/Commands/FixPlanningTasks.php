<?php

namespace App\Console\Commands;

use App\Helpers\DatesUtility;
use App\Helpers\ListHelper;
use Carbon\Carbon;
use DB;
use Illuminate\Console\Command;

class FixPlanningTasks extends Command
{
    use DatesUtility;
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'task:fixPlanningTasksMutlipleDays';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Command description';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        $now = Carbon::now();

        $planned_sample = DB::table('tasks')->select(['tasks.sample_number'])
            ->selectRaw("0 as `part`, MAX(`tasks`.`original_due_date`) as due_date, MAX(`tasks`.`days`) as days")
            ->whereNull('tasks.date_reviewed')
            ->whereNull('tasks.receive_date')
            ->groupBy('tasks.sample_number')
            ->get();

        $samples = $planned_sample->pluck('sample_number')->toArray();
        DB::table('task_version')->whereIn('sample_number', $samples)->where('state', 1)->delete();

        foreach ($planned_sample->groupBy('days')->toArray() as $days => $tasks) {
            $task_versions = array_map(function ($item) {
                $item = json_decode(json_encode($item), true);

                $due = Carbon::parse($item['due_date']);
                $days = $item['days'] > 0 ? $item['days'] : 1;

                for ($i = 0; $i < $days; $i++) {
                    $due->subDays(1);

                    while (DatesUtility::dateIsHoliday($due->toDateString())) {
                        $due->subDays(1);
                    }
                }
                $item['due_date'] = $due->toDateString();

                return (object) $item;
            }, $tasks);

            $list_helper = new ListHelper();
            $list_helper->addParts($task_versions, [], 0, $days, $now, true);
        }
    }
}

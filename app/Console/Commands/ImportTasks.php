<?php

namespace App\Console\Commands;

use Illuminate\Console\Command;
use App\Helpers\ImportHelper;

class ImportTasks extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'task:import';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Task Import Command';

    /**
     * import service
     */
    protected $import;

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct(ImportHelper $import)
    {
        parent::__construct();

        $this->import = $import;
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        $ids = $this->import->getNewXmlFile();


        if(!empty($ids)){
            //event(new \App\Events\NewImportEvent);
            event(new \App\Events\LogsUpdateEvent($ids));
        }

        $this->info('Tasks Imported Successfully.');
    }
}

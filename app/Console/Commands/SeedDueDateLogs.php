<?php

namespace App\Console\Commands;

use App\Helpers\serviceHelper;
use Carbon\Carbon;
use Illuminate\Console\Command;
use DB;

class SeedDueDateLogs extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'duedatelogs:seed';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Seed due date logs table';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function handle()
    {
        $now = Carbon::now();
        $week = 1;
        $year = 2018;

        $current_year = $now->year;
        $current_week = $now->weekOfYear;

        $helper = new serviceHelper;
        $insert_logs = [];
        while ($year <= $current_year) {
            $weeks_limit = $year == $current_year ? ($current_week - 1) : $this->getIsoWeeksInYear($year);
            $week = 1;

            while ($week < $weeks_limit) {
                $specific_week = Carbon::now()->setISODate($year, $week);
                $startDate = $specific_week->copy()->startOfWeek()->toDateString();
                $endDate = $specific_week->copy()->endOfWeek()->toDateString();

                $query = $helper->getDuedatesIntervalQuery($startDate, $endDate);
                $dates = DB::select(DB::raw($query));

                foreach ($dates as $date) {
                    if ($date->days > 0 && $date->days <= 10) {
                        $insert_logs[] = [
                            'row_week' => $date->row_week,
                            'row_year' => $date->row_year,
                            'sample_number' => $date->sample_number,
                            'login_date' => $date->login_date,
                            'tv_due_date' => $date->due_date,
                            'tv_created_at' => $date->created_at,
                            'state' => $date->state,
                            'days' => $date->days,
                            'created_at' => $now->toDateTimeString(),
                        ];
                    }
                }
                $week++;
            }

            $year++;
        }

        DB::table('due_date_logs')->insert($insert_logs);
    }

    private function getIsoWeeksInYear($year)
    {
        $date = new \DateTime;
        $date->setISODate($year, 53);
        return ($date->format("W") === "53" ? 53 : 52);
    }
}

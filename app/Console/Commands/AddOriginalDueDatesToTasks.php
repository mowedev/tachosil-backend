<?php

namespace App\Console\Commands;

use DB;
use Illuminate\Console\Command;

class AddOriginalDueDatesToTasks extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'task:resetOriginalDueDates';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Command description';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        DB::table('tasks')
            ->join('subtasks', 'subtasks.sample_number', '=', 'tasks.sample_number')
            ->groupBy('subtasks.sample_number')
            ->update(['tasks.original_due_date' => DB::raw('`subtasks`.`due_date`')]);
    }
}

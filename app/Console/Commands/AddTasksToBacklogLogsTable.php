<?php

namespace App\Console\Commands;

use Illuminate\Console\Command;
use Carbon\Carbon;
use DB;

class AddTasksToBacklogLogsTable extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'backlog:insert';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Command description';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        $today = Carbon::today();

        $samples_already_logged = DB::table('backlog_logs')->pluck('sample_number')->toArray();

        $missed = DB::table('subtasks')
            ->selectRaw('subtasks.sample_number, MAX(due_date) as due_date')
            ->selectRaw('MAX(tasks.analysis) as analysis, MAX(tasks.duration) as duration, MAX(tasks.batch) as batch')
            ->selectRaw('MAX(tasks.product) as product, MAX(tasks.days) as days, MAX(tasks.description) as description')
            ->selectRaw('MAX(tasks.status) as status, MAX(tasks.project) as project, MAX(tasks.original_due_date) as original_due_date')
            ->selectRaw('MAX(tasks.login_date) as login_date, MAX(tasks.receive_date) as receive_date, MAX(tasks.date_completed) as date_completed, MAX(tasks.date_reviewed) as date_reviewed')
            ->join('tasks', 'tasks.sample_number', '=', 'subtasks.sample_number')
            ->whereNotIn('subtasks.sample_number', $samples_already_logged)
            ->where('due_date', '<=', $today->toDateString())
            ->whereNull('tasks.date_reviewed')
            ->groupBy('subtasks.sample_number')
            // ->pluck('sample_number')
            ->get();
        
        $missed_samples = $missed->pluck('sample_number')->toArray();
        
        $task_versions = DB::table('task_version')
            ->selectRaw('sample_number, MAX(due_date) as due_date')
            ->where('state', 1)
            ->where('is_hard_moved_from_backlog', 0)
            ->whereIn('sample_number', $missed_samples)
            ->groupBy('sample_number')
            ->get();
        
        $task_version_groupBy_sample = [];
        foreach ($task_versions as $task_version) {
            $task_version_groupBy_sample[$task_version->sample_number] = $task_version->due_date;
        }

        $backlog = [];
        foreach ($missed as $task) {
            if (isset($task_version_groupBy_sample[$task->sample_number])) {
                $tv_due_date = $task_version_groupBy_sample[$task->sample_number];
                if ($task->due_date < $today && $tv_due_date < $task->due_date) {
                    $backlog[] = $task;
                }
            }
        }

        $now = Carbon::now()->toDateTimeString();
        $backlog = array_map(function ($item) use ($now) {
            return [
                'batch' => $item->batch,
                'product' => $item->product,
                'sample_number' => $item->sample_number,
                'project' => $item->project,
                'description' => $item->description,
                'due_date' => $item->due_date,
                'date_reviewed' => $item->date_reviewed,
                'date_completed' => $item->date_completed,
                'login_date' => $item->login_date,
                'receive_date' => $item->receive_date,
                'status' => $this->getStatus($item), 
                'duration' => $item->duration,
                'created_at' => $now,
            ];
        }, $backlog);

        DB::table('backlog_logs')->insert($backlog);
    }

    private function getStatus($task)
    {
        $inProgress = DB::table("subtasks")->distinct()->whereNotNull("receive_date")->pluck("sample_number")->toArray();
        if (!is_null($task->date_reviewed)) {
            return 1;
        } elseif (in_array($task->sample_number, $inProgress)) {
            return 2;
        } else {
            return 3;
        }
    }
}

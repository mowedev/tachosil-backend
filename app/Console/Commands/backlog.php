<?php

namespace App\Console\Commands;

use Illuminate\Console\Command;
use App\Helpers\ImportHelper;

class backlog extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'task:backlog';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Create baclog for every day after end of day';

    /**
     * import service
     */
    protected $import;

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct(ImportHelper $import)
    {
        parent::__construct();

        $this->import = $import;
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        $this->import->getBacklogProducts();

        $this->info('Backlog Completed Successfully.');
    }
}

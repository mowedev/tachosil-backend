<?php

namespace App\Console\Commands;

use App\Helpers\serviceHelper;
use Carbon\Carbon;
use DB;
use Illuminate\Console\Command;

class UpdateDueDateLogs extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'duedatelogs:update';

    /**
     * The console command description.
     * Run once every day
     * 30 23 * * * php artisan duedatelogs:update
     * @var string
     */
    protected $description = 'Update due date logs table';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        $now = Carbon::now();

        $year = $now->year;
        $week = $now->weekOfYear;

        // if ($week === 0) {
        //     $year = $year - 1;
        //     $week = $this->getIsoWeeksInYear($year);
        // }

        $helper = new serviceHelper;
        $insert_logs = [];

        $specific_week = Carbon::now()->setISODate($year, $week);
        $startDate = $specific_week->copy()->startOfWeek()->toDateString();
        $endDate = $specific_week->copy()->endOfWeek()->toDateString();

        $query = $helper->getDuedatesIntervalQuery($startDate, $endDate);
        $dates = DB::select(DB::raw($query));

        foreach ($dates as $date) {
            if ($date->days > 0 && $date->days <= 10 && $date->created_at == $now->toDateString()) { // assuming the cron will run everyday at 11:30 PM
                $insert_logs[] = [
                    'row_week' => $date->row_week,
                    'row_year' => $date->row_year,
                    'sample_number' => $date->sample_number,
                    'login_date' => $date->login_date,
                    'tv_due_date' => $date->due_date,
                    'tv_created_at' => $date->created_at,
                    'state' => $date->state,
                    'days' => $date->days,
                    'created_at' => $now->toDateTimeString(),
                ];
            }
        }

        DB::table('due_date_logs')->insert($insert_logs);
    }

    private function getIsoWeeksInYear($year)
    {
        $date = new \DateTime;
        $date->setISODate($year, 53);
        return ($date->format("W") === "53" ? 53 : 52);
    }
}

<?php

namespace App\Listeners;

use App\Events\NewImportEvent;
use App\Helpers\ehsHelper;
use App\Helpers\qualityHelper;
use App\Helpers\serviceHelper;
use App\Helpers\TaskHelper;
use App\Helpers\ConfigurationHelper;
use App\Http\Controllers\Api\CharsController;
use App\Http\Controllers\Api\TeamsController;
use App\Http\Controllers\Api\TasksController;
use Illuminate\Http\Request;

class NewImportListener
{
    /**
     * Create the event listener.
     *
     * @return void
     */
    public function __construct()
    {
        //
    }

    /**
     * Handle the event.
     *
     * @param  Event  $event
     * @return void
     */
    public function handle(NewImportEvent $event)
    {
        try {
            $client = new \GuzzleHttp\Client(["base_uri" => "https://socket.api.gethuddly.com:443"]);
            $options = [
                'form_params' => [

                    'instance' => env('INSTANCE_NAME'),
                    'event' => 'pulse',
                    'data' => [
                        'dashboard' => (new CharsController)->prepareDashboardData(new TaskHelper, new ehsHelper, new qualityHelper, new serviceHelper, new ConfigurationHelper, new Request)->toArray(),
                        'team' => $event->has_team ? (new TeamsController)->prepareSimpleTrend() : null,
                    ],
                ]   ,
            ];

            $response = $client->post("/socket/emit", $options);

        } catch (\Exception $e) {
            dd("test");
        }
    }
}

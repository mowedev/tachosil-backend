<?php

namespace App\Listeners;

use App\Events\ProductivityUpdateEvent;
use App\Helpers\TaskHelper;
use App\Http\Controllers\Api\TasksController;
use GuzzleHttp\Client;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Http\Request;
use Illuminate\Queue\InteractsWithQueue;

class ProductivityUpdateListener implements ShouldQueue
{

    use InteractsWithQueue;
    /**
     * Create the event listener.
     *
     * @return void
     */
    public function __construct()
    {
        //
    }

    /**
     * Handle the event.
     *
     * @param  ProductivityUpdateEvent  $event
     * @return void
     */
    public function handle(ProductivityUpdateEvent $event)
    {
        //
        try {

            $client = new \GuzzleHttp\Client(["base_uri" => "https://socket.api.gethuddly.com:443"]);
            $options = [

                'form_params' => [
                'instance' => env('INSTANCE_NAME'),
                'event' => 'productivity',
                    'data' => [
                    'old_week' => $event->oldWeek,
                    'new_week' => $event->newWeek,
                    'productivity_old' => ($event->oldWeek) ? (new TasksController)->getTasksByWeekIndex(new TaskHelper, $event->oldWeek, new Request, true) : [],
                    'productivity_new' => (new TasksController)->getTasksByWeekIndex(new TaskHelper, $event->newWeek, new Request, true),
                    'missing' => (new TasksController)->allMissingDataSample(new TaskHelper, true),
                    'backlog' => (new TasksController)->getAllTimeBackLog(new TaskHelper, true),
                    'id' => $event->userId,
                    ],
                ],
            ];

            $response = $client->post("/socket/emit", $options);

        } catch (\Exception $e) {
            dd($e);
        }
    }
}

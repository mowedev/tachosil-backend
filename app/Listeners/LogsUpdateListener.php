<?php

namespace App\Listeners;

use App\Events\LogsUpdateEvent;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Contracts\Queue\ShouldQueue;
use App\Http\Controllers\Api\LogsController;
use Illuminate\Http\Request;


class LogsUpdateListener implements ShouldQueue
{
    use InteractsWithQueue;

    /**
     * Create the event listener.
     *
     * @return void
     */
    public function __construct()
    {
        //
    }

    /**
     * Handle the event.
     *
     * @param  LogsUpdateEvent  $event
     * @return void
     */
    public function handle(LogsUpdateEvent $event)
    {
        //
        try{

            $regionUrl = env('REGION_URL');
            $boardName = env('BOARD_NAME');
            $client = new \GuzzleHttp\Client(["base_uri" => $regionUrl]);
            $options = [

                'form_params' => [
                    'data' =>  (new LogsController)->getLogsByIds($event->importedIds),
                    'boardName' => $boardName
                ],
            ];

            $response = $client->post("/logs/board/update", $options); 

        }catch (\Exception $e) {
            dd($e);
        }
    }
}

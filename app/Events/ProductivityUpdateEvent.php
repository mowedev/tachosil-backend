<?php

namespace App\Events;

use Illuminate\Broadcasting\Channel;
use Illuminate\Queue\SerializesModels;
use Illuminate\Broadcasting\PrivateChannel;
use Illuminate\Broadcasting\PresenceChannel;
use Illuminate\Foundation\Events\Dispatchable;
use Illuminate\Broadcasting\InteractsWithSockets;
use Illuminate\Contracts\Broadcasting\ShouldBroadcast;

class ProductivityUpdateEvent
{
    use Dispatchable, InteractsWithSockets, SerializesModels;

    public $oldWeek;
    public $newWeek;
    public $userId;

    /**
     * Create a new event instance.
     *
     * @return void
     */
    public function __construct($oldWeek, $newWeek)
    {
        //
        $this->oldWeek = $oldWeek;
        $this->newWeek = $newWeek;
        $this->userId = app('request')->token;
    }

    /**
     * Get the channels the event should broadcast on.
     *
     * @return \Illuminate\Broadcasting\Channel|array
     */
    public function broadcastOn()
    {
        return new PrivateChannel('channel-name');
    }
}

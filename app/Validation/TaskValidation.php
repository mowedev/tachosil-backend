<?php

namespace App\Validation;

class TaskValidation implements ValidationInterface
{

    public function common()
    {

        $rules = [];

        return $rules;

    }

    public function insert()
    {

        $rules = [

        ];

        return array_merge($this->common(), $rules);

    }

    public function update()
    {

        $rules = [

        ];

        return array_merge($this->common(), $rules);

    }

    public function importExcel()
    {

        $rules = [

            'file' => 'required|file|mimes:xlsx,xls',
        ];

        return $rules;
    }

    public function MoveTask($task_id)
    {
        $rules = [
            'task_id' => 'required|integer|exists:tasks,id',
            'day' => 'required|integer|min:1|max:5',
            'week' => 'sometimes|integer|min:1|max:52',
        ];
        $rules = array_merge($this->common(), $rules);
        return $rules;
    }

    public function addTaskUser()
    {
        $rules = [
            'description' => 'required|string',
            'due_date' => 'required|date|date_format:"Y-m-d"|after:yesterday',
            'due_date' => [
                'required',
                'date',
                'date_format:"Y-m-d"',
                'after:yesterday',
                function ($attribute, $value, $fail) {
                    \DB::table('holidays')->where('date', $value)->count() === 0 ?: $fail($attribute . ' can\'t be on a holiday.');
                },
            ],
            'batch' => 'sometimes|string|unique:tasks,batch|nullable',
            'days' => 'required|numeric|min:1',
            'duration' => 'required|numeric|gt:0',
            'status' => 'required|in:1,2,3',
        ];
        $rules = array_merge($this->common(), $rules);
        return $rules;
    }

    public function updateTaskUser($id)
    {
        $rules = [
            'description' => 'required|string',
            'due_date' => [
                'required',
                'date',
                'date_format:"Y-m-d"',
                'after:yesterday',
                function ($attribute, $value, $fail) {
                    \DB::table('holidays')->where('date', $value)->count() === 0 ?: $fail($attribute . ' can\'t be on a holiday.');
                },
            ],
            'batch' => 'sometimes|string|nullable|unique:tasks,batch,' . $id,
            'days' => 'required|numeric|min:1',
            'duration' => 'required|numeric|gt:0',
            'status' => 'required|in:1,2,3',
        ];

        $rules = array_merge($this->common(), $rules);
        return $rules;
    }

}

<?php

namespace App\Validation;



interface ValidationInterface

{

	public function common();
	public function insert();
	public function update();

}


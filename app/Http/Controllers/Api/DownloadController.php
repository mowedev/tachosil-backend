<?php

namespace App\Http\Controllers\Api;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Storage;
use Response;
use App\Models\File;
use App\Models\Configuration;
use Illuminate\Contracts\Filesystem\FileNotFoundException;
use Exception;

class DownloadController extends Controller
{
  public function getDownloadUrl($id) {

    try{

      $file = File::find($id);
      if(!$file) {
        return json_response()->success("Invalid id");
      }
      $fileName = $file->name;
      $ftp = Storage::createFtpDriver([
              'host'     => '159.69.215.250',
              'username' => 'takeda',
              'password' => 'TAX@2018##',
              'port'     => '21', 
              'timeout'  => '100', 
              'ssl'      => true,
              'root' => '/var/www/html/QCB/tachosil',

           // 'disable_asserts' => true,
      ]);

      //$fileName = "AGILE_failedd.xml";
      $fileContent = $ftp->get($fileName); // read file content 
      $filePath = storage_path().'/app/public/xmls/'.$fileName;
      file_put_contents($filePath, $fileContent);
      $url =  config('custom.backend_url').'/files/download/'.$fileName;
      return $url;

    }catch(Exception $e){

      $path = storage_path().'/app/public/xmls/'.$fileName;

      if (file_exists($path)) {

        $url =  config('custom.backend_url').'/files/download/'.$fileName;
        return $url;

      }else{
        return 0;
      }
    } 
  }

  public function download($fileName)
  {
    $path = storage_path().'/app/public/xmls/'.$fileName;
    if (file_exists($path)) {
        return Response::download($path);
    }else{
      return 0;
    }
  }
}

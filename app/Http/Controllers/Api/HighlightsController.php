<?php

namespace App\Http\Controllers\Api;

use App\Http\Controllers\Controller;
use App\Models\Highlight;
use App\Models\HighlightEmoji;
use Illuminate\Http\Request;
use Validator;

class HighlightsController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */

    /**
     * List and sort highlights
     *
     * @return \Illuminate\Http\Response
     *
     * @SWG\Get(
     *     path="/api/v2/highlights",
     *     description="List All highlights with sorting ",
     *     operationId="highlights.list",
     *     produces={"application/json"},
     *     @SWG\Parameter(
     *         name="sort",
     *         in="query",
     *         required=false,
     *         type="string"
     *     ),
     *     @SWG\Parameter(
     *         name="order",
     *         in="query",
     *         required=false,
     *         type="string"
     *     ),
     *     tags={"Highlights"},
     *     security={
     *     {"passport": {}},
     *   },
     *     @SWG\Response(response=200, ref="#/responses/highlights")
     * )
     */

    public function index(Request $request)
    {
        //
        $sortBy = $request->sort;
        $sortOrder = $request->order;

        $pageSize = config('custom.highlights.page');

        $highlights = Highlight::Sort($sortBy, $sortOrder)->paginate($pageSize)->toArray();

        return json_response()->paginate($highlights);
    }

    /**
     * Return the latest 5 highlights in team page in the same order as highlights
     *
     * @return \Illuminate\Http\Response
     *
     * @SWG\Get(
     *     path="/api/v2/team/highlights",
     *     description="Return the latest 5 highlights in team page in the same order as highlights",
     *     operationId="team.highlights.list",
     *     produces={"application/json"},
     *     @SWG\Parameter(
     *          name="sort",
     *          in="query",
     *          required=false,
     *          type="string"
     *     ),
     *     @SWG\Parameter(
     *          name="order",
     *          in="query",
     *          required=false,
     *          type="string"
     *     ),
     *     tags={"Highlights"},
     *     security={
     *          {"passport": {}},
     *     },
     *     @SWG\Response(response=200, ref="#/responses/highlights")
     * )
     */
    public function teamHighlights(Request $request)
    {
        $sortBy = $request->get('sort', 'due_date');
        $sortOrder = $request->get('order', 'DESC');

        $highlights = Highlight::select([
            'id', 'title', 'description', 'due_date',
        ])->with(['emojies'])->take(5)->Sort($sortBy, $sortOrder)
            ->get()->toArray();

        return json_response()->success($highlights);
    }

    public function teamLatestHighlight(Request $request)
    {
        $sortBy = $request->get('sort', 'due_date');
        $sortOrder = $request->get('order', 'DESC');

        $highlight = Highlight::select([
            'id', 'title', 'description', 'due_date',
        ])->with(['emojies'])->orderBy($sortBy, $sortOrder)
            ->first()->toArray();

        return json_response()->success($highlight);
    }

    /**
     * Add/Remove emojies to/from highlights
     *
     * @return \Illuminate\Http\Response
     *
     * @SWG\Post(
     *     path="/api/v2/team/highlights/emoji",
     *     description="Add/Remove emojies to/from highlights",
     *     produces={"application/json"},
     *     @SWG\Parameter(
     *         name="highlight_id",
     *         in="formData",
     *         required=true,
     *         type="number"
     *     ),
     *     @SWG\Parameter(
     *         name="emoji_id",
     *         in="formData",
     *         required=true,
     *         type="number"
     *     ),
     *     @SWG\Parameter(
     *         name="increment",
     *         in="formData",
     *         required=true,
     *         type="number"
     *     ),
     *     tags={"Highlights"},
     *     security={
     *          {"passport": {}},
     *     },
     *     @SWG\Response(response=200, ref="#/responses/chengeEmoji")
     * )
     */
    public function emoji(Request $request)
    {
        $validator = Validator::make($request->all(), [
            'highlight_id' => 'required|exists:highlights,id',
            'emoji_id' => 'required|exists:emojies,id',
            'increment' => 'required|integer',
        ]);

        if ($validator->fails()) {
            return json_response()->validationErrors([
                'fields' => $validator->errors(),
            ]);
        }

        $highlight_id = $request->get('highlight_id');
        $emoji_id = $request->get('emoji_id');
        $increment = $request->get('increment');

        $highlight = HighlightEmoji::where('emoji_id', $emoji_id)
        ->where('highlight_id', $highlight_id)->first();

        if (!$highlight) {
            $highlight = HighlightEmoji::create([
                'highlight_id' => $highlight_id,
                'emoji_id' => $emoji_id,
                'count' => 1,
            ]);
        // } elseif (($highlight->count + (integer) $increment) <= 0) {
        //     $highlight->delete();
        //     return json_response()->success(null);
        } else {
            $highlight->count += (integer) $increment;
            if ($highlight->count < 0) {
                $highlight->count++;
                return json_response()->success($highlight);
            }
            $highlight->save();
        }

        return json_response()->success($highlight);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */

    /**
     * Create Highlight
     *
     * @return \Illuminate\Http\JsonResponse
     *
     * @SWG\Post(
     *     path="/api/v2/highlights",
     *     description="Creating a new highlight",
     *     operationId="highlight.create",
     *     produces={"application/json"},
     *         @SWG\Parameter(
     *         name="Highligt",
     *         description="Highlight data",
     *         in="body",
     *         required=false,
     *         type="object",
     *         @SWG\Schema(
     *             type="object",
     *           required={"title","description","due_date"},
     *             @SWG\Property(
     *                 property="title",
     *                 type="string",
     *             ),
     *             @SWG\Property(
     *                 property="description",
     *                 type="string",
     *             ),
     *             @SWG\Property(
     *                 property="due_date",
     *                 type="string",
     *             ),
     *         )
     *     ),
     *     security={
     *     {"passport": {}},
     *   },
     *     tags={"Highlights"},
     *     @SWG\Response(response=200, description="Highlight Created Successfully",ref="#/responses/createUpdateHighlight")
     *  )
     *  )
     */

    public function store(Request $request)
    {
        //
        $validator = Validator::make($request->all(), [
            'title' => 'required',
            'description' => 'required',
            'due_date' => 'required|date',
        ]);

        if ($validator->fails()) {

            return json_response()->validationErrors([
                'fields' => $validator->errors(),
            ]);

        }

        $highlight = Highlight::create($request->all());

        return json_response()->success($highlight);
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */

    /**
     * Update Highlight
     *
     * @return \Illuminate\Http\JsonResponse
     *
     * @SWG\PUT(
     *     path="/api/v2/highlights/{id}",
     *     description="Update an existing highlight",
     *     operationId="highlight.update",
     *     produces={"application/json"},
     *         @SWG\Parameter(
     *         name="Highligt",
     *         description="Highlight data",
     *         in="body",
     *         required=false,
     *         type="object",
     *         @SWG\Schema(
     *             type="object",
     *           required={"title","description","due_date"},
     *             @SWG\Property(
     *                 property="title",
     *                 type="string",
     *             ),
     *             @SWG\Property(
     *                 property="description",
     *                 type="string",
     *             ),
     *             @SWG\Property(
     *                 property="due_date",
     *                 type="string",
     *             ),
     *         )
     *     ),
     *     security={
     *     {"passport": {}},
     *   },
     *     tags={"Highlights"},
     *     @SWG\Response(response=200, description="Highlight Updated Successfully",ref="#/responses/createUpdateHighlight")
     *  )
     *  )
     */

    public function update(Request $request, $id)
    {
        //
        $highlight = Highlight::find($id);

        if (is_null($highlight)) {
            return json_response()->notFound("Highlight not found");
        }

        $validator = Validator::make($request->all(), [

            'title' => 'required',
            'description' => 'required',
            'due_date' => 'required|date',

        ]
        );

        if ($validator->fails()) {

            return json_response()->validationErrors([
                'fields' => $validator->errors(),
            ]);

        }

        $highlight->update($request->all());

        return json_response()->success($highlight);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */

    /**
     * Delete Highlight .
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     *
     * @SWG\Delete(
     *     path="/api/v2/highlights/{id}",
     *     description="remove highlight",
     *     produces={"application/json"},
     *     operationId="highlight.delete",
     *     @SWG\Parameter(
     *         name="id",
     *         in="path",
     *         required=true,
     *         type="integer",
     *         description = "highlight id "
     *     ),
     *     tags={"Highlights"},
     *     security={
     *     {"passport": {}},
     *   },
     *     @SWG\Response(response=200,
     *         description=" Highlight Deleted Successfully ",
     *         @SWG\Items(
     *             @SWG\Property(
     *                 property="data",
     *                 type="object",
     *                 @SWG\Property(
     *                      property="messages",
     *                      type="string",
     *                      description="succes messages"
     *                 ),
     *     )  ) )
     * )
     */

    public function destroy($id)
    {
        $highlight = Highlight::find($id);

        if (is_null($highlight)) {
            return json_response()->notFound("Highlight not found");
        }

        $highlight->delete();

        return json_response()->success("Highlight Deleted Successfully");
    }
}

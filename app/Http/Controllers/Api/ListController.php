<?php

namespace App\Http\Controllers\Api;

use App\Helpers\ConfigurationHelper;
use App\Helpers\ListHelper;
use App\Http\Controllers\Controller;
use App\Models\Task;
use App\Models\TaskVersion;
use App\Validation\TaskValidation;
use Carbon\Carbon;
use Illuminate\Http\Request;

class ListController extends Controller
{

    /**
     * add task
     *
     * @return \Illuminate\Http\JsonResponse
     *
     * @SWG\Post(
     *     path="/api/v2/list/task/add",
     *     description="add tasks defined by users",
     *     operationId="userdefined.add.list.task",
     *     produces={"application/json"},
     *     tags={"List View"},
     *     @SWG\Parameter(
     *         name="Task",
     *         description="task parameters",
     *         in="body",
     *         required=false,
     *         type="object",
     *         @SWG\Schema(
     *             type="object",
     *             required={"due_date","description"},
     *             @SWG\Property(
     *                 property="due_date",
     *                 type="string",
     *
     *             ),
     *             @SWG\Property(
     *                 property="sample_number",
     *                 type="integer",
     *
     *             ),
     *             @SWG\Property(
     *                 property="duration",
     *                 type="number",
     *             ),
     *             @SWG\Property(
     *                 property="batch",
     *                 type="number",
     *             ),
     *             @SWG\Property(
     *                 property="product",
     *                 type="string",
     *             ),
     *             @SWG\Property(
     *                 property="stage",
     *                 type="string",
     *             ),
     *             @SWG\Property(
     *                 property="description",
     *                 type="string",
     *             ),
     *             @SWG\Property(
     *                 property="status",
     *                 type="string",
     *             ),
     *             @SWG\Property(
     *                 property="login_date",
     *                 type="string",
     *             ),
     *             @SWG\Property(
     *                 property="receive_date",
     *                 type="string",
     *             ),
     *             @SWG\Property(
     *                 property="date_completed",
     *                 type="string",
     *             ),
     *             @SWG\Property(
     *                 property="date_reviewed",
     *                 type="string",
     *             ),
     *         )
     *     ),
     *     @SWG\Response(response=200, ref="#/responses/addtask")
     * )
     */
    public function addTask(Request $request, TaskValidation $validation, ListHelper $listHelper)
    {
        $validator = app('validator')->make($request->all(), $validation->addTaskUser());

        if ($validator->fails()) {
            return json_response()->validationErrors([
                'fileds' => $validator->errors(),
            ]);
        }

        $task = $listHelper->addTaskByUser($request);
        $dueDate = $request->due_date;
        $week = (new Carbon($dueDate))->weekOfYear;

        /*define('ARTISAN_BINARY', base_path().'/artisan');
        call_in_background('queue:work');*/

        event(new \App\Events\ProductivityUpdateEvent(null, $week));

        return json_response()->success($task);
    }

    /**
     * Display Tasks
     *
     * @return \Illuminate\Http\JsonResponse
     *
     * @SWG\Get(
     *     path="/api/v2/list/task/{index}",
     *     description="Return tasks defined by users",
     *     operationId="userdefined.list.task",
     *     produces={"application/json"},
     *     tags={"List View"},
     *     @SWG\Parameter(
     *         name="index",
     *         in="path",
     *         required=true,
     *         type="string",
     *         description = "Week number between 1 to 52"
     *     ),
     *     @SWG\Parameter(
     *         name="year",
     *         in="query",
     *         required=false,
     *         type="string",
     *         description = "year number"
     *     ),
     *     @SWG\Response(response=200, ref="#/responses/tasksListView")
     * )
     */
    public function index(Request $request, $index, ListHelper $listHelper)
    {
        if ($index <= 0 || $index > 52) {
            return json_response()->error(line("messages.not_week"));
        }

        return $listHelper->getTaskUserDefined($index);
    }

    /**
     * Remove Tasks
     *
     * @return \Illuminate\Http\JsonResponse
     *
     * @SWG\Delete(
     *     path="/api/v2/list/remove/task/{id}",
     *     description="delete task defined by user",
     *     operationId="userdefined.delete.task",
     *     produces={"application/json"},
     *     tags={"List View"},
     *     @SWG\Parameter(
     *         name="id",
     *         in="path",
     *         required=true,
     *         type="string",
     *         description = "task id to removed"
     *     ),
     *     @SWG\Response(response=200,
     *         description=" Task Deleted Successfully ",
     *         @SWG\Items(
     *             @SWG\Property(
     *                 property="data",
     *                 type="object",
     *                 @SWG\Property(
     *                      property="messages",
     *                      type="string",
     *                      description="success messages"
     *                 ),
     *     )  )
     *     )
     * )
     */
    public function removeTask($id)
    {
        $task = Task::where("id", $id)->UserDefined(1)->first();

        if ($task) {
            $task_sample_number = $task->sample_number;
            $version = TaskVersion::where('sample_number', $task_sample_number)
                ->where('state', 1)
                ->orderBy('part', 'DESC')
                ->first();
            $dueDate = $version->due_date;

            $task->delete();
            TaskVersion::where('sample_number', $task_sample_number)->delete();

            $week = (new Carbon($dueDate))->weekOfYear;

            //event(new \App\Events\NewImportEvent);
            event(new \App\Events\ProductivityUpdateEvent(null, $week));

            return json_response()->success(line('messages.task_deleted'));
        } else {
            return json_response()->success(line('messages.task_not_found'));
        }
    }

    /**
     * update task
     *
     * @return \Illuminate\Http\JsonResponse
     *
     * @SWG\Put(
     *     path="/api/v2/list/update/task/{id}",
     *     description="update tasks defined by users",
     *     operationId="userdefined.update.list.task",
     *     produces={"application/json"},
     *     tags={"List View"},
     *      @SWG\Parameter(
     *         name="id",
     *         in="path",
     *         required=true,
     *         type="string",
     *         description = "task id to update"
     *     ),
     *     @SWG\Parameter(
     *         name="Task",
     *         description="task parameters",
     *         in="body",
     *         required=false,
     *         type="object",
     *         @SWG\Schema(
     *             type="object",
     *             required={"due_date","description"},
     *             @SWG\Property(
     *                 property="due_date",
     *                 type="string",
     *
     *             ),
     *             @SWG\Property(
     *                 property="sample_number",
     *                 type="integer",
     *
     *             ),
     *             @SWG\Property(
     *                 property="duration",
     *                 type="number",
     *             ),
     *             @SWG\Property(
     *                 property="batch",
     *                 type="number",
     *             ),
     *             @SWG\Property(
     *                 property="product",
     *                 type="string",
     *             ),
     *             @SWG\Property(
     *                 property="stage",
     *                 type="string",
     *             ),
     *             @SWG\Property(
     *                 property="description",
     *                 type="string",
     *             ),
     *             @SWG\Property(
     *                 property="status",
     *                 type="string",
     *             ),
     *             @SWG\Property(
     *                 property="login_date",
     *                 type="string",
     *             ),
     *             @SWG\Property(
     *                 property="receive_date",
     *                 type="string",
     *             ),
     *             @SWG\Property(
     *                 property="date_completed",
     *                 type="string",
     *             ),
     *             @SWG\Property(
     *                 property="date_reviewed",
     *                 type="string",
     *             ),
     *         )
     *     ),
     *     @SWG\Response(response=200, ref="#/responses/addtask")
     * )
     */

    public function updateTask($id, Request $request, ListHelper $listHelper, TaskValidation $validation)
    {
        $task = Task::find($id);
        //  dd($task);
        if ($task == null) {
            return json_response()->notFound("Task not found");
        }

        $data = $request->all();
        //dd($data);
        $validator = app('validator')->make($data, $validation->updateTaskUser($id));

        if ($validator->fails()) {

            return json_response()->validationErrors([
                'fileds' => $validator->errors(),
            ]);

        }

        $task = $listHelper->updateUserDefinedTask($id, $request);
        if (is_null($task)) {
            return json_response()->forbidden();
        }

        $dueDate = $task['due_date'];
        $week = (new Carbon($dueDate))->weekOfYear;
        event(new \App\Events\ProductivityUpdateEvent(null, $week));

        return json_response()->success($task);
    }

    /**
     * Validate lock screen code
     *
     * @return \Illuminate\Http\JsonResponse
     *
     * @SWG\Post(
     *     path="/api/v2/lock/code",
     *     description="Validate lock screen code ",
     *     operationId="code.lock.validate",
     *     produces={"application/json"},
     *     tags={"List View"},
     *     @SWG\Parameter(
     *         name="Code",
     *         description="Lock screen code",
     *         in="body",
     *         required=false,
     *         type="object",
     *         @SWG\Schema(
     *             type="object",
     *             required={"code"},
     *             @SWG\Property(
     *                 property="code",
     *                 type="string",
     *
     *             ),
     *         )
     *     ),
     *     @SWG\Response(response=200,
     *         description="Lock code is validated",
     *         @SWG\Items(
     *             @SWG\Property(
     *                 property="data",
     *                 type="object",
     *                 @SWG\Property(
     *                      property="messages",
     *                      type="string",
     *                      description="Success Message"
     *                 ),
     *     )  )
     *     )
     * )
     */

    public function checkLockCode(Request $request)
    {
        if ($request->has('code')) {
            $configurationsHelper = new ConfigurationHelper;
            $configurations = $configurationsHelper->getBoardConfigurations();
            $originalCode = isset($configurations['dashboard']['lock_code']) ? $configurations['dashboard']['lock_code'] : "11111";
            $requestCode = $request->code;
            if ($originalCode == $requestCode) {
                return json_response()->success("Valid Code");
            }
        }

        return json_response()->notFound();
    }

    /**
     * Return meeting timestamp object set on server
     *
     * @return \Illuminate\Http\JsonResponse
     *
     * @SWG\Get(
     *     path="/api/v2/meeting-time",
     *     description="Return meeting timestamp object set on server",
     *     produces={"application/json"},
     *     tags={"Config"},
     *     @SWG\Response(response=200,
     *          description="",
     *          @SWG\Items(
     *               @SWG\Property(
     *                    property="data",
     *                    type="object",
     *                    @SWG\Property(
     *                         property="timezone_type",
     *                         type="number",
     *                    ),
     *                    @SWG\Property(
     *                         property="timezone",
     *                         type="string",
     *                    ),
     *                    @SWG\Property(
     *                         property="timestamp",
     *                         type="number",
     *                    ),
     *               )
     *          )
     *     )
     * )
     */
    public function meetingTime()
    {
        $meeting_time = config('custom.meeting_time');
        $carbon = Carbon::parse($meeting_time);
        $time = (array) $carbon->getTimezone();
        $time['timestamp'] = $carbon->getTimestamp();

        return json_response()->success($time);
    }
}

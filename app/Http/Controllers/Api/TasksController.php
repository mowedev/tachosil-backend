<?php

namespace App\Http\Controllers\Api;

use App\Helpers\CommanUtility;
use App\Helpers\CumulativeAverageHelper;
use App\Helpers\formatUtility;
use App\Helpers\ImportHelper;
use App\Helpers\ListHelper;
use App\Helpers\qualityHelper;
use App\Helpers\TaskHelper;
use App\Http\Controllers\Controller;
use App\Models\Duration;
use App\Models\Task;
use App\Models\Subtask;
use App\Models\TaskVersion;
use App\Validation\TaskValidation;
use Carbon\Carbon;
use Illuminate\Database\Eloquent\ModelNotFoundException;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Validator;

class TasksController extends Controller
{
    use formatUtility;use CommanUtility;
    /**
     * [importTasks from excel file not used for now]
     * @param  Request      $request      [description]
     * @param  ImportHelper $importHelper [description]
     * @return [type]                     [description]
     */
    public function importTasks(Request $request, ImportHelper $importHelper)
    {
        $importHelper->importTasksFromExcelFile($request);
        event(new \App\Events\NewImportEvent);
        return json_response()->success(line('messages.tasks_inserted'));
    }

    public function importDuratio(Request $request, ImportHelper $importHelper)
    {
        $importHelper->importDurationFromExcel($request);
        event(new \App\Events\NewImportEvent);
        return json_response()->success(line('messages.duration_inserted'));
    }

    /**
     * Display week tasks
     *
     * @return \Illuminate\Http\JsonResponse
     *
     * @SWG\Get(
     *     path="/api/v2/task/week/{index}",
     *     description="Return week tasks",
     *     operationId="task.list",
     *     produces={"application/json"},
     *     tags={"Productivity"},
     *     @SWG\Parameter(
     *         name="index",
     *         in="path",
     *         required=true,
     *         type="integer"
     *     ),
     *     @SWG\Parameter(
     *         name="year",
     *         in="query",
     *         required=false,
     *         type="integer"
     *     ),
     *     @SWG\Parameter(
     *         name="type",
     *         in="query",
     *         required=false,
     *         type="string"
     *     ),
     *     @SWG\Parameter(
     *         name="mode",
     *         in="query",
     *         required=false,
     *         type="string"
     *     ),
     *     @SWG\Response(response=200, ref="#/responses/tasks")
     * )
     */
    public function getTasksByWeekIndex(TaskHelper $taskHelper, $index, Request $request, $is_json = false)
    {
        if ($index <= 0 || $index > 53) {
            return json_response()->error(line("messages.not_week"));
        }

        $type = $request->get('type', 'calendar');

        if ($taskHelper->isPastWeek($index)) {
            $week = $taskHelper->getWeekDates($index);
            $tasks = $taskHelper->pastWeeksData($week, $type);
        } else if ($taskHelper->isFutureWeek($index)) {
            $week = $taskHelper->getWeekDates($index);
            $tasks = $taskHelper->futureWeeksData($week, $type);
        } else {
            $tasks = $taskHelper->futureAndPastWeekData($index, $type);
        }

        $mode = $request->get("mode");

        if ($mode == "export") {
            $formattedTasks = $taskHelper->listFormatForExport($tasks);
            $alltimeBacklogTasks = $taskHelper->getFullTimeBacklog();
            $formattedBacklogTasks = $taskHelper->backlogFormatForExport($alltimeBacklogTasks);

            $csv = $taskHelper->writeCsv($formattedTasks, $formattedBacklogTasks);
            $file = ["url" => $csv];
            return json_response()->success($file);
        } else if ($mode == "yearexport") {
            $now = Carbon::now();
            $year = $now->year;
            $endYear = $year+1;
            $firstDay = (new Carbon("first day of January $year"))->format("Y-m-d");
            $lastDay = (new Carbon("last day of December $endYear"))->format("Y-m-d");

            $doneTasks = $taskHelper->getDoneTasks($firstDay, $lastDay);

            $counter_number = count($doneTasks);
            $plannedTasks = $taskHelper->getPlannedTasks($firstDay, $lastDay, $counter_number);

            $allTasks = array_merge($doneTasks, $plannedTasks);

            $alltimeBacklogTasks = $taskHelper->getFullTimeBacklog();
            $formattedBacklogTasks = $taskHelper->backlogFormatForExport($alltimeBacklogTasks);

            $csv = $taskHelper->writeCsv($allTasks, $formattedBacklogTasks);
            $file = ["url" => $csv];
            return json_response()->success($file);
        }

        if ($is_json) {
            return $tasks;
        }

        return json_response()->success($tasks);
    }

    /**
     * Import Tasks
     *
     * @return \Illuminate\Http\JsonResponse
     *
     * @SWG\Post(
     *     path="/api/v2/task/xml/import",
     *     description="Import tasks and subtasks",
     *     operationId="tasks.import",
     *     produces={"application/json"},
     *     tags={"Tasks"},
     *     @SWG\Parameter(
     *         name="file",
     *         in="formData",
     *         required=true,
     *         type="file",
     *         description="Upload file"
     *     ),
     *     @SWG\Response(
     *         response=200,
     *         description="tasks imported.",
     *         @SWG\Schema(
     *             type="object",
     *             @SWG\Property(
     *                 property="message",
     *                 type="string",
     *                 description="Success message."
     *             )
     *         )
     *    )
     * )
     */
    public function importTaskFromXml(Request $request, ImportHelper $importHelper)
    {
        $file = $importHelper->insertTasksXml($request, true);
        $ids = [];
        array_push($ids, $file[0]['id']);

        //event(new \App\Events\NewImportEvent);
        event(new \App\Events\LogsUpdateEvent($ids));

        //event(new \App\Events\ProductivityUpdateEvent);
        return json_response()->success(line('messages.tasks_inserted'));
    }

    /**
     * Update tasks duration
     *
     * @return \Illuminate\Http\JsonResponse
     *
     * @SWG\Put(
     *     path="/api/v2/task/reset/duration/{id}",
     *     description="update task duration",
     *     operationId="tasks.resetduration",
     *     produces={"application/json"},
     *     @SWG\Parameter(
     *         name="id",
     *         in="path",
     *         required=true,
     *         type="integer",
     *         description="task id",
     *     ),
     *     @SWG\Parameter(
     *         name="task",
     *         in="body",
     *         type="object",
     *         @SWG\Schema(
     *             type="object",
     *             required={"duration"},
     *             @SWG\Property(
     *                 property="duration",
     *                 type="integer",
     *                 description="task duration"
     *             ),
     *         ),
     *         description="task duration",
     *     ),
     *     tags={"Tasks"},
     *     @SWG\Response(response=200,
     *     description="Tasks reset duration",
     *     @SWG\Schema(
     *             type="object",
     *             @SWG\Property(
     *                 property="message",
     *                 type="string",
     *                 description="Success message."
     *             )
     *         )
     *     )
     * )
     */
    public function updateTaskDuration(Request $request, $id, ListHelper $helper)
    {
        try {
            $task = Task::findOrFail($id);
            $sample_number = $task->sample_number;
            $version = TaskVersion::where('sample_number', $sample_number)
                ->where('state', 1)
                ->orderBy('due_date', 'DESC')
                ->first();

            $dueDate = $version->due_date;
            $week = (new Carbon($dueDate))->weekOfYear;

            $product = $task->product;
            $project = $task->project;
            $analysis = $task->analysis;
            $newDuration = $request->duration;
            $days = $request->days === 0 ? 1 : $request->days;

            $helper->overrideDuration($product, $analysis, $newDuration, ($newDuration * $days), $days, TRUE, $task, $project);
            //event(new \App\Events\NewImportEvent);
            event(new \App\Events\ProductivityUpdateEvent(null, $week));

            return json_response()->success("Task Updated");
        } catch (ModelNotFoundException $e) {
            return json_response()->notFound();
        }
    }

    /**
     * Tasks reorder
     *
     * @return \Illuminate\Http\JsonResponse
     *
     * @SWG\Put(
     *     path="/api/v2/task/reorder/{id}",
     *     description="reorder tasks",
     *     operationId="tasks.reorder",
     *     produces={"application/json"},
     *     @SWG\Parameter(
     *         name="id",
     *         in="path",
     *         required=true,
     *         type="integer",
     *         description="task id",
     *     ),
     *     @SWG\Parameter(
     *         name="task",
     *         in="body",
     *         type="object",
     *         @SWG\Schema(
     *             type="object",
     *             required={"order"},
     *             @SWG\Property(
     *                 property="order",
     *                 type="integer",
     *                 description="task reorder"
     *             ),
     *         ),
     *         description="task reorder",
     *     ),
     *     tags={"Tasks"},
     *     @SWG\Response(response=200,
     *     description="Tasks reorder",
     *     @SWG\Schema(
     *             type="object",
     *             @SWG\Property(
     *                 property="message",
     *                 type="string",
     *                 description="Success message."
     *             )
     *         )
     *     )
     * )
     */
    public function updateTasksOrder(Request $request, $id)
    {
        try {
            $task = Task::findOrFail($id);
            $taskSample = $task->sample_number;

            $TaskVersionData = TaskVersion::where('sample_number', $taskSample)->where('state', 1)->first();

            $id = $TaskVersionData->id;
            $duedate = $TaskVersionData->due_date;

            $prevOrder = $TaskVersionData->order;
            $nextOrder = $request->order;

            if ($prevOrder > $nextOrder) {
                //prev = 12
                // next = 4
                //reduce order
                $preMax = $prevOrder - 1;
                DB::statement('update task_version set `order` = `order`+1 where due_date = "' . $duedate . '" and `order` between ' . $nextOrder . ' and ' . $preMax);

                DB::statement('update task_version set `order` = ' . $nextOrder . ' where id = ' . $id);
            } else {
                //increase order
                // prev = 4
                // next = 5
                $postMin = $prevOrder + 1; //5
                DB::statement('update task_version set `order` = `order`-1 where due_date = "' . $duedate . '" and `order` between ' . $postMin . ' and ' . $nextOrder);

                DB::statement('update task_version set `order` = ' . $nextOrder . ' where id = ' . $id);
            }

            $week = (new Carbon($duedate))->weekOfYear;
            event(new \App\Events\ProductivityUpdateEvent(null, $week));

            return json_response()->success(line('messages.task_order_updated'));
        } catch (ModelNotFoundException $e) {
            return json_response()->notFound();
        }
    }

    public function getNewXml(ImportHelper $importHelper)
    {
        $importHelper->getNewXmlFile();
        return json_response()->success(line('messages.tasks_inserted'));
    }

    /**
     * All Time BackLog
     *
     * @return \Illuminate\Http\Response
     *
     * @SWG\Get(
     *     path="/api/v2/backlog/alltime",
     *     description="Get All Time Backlog",
     *     produces={"application/json"},
     *     tags={"Productivity"},
     *     @SWG\Response(response=200, ref="#/responses/tasks")
     * )
     */
    public function getAllTimeBackLog(TaskHelper $taskHelper, $is_json = false)
    {
        if ($is_json) {

            return $taskHelper->getFullTimeBacklog();

        } else {

            return json_response()->success($taskHelper->getFullTimeBacklog());
        }
    }

    /**
     * Move Task
     *
     * @return \Illuminate\Http\JsonResponse
     *
     * @SWG\Post(
     *     path="/api/v2/task/move",
     *     description="move task to furture due date",
     *     operationId="tasks.resetduedate",
     *     produces={"application/json"},
     *     @SWG\Parameter(
     *         name="task",
     *         in="body",
     *         type="object",
     *         @SWG\Schema(
     *             type="object",
     *             required={"duration"},
     *             @SWG\Property(
     *                 property="task_id",
     *                 type="integer",
     *                 description="task id"
     *             ),
     *             @SWG\Property(
     *                 property="week",
     *                 type="integer",
     *                 description="week number"
     *             ),
     *             @SWG\Property(
     *                 property="day",
     *                 type="integer",
     *                 description="day number"
     *             ),
     *         ),
     *         description="task move by front",
     *     ),
     *     tags={"Tasks"},
     *     @SWG\Response(response=200,
     *     description="task moved successfully",
     *     @SWG\Schema(
     *             type="object",
     *             @SWG\Property(
     *                 property="message",
     *                 type="string",
     *                 description="Success message."
     *             )
     *         )
     *     )
     * )
     */

    public function moveTask(Request $request, TaskHelper $taskHelper, TaskValidation $validation)
    {
        $validator = Validator::make($request->all(), [
            'task_id' => 'required|exists:tasks,id',
            'part' => 'required|min:1|numeric',
            'duedate' => 'required|date_format:Y-m-d|after:yesterday',
        ]);

        if ($validator->fails()) {

            return json_response()->validationErrors([

                'fields' => $validator->errors(),

            ]);
        }

        $dates = $taskHelper->move($request->all());
        $oldWeek = (new Carbon($dates['old']))->weekOfYear;
        $newWeek = (new Carbon($dates['new']))->weekOfYear;

        event(new \App\Events\ProductivityUpdateEvent($oldWeek, $newWeek));

        //dd(event(new \App\Events\NewImportEvent));
        return json_response()->success("Task Moved Successfully");
    }

    /**
     * List Quality Trend for every month in the year
     *
     * @return \Illuminate\Http\Response
     *
     * @SWG\Get(
     *     path="/api/v2/quality/trend",
     *     description="List quality trend for every month in the year",
     *     operationId="quality.trend.year",
     *     produces={"application/json"},
     *     @SWG\Parameter(
     *         name="year",
     *         in="query",
     *         required=false,
     *         type="string",
     *     ),
     *     tags={"Quality"},
     *     security={
     *     {"passport": {}},
     *   },
     *     @SWG\Response(response=200, ref="#/responses/qualityTrendYear")
     * )
     */

    public function getQualityTrend(TaskHelper $taskHelper, qualityHelper $qualityHelper)
    {
        // $fullMonthsNames = get_month_full_names();
        // $monthsNames = get_month_names();
        // $month = 1;
        // $tasks = $taskHelper->getAllTasksGroupByMonth();
        $tasks = CumulativeAverageHelper::getAllTasksGroupByMonth();
        $now = Carbon::now();
        $currentMonth = $now->month;

        $all = CumulativeAverageHelper::getCumulativeAverage("quality", $tasks);

        // for ($i = 0; $i < 12; $i++) {
        //     // $all[$i]['month'] = $month;
        //     $all[$i]['name'] = $monthsNames[$month];
        //     $number_of_tasks = isset($tasks[$fullMonthsNames[$month]]) ? $tasks[$fullMonthsNames[$month]] : 0;
        //     $notRun = $qualityHelper->getNotRunThisMonth($fullMonthsNames[$month]);
        //     $lims = $qualityHelper->getLimsThisMonth($fullMonthsNames[$month]);
        //     $all[$i]['trend'] = ($i + 1 < $currentMonth) ? $qualityHelper->calculateQualityTrend($number_of_tasks, $notRun, $lims) : 0;
        //     $month++;
        // }

        return json_response()->success($all);
    }

    /**
     * All missing data samples
     *
     * @return \Illuminate\Http\Response
     *
     * @SWG\Get(
     *     path="/api/v2/samples/missed",
     *     description="Get All missing data samples",
     *     produces={"application/json"},
     *     tags={"Productivity"},
     *     @SWG\Response(response=200, ref="#/responses/tasks")
     * )
     */

    public function allMissingDataSample(TaskHelper $taskHelper, $is_json = false)
    {
        if ($is_json) {

            return $taskHelper->getAllMissingSamples();

        } else {
            return json_response()->success($taskHelper->getAllMissingSamples());
        }

    }

    public function getDailyBacklog(ImportHelper $importHelper)
    {
        $importHelper->getBacklogProducts();
        return json_response()->success("backlog items added");
    }

    public function listDailyBacklog(Request $request, TaskHelper $taskHelper, $date)
    {
        $today = Carbon::today()->format("Y-m-d");
        $status = $this->getTasksStatus();

        /**
         * Because task version would start from received_date we'll use max(due date) from subtasks
         */
        $missed_due_sample_numbers = Subtask::select(['sample_number'])
            ->selectRaw('MAX(due_date) as due_date')
            ->where('due_date', '<', $today)
            ->groupBy('sample_number')
            ->pluck('sample_number')
            ->toArray();

        $missed_due_sample_numbers = $missed_due_sample_numbers ? implode(',', $missed_due_sample_numbers) : "''";

        $tasks = TaskVersion::with('task.subtasks')
            ->selectRaw('MAX(`task_version`.`id`) id, MAX(`task_version`.`order`) `order`, MAX(`task_version`.`part`) `part`, MAX(`tasks`.`original_due_date`) `due_date`')
            ->selectRaw('MAX(`task_version`.`state`) state, MAX(`task_version`.`move_state`) move_state, MAX(`task_version`.`created_at`) created_at, MAX(`task_version`.`updated_at`) updated_at')
            ->selectRaw('`task_version`.`sample_number`, MAX(tasks.original_due_date) as original_due_date, MAX(`task_version`.`is_hard_moved_from_backlog`) is_hard_moved_from_backlog, MAX(`task_version`.`is_hard_moved_to_backlog`) is_hard_moved_to_backlog, MAX(`tasks`.`description`) description')
            ->join('tasks', 'task_version.sample_number', '=', 'tasks.sample_number')
            ->havingRaw("(task_version.sample_number IN ($missed_due_sample_numbers) AND original_due_date < '$today' AND original_due_date = '$date')")
            ->orHavingRaw("(is_hard_moved_to_backlog = 1)")
            ->whereNull('tasks.date_reviewed')
            ->whereNull('tasks.date_completed')
            ->where('task_version.state', 1)
            ->groupBy('task_version.sample_number')
            ->get();
        $tasks = (new \Illuminate\Pagination\LengthAwarePaginator($tasks, $tasks->count(), 6, $request->page, ['path' => $request->fullUrl()]))->toArray();
        $tasks['data'] = $this->formatDailyBacklog($tasks['data']);
        $tasks['data'] = $this->updateBacklogItemsStatus($tasks['data']);

        return $tasks;
    }

    public function updateBacklogItemsStatus($tasks)
    {
        $status = $this->getTasksStatus();

        $inProgress = DB::table("subtasks")->distinct()->whereNotNull("receive_date")->pluck("sample_number")->toArray();

        foreach ($tasks as $key => $task) {

            if (!is_null($task['date_reviewed'])) {

                $tasks[$key]['status'] = 1;

            } else {

                if (in_array($task["sample_number"], $inProgress)) {

                    $tasks[$key]['status'] = 2;

                } else {

                    $tasks[$key]['status'] = 3;
                }
            }
        }

        return $tasks;
    }

    public function moveAllTimeBacklogTask(Request $request, TaskHelper $taskHelper)
    {
        $validator = Validator::make($request->all(), [

            'task_id' => 'required|exists:tasks,id',
            'duedate' => 'required|date_format:Y-m-d|after:yesterday',
            //'order' => 'required|numeric'

        ]);

        if ($validator->fails()) {

            return json_response()->validationErrors([

                'fields' => $validator->errors(),

            ]);
        }

        $dates = $taskHelper->moveAllTimeBacklog($request->all());

        $oldWeek = (new Carbon($dates['old']))->weekOfYear;
        $newWeek = (new Carbon($dates['new']))->weekOfYear;

        event(new \App\Events\ProductivityUpdateEvent($oldWeek, $newWeek));

        return json_response()->success("Task Moved Successfully");
    }

/*    public function moveTaskToBacklog(Request $request)
{
$validator = Validator::make($request->all(), [

'task_id' => 'required|exists:tasks,id',

]);

if ($validator->fails()) {

return json_response()->validationErrors([

'fields' => $validator->errors(),

]);
}

$task_id = $request->task_id;

$sample_number = Task::where('id', $task_id)->value('sample_number');
$task_version = TaskVersion::where('sample_number', $sample_number)->where('state', 1)->first();
$due_date = $task_version->due_date;
$order = $task_version->order;
$task_version->is_hard_moved_to_backlog = 1;
$task_version->save();

return json_response()->success("Task Moved Successfully");
}*/

}

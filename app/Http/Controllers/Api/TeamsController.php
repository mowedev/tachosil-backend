<?php

namespace App\Http\Controllers\Api;

use App\Formatters\AdminTaskFormatter;
use App\Helpers\AdminTasksUtility;
use App\Helpers\CommanUtility;
use App\Helpers\formatUtility;
use App\Helpers\TeamHelper;
use App\Helpers\ConfigurationHelper;
use App\Http\Controllers\Controller;
use App\Models\Capacity;
use App\Models\DemandRate;
use App\Models\State;
use App\Models\Leadership;
use App\Models\Team;
use App\Models\TeamAvatar;
use App\Models\TeamDailyStatus;
use App\Models\TeamMember;
use App\Models\TeamState;
use App\Models\TeamLeadership;
use Carbon\Carbon;
use DB;
use Illuminate\Http\Request;
use Validator;

class TeamsController extends Controller
{
    use formatUtility;use CommanUtility;use AdminTasksUtility;

    /**
     * List States
     *
     * @return \Illuminate\Http\Response
     *
     * @SWG\Get(
     *     path="/api/v2/team/states",
     *     description="List all the states",
     *     operationId="team.states",
     *     produces={"application/json"},
     *     tags={"Team"},
     *     security={
     *     {"passport": {}},
     *   },
     *     @SWG\Response(response=200, ref="#/responses/states")
     * )
     */

    public function getStates()
    {
        $states = State::all();
        return json_response()->success($states);
    }


    public function getLeaderships()
    {
        $leaderships = Leadership::all();
        return json_response()->success($leaderships);
    }

    /**
     * Get Team of specific day
     *
     * @return \Illuminate\Http\Response
     *
     * @SWG\Get(
     *     path="/api/v2/team/number/{date}",
     *     description="Get Team of specific day with format Y-M-D ",
     *     operationId="team.list",
     *     produces={"application/json"},
     *     tags={"Team"},
     *     @SWG\Parameter(
     *         name="date",
     *         in="path",
     *         required=true,
     *         type="string"
     *     ),
     *     security={
     *     {"passport": {}},
     *   },
     *     @SWG\Response(response=200, ref="#/responses/team")
     * )
     */

    public function getTeamByDate(TeamHelper $teamHelper, $date)
    {
        $team = Team::where('date', $date)->first();
        $latestHighlight = $teamHelper->getLatestHighlight();

        if (!is_null($team)) {

            $team = $team->makeHidden(['date', 'id'])->toArray();
            $usedEmp = (int) $teamHelper->getUsedUsersNumber($date, [1, 2, 3]);
            $usedManagers = (int) $teamHelper->getUsedUsersNumber($date, [4, 5, 6]);

            $team["used_emp"] = $usedEmp;
            $team["used_managers"] = $usedManagers;

            $team["remaining_emp"] = $team["no_of_employees"] - $usedEmp;
            $team["remaining_managers"] = $team["no_of_managers"] - $usedManagers;
            $team["latest_highlight"] = $latestHighlight;

        }
        return json_response()->success($team);
    }

    /**
     * Modify the team state
     *
     * @return \Illuminate\Http\JsonResponse
     *
     * @SWG\Post(
     *     path="/api/v2/team/state",
     *     description="Modify the state of team member",
     *     operationId="team.state.modify",
     *     produces={"application/json"},
     *         @SWG\Parameter(
     *         name="data",
     *         description="data",
     *         in="body",
     *         required=false,
     *         type="object",
     *         @SWG\Schema(
     *             type="object",
     *             required={"state_id","increment"},
     *             @SWG\Property(
     *                 property="state_id",
     *                 type="number",
     *                 description="the id of the state "
     *             ),
     *             @SWG\Property(
     *                 property="increment",
     *                 type="number",
     *                 description="this determine whether the number of members in this state will increase of decrease and this value must be in [-1,1]"
     *             ),
     *             @SWG\Property(
     *                 property="date",
     *                 type="string",
     *                 description="the date in which the state need to be changed"
     *             ),
     *         )
     *     ),
     *     security={
     *     {"passport": {}},
     *   },
     *     tags={"States"},
     *     @SWG\Response(response=200, description="State Changed Successfully",ref="#/responses/modifyStateOfTeam")
     *  )
     *  )
     */

    public function modifyTeamState(Request $request, TeamHelper $teamHelper)
    {
        $validator = Validator::make($request->all(),
            [
                'state_id' => 'required|exists:states,id',
                'increment' => 'required|numeric|in:-1,1',
            ]
        );

        if ($validator->fails()) {
            return json_response()->validationErrors([
                'fields' => $validator->errors(),
            ]);
        }

        $state_id = $request->state_id;
        $increment = $request->increment;
        $date = $request->date;

        if ($date == null) {
            $date = date('Y-m-d');
        } else {
            $date = Carbon::parse($date)->toDateString();
        }

        // $team = Team::where('date', $date)->first();
        // $totalManagers = is_null($team) ? 10 : $team['no_of_managers'];
        $totalManagers = config('custom.number_of_managers', 99);

        $teamState = TeamState::where('date', $date)->where('state_id', $state_id)->first();

        $usedManagers = (int) $teamHelper->getUsedUsersNumber($date, $state_id);
        $usedAfterIncrement = $usedManagers + $increment;
        // $data = [
        //     'team_state' => $teamState,
        //     'managers_limit' => $totalManagers,
        // ];

        if (0 <= $usedAfterIncrement && $usedAfterIncrement <= $totalManagers) {
            $newTeamState = $teamHelper->insertOrUpdateTeamState($teamState, $date, $state_id, $increment);
            event(new \App\Events\NewImportEvent);
            return json_response()->success($newTeamState);
        }
        
        return json_response()->success($teamState);
    }

    public function modifyLeadershipState(Request $request, TeamHelper $teamHelper)
    {
        $validator = Validator::make($request->all(),
            [
                'state_id' => 'required|exists:leaderships,id',
                'increment' => 'required|numeric|in:-1,1',
            ]
        );

        if ($validator->fails()) {
            return json_response()->validationErrors([
                'fields' => $validator->errors(),
            ]);
        }

        $leadership_id = $request->state_id;
        $increment = $request->increment;
        $date = $request->date;

        if ($date == null) {
            $date = date('Y-m-d');
        } else {
            $date = Carbon::parse($date)->toDateString();
        }

        // $team = Team::where('date', $date)->first();
        // $totalManagers = is_null($team) ? 10 : $team['no_of_managers'];
        $totalManagers = config('custom.number_of_managers', 99);

        $teamState = TeamLeadership::where('date', $date)->where('leadership_id', $leadership_id)->first();

        $usedManagers = (int) $teamHelper->getUsedLeadersNumbers($date, $leadership_id);
        $usedAfterIncrement = $usedManagers + $increment;
        // $data = [
        //     'team_state' => $teamState,
        //     'managers_limit' => $totalManagers,
        // ];

        if (0 <= $usedAfterIncrement && $usedAfterIncrement <= $totalManagers) {
            $newTeamState = $teamHelper->insertOrUpdateLeadershipState($teamState, $date, $leadership_id, $increment);
            event(new \App\Events\NewImportEvent);
            return json_response()->success($newTeamState);
        }
        
        return json_response()->success($teamState);
    }

    public function leadershipChart(Request $request)
    {
        $year = date('Y');
        $month = date('m');
        $week = date('W');

        if ($request->get('year') && $request->get('week')) {
            $year = $request->get('year');
            $week = $request->get('week');
        }

        $first_of_month = Carbon::create($year, $month, 1);
        $first_of_month->setISODate($year, $week); // set week
        $start_date = $first_of_month->startOfWeek()->toDateString();
        $end_date = $first_of_month->endOfWeek()->toDateString();

        $swiper_steps = TeamHelper::getLeadershipSteps();
        $leadership_data = TeamHelper::getLeadershipChartData($start_date, $end_date);
        $formatted_leadership_data = $this->formatLeadershipChart($leadership_data, $start_date, $end_date);

        return json_response()->success([
            'data' => $formatted_leadership_data,
            'swiper_steps' => $swiper_steps,
        ]);
    }

    public function moodVsLeadershipChart(Request $request)
    {
        $year = date('Y');
        $month = date('m');
        $week = date('W');
        
        if ($request->get('year') && $request->get('week')) {
            $year = $request->get('year');
            $week = $request->get('week');
        }

        $first_of_month = Carbon::create($year, $month, 1);
        $first_of_month->setISODate($year, $week); // set week
        $start_date = $first_of_month->startOfWeek()->toDateString();
        $end_date = $first_of_month->endOfWeek()->toDateString();
        
        $leadership_swiper_steps = TeamHelper::getLeadershipSteps();
        $leadership_data = TeamHelper::getLeadershipChartGroupedData($start_date, $end_date);
        // $formatted_leadership_data = $this->formatTeamChart($leadership_data, $start_date, $end_date);

        $mood_swiper_steps = TeamHelper::getFirstTrend('week');
        $mood_data = TeamHelper::getTrends($start_date, $end_date);
        // $formatted_mood_data = $this->formatTeamChart($mood_data, $start_date, $end_date);

        $formatted_data = $this->moodVsLeadershipChartFormat($mood_data, $leadership_data, $start_date, $end_date);
        $swiper_steps = $leadership_swiper_steps >= $mood_swiper_steps ? $leadership_swiper_steps : $mood_swiper_steps;

        return json_response()->success([
            'data' => $formatted_data,
            'swiper_steps' => $swiper_steps,
        ]);
    }

    /**
     * Change/Add a team member mood for a specific date
     *
     * @return \Illuminate\Http\Response
     *
     * @SWG\Post(
     *     path="/api/v2/team/mood",
     *     description="Change/Add a team member mood for a specific date",
     *     operationId="team.state.change_mood",
     *     produces={"application/json"},
     *     @SWG\Parameter(
     *         name="date",
     *         in="formData",
     *         type="string"
     *     ),
     *     @SWG\Parameter(
     *         name="state_id",
     *         in="formData",
     *         required=true,
     *         type="number"
     *     ),
     *     @SWG\Parameter(
     *         name="index",
     *         in="formData",
     *         required=true,
     *         type="number"
     *     ),
     *     tags={"Team"},
     *     security={
     *          {"passport": {}},
     *     },
     *     @SWG\Response(response=200, ref="#/responses/changeMood")
     * )
     */

    public function changeMood(Request $request, TeamHelper $teamHelper)
    {
        $validator = Validator::make($request->all(),
            [
                'date' => 'nullable|date',
                'state_id' => 'required|min:0',
                'index' => 'required|numeric',
            ]
        );

        if ($validator->fails()) {
            return json_response()->validationErrors([
                'fields' => $validator->errors(),
            ]);
        }

        $date = $request->get('date', Carbon::now()->toDateString());
        $index = $request->get('index');
        $state_id = $request->get('state_id');

        // $team = Team::where('date', $date)->first();

        // if (!is_null($team)) {
        // }

        $team_state = TeamDailyStatus::where('date', $date)
            ->where('index', $index)->first();

        if ($team_state) {
            $team_state->state_id = $state_id;
            $team_state->save();
        } else {
            $team_state = TeamDailyStatus::create([
                'date' => $date,
                'index' => $index,
                'state_id' => $state_id,
            ]);
        }
        event(new \App\Events\NewImportEvent(true));
        return json_response()->success($team_state);
    }

    /**
     * Get States Info of specific day
     *
     * @return \Illuminate\Http\Response
     *
     * @SWG\Get(
     *     path="/api/v2/team/state/{date}",
     *     description="Get States Info of specific day with format Y-M-D",
     *     operationId="team.state.info",
     *     produces={"application/json"},
     *     @SWG\Parameter(
     *         name="date",
     *         in="path",
     *         required=true,
     *         type="string"
     *     ),
     *     tags={"States"},
     *     security={
     *     {"passport": {}},
     *   },
     *     @SWG\Response(response=200, ref="#/responses/statesInfo")
     * )
     */

    public function getStatesInfoByDate($date, TeamHelper $helper)
    {
        // $teamStates = TeamState::where('date', $date)->get();
        $teamStates = TeamDailyStatus::where('date', $date)->orderBy('index', 'ASC')->get();

        $number_of_employees = ConfigurationHelper::config('team_member_count', 10);

        $data = [];
        for ($i = 0; $i < $number_of_employees; $i++) {
            $state = $teamStates->where('index', $i + 1)->first();
            if ($state) {
                $data[$i] = [
                    'index' => $i + 1,
                    'state_id' => $state->state_id,
                ];
            } else {
                $data[$i] = [
                    'index' => $i + 1,
                    'state_id' => 0,
                ];
            }
        }

        return json_response()->success($data);
    }

    /**
     * Get Leader presence states info of specific day with format Y-M-D"
     *
     * @return \Illuminate\Http\Response
     *
     * @SWG\Get(
     *     path="/api/v2/team/leader-presence/state/{date}",
     *     description="Get Leader presence states info of specific day with format Y-M-D",
     *     produces={"application/json"},
     *     @SWG\Parameter(
     *          name="date",
     *          in="path",
     *          required=true,
     *          type="string"
     *     ),
     *     tags={"States"},
     *     security={
     *          {"passport": {}},
     *     },
     *     @SWG\Response(response=200, ref="#/responses/statesInfo")
     * )
     */
    public function getLeaderPresenceStatesInfoByDate($date, TeamHelper $helper)
    {
/*        $teamStates = TeamState::where('date', $date)->whereIn('state_id', [4, 5, 6])->get();
        $totalManagers = config('custom.number_of_managers', 99);

        $data = [
            'team_states' => $teamStates,
            'managers_limit' => $totalManagers,
        ];
        return json_response()->success($data);*/

        $leadershipState = TeamLeadership::where('date', $date)->get();
        $totalManagers = config('custom.number_of_managers', 99);

        $data = [
            'team_states' => $leadershipState,
            'managers_limit' => $totalManagers,
        ];
        return json_response()->success($data);
    }



    /**
     * Get Trend state of specific day
     *
     * @return \Illuminate\Http\Response
     *
     * @SWG\Get(
     *     path="/api/v2/state/trend/{date}",
     *     description="Get Trend state of specific day  ",
     *     operationId="team.state.trend",
     *     produces={"application/json"},
     *     @SWG\Parameter(
     *         name="date",
     *         in="path",
     *         required=true,
     *         type="string"
     *     ),
     *     tags={"States"},
     *     security={
     *     {"passport": {}},
     *   },
     *     @SWG\Response(response=200, ref="#/responses/trend")
     * )
     */
    public function getTrendState($date)
    {
        $trendState = TeamState::where('date', $date)
            ->whereNotIn("state_id", [4, 5, 6])
            ->orderBy('number_of_members', 'desc')
            ->orderBy('state_id', 'desc')
            ->first();

        return json_response()->success($trendState);
    }

    /**
     * Get the trend of the current month from the first day of the month to the current day.
     *
     * @return \Illuminate\Http\JsonResponse
     *
     * @SWG\Get(
     *     path="/api/v2/team/trend",
     *     description="Get the trend of the current month from the first day of the month to the current day.",
     *     produces={"application/json"},
     *     security={
     *          {"passport": {}},
     *     },
     *     tags={"Team"},
     *     @SWG\Response(response=200, description="Trend returned Successfully",ref="#/responses/trendMonth")
     *  )

     */
    public function getTrend(Request $request)
    {
        $year = date('Y');
        $month = date('m');
        // $week = date('W');

        if ($request->get('year') && $request->get('month')) {
            // if ($request->get('year') && $request->get('week')) {
            $year = $request->get('year');
            $month = $request->get('month');
            // $week = $request->get('week');
        }

        $first_of_month = Carbon::create($year, $month, 1);
        $start_date = $first_of_month->format("Y-m-d");

        if ($request->get('year') && $request->get('month') && $request->get('month') != date('m')) {
            $end_date = $first_of_month->endOfMonth()->toDateString();
        } else {
            $end_date = Carbon::now()->toDateString();
        }

        // $first_of_month->setISODate($year, $week); // set week
        // $start_date = $first_of_month->startOfWeek()->toDateString();
        // $end_date = $first_of_month->endOfWeek()->toDateString();

        $swiper_steps = TeamHelper::getFirstTrend();
        $trends = TeamHelper::getTrends($start_date, $end_date);
        $formatted_trends = $this->formatTeamChart($trends, $start_date, $end_date);

        return json_response()->success([
            'data' => $formatted_trends,
            'swiper_steps' => $swiper_steps,
        ]);
    }

    public function getTrendWeekly(Request $request)
    {
        $year = date('Y');
        $month = date('m');
        $week = date('W');

        if ($request->get('year') && $request->get('week')) {
            $year = $request->get('year');
            $week = $request->get('week');
        }

        $first_of_month = Carbon::create($year, $month, 1);
        $first_of_month->setISODate($year, $week); // set week
        $start_date = $first_of_month->startOfWeek()->toDateString();
        $end_date = $first_of_month->endOfWeek()->toDateString();

        $swiper_steps = TeamHelper::getFirstTrend('week');
        $trends = TeamHelper::getTrends($start_date, $end_date);
        $formatted_trends = $this->formatTeamChart($trends, $start_date, $end_date);

        return json_response()->success([
            'data' => $formatted_trends,
            'swiper_steps' => $swiper_steps,
        ]);
    }

    /**
     * Get the trend of the past five days.
     *
     * @return \Illuminate\Http\JsonResponse
     *
     * @SWG\Get(
     *     path="/api/v2/team/trend/simple",
     *     description="Get the trend of the past five days.",
     *     produces={"application/json"},
     *     security={
     *          {"passport": {}},
     *     },
     *     tags={"Team"},
     *     @SWG\Response(response=200, description="Trend returned Successfully",ref="#/responses/trendMonth")
     *  )

     */
    public function getSimpleTrend()
    {
        $formatted_trends = $this->prepareSimpleTrend();
        return json_response()->success($formatted_trends);
    }

    public function prepareSimpleTrend()
    {
        $year = date('Y');
        $month = date('m');
        $limit = 5; // 5 days
        
        $start_date = Carbon::now()->subWeeks(1)->format("Y-m-d");
        $end_date = Carbon::now()->toDateString();

        $trends = TeamHelper::getTrends($start_date, $end_date);
        $formatted_trends = $this->formatTeamChart($trends, $start_date, $end_date, $limit);
        return $formatted_trends;
    }

    /**
     * Get the trend of the month
     *
     * @return \Illuminate\Http\JsonResponse
     *
     * @SWG\Get(
     *     path="/api/v2/state/trend/month/{month}",
     *     description="Modify the state of team member",
     *     operationId="team.state.month.trend",
     *     produces={"application/json"},
     *      @SWG\Parameter(
     *         name="month",
     *         in="path",
     *         required=true,
     *         type="integer"
     *     ),
     *     @SWG\Parameter(
     *         name="year",
     *         in="query",
     *         required=false,
     *         type="string"
     *     ),
     *     security={
     *     {"passport": {}},
     *   },
     *     tags={"States"},
     *     @SWG\Response(response=200, description="Trend returned Successfully",ref="#/responses/trendMonth")
     *  )

     */
    public function getTrendByMonth($month)
    {
        $year = app('request')->input("year");
        $year = !is_null($year) ? $year : carbon::now()->year;

        $firstOfMonth = Carbon::create($year, $month, 1);
        $startDate = $firstOfMonth->format("Y-m-d");
        $endDate = $firstOfMonth->endOfMonth()->format("Y-m-d");

        $trends = DB::select(DB::raw("select stat.`date`,  Round((sum(stat.val)/sum(stat.number_of_members)),2) number
                from
                (
                SELECT `date`,
                state_id,
                number_of_members,
                if(state_id = 1,
                    number_of_members * 5,
                    if(state_id = 2,
                        number_of_members * 3,
                        if(state_id = 3,
                            number_of_members * 1,
                            0
                        )
                    )
                )  val
                FROM `team_states` where state_id in (1,2,3) and (`date` BETWEEN '$startDate' and '$endDate')
                ) as stat
                group by stat.`date`
			"));

        $trends = json_decode(json_encode($trends), true);
        $trends = array_column($trends, 'number', 'date');
        $formattedTrends = $this->formatTeamChart($trends, $startDate, $endDate);

        return json_response()->success($formattedTrends);
    }

    /**
     * Update the capacity of a given date
     *
     * @return \Illuminate\Http\JsonResponse
     *
     * @SWG\PUT(
     *     path="/api/v2/capacity/modify",
     *     description="Modify the capacity of a given date",
     *     operationId="capacity.update",
     *     produces={"application/json"},
     *         @SWG\Parameter(
     *         name="data",
     *         description="data",
     *         in="body",
     *         required=false,
     *         type="object",
     *         @SWG\Schema(
     *             type="object",
     *             required={"date","capacity"},
     *             @SWG\Property(
     *                 property="date",
     *                 type="string",
     *                 description="the date on which the capacity should be changed , if there is no date entered so the current date will be taken instead"
     *             ),
     *             @SWG\Property(
     *                 property="capacity",
     *                 type="number",
     *                 description="the capacity number "
     *             ),
     *         )
     *     ),
     *     security={
     *     {"passport": {}},
     *   },
     *     tags={"Capacity"},
     *     @SWG\Response(response=200, description="Capacity Changed Successfully",ref="#/responses/modifyCapacity")
     *  )
     *  )
     */

    public function updateCapacityByDate(Request $request)
    {
        $validator = Validator::make($request->all(), [
            'date' => 'required|date|date_format:Y-m-d|exists:capacities,date',
            'capacity' => 'required|numeric',
        ]);

        if ($validator->fails()) {
            return json_response()->validationErrors([
                'fields' => $validator->errors(),
            ]);
        }

        $date = $request->date;

        $doneHours = $this->getDoneHoursByDueDateDashboard([$date, $date]);
        $adminCap = $this->getAdminTasks(['firstOfWeek' => $date, 'lastOfWeek' => $date]);
        $adminCap = isset($adminCap[$date]) ? AdminTaskFormatter::formatAdminTasks($adminCap[$date]) : AdminTaskFormatter::getEmptyAdminTasks();
        $nonTestTasksHours = $adminCap['NonTestTask'];
        $meetingHours = $adminCap['Meeting'];
        $trainingHours = $adminCap['Training'];
        $adminTasksHours = $adminCap['AdminTask'];
        
        $adminHours = $nonTestTasksHours + $meetingHours + $trainingHours + $adminTasksHours;
        $capacity = $request->capacity;
        $actual_capacity = $capacity + $adminHours;

        Capacity::where('date', $date)->update(['capacity' => $actual_capacity]);

        $response['capacity'] = $actual_capacity - $adminHours;
        $response['actual_capacity'] = $actual_capacity;

        if ($actual_capacity == 0) {
            $response['productivity'] = null;
        } elseif (Carbon::parse($date)->gt(Carbon::now())) {
            $response['productivity'] = 0;
        } else {
            $response['productivity'] = round((($doneHours + ($nonTestTasksHours + $meetingHours + $trainingHours + ($adminTasksHours * 0.8))) / ($response['actual_capacity'])) * 100);
        }

        $week = (new Carbon($date))->weekOfYear;
        
        event(new \App\Events\ProductivityUpdateEvent(null,$week));

        return json_response()->success($response);
    }

    public function updateDemandRateByDate(Request $request)
    {

        $validator = Validator::make($request->all(), [

            'date' => 'required|date|date_format:Y-m-d|exists:demand_rates,date',
            'demand_rate' => 'required|numeric',

        ]
        );

        if ($validator->fails()) {

            return json_response()->validationErrors([
                'fields' => $validator->errors(),
            ]);

        }

        $date = $request->date;
        $newDemandRate = $request->demand_rate;

        DemandRate::where('date', $date)->update(['demand_rate' => $newDemandRate]);

        event(new \App\Events\NewImportEvent);

        return json_response()->success(line('messages.demandrate_updated'));

    }

    /**
     * Get team calendar
     *
     * @return \Illuminate\Http\JsonResponse
     *
     * @SWG\Get(
     *     path="/api/v2/team/calendar",
     *     description="get team calendar",
     *     operationId="team.calendar",
     *     produces={"application/json"},
     *     security={
     *     {"passport": {}},
     *   },
     *     tags={"Team"},
     *     @SWG\Response(response=200, description="Team calendar returned Successfully",ref="#/responses/calendar")
     *  )
     *  )
     */
    public function getTeamCalendar()
    {
        $today = Carbon::today();

        $calendar = [];

        for ($i = 0; $i < 3; $i++) {

            if ($today->isSaturday()) {
                $today->addDays(2);
            }

            if ($today->isSunday()) {
                $today->addDays(1);
            }

            $calendar[$i]["date"] = $today->format("Y-m-d");

            $calendar[$i]["name"] = get_german_day($today->format('l'));
            $calendar[$i]["is_today"] = $today->format("Y-m-d") == carbon::today()->format("Y-m-d");

            $today->addDays(1);
        }

        return json_response()->success($calendar);
    }

    public function teamMembers()
    {
        $members = TeamMember::leftJoin('team_avatars', 'team_avatars.id', '=', 'team_members.avatar_id')
            ->select(['team_members.id as index', 'team_members.avatar_id', 'team_avatars.avatar'])
            ->orderBy('index', 'ASC')
            ->get();

        return json_response()->success($members);
    }

    public function avatars()
    {
        $avatars = TeamAvatar::get();
        return json_response()->success($avatars);
    }

    public function addAvatar(Request $request)
    {
        $validator = Validator::make($request->all(),
            [
                'member_id' => 'required',
                'avatar' => 'file',
            ]
        );

        if ($validator->fails()) {
            return json_response()->validationErrors([
                'fields' => $validator->errors(),
            ]);
        }

        $member_id = $request->get('member_id');
        if ($request->hasFile('avatar')) {
            $file = $request->file('avatar');
            //Upload image
            $original_name = $file->getClientOriginalName();
            $file_name = str_random(30) . '.' . $file->getClientOriginalExtension();
            $file->storeAs("public/avatars", $file_name);
            $avatar = TeamAvatar::create([
                'avatar' => $file_name,
            ]);
        } elseif ($request->get('avatar_id')) {
            $avatar_id = $request->get('avatar_id');
            $avatar = TeamAvatar::find($avatar_id);
        } else {
            return json_response()->error("Please provide an avatar(as a file) or a valid avatar_id");
        }

        //Save new member
        $member = TeamMember::find($member_id);
        $member->update(['avatar_id' => $avatar->id]);

        return json_response()->success($member);
    }
}

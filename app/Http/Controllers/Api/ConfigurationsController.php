<?php

namespace App\Http\Controllers\Api;

use App\Helpers\CommanUtility;
use App\Helpers\ConfigurationHelper;
use App\Http\Controllers\Controller;
use App\Models\DemandRate;
use App\Models\Section;
use Illuminate\Http\Request;
use Response;
use Validator;

class ConfigurationsController extends Controller
{

    use CommanUtility;

    /**
     * Configure DemandRates Or Capacities
     *
     * @return \Illuminate\Http\JsonResponse
     *
     * @SWG\Post(
     *     path="/api/v2/config",
     *     description="configure the level demand rates or capacities ",
     *     produces={"application/json"},
     *     @SWG\Parameter(
     *         name="days",
     *         in="body",
     *         type="object",
     *         @SWG\Schema(
     *             type="object",
     *             @SWG\Property(
     *                 property="Saturday",
     *                 type="integer",
     *                 description="value for saturday"
     *             ),
     *             @SWG\Property(
     *                 property="Sunday",
     *                 type="integer",
     *                 description="value for Sunday"
     *             ),
     *             @SWG\Property(
     *                 property="Monday",
     *                 type="integer",
     *                 description="value for Monday"
     *             ),
     *             @SWG\Property(
     *                 property="Tuesday",
     *                 type="integer",
     *                 description="value for Tuesday"
     *             ),
     *             @SWG\Property(
     *                 property="Wednesday",
     *                 type="integer",
     *                 description="value for Wednesday"
     *             ),
     *             @SWG\Property(
     *                 property="Thursday",
     *                 type="integer",
     *                 description="value for Thursday"
     *             ),
     *             @SWG\Property(
     *                 property="Friday",
     *                 type="integer",
     *                 description="value for Friday"
     *             ),
     *             @SWG\Property(
     *                 property="type",
     *                 type="string",
     *                 description="demand_rate or capacity"
     *             ),
     *         ),
     *     ),
     *     tags={"Configurable Values"},
     *     @SWG\Response(response=200,
     *     description="values configured successfully",
     *     @SWG\Schema(
     *             type="object",
     *             @SWG\Property(
     *                 property="message",
     *                 type="string",
     *                 description="Success message."
     *             )
     *         )
     *     )
     * )
     */

/*     public function configure(Request $request)
{

$validator = Validator::make($request->all(),
[
'type' => 'required|in:demand_rate,capacity',
'Saturday' => 'required|numeric',
'Sunday' => 'required|numeric',
'Monday' => 'required|numeric',
'Tuesday' => 'required|numeric',
'Wednesday' => 'required|numeric',
'Thursday' => 'required|numeric',
'Friday' => 'required|numeric',
]);

if ($validator->fails()) {

return json_response()->validationErrors([
'fields' => $validator->errors(),
]);

}

$type = $request->type; // demand_rate  or capacity
$tableName = ($type == "demand_rate") ? "demand_rates" : "capacities";

$year = Carbon::now()->year;
$date = (new Carbon("first day of January $year"))->format("Y-m-d");
$data[0]['date'] = $date;
$data[0][$type] = 20;

for ($i = 1; $i < 365; $i++) {

$date = (new Carbon($date))->addDays(1)->format("Y-m-d");
$data[$i]['date'] = $date;
$data[$i][$type] = 20;

}

DB::table($tableName)->truncate();
DB::table($tableName)->insert($data);

foreach ($request->except(['type']) as $key => $value) {
DB::statement("update $tableName set $type = $value where DAYNAME(`date`) = '$key' ");
}

return json_response()->success("Values has been configured");
}
 */
    public function updateAllConfigurations(Request $request, ConfigurationHelper $configurationsHelper)
    {
        $keys = $configurationsHelper->getConfigurationsKeys();
        $rules = $configurationsHelper->getConfigurationsRules();
        $data = $request->all();
        $validator = $this->validateConfigurations($data, $rules);

        if ($validator->fails()) {
            return $validator->errors();
        }

        $configurationsHelper->updateConfigurations($data);
        return [];
    }

    public function getAllConfigurations(ConfigurationHelper $configurationHelper)
    {
        //$teamCapacities = $configurationHelper->getTeamCapacities();
        $leaderships = $configurationHelper->ListLeaderships();
        $challenges = $configurationHelper->ListChallengesStatuses();
        $configurations = $configurationHelper->listConfigurationsGroupedBySection();
        $configurations['team'] = $leaderships;
        $configurations['challenges'] = $challenges;
        //$configurations['productivity']['capacity'] = $teamCapacities;
        return $configurations;
    }

    public function listSections()
    {
        $sections = Section::all()->toArray();

        return $sections;
    }

    public function listConfigurationsBySection($id, ConfigurationHelper $configurationHelper)
    {
        $section = Section::find($id);

        if ($section) {

            $name = $section->name;
            $configSections = config('custom.config_sections');

            if (in_array($name, $configSections)) {

                $configurations = $configurationHelper->getSectionConfigurations($id);

            } else {

                if ($name == "team") {

                    $configurations = $configurationHelper->getTeamConfigurations();

                } else if ($name == "challenges") {

                    $configurations = $configurationHelper->getChallengesConfigurations();
                }
            }

            return $configurations;

        } else {
            return 0;
        }
    }

    private function validateConfigurations($input, $rules)
    {
        $validator = Validator::make($input, $rules);
        return $validator;
    }
}

<?php

namespace App\Http\Controllers\Api;

use App\Helpers\CommanUtility;
use App\Helpers\formatUtility;
use App\Helpers\serviceHelper;
use App\Http\Controllers\Controller;
use Carbon\Carbon;
use Illuminate\Http\Request;
use Illuminate\Pagination\LengthAwarePaginator;

class ServiceController extends Controller
{
    use CommanUtility;use formatUtility;

    /**
     * List data Of Service Daily
     *
     * @return \Illuminate\Http\Response
     *
     * @SWG\GET(
     *     path="/api/v2/service/daily",
     *     description="List data Of Service Daily",
     *     operationId="list.service.daily",
     *     produces={"application/json"},
     *     tags={"Service"},
     *     security={
     *     {"passport": {}},
     *   },
     *     @SWG\Response(response=200, ref="#/responses/serviceDaily")
     * )
     */

    public function getServicesDataDaily(Request $request, serviceHelper $helper)
    {
        $now = Carbon::now();

        $PlannedDoneDate = $this->getLastWorkingDay($now);

        $scheduledSamples = $helper->getScheduledTasksCount();
        $backlogedSamples = $helper->getBacklogTasksCount();

        $rescheduled = $helper->getRescheduledValue($scheduledSamples, $backlogedSamples, new Carbon($PlannedDoneDate));

        // $allRoutines = $helper->getAllRoutine();
        // $allSpecials = $helper->getAllSpecial();
        $routine = $helper->getRoutineByDate($PlannedDoneDate);
        $special = $helper->getSpecialByDate($PlannedDoneDate);

        $dates = $helper->getDueDatesByInterval($now->copy()->startOfWeek(), $now->copy()->endOfWeek(), $PlannedDoneDate);

        $dueDatesGroups = ['two', 'five', 'ten'];

        foreach ($dueDatesGroups as $group) {
            $count[$group] = isset($dates[$group][$PlannedDoneDate]) ? count($dates[$group][$PlannedDoneDate]) : 0;
        }

        $data = collect([
            'rescheduled' => $rescheduled,
            'routine' => $routine ? $routine->total : 0,
            'special' => $special ? $special->total : 0,
            'twoDays' => $count['two'],
            'fiveDays' => $count['five'],
            'tenDays' => $count['ten'],
        ]);

        return json_response()->success($data);
    }

    public function getDueDatesDaily(serviceHelper $helper)
    {
        $data = $helper->getDueDatesQueryForToday();

        return json_response()->success($data);
    }

    /**
     * Get the number of Routine samples for every day in the current month
     *
     * @return \Illuminate\Http\Response
     *
     * @SWG\Get(
     *     path="/api/v2/service/routine/month",
     *     description="get the number of routine samples for every day in the month",
     *     operationId="service.routine.month",
     *     produces={"application/json"},
     *             @SWG\Property(
     *                 property="year",
     *                 type="string",
     *                 description="the year"
     *             ),
     *             @SWG\Property(
     *                 property="month",
     *                 type="string",
     *                 description="the month"
     *             ),
     *     tags={"Service"},
     *     security={
     *     {"passport": {}},
     *   },
     *     @SWG\Response(response=200, ref="#/responses/serviceRoutineMonth")
     * )
     */

    public function getRoutineSamplesOfMonth(serviceHelper $helper)
    {
        $year = app('request')->input("year");
        $year = !is_null($year) ? $year : carbon::now()->year;

        $month = app('request')->input("month");
        $month = !is_null($month) ? $month : carbon::now()->month;

        $start = Carbon::createFromDate($year, $month, 1);
        $startDate = (new Carbon($start));

        $endDate = $start->endOfMonth();

        $allRoutines = $helper->getAllRoutine();

        $all = [];

        for ($i = 0; $i < $endDate->day; $i++) {
            $date = Carbon::createFromDate($year, $month, $i + 1);
            $all[$i]['index'] = $i + 1;
            $all[$i]['value'] = $helper->getServicesValue($allRoutines, $date);
        }

        return json_response()->success($all);
    }

    /**
     * Get the number of Special samples for every day in the current month
     *
     * @return \Illuminate\Http\Response
     *
     * @SWG\Get(
     *     path="/api/v2/service/special/month",
     *     description="get the number of special samples for every day in the month",
     *     operationId="service.special.month",
     *     produces={"application/json"},
     *             @SWG\Property(
     *                 property="year",
     *                 type="string",
     *                 description="the year"
     *             ),
     *             @SWG\Property(
     *                 property="month",
     *                 type="string",
     *                 description="the month"
     *             ),
     *     tags={"Service"},
     *     security={
     *     {"passport": {}},
     *   },
     *     @SWG\Response(response=200, ref="#/responses/serviceSpecialMonth")
     * )
     */

    public function getSpecialSamplesOfMonth(serviceHelper $helper)
    {
        $year = app('request')->input("year");
        $year = !is_null($year) ? $year : carbon::now()->year;

        $month = app('request')->input("month");
        $month = !is_null($month) ? $month : carbon::now()->month;

        $start = Carbon::createFromDate($year, $month, 1);
        $startDate = (new Carbon($start));

        $endDate = $start->endOfMonth();
        $allSpecial = $helper->getAllSpecial();

        $all = [];

        for ($i = 0; $i < $endDate->day; $i++) {

            $date = $startDate;
            $all[$i]['index'] = $i + 1;
            $all[$i]['value'] = $helper->getServicesValue($allSpecial, $date);
            $startDate->addDay(1);
        }

        return json_response()->success($all);
    }

    public function getRoutineSamplesOfWeek(Request $request, serviceHelper $helper)
    {
        $now = carbon::now();
        $year = $request->get("year", $now->year);
        $week = $request->get("week", $now->weekOfYear);

        $data['data'] = $helper->getRoutineSamplesOfWeek($year, $week);
        $data['swiper_steps'] = $helper->getRoutineSamplesOfWeekSteps($year, $week);

        return json_response()->success($data);
    }

    public function getSpecialSamplesOfWeek(Request $request, serviceHelper $helper)
    {
        $now = carbon::now();
        $year = $request->get("year", $now->year);
        $week = $request->get("week", $now->weekOfYear);

        $data['data'] = $helper->getSpecialSamplesOfWeek($year, $week);
        $data['swiper_steps'] = $helper->getSpecialSamplesOfWeekSteps($year, $week);

        return json_response()->success($data);
    }

    /**
     * Get the number of due dates for the intervals in everyday in the current month
     *
     * @return \Illuminate\Http\Response
     *
     * @SWG\GET(
     *     path="/api/v2/duedate/month",
     *     description="get the number of due dates for the intervals in everyday in the current month ",
     *     operationId="duedates.month",
     *     produces={"application/json"},
     *             @SWG\Property(
     *                 property="year",
     *                 type="string",
     *                 description="the year"
     *             ),
     *             @SWG\Property(
     *                 property="month",
     *                 type="string",
     *                 description="the month"
     *             ),
     *     tags={"Service"},
     *     security={
     *     {"passport": {}},
     *   },
     *     @SWG\Response(response=200, ref="#/responses/duedateMonth")
     * )
     */

    public function getDueDatesIntervalForCurrentMonth(serviceHelper $helper)
    {
        $year = app('request')->input("year");
        $year = !is_null($year) ? $year : carbon::now()->year;

        $month = app('request')->input("month");
        $month = !is_null($month) ? $month : carbon::now()->month;

        $start = Carbon::createFromDate($year, $month, 1);

        $startDate = (new Carbon($start));

        $endDate = $start->endOfMonth();

        $startDateString = $startDate->format("Y-m-d");

        $endDateString = $endDate->format("Y-m-d");

        $dates = $helper->getDueDatesByInterval($startDateString, $endDateString);

        $finalResult = [];
        $i = 0;

        $dueDatesGroups = ['two', 'five', 'ten'];

        $dueDatesGroups = ['two', 'five', 'ten'];

        while ($startDate->format("Y-m-d") <= $endDate->format("Y-m-d")) {
            $startDateString = $startDate->format("Y-m-d");

            foreach ($dueDatesGroups as $group) {
                $count[$group] = isset($dates[$group][$startDateString]) ? count($dates[$group][$startDateString]) : 0;
            }

            $currentDay = Carbon::now()->format("Y-m-d");

            $finalResult[$i]["index"] = $i + 1;
            $finalResult[$i]["twoDays"] = ($startDateString < $currentDay) ? $count['two'] : 0;
            $finalResult[$i]["fiveDays"] = ($startDateString < $currentDay) ? $count['five'] : 0;
            $finalResult[$i]["tenDays"] = ($startDateString < $currentDay) ? $count['ten'] : 0;

            $i++;

            $startDate->addDay();
        }

        return json_response()->success($finalResult);
    }

    public function getDueDatesIntervalForCurrentWeek(Request $request, serviceHelper $helper)
    {
        $now = carbon::now();
        $year = $request->get("year", $now->year);
        $week = $request->get("week", $now->weekOfYear);

        $specific_week = $now->setISODate($year, $week);
        $startDate = $specific_week->startOfWeek();
        $endDate = $specific_week->copy()->endOfWeek();

        $startDateString = $startDate->format("Y-m-d");
        $endDateString = $endDate->format("Y-m-d");
        // dd($startDateString, $endDateString);

        $data = $helper->getDueDatesByInterval($startDateString, $endDateString, null, true);

        $dates = $data['data'];

        $finalResult = [];
        $i = 0;

        $dueDatesGroups = ['two', 'five', 'ten'];

        while ($startDate->format("Y-m-d") <= $endDate->format("Y-m-d")) {
            $startDateString = $startDate->format("Y-m-d");

            foreach ($dueDatesGroups as $group) {
                $count[$group] = isset($dates[$group][$startDateString]) ? count($dates[$group][$startDateString]) : 0;
            }

            $currentDay = Carbon::now()->format("Y-m-d");

            $finalResult[$i]["index"] = $i + 1;
            $finalResult[$i]["year"] = $startDate->year;
            $finalResult[$i]["month"] = $startDate->month;
            $finalResult[$i]["day"] = $startDate->day;
            $finalResult[$i]["week"] = (integer) $startDate->format('W');
            $finalResult[$i]["date"] = $startDate->toDateString();
            $finalResult[$i]["twoDays"] = ($startDateString < $currentDay) ? $count['two'] : 0;
            $finalResult[$i]["fiveDays"] = ($startDateString < $currentDay) ? $count['five'] : 0;
            $finalResult[$i]["tenDays"] = ($startDateString < $currentDay) ? $count['ten'] : 0;

            $i++;
            $startDate->addDay();
        }

        $data['data'] = $finalResult;

        return json_response()->success($data);
    }

    /**
     * Get the number of rescheduled samples for every day in the current month
     *
     * @return \Illuminate\Http\Response
     *
     * @SWG\Get(
     *     path="/api/v2/rescheduled/month",
     *     description="get the number of rescheduled samples for every day in the month",
     *     operationId="service.rescheduled.month",
     *     produces={"application/json"},
     *             @SWG\Property(
     *                 property="year",
     *                 type="string",
     *                 description="the year"
     *             ),
     *             @SWG\Property(
     *                 property="month",
     *                 type="string",
     *                 description="the month"
     *             ),
     *     tags={"Service"},
     *     security={
     *     {"passport": {}},
     *   },
     *     @SWG\Response(response=200, ref="#/responses/serviceRescheduledMonth")
     * )
     */

    public function getRescheduledOfMonth(serviceHelper $helper)
    {
        $year = app('request')->input("year");
        $year = !is_null($year) ? $year : carbon::now()->year;

        $month = app('request')->input("month");
        $month = !is_null($month) ? $month : carbon::now()->month;

        $start = Carbon::createFromDate($year, $month, 1);
        $startDate = (new Carbon($start));

        $endDate = $start->endOfMonth();

        $scheduledSamples = $helper->getScheduledTasksCount();
        $backlogedSamples = $helper->getBacklogTasksCount();

        $all = [];

        for ($i = 0; $i < $endDate->day; $i++) {
            $date = $startDate;
            $all[$i]['index'] = $i + 1;
            $all[$i]['value'] = $helper->getRescheduledValue($scheduledSamples, $backlogedSamples, $date);
            $startDate->addDay();
        }

        return json_response()->success($all);
    }

    public function getRescheduledSamplesOfWeek(Request $request, serviceHelper $helper)
    {
        $now = carbon::now();
        $year = $request->get("year", $now->year);
        $week = $request->get("week", $now->weekOfYear);

        $data['data'] = $helper->getRescheduledSamplesOfWeek($year, $week);
        $data['swiper_steps'] = $helper->getRescheduledSamplesOfWeekSteps($year, $week);

        return json_response()->success($data);
    }

    public function samples(Request $request, serviceHelper $helper)
    {
        $type = $request->get('type');
        $testing_date = $request->get('testing_date');

        $now = !$testing_date ? Carbon::now() : Carbon::parse($testing_date);
        $last_working_day = $this->getLastWorkingDay($now);
        $date = Carbon::parse($last_working_day);

        $samples = [];

        /**
         * Removed for testing cause 2019-06-20 is a holyday
         */
        // $on_holyday = DB::table('holidays')->where('date', $date->toDateString())
        //     ->exists();

        // if ($on_holyday) {
        //     return json_response()->success($samples);
        // }

        switch ($type) {
            case 'routine':
                $samples = $helper->getRoutineSamplesByDate($date);
                break;
            case 'special':
                $samples = $helper->getSpecialSamplesByDate($date);
                break;
            case 'rescheduled':
                $samples = $helper->getRescheduleSamplesByDate($date);
                break;
        }

        if (in_array($type, ['due_date_2', 'due_date_5', 'due_date_10'])) {
            if ($type == 'due_date_2') {
                $samples = $helper->getDueDatesByDate($now->copy()->startOfWeek(), $now->copy()->endOfWeek(), $date->toDateString(), 'two');
                // $samples = $helper->getDueDatesByDate($date, 'two');
            } elseif ($type == 'due_date_5') {
                $samples = $helper->getDueDatesByDate($now->copy()->startOfWeek(), $now->copy()->endOfWeek(), $date->toDateString(), 'five');
                // $samples = $helper->getDueDatesByDate($date, 'five');
            } else {
                $samples = $helper->getDueDatesByDate($now->copy()->startOfWeek(), $now->copy()->endOfWeek(), $date->toDateString(), 'ten');
                // $samples = $helper->getDueDatesByDate($date, 'ten');
            }

            $page = $request->get('page', 1);
            $page_size = 6;
            $pagination_offset = $page * $page_size - $page_size;
            $paged_data = $samples->slice($pagination_offset, $page_size)->all();
            $paged_data = array_values($paged_data);
            $samples = (new LengthAwarePaginator($paged_data, $samples->count(), $page_size, $page, ['path' => $request->fullUrl()]))->toArray();
        }

        return json_response()->paginate($samples);
    }
}

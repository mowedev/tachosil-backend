<?php

namespace App\Http\Controllers\Api;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class ResponsesController extends Controller
{
   /**
     *@SWG\Response(
     *    response="tasks",
     *    description="week task Json response",
     *    @SWG\Schema(
     *        type="array",
     *            @SWG\Items(
     *                @SWG\Property(
     *                    property="day",
     *                    type="string",
     *                ),
     *                @SWG\Property(
     *                    property="productivity",
     *                    type="number",
     *                ),
     *                @SWG\Property(
     *                    property="capacity",
     *                    type="integer",
     *                ),
     *                @SWG\Property(
     *                    property="tasks",
     *                    type="array",
     *                    @SWG\Items(
     *                        @SWG\Property(
     *                            property="id",
     *                            type="integer",
     *                        ),
     *                        @SWG\Property(
     *                            property="order",
     *                            type="integer",
     *                        ),
     *                        @SWG\Property(
     *                            property="sample_number",
     *                            type="integer",
     *                        ),
     *                        @SWG\Property(
     *                            property="description",
     *                            type="string",
     *                        ),
     *                        @SWG\Property(
     *                            property="status",
     *                            type="string",
     *                        ),
     *                        @SWG\Property(
     *                            property="due_date",
     *                            type="string",
     *                        ),
     *                        @SWG\Property(
     *                            property="subtasks",
     *                            type="array",
     *                            @SWG\Items(
     *                                @SWG\Property(
     *                                    property="id",
     *                                    type="integer",
     *                                ),
     *                                @SWG\Property(
     *                                    property="sample_number",
     *                                    type="integer",
     *                                ),
     *                                @SWG\Property(
     *                                    property="reported_name",
     *                                    type="string",
     *                                ),
     *                                @SWG\Property(
     *                                    property="analysis",
     *                                    type="string",
     *                                ),
     *                                @SWG\Property(
     *                                    property="sample_status",
     *                                    type="string",
     *                                ),
     *                                @SWG\Property(
     *                                    property="test_status",
     *                                    type="string",
     *                                ),
     *                            )
     *                        )
     *                    )
     *                ),
     *                @SWG\Property(
     *                    property="backlog",
     *                    type="array",
     *                        @SWG\Items(
     *                            @SWG\Property(
     *                                property="id",
     *                                type="integer",
     *                            ),
     *                            @SWG\Property(
     *                                property="order",
     *                                type="integer",
     *                            ),
     *                            @SWG\Property(
     *                                property="sample_number",
     *                                type="integer",
     *                            ),
     *                            @SWG\Property(
     *                                property="description",
     *                                type="string",
     *                            ),
     *                            @SWG\Property(
     *                                property="status",
     *                                type="string",
     *                            ),
     *                            @SWG\Property(
     *                                property="due_date",
     *                                type="string",
     *                            ),
     *                            @SWG\Property(
     *                                property="subtasks",
     *                                type="array",
     *                                @SWG\Items(
     *                                    @SWG\Property(
     *                                        property="id",
     *                                        type="integer",
     *                                    ),
     *                                    @SWG\Property(
     *                                        property="sample_number",
     *                                        type="integer",
     *                                    ),
     *                                    @SWG\Property(
     *                                        property="reported_name",
     *                                        type="string",
     *                                    ),
     *                                    @SWG\Property(
     *                                        property="analysis",
     *                                        type="string",
     *                                    ),
     *                                    @SWG\Property(
     *                                        property="sample_status",
     *                                        type="string",
     *                                    ),
     *                                    @SWG\Property(
     *                                        property="test_status",
     *                                        type="string",
     *                                    ),
     *                                )
     *                            )
     *                        )
     *                    ),
     *                )
     *            )
     *        ),
     *    )

    /**
     *    @SWG\Response(
     *         response="tasksListView",
     *         description="week task Json response",
     *           @SWG\Schema(
     *                type="array",
     *                 @SWG\Items(
     *                    @SWG\Property(
     *                        property="date",
     *                        type="string",
     *                    ),
     *                    @SWG\Property(
     *                        property="day",
     *                        type="string",
     *                    ),
     *                        @SWG\Property(
     *                                  property="tasks",
     *                             type="array",
     *                             @SWG\Items(
     *                                       @SWG\Property(
     *                                            property="id",
     *                                       type="integer",
                                                  ),
                                                  @SWG\Property(
     *                                            property="order",
     *                                       type="integer",
                                                  ),
                                                  @SWG\Property(
     *                                            property="sample_number",
     *                                       type="integer",
                                                  ),
                                                  @SWG\Property(
     *                                            property="description",
     *                                       type="string",
                                                  ),
                                                  @SWG\Property(
     *                                            property="status",
     *                                       type="string",
                                                  ),
                                                  @SWG\Property(
     *                                            property="due_date",
     *                                       type="string",
                                                  ),
                                                  @SWG\Property(
     *                                            property="subtasks",
     *                                       type="array",
     *                                       @SWG\Items(
     *                                            @SWG\Property(
     *                                                      property="id",
     *                                                 type="integer",
                                                            ),
                                                            @SWG\Property(
     *                                                      property="sample_number",
     *                                                 type="integer",
                                                            ),
                                                            @SWG\Property(
     *                                                      property="reported_name",
     *                                                 type="string",
                                                            ),
                                                            @SWG\Property(
     *                                                      property="analysis",
     *                                                 type="string",
                                                            ),
                                                            @SWG\Property(
     *                                                      property="sample_status",
     *                                                 type="string",
                                                            ),
                                                            @SWG\Property(
     *                                                      property="test_status",
     *                                                 type="string",
                                                            ),
     *                                       )
                                                  )
     *                                       )
     *
     *                                  )
                         )
                    ),
     *
     *     )

/**
     * @SWG\Response(
     *     response="states",
     *     description="Listing of all states",
     *         @SWG\Items(
     *             @SWG\Property(
     *                 property="data",
     *                 type="array",
     *         @SWG\Items(
     *                     @SWG\Property(
     *                         property="id",
     *                         type="number",
     *                     ),
     *                     @SWG\Property(
     *                         property="name",
     *                         type="string",
     *                     ),
     *     ),
     *   )
     * ),
     * )
     */

/**
     * @SWG\Response(
     *     response="guageDay",
     *     description="Get the completed tasks percent In Current Day",
     *         @SWG\Items(
     *              @SWG\Property(
     *                 property="data",
     *                 type="object",
     *                     @SWG\Property(
     *                         property="value",
     *                         type="number",
     *                     )
     * )
     *    )
     * )
     */

     /**
     * @SWG\Response(
     *     response="scheduleAdherenece",
     *     description="Get the completed tasks percent for each month in current year ",
     *         @SWG\Items(
     *             @SWG\Property(
     *                 property="data",
     *                 type="array",
     *         @SWG\Items(
     *                     @SWG\Property(
     *                         property="actual",
     *                         type="number",
     *                     ),
     *                     @SWG\Property(
     *                         property="month",
     *                         type="number",
     *                     ),
     *                     @SWG\Property(
     *                         property="name",
     *                         type="string",
     *                     ),
     *                     @SWG\Property(
     *                         property="target",
     *                         type="number",
     *                     ),
     *     ),
     *   )
     * ),
     * )
     */

     /**
     * @SWG\Response(
     *     response="scheduleAdhereneceWeeks",
     *     description="Get the completed tasks percent for 12 week ",
     *         @SWG\Items(
     *             @SWG\Property(
     *                 property="data",
     *                 type="array",
     *         @SWG\Items(
     *                     @SWG\Property(
     *                         property="actual",
     *                         type="number",
     *                     ),
     *                     @SWG\Property(
     *                         property="week",
     *                         type="number",
     *                     ),
     *                     @SWG\Property(
     *                         property="name",
     *                         type="string",
     *                     ),
     *                     @SWG\Property(
     *                         property="target",
     *                         type="number",
     *                     ),
     *     ),
     *   )
     * ),
     * )
     */

          /**
     * @SWG\Response(
     *     response="demandRate",
     *     description="Get the demand rates for 12 week ",
     *         @SWG\Items(
     *             @SWG\Property(
     *                 property="data",
     *                 type="array",
     *         @SWG\Items(
     *                     @SWG\Property(
     *                         property="week",
     *                         type="number",
     *                     ),
     *                     @SWG\Property(
     *                         property="value",
     *                         type="string",
     *                     ),
     *     ),
     *   )
     * ),
     * )
     */
          /**
     * @SWG\Response(
     *     response="productivity",
     *     description="Get the productivity for 12 week ",
     *         @SWG\Items(
     *             @SWG\Property(
     *                 property="data",
     *                 type="array",
     *         @SWG\Items(
     *                     @SWG\Property(
     *                         property="week",
     *                         type="number",
     *                     ),
     *                     @SWG\Property(
     *                         property="value",
     *                         type="string",
     *                     ),
     *     ),
     *   )
     * ),
     * )
     */

     /**
     * @SWG\Response(
     *     response="dashboard",
     *     description="Listing of all dashboard values",
     *         @SWG\Items(
     *             @SWG\Property(
     *                 property="data",
     *                 type="array",
     *         @SWG\Items(
     *                     @SWG\Property(
     *                         property="accidents",
     *                         type="number",
     *                     ),
     *                     @SWG\Property(
     *                         property="observations",
     *                         type="number",
     *                     ),
     *                     @SWG\Property(
     *                         property="freeDays",
     *                         type="number",
     *                     ),
     *                     @SWG\Property(
     *                         property="sixs",
     *                         type="number",
     *                     ),
     *                     @SWG\Property(
     *                         property="guageAnalytic",
     *                         type="number",
     *                     ),
     *                     @SWG\Property(
     *                         property="quality-not-run",
     *                         type="number",
     *                     ),
     *                     @SWG\Property(
     *                         property="quality-certificates",
     *                         type="number",
     *                     ),
     *                     @SWG\Property(
     *                         property="quality-oos",
     *                         type="number",
     *                     ),
     *                     @SWG\Property(
     *                         property="service-routine",
     *                         type="number",
     *                     ),
     *                     @SWG\Property(
     *                         property="service-special",
     *                         type="number",
     *                     ),
     *                     @SWG\Property(
     *                         property="capacity",
     *                         type="number",
     *                     ),
     *                     @SWG\Property(
     *                         property="plannedHours",
     *                         type="number",
     *                     ),
     *                     @SWG\Property(
     *                         property="doneHours",
     *                         type="number",
     *                     ),
     *                     @SWG\Property(
     *                         property="last_import_time",
     *                         type="string",
     *                     ),
     *                     @SWG\Property(
     *                         property="latest_highlight",
     *                         type="object",
     *                     ),
     *     ),
     *   )
     * ),
     * )
     */

    /**
     * @SWG\Response(
     *     response="typePerMonth",
     *     description="Listing of all errors per month",
     *     @SWG\Items(
     *          @SWG\Property(
     *               property="data",
     *               type="array",
     *               @SWG\Items(
     *                    @SWG\Property(
     *                         property="date",
     *                         type="string",
     *                    ),
     *                    @SWG\Property(
     *                         property="value",
     *                         type="number",
     *                    ),
     *               ),
     *          )
     *     ),
     * )
     */

    /**
     * @SWG\Response(
     *     response="typePerWeek",
     *     description="Listing of all errors per week",
     *     @SWG\Items(
     *          @SWG\Property(
     *               property="data",
     *               type="array",
     *               @SWG\Items(
     *                    @SWG\Property(
     *                         property="index",
     *                         type="number",
     *                    ),
     *                    @SWG\Property(
     *                         property="value",
     *                         type="number",
     *                    ),
     *               ),
     *          )
     *     ),
     * )
     */

    /**
     * @SWG\Response(
     *     response="sixsYearAverage",
     *     description="list the data of sixS of current month",
     *     @SWG\Items(
     *          @SWG\Property(
     *               property="data",
     *               type="array",
     *               @SWG\Items(
     *                    @SWG\Property(
     *                         property="month",
     *                         type="number",
     *                         description="",
     *                    ),
     *                    @SWG\Property(
     *                         property="name",
     *                         type="string",
     *                         description="",
     *                    ),
     *                    @SWG\Property(
     *                         property="target",
     *                         type="number",
     *                         description="",
     *                    ),
     *                    @SWG\Property(
     *                         property="data",
     *                         type="array",
     *                         description="",
     *                         @SWG\Items(
     *                              @SWG\Property(
     *                                   property="date",
     *                                   type="string",
     *                                   description="",
     *                              ),
     *                              @SWG\Property(
     *                                   property="safety",
     *                                   type="number",
     *                                   description="",
     *                              ),
     *                              @SWG\Property(
     *                                   property="order",
     *                                   type="number",
     *                                   description="",
     *                              ),
     *                              @SWG\Property(
     *                                   property="documents",
     *                                   type="number",
     *                                   description="",
     *                              ),
     *                              @SWG\Property(
     *                                   property="materials",
     *                                   type="number",
     *                                   description="",
     *                              ),
     *                              @SWG\Property(
     *                                   property="clothing",
     *                                   type="number",
     *                                   description="",
     *                              ),
     *                              @SWG\Property(
     *                                   property="security",
     *                                   type="number",
     *                                   description="",
     *                              ),
     *                              @SWG\Property(
     *                                   property="discipline",
     *                                   type="number",
     *                                   description="",
     *                              ),
     *                              @SWG\Property(
     *                                   property="average",
     *                                   type="number",
     *                                   description="",
     *                              ),
     *                              @SWG\Property(
     *                                   property="month",
     *                                   type="number",
     *                                   description="",
     *                              ),
     *                         )
     *                    ),
     *               ),
     *          )
     *     ),
     * )
     */


     /**
     * @SWG\Response(
     *     response="qualityTrendYear",
     *     description="Listing quality trend of every month in the year ",
     *         @SWG\Items(
     *             @SWG\Property(
     *                 property="data",
     *                 type="array",
     *         @SWG\Items(
     *                     @SWG\Property(
     *                         property="name",
     *                         type="string",
     *                     ),
     *                     @SWG\Property(
     *                         property="trend",
     *                         type="number",
     *                     ),
     *     ),
     *   )
     * ),
     * )
     */

     /**
     * @SWG\Response(
     *     response="serviceRoutineMonth",
     *     description="Get the number of routine samples for every day in the month ",
     *         @SWG\Items(
     *             @SWG\Property(
     *                 property="data",
     *                 type="array",
     *         @SWG\Items(
     *                     @SWG\Property(
     *                         property="index",
     *                         type="number",
     *                     ),
     *                     @SWG\Property(
     *                         property="value",
     *                         type="number",
     *                     ),
     *     ),
     *   )
     * ),
     * )
     */

          /**
     * @SWG\Response(
     *     response="serviceSpecialMonth",
     *     description="Get the number of special samples for every day in the month ",
     *         @SWG\Items(
     *             @SWG\Property(
     *                 property="data",
     *                 type="array",
     *         @SWG\Items(
     *                     @SWG\Property(
     *                         property="index",
     *                         type="number",
     *                     ),
     *                     @SWG\Property(
     *                         property="value",
     *                         type="number",
     *                     ),
     *     ),
     *   )
     * ),
     * )
     */

                    /**
     * @SWG\Response(
     *     response="duedateMonth",
     *     description="get the number of due dates for the intervals in everyday in the current month ",
     *         @SWG\Items(
     *             @SWG\Property(
     *                 property="data",
     *                 type="array",
     *         @SWG\Items(
     *                     @SWG\Property(
     *                         property="index",
     *                         type="number",
     *                     ),
     *                     @SWG\Property(
     *                         property="twoDays",
     *                         type="number",
     *                     ),
     *                     @SWG\Property(
     *                         property="fiveDays",
     *                         type="number",
     *                     ),
     *                     @SWG\Property(
     *                         property="tenDays",
     *                         type="number",
     *                     ),
     *     ),
     *   )
     * ),
     * )
     */

                                        /**
     * @SWG\Response(
     *     response="serviceRescheduledMonth",
     *     description="get the number of rescheduled samples for everyday in the current month ",
     *         @SWG\Items(
     *             @SWG\Property(
     *                 property="data",
     *                 type="array",
     *         @SWG\Items(
     *                     @SWG\Property(
     *                         property="index",
     *                         type="number",
     *                     ),
     *                     @SWG\Property(
     *                         property="value",
     *                         type="number",
     *                     ),
     *     ),
     *   )
     * ),
     * )
     */


/**
     * @SWG\Response(
     *     response="adminTasks",
     *     description="Listing of all admin Tasks",
     *         @SWG\Items(
     *             @SWG\Property(
     *                 property="data",
     *                 type="array",
     *         @SWG\Items(
     *                     @SWG\Property(
     *                         property="id",
     *                         type="number",
     *                     ),
     *                     @SWG\Property(
     *                         property="date",
     *                         type="string",
     *                     ),
     *                     @SWG\Property(
     *                         property="duration",
     *                         type="string",
     *                     ),
     *                     @SWG\Property(
     *                         property="category",
     *                         type="string",
     *                     ),
     *                     @SWG\Property(
     *                         property="description",
     *                         type="string",
     *                     ),
     *     ),
     *   )
     * ),
     * )
     */

/**
     * @SWG\Response(
     *     response="problems",
     *     description="Listing of problems ",
     *         @SWG\Items(
     *             @SWG\Property(
     *                 property="data",
     *                 type="array",
     *         @SWG\Items(
     *                     @SWG\Property(
     *                         property="id",
     *                         type="number",
     *                     ),
     *                     @SWG\Property(
     *                         property="category_id",
     *                         type="number",
     *                     ),
     *                     @SWG\Property(
     *                         property="status_id",
     *                         type="number",
     *                     ),
     *                     @SWG\Property(
     *                         property="description",
     *                         type="string",
     *                     ),
     *                     @SWG\Property(
     *                         property="solution",
     *                         type="string",
     *                     ),
     *                     @SWG\Property(
     *                         property="responsible",
     *                         type="string",
     *                     ),
     *                     @SWG\Property(
     *                         property="due_date",
     *                         type="string",
     *                     ),
     *     ),
     *   )
     * ),
     * )
     */

/**
     * @SWG\Response(
     *     response="highlights",
     *     description="Listing of highlights ",
     *         @SWG\Items(
     *             @SWG\Property(
     *                 property="data",
     *                 type="array",
     *         @SWG\Items(
     *                     @SWG\Property(
     *                         property="id",
     *                         type="number",
     *                     ),
     *                     @SWG\Property(
     *                         property="title",
     *                         type="string",
     *                     ),
     *                     @SWG\Property(
     *                         property="description",
     *                         type="string",
     *                     ),
     *                     @SWG\Property(
     *                         property="due_date",
     *                         type="string",
     *                     ),
     *                     @SWG\Property(
     *                         property="created_at",
     *                         type="string",
     *                     ),
     *     ),
     *   )
     * ),
     * )
     */


/**
     * @SWG\Response(
     *     response="team",
     *     description="get the team members in specific day",
     *         @SWG\Items(
     *             @SWG\Property(
     *                 property="data",
     *                 type="object",
     *                     @SWG\Property(
     *                         property="no_of_employees",
     *                         type="number",
     *                     ),
     *                     @SWG\Property(
     *                         property="no_of_managers",
     *                         type="number",
     *                     ),
     *                     @SWG\Property(
     *                         property="used_emp",
     *                         type="number",
     *                     ),
     *                     @SWG\Property(
     *                         property="used_managers",
     *                         type="number",
     *                     ),
     *                     @SWG\Property(
     *                         property="remaining_emp",
     *                         type="number",
     *                     ),
     *                     @SWG\Property(
     *                         property="remaining_managers",
     *                         type="number",
     *                     ),
     *   )
     * ),
     * )
     */

     /**
     * @SWG\Response(
     *     response="modifyStateOfTeam",
     *     description="modify the state of members in specific day",
     *         @SWG\Items(
     *             @SWG\Property(
     *                 property="data",
     *                 type="array",
     *         @SWG\Items(
     *                     @SWG\Property(
     *                         property="date",
     *                         type="date",
     *                     ),
     *                     @SWG\Property(
     *                         property="state_id",
     *                         type="number",
     *                     ),
     *                     @SWG\Property(
     *                         property="number_of_members",
     *                         type="number",
     *                     ),
     *                     @SWG\Property(
     *                         property="state_name",
     *                         type="string",
     *                     ),
     *     ),
     *   )
     * ),
     * )
     */

     /**
     * @SWG\Response(
     *     response="changeMood",
     *     description="",
     *     @SWG\Items(
     *          @SWG\Property(
     *               property="data",
     *               type="array",
     *               @SWG\Items(
     *                    @SWG\Property(
     *                         property="id",
     *                         type="number",
     *                    ),
     *                    @SWG\Property(
     *                         property="date",
     *                         type="string",
     *                    ),
     *                    @SWG\Property(
     *                         property="index",
     *                         type="number",
     *                    ),
     *                    @SWG\Property(
     *                         property="state_id",
     *                         type="number",
     *                    ),
     *               ),
     *          )
     *     ),
     * )
     */
    /**
     * @SWG\Response(
     *     response="chengeEmoji",
     *     description="",
     *     @SWG\Items(
     *          @SWG\Property(
     *               property="data",
     *               type="array",
     *               @SWG\Items(
     *                    @SWG\Property(
     *                         property="id",
     *                         type="number",
     *                    ),
     *                    @SWG\Property(
     *                         property="highlight_id",
     *                         type="number",
     *                    ),
     *                    @SWG\Property(
     *                         property="emoji_id",
     *                         type="number",
     *                    ),
     *                    @SWG\Property(
     *                         property="count",
     *                         type="number",
     *                    ),
     *               ),
     *          )
     *     ),
     * )
     */

     /**
     * @SWG\Response(
     *     response="createUpdateProblem",
     *     description="Updating problem data ",
     *         @SWG\Items(
     *             @SWG\Property(
     *                 property="data",
     *                 type="array",
     *         @SWG\Items(
     *                     @SWG\Property(
     *                         property="category_id",
     *                         type="number",
     *                     ),
     *                     @SWG\Property(
     *                         property="status_id",
     *                         type="number",
     *                     ),
     *                     @SWG\Property(
     *                         property="description",
     *                         type="string",
     *                     ),
     *                     @SWG\Property(
     *                         property="solution",
     *                         type="string",
     *                     ),
     *                     @SWG\Property(
     *                         property="responsible",
     *                         type="string",
     *                     ),
     *                     @SWG\Property(
     *                         property="due_date",
     *                         type="string",
     *                     ),
     *     ),
     *   )
     * ),
     * )
     */

          /**
     * @SWG\Response(
     *     response="createUpdateHighlight",
     *     description="Updating highlight data ",
     *         @SWG\Items(
     *             @SWG\Property(
     *                 property="data",
     *                 type="array",
     *         @SWG\Items(
     *                     @SWG\Property(
     *                         property="id",
     *                         type="number",
     *                     ),
     *                     @SWG\Property(
     *                         property="title",
     *                         type="string",
     *                     ),
     *                     @SWG\Property(
     *                         property="description",
     *                         type="string",
     *                     ),
     *                     @SWG\Property(
     *                         property="due_date",
     *                         type="string",
     *                     ),
     *                     @SWG\Property(
     *                         property="created_at",
     *                         type="string",
     *                     ),
     *     ),
     *   )
     * ),
     * )
     */

     /**
     * @SWG\Response(
     *     response="statesInfo",
     *     description="Listing states info of specific day",
     *         @SWG\Items(
     *             @SWG\Property(
     *                 property="data",
     *                 type="array",
     *         @SWG\Items(
     *                     @SWG\Property(
     *                         property="state_id",
     *                         type="number",
     *                     ),
     *                     @SWG\Property(
     *                         property="number_of_members",
     *                         type="number",
     *                     ),
     *                     @SWG\Property(
     *                         property="state_name",
     *                         type="string",
     *                     ),
     *     ),
     *   )
     * ),
     * )
     */


     /**
     * @SWG\Response(
     *     response="problemsCategories",
     *     description="Listing problems categories ",
     *         @SWG\Items(
     *             @SWG\Property(
     *                 property="data",
     *                 type="array",
     *         @SWG\Items(
     *                     @SWG\Property(
     *                         property="id",
     *                         type="number",
     *                     ),
     *                     @SWG\Property(
     *                         property="category",
     *                         type="string",
     *                     ),
     *     ),
     *   )
     * ),
     * )
     */

     /**
     * @SWG\Response(
     *     response="problemsStatuses",
     *     description="Listing problems statuses ",
     *         @SWG\Items(
     *             @SWG\Property(
     *                 property="data",
     *                 type="array",
     *         @SWG\Items(
     *                     @SWG\Property(
     *                         property="id",
     *                         type="number",
     *                     ),
     *                     @SWG\Property(
     *                         property="status",
     *                         type="string",
     *                     ),
     *     ),
     *   )
     * ),
     * )
     */

    /**
     * @SWG\Response(
     *     response="trend",
     *     description="Listing states info of specific day",
     *     @SWG\Items(
     *          @SWG\Property(
     *               property="date",
     *               type="string",
     *          ),
     *          @SWG\Property(
     *               property="state_id",
     *               type="number",
     *          ),
     *          @SWG\Property(
     *               property="number_of_members",
     *               type="number",
     *          ),
     *     ),
     * )
     */

     /**
     * @SWG\Response(
     *     response="addtask",
     *     description="create user defined task ",
     *         @SWG\Items(
     *                     @SWG\Property(
     *                         property="id",
     *                         type="number",
     *                     ),
     *                     @SWG\Property(
     *                         property="sample_number",
     *                         type="string",
     *                     ),
     *                     @SWG\Property(
     *                         property="batch",
     *                         type="string",
     *                     ),
     *                     @SWG\Property(
     *                         property="description",
     *                         type="string",
     *                     ),
     *                     @SWG\Property(
     *                         property="due_date",
     *                         type="string",
     *                     ),
     *                     @SWG\Property(
     *                         property="product",
     *                         type="string",
     *                     ),
     *                     @SWG\Property(
     *                         property="stage",
     *                         type="string",
     *                     ),
     *                     @SWG\Property(
     *                         property="status",
     *                         type="string",
     *                     ),
     *                     @SWG\Property(
     *                         property="login_date",
     *                         type="string",
     *                     ),
     *                     @SWG\Property(
     *                         property="receive_date",
     *                         type="string",
     *                     ),
     *                     @SWG\Property(
     *                         property="date_completed",
     *                         type="string",
     *                     ),
     *                     @SWG\Property(
     *                         property="date_reviewed",
     *                         type="string",
     *                     )
     * ),
     * )
     */

    /**
     * @SWG\Response(
     *     response="trendMonth",
     *     description="Get The trend states of the given month",
     *     @SWG\Items(
     *          @SWG\Property(
     *               property="data",
     *               type="array",
     *               @SWG\Items(
     *                    @SWG\Property(
     *                         property="state_id",
     *                         type="number",
     *                    ),
     *                    @SWG\Property(
     *                         property="name",
     *                         type="string",
     *                    ),
     *                    @SWG\Property(
     *                         property="total",
     *                         type="number",
     *                    ),
     *                    @SWG\Property(
     *                         property="date",
     *                         type="string",
     *                    ),
     *               ),
     *          )
     *     ),
     * )
     */

     /**
     * @SWG\Response(
     *     response="modifyCapacity",
     *     description="Modifying the capacity of specific day",
     *         @SWG\Items(
     *                     @SWG\Property(
     *                         property="id",
     *                         type="number",
     *                     ),
     *                     @SWG\Property(
     *                         property="date",
     *                         type="string",
     *                     ),
     *                     @SWG\Property(
     *                         property="capacity",
     *                         type="number",
     *                     ),
     * ),
     * )
     */

          /**
     * @SWG\Response(
     *     response="showAdminTask",
     *     description="Get Specific task information",
     *         @SWG\Items(
     *                     @SWG\Property(
     *                         property="id",
     *                         type="number",
     *                     ),
     *                     @SWG\Property(
     *                         property="date",
     *                         type="string",
     *                     ),
     *                     @SWG\Property(
     *                         property="duration",
     *                         type="number",
     *                     ),
     *                     @SWG\Property(
     *                         property="category",
     *                         type="string",
     *                     ),
     *                     @SWG\Property(
     *                         property="description",
     *                         type="string",
     *                     ),
     * ),
     * )
     */


     /**
     * @SWG\Response(
     *     response="calendar",
     *     description="return team calendar",
     *         @SWG\Items(
     *                     @SWG\Property(
     *                         property="date",
     *                         type="string",
     *                     ),
     *                     @SWG\Property(
     *                         property="name",
     *                         type="string",
     *                     ),
     *                     @SWG\Property(
     *                         property="is_today",
     *                         type="number",
     *                     ),
     * ),
     * )
     */
/**
     * @SWG\Response(
     *     response="charts_response",
     *     description="Listing of all dashboard values",
     *         @SWG\Items(
     *             @SWG\Property(
     *                 property="data",
     *                 type="array",
     *         @SWG\Items(
     *                     @SWG\Property(
     *                         property="accidents",
     *                         type="number",
     *                     ),
     *                     @SWG\Property(
     *                         property="observations",
     *                         type="number",
     *                     ),
     *                     @SWG\Property(
     *                         property="freeDays",
     *                         type="number",
     *                     ),
     *                     @SWG\Property(
     *                         property="sixs",
     *                         type="number",
     *                     ),
     *                     @SWG\Property(
     *                         property="guageAnalytic",
     *                         type="number",
     *                     ),
     *                     @SWG\Property(
     *                         property="quality-not-run",
     *                         type="number",
     *                     ),
     *                     @SWG\Property(
     *                         property="quality-certificates",
     *                         type="number",
     *                     ),
     *                     @SWG\Property(
     *                         property="quality-oos",
     *                         type="number",
     *                     ),
     *     ),
     *   )
     * ),
     * )
     */





     /**
     * @SWG\Response(
     *     response="modifyValueOfType",
     *     description="modify the value of type in specific day",
     *         @SWG\Items(
     *             @SWG\Property(
     *                 property="data",
     *                 type="array",
     *         @SWG\Items(
     *                     @SWG\Property(
     *                         property="date",
     *                         type="string",
     *                     ),
     *                     @SWG\Property(
     *                         property="type",
     *                         type="string",
     *                     ),
     *                     @SWG\Property(
     *                         property="value",
     *                         type="number",
     *                     ),
     *     ),
     *   )
     * ),
     * )
     */

     /**
     * @SWG\Response(
     *     response="ehsDaily",
     *     description="list the data of ehs of current day",
     *         @SWG\Items(
     *             @SWG\Property(
     *                 property="data",
     *                 type="array",
     *         @SWG\Items(
     *                     @SWG\Property(
     *                         property="accidents",
     *                         type="number",
     *                     ),
     *                     @SWG\Property(
     *                         property="observations",
     *                         type="number",
     *                     ),
     *                     @SWG\Property(
     *                         property="freeDays",
     *                         type="number",
     *                     ),
     *                     @SWG\Property(
     *                         property="average",
     *                         type="number",
     *                     ),
     *     ),
     *   )
     * ),
     * )
     */

          /**
     * @SWG\Response(
     *     response="qualityDaily",
     *     description="list the data of quality of current day",
     *         @SWG\Items(
     *             @SWG\Property(
     *                 property="data",
     *                 type="array",
     *         @SWG\Items(
     *                     @SWG\Property(
     *                         property="quality-not-run",
     *                         type="number",
     *                     ),
     *                     @SWG\Property(
     *                         property="quality-lab-error",
     *                         type="number",
     *                     ),
     *                     @SWG\Property(
     *                         property="quality-lims",
     *                         type="number",
     *                     ),
     *                     @SWG\Property(
     *                         property="quality-product-defects",
     *                         type="number",
     *                     ),
     *                     @SWG\Property(
     *                         property="quality-certificates",
     *                         type="number",
     *                     ),
     *                     @SWG\Property(
     *                         property="quality-others",
     *                         type="number",
     *                     ),
     *     ),
     *   )
     * ),
     * )
     */

     /**
     * @SWG\Response(
     *     response="serviceDaily",
     *     description="list the data of service of current day",
     *         @SWG\Items(
     *             @SWG\Property(
     *                 property="data",
     *                 type="array",
     *         @SWG\Items(
     *                     @SWG\Property(
     *                         property="rescheduled",
     *                         type="number",
     *                     ),
     *                     @SWG\Property(
     *                         property="routine",
     *                         type="number",
     *                     ),
     *                     @SWG\Property(
     *                         property="special",
     *                         type="number",
     *                     ),
     *                     @SWG\Property(
     *                         property="twoDays",
     *                         type="number",
     *                     ),
     *                     @SWG\Property(
     *                         property="fiveDays",
     *                         type="number",
     *                     ),
     *                     @SWG\Property(
     *                         property="tenDays",
     *                         type="number",
     *                     ),
     *     ),
     *   )
     * ),
     * )
     */


     /**
     * @SWG\Response(
     *     response="modifyValuesOfSixS",
     *     description="modify the values of Six S of current month",
     *         @SWG\Items(
     *             @SWG\Property(
     *                 property="data",
     *                 type="array",
     *         @SWG\Items(
     *                     @SWG\Property(
     *                         property="accidents",
     *                         type="number",
     *                     ),
     *                     @SWG\Property(

     *                         property="observations",
     *                         type="number",
     *                     ),
     *                     @SWG\Property(
     *                         property="freeDays",
     *                         type="number",
     *                     ),
     *                     @SWG\Property(
     *                         property="average",
     *                         property="safety",
     *                         type="number",
     *                     ),
     *                     @SWG\Property(
     *                         property="order",
     *                         type="number",
     *                     ),
     *                     @SWG\Property(
     *                         property="documents",
     *                         type="number",
     *                     ),
     *                     @SWG\Property(
     *                         property="materials",
     *                         type="number",
     *                     ),
     *                     @SWG\Property(
     *                         property="clothing",
     *                         type="number",
     *                     ),
     *                     @SWG\Property(
     *                         property="security",
     *                         type="number",
     *                     ),
     *                     @SWG\Property(
     *                         property="discipline",
     *                         type="number",
     *                     ),
     *     ),
     *   )
     * ),
     * )
     */



     /**
     * @SWG\Response(
     *     response="listSixsMonth",
     *     description="list the data of sixS of current month",
     *         @SWG\Items(
     *             @SWG\Property(
     *                 property="data",
     *                 type="array",
     *         @SWG\Items(
     *                     @SWG\Property(
     *                         property="date",
     *                         type="string",
     *                     ),
     *                     @SWG\Property(
     *                         property="safety",
     *                         type="number",
     *                     ),
     *                     @SWG\Property(
     *                         property="order",
     *                         type="number",
     *                     ),
     *                     @SWG\Property(
     *                         property="documents",
     *                         type="number",
     *                     ),
     *                     @SWG\Property(
     *                         property="materials",
     *                         type="number",
     *                     ),
     *                     @SWG\Property(
     *                         property="clothing",
     *                         type="number",
     *                     ),
     *                     @SWG\Property(
     *                         property="security",
     *                         type="number",
     *                     ),
     *                     @SWG\Property(
     *                         property="discipline",
     *                         type="number",
     *                     ),
     *     ),
     *   )
     * ),
     * )
     */

}

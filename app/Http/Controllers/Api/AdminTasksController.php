<?php

namespace App\Http\Controllers\Api;

use App\Formatters\AdminTaskFormatter;
use App\Helpers\AdminTasksUtility;
use App\Helpers\CommanUtility;
use App\Http\Controllers\Controller;
use App\Models\AdminTask;
use Carbon\Carbon;
use Illuminate\Http\Request;
use Validator;

class AdminTasksController extends Controller
{
    use CommanUtility;
    use AdminTasksUtility;

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */

    /**
     * List Admin Tasks
     *
     * @return \Illuminate\Http\Response
     *
     * @SWG\Get(
     *     path="/api/v2/adminTasks",
     *     description="List all the admin tasks",
     *     operationId="admin.task.list",
     *     produces={"application/json"},
     *     tags={"Admin Tasks"},
     *     security={
     *     {"passport": {}},
     *   },
     *     @SWG\Response(response=200, ref="#/responses/adminTasks")
     * )
     */
    public function index()
    {
        //
        $tasks = AdminTask::all();
        return json_response()->success($tasks);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */

    /**
     * Update Admin Task
     *
     * @return \Illuminate\Http\JsonResponse
     *
     * @SWG\Put(
     *     path="/api/v2/adminTasks/{id}",
     *     description="Updating an Admin Task ",
     *     operationId="admin.task.update",
     *     produces={"application/json"},
     *     @SWG\Parameter(
     *         name="id",
     *         in="path",
     *         required=true,
     *         type="string",
     *         description = "task id "
     *     ),
     *         @SWG\Parameter(
     *         name="Task",
     *         description="Admin Task",
     *         in="body",
     *         required=false,
     *         type="object",
     *         @SWG\Schema(
     *             type="object",
     *             required={"date","duration","category","description"},
     *             @SWG\Property(
     *                 property="date",
     *                 type="string",
     *
     *             ),
     *             @SWG\Property(
     *                 property="duration",
     *                 type="number",
     *             ),
     *             @SWG\Property(
     *                 property="category",
     *                 type="string",
     *             ),
     *             @SWG\Property(
     *                 property="description",
     *                 type="string",
     *             ),
     *         )
     *     ),
     *     security={
     *     {"passport": {}},
     *   },
     *     tags={"Admin Tasks"},
     *     @SWG\Response(response=200, description="Task Updated Successfully",ref="#/responses/showAdminTask")
     *  )
     *  )
     */

    public function update(Request $request)
    {
        $validator = Validator::make($request->all(),
            [
                'date' => 'required|date',
                // 'duration' => 'required|numeric',
                'categories' => 'required|array',
                // 'category' => 'required|in:Meeting,AdminTask,Training,NonTestTask',
                //'description' => 'required',
            ]
        );

        if ($validator->fails()) {
            return json_response()->validationErrors([
                'fields' => $validator->errors(),
            ]);
        }

        $valid_categories = [
            'Meeting' => 0, 'AdminTask' => 0, 'Training' => 0, 'NonTestTask' => 0,
        ];
        $categories = $request->get('categories');

        if (!empty(array_diff_key($valid_categories, $categories))) {
            return json_response()->validationErrors([
                'fields' => [
                    'categories' => [
                        "Categories field must contain 'Meeting', 'AdminTask', 'Training', 'NonTestTask'",
                    ],
                ],
            ]);
        }

        $date = $request->get('date');

        foreach ($categories as $category => $value) {
            $task = AdminTask::updateOrCreate(
                ['date' => $date, 'category' => $category],
                ['duration' => (double) $value]
            );
        }

        $doneHours = $this->getDoneHoursByDueDateDashboard([$date, $date]);
        $adminCap = $this->getAdminTasks(['firstOfWeek' => $date, 'lastOfWeek' => $date]);
        $adminCap = isset($adminCap[$date]) ? AdminTaskFormatter::formatAdminTasks($adminCap[$date]) : AdminTaskFormatter::getEmptyAdminTasks();
        $nonTestTasksHours = $adminCap['NonTestTask'];
        $meetingHours = $adminCap['Meeting'];
        $trainingHours = $adminCap['Training'];
        $adminTasksHours = $adminCap['AdminTask'];
        $capacity = $this->getCapacity([$date, $date]);

        $adminHours = $nonTestTasksHours + $meetingHours + $trainingHours + $adminTasksHours;
        $actual_capacity = isset($capacity[$date]) ? $capacity[$date] : 32;

        $response['capacity'] = $actual_capacity - $adminHours;
        $response['actual_capacity'] = $actual_capacity;

        if ($actual_capacity == 0) {
            $response['productivity'] = null;
        } elseif (Carbon::parse($date)->gt(Carbon::now())) {
            $response['productivity'] = 0;
        } else {
            $response['productivity'] = round((($doneHours + ($nonTestTasksHours + $meetingHours + $trainingHours + ($adminTasksHours * 0.8))) / ($response['actual_capacity'])) * 100);
        }

        $week = (new Carbon($date))->weekOfYear;

        event(new \App\Events\ProductivityUpdateEvent(null, $week));

        return json_response()->success($response);
    }
}

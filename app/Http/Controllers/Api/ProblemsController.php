<?php

namespace App\Http\Controllers\Api;

use App\Http\Controllers\Controller;
use App\Models\Problem;
use DB;
use Illuminate\Http\Request;
use Illuminate\Pagination\LengthAwarePaginator;
use Validator;

class ProblemsController extends Controller
{
    /**
     * Filter Problems
     *
     * @return \Illuminate\Http\Response
     *
     * @SWG\Get(
     *     path="/api/v2/problems",
     *     description="List All problem with filering and sorting ",
     *     operationId="problems.list",
     *     produces={"application/json"},
     *     @SWG\Parameter(
     *         name="cat_id",
     *         in="query",
     *         required=false,
     *         type="string"
     *     ),
     *     @SWG\Parameter(
     *         name="sort",
     *         in="query",
     *         required=false,
     *         type="string"
     *     ),
     *     @SWG\Parameter(
     *         name="order",
     *         in="query",
     *         required=false,
     *         type="string"
     *     ),
     *     tags={"Problems"},
     *     security={
     *     {"passport": {}},
     *   },
     *     @SWG\Response(response=200, ref="#/responses/problems")
     * )
     */

    public function index(Request $request)
    {
        if ($request->cat_id) {
            $categories_ids = get_problem_categories($request->cat_id);
        } else {
            $categories_ids = [];
        }
        $sortBy = $request->sort;
        $sortOrder = $request->order;

        // $pageSize = 2;
        $pageSize = config('custom.problems.page');

        if (in_array('all', $categories_ids)) {
            $categories_ids = [
                1, 2, 3, 4, 5
            ];
        }
        // dd($categories_ids);
        $problems_1 = Problem::FilterProblems($categories_ids)->SortProblems($sortBy, $sortOrder)
            ->whereIn('status_id', [1, 2, 3, 4])
            ->get();
        $problems_2 = Problem::FilterProblems($categories_ids)->SortProblems($sortBy, $sortOrder)
            ->whereIn('status_id', [5])
            ->get();

        $problems = $problems_1->concat($problems_2);
        // dd($problems);
        $page = $request->get('page', 1);
        $pagination_offset = $page * $pageSize - $pageSize;
        $pagedData = $problems->slice($pagination_offset, $pageSize)->all();
        $pagedData = array_values($pagedData);
        $problems = (new LengthAwarePaginator($pagedData, $problems->count(), $pageSize, $page))->toArray();
        $meta = DB::table('problems')->rightJoin('problem_statuses', 'problem_statuses.id', '=', 'problems.status_id')
            ->select(['problem_statuses.id'])
            ->selectRaw('COUNT(*) as count')
            ->whereIn('problems.category_id', $categories_ids)
            ->groupBy('problem_statuses.id')->pluck('count', 'id')->toArray();

        $problems['statuses_global_count'][1] = isset($meta[1]) ? $meta[1] : 0;
        $problems['statuses_global_count'][2] = isset($meta[2]) ? $meta[2] : 0;
        $problems['statuses_global_count'][3] = isset($meta[3]) ? $meta[3] : 0;
        $problems['statuses_global_count'][4] = isset($meta[4]) ? $meta[4] : 0;
        $problems['statuses_global_count'][5] = isset($meta[5]) ? $meta[5] : 0;

        return json_response()->paginate($problems);
    }

    /**
     * Create Problem
     *
     * @return \Illuminate\Http\JsonResponse
     *
     * @SWG\Post(
     *     path="/api/v2/problem",
     *     description="Creating a new problem",
     *     operationId="problem.create",
     *     produces={"application/json"},
     *         @SWG\Parameter(
     *         name="Problem",
     *         description="Problem data",
     *         in="body",
     *         required=false,
     *         type="object",
     *         @SWG\Schema(
     *             type="object",
     *             required={"category_id","status_id","description","solution","responsible","due_date"},
     *             @SWG\Property(
     *                 property="category_id",
     *                 type="number",
     *             ),
     *             @SWG\Property(
     *                 property="status_id",
     *                 type="number",
     *             ),
     *             @SWG\Property(
     *                 property="description",
     *                 type="string",
     *             ),
     *             @SWG\Property(
     *                 property="solution",
     *                 type="string",
     *             ),
     *             @SWG\Property(
     *                 property="responsible",
     *                 type="string",
     *             ),
     *             @SWG\Property(
     *                 property="due_date",
     *                 type="string",
     *             ),
     *         )
     *     ),
     *     security={
     *     {"passport": {}},
     *   },
     *     tags={"Problems"},
     *     @SWG\Response(response=200, description="Problem Created Successfully",ref="#/responses/createUpdateProblem")
     *  )
     *  )
     */

    public function store(Request $request)
    {
        $validator = Validator::make($request->all(), [

            'category_id' => 'required|exists:problem_categories,id',
            'status_id' => 'required|exists:problem_statuses,id',
            'description' => 'required',
            'solution' => 'required',
            'responsible' => 'required',
            'due_date' => 'required|date',

        ]
        );

        if ($validator->fails()) {

            return json_response()->validationErrors([
                'fields' => $validator->errors(),
            ]);

        }

        $problem = Problem::create($request->all());

        return json_response()->success($problem);
    }

    /**
     * Update Problem
     *
     * @return \Illuminate\Http\JsonResponse
     *
     * @SWG\PUT(
     *     path="/api/v2/problem/{id}",
     *     description="Updating a problem",
     *     operationId="problem.update",
     *     produces={"application/json"},
     *     @SWG\Parameter(
     *         name="id",
     *         in="path",
     *         required=true,
     *         type="string"
     *     ),
     *         @SWG\Parameter(
     *         name="Problem",
     *         description="Problem data",
     *         in="body",
     *         required=false,
     *         type="object",
     *         @SWG\Schema(
     *             type="object",
     *             required={"category_id","status_id","description","solution","responsible","due_date"},
     *             @SWG\Property(
     *                 property="category_id",
     *                 type="number",
     *             ),
     *             @SWG\Property(
     *                 property="status_id",
     *                 type="number",
     *             ),
     *             @SWG\Property(
     *                 property="description",
     *                 type="string",
     *             ),
     *             @SWG\Property(
     *                 property="solution",
     *                 type="string",
     *             ),
     *             @SWG\Property(
     *                 property="responsible",
     *                 type="string",
     *             ),
     *             @SWG\Property(
     *                 property="due_date",
     *                 type="string",
     *             ),
     *         )
     *     ),
     *     security={
     *     {"passport": {}},
     *   },
     *     tags={"Problems"},
     *     @SWG\Response(response=200, description="Problem Updated Successfully",ref="#/responses/createUpdateProblem")
     *  )
     *  )
     */

    public function update(Request $request, $id)
    {
        $validator = Validator::make($request->all(), [

            'category_id' => 'required|exists:problem_categories,id',
            'status_id' => 'required|exists:problem_statuses,id',
            'description' => 'required',
            'solution' => 'required',
            'responsible' => 'required',
            'due_date' => 'required|date',

        ]
        );

        if ($validator->fails()) {

            return json_response()->validationErrors([
                'fields' => $validator->errors(),
            ]);

        }

        $problem = Problem::find($id);

        $problem->update($request->all());

        return json_response()->success($problem);
    }

    /**
     * Delete Problem .
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     *
     * @SWG\Delete(
     *     path="/api/v2/problem/{id}",
     *     description="remove problem",
     *     produces={"application/json"},
     *     operationId="problem.delete",
     *     @SWG\Parameter(
     *         name="id",
     *         in="path",
     *         required=true,
     *         type="integer",
     *         description = "problem id "
     *     ),
     *     tags={"Problems"},
     *     security={
     *     {"passport": {}},
     *   },
     *     @SWG\Response(response=200,
     *         description=" Problem Deleted Successfully ",
     *         @SWG\Items(
     *             @SWG\Property(
     *                 property="data",
     *                 type="object",
     *                 @SWG\Property(
     *                      property="messages",
     *                      type="string",
     *                      description="succes messages"
     *                 ),
     *     )  ) )
     * )
     */

    public function destroy(Problem $problem)
    {
        $problem->delete();
        return json_response()->success("Problem Deleted Successfully");
    }
}

<?php

namespace App\Http\Controllers\Api;

use App\Helpers\CommanUtility;
use App\Helpers\ehsHelper;
use App\Helpers\formatUtility;
use App\Helpers\qualityHelper;
use App\Helpers\TypeHelper;
use App\Http\Controllers\Controller;
use App\Models\TypeValue;
use Carbon\Carbon;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Validator;

class TypesController extends Controller
{
    use CommanUtility;use formatUtility;

    //

    /**
     * Modify the type value
     *
     * @return \Illuminate\Http\JsonResponse
     *
     * @SWG\Post(
     *     path="/api/v2/types/modify",
     *     description="Modify the value of type in specific date",
     *     operationId="type.value.modify",
     *     produces={"application/json"},
     *         @SWG\Parameter(
     *         name="data",
     *         description="data",
     *         in="body",
     *         required=false,
     *         type="object",
     *         @SWG\Schema(
     *             type="object",
     *             required={"increment"},
     *             @SWG\Property(
     *                 property="type",
     *                 type="string",
     *             ),
     *             @SWG\Property(
     *                 property="increment",
     *                 type="number",
     *                 description="this determine whether the value of error will increase or decrease and this value must be in [-1,1]"
     *             ),
     *             @SWG\Property(
     *                 property="date",
     *                 type="string",
     *                 description="the date in which the state need to be changed"
     *             ),
     *             @SWG\Property(
     *                 property="freeDays",
     *                 type="number",
     *                 description="free Days"
     *             ),
     *         )
     *     ),
     *     security={
     *     {"passport": {}},
     *   },
     *     tags={"EHS"},
     *     @SWG\Response(response=200, description="type Value Changed Successfully",ref="#/responses/modifyValueOfType")
     *  )
     *  )
     */

    public function modifyValue(Request $request, ehsHelper $helper)
    {

        $types = config('custom.types');

        $validator = Validator::make($request->all(),

            [
                'increment' => 'required|numeric|in:-1,1',
                'type' => 'required|in:' . implode(',', $types),
            ]
        );

        if ($validator->fails()) {

            return json_response()->validationErrors([
                'fields' => $validator->errors(),
            ]);

        }

        $increment = $request->increment;
        $type = $request->type;
        $date = $request->date;

        if ($date == null) {

            $date = date('Y-m-d');

        }

        $typeValue = TypeValue::where('date', $date)->where('type', $type)->first();

        if (!$typeValue) {
            $newTypeValue = new TypeValue;
            $increment = ($increment == -1) ? 0 : 1;
            $newTypeValue->date = $date;
            $newTypeValue->type = $type;
            $newTypeValue->value = $increment;
            $newTypeValue->save();
            $newTypeValue->freeDays = $helper->calculateFreeDays();

            event(new \App\Events\NewImportEvent);
            return json_response()->success($newTypeValue);
        }

        $oldValue = $typeValue->value;
        $newValue = $oldValue + $increment;
        $newValue = ($newValue < 0) ? 0 : $newValue;
        $typeValue->value = $newValue;
        $typeValue->save();
        $typeValue->freeDays = $helper->calculateFreeDays();

        event(new \App\Events\NewImportEvent);
        return json_response()->success($typeValue);
        // $date = Carbon::parse($date);
        // $request->request->add(['year' => $date->year, 'week' => $date->format('W')]);
        // dd($request->all());
        // return $this->listTypeValuesPerWeek($request);
    }

    /**
     * List Types Of Specific Month
     *
     * @return \Illuminate\Http\Response
     *
     * @SWG\POST(
     *     path="/api/v2/types/month",
     *     description="List all the types of specific month",
     *     operationId="list.types.month",
     *     produces={"application/json"},
     *     tags={"EHS"},
     *         @SWG\Parameter(
     *         name="data",
     *         description="data object",
     *         in="body",
     *         required=false,
     *         type="object",
     *         @SWG\Schema(
     *             type="object",
     *             @SWG\Property(
     *                 property="year",
     *                 type="string",
     *                 description="the year"
     *             ),
     *             @SWG\Property(
     *                 property="month",
     *                 type="string",
     *                 description="the month"
     *             ),
     *             @SWG\Property(
     *                 property="type",
     *                 type="string",
     *                 description="the type"
     *             ),
     * ) ) ,
     *     security={
     *     {"passport": {}},
     *   },
     *     @SWG\Response(response=200, ref="#/responses/typePerMonth")
     * )
     */

    public function listTypeValuesPerMonth(Request $request)
    {

        $types = config('custom.types');

        $validator = Validator::make($request->all(), [
            'type' => 'required|in:' . implode(',', $types),
        ]);

        if ($validator->fails()) {
            return json_response()->validationErrors([
                'fields' => $validator->errors(),
            ]);
        }

        $year = app('request')->input("year");
        $year = !is_null($year) ? $year : carbon::now()->year;

        $month = app('request')->input("month");
        $month = !is_null($month) ? $month : carbon::now()->month;

        $type = $request->type;

        $start = Carbon::createFromDate($year, $month, 1);
        $startDate = $start->format("Y-m-d");
        $endDate = $start->endOfMonth()->format("Y-m-d");

        $TypeValues = DB::table("type_values")
            ->selectRaw('day(date) num, value')
            ->whereBetween('date', [$startDate, $endDate])
            ->where('type', $type)
            ->orderBy('date')
            ->get()
            ->toArray();

        $TypeValues = json_decode(json_encode($TypeValues), true);
        $daysExists = array_column($TypeValues, "value", "num");

        foreach ($daysExists as $key => $value) {

            if (Carbon::now()->day < $key && $month >= Carbon::now()->month) {
                unset($daysExists[$key]);
            }
        }

        $daysInMonth = $start->daysInMonth;

        $list = init_types_month($daysInMonth, $daysExists);

        return json_response()->success($list);
    }

    /**
     * List Types Of Specific Week
     *
     * @return \Illuminate\Http\Response
     *
     * @SWG\POST(
     *     path="/api/v2/types/week",
     *     description="List all the types of specific week",
     *     operationId="list.types.week",
     *     produces={"application/json"},
     *     tags={"EHS"},
     *     @SWG\Parameter(
     *          name="year",
     *          in="formData",
     *          type="string"
     *     ),
     *     @SWG\Parameter(
     *          name="week",
     *          in="formData",
     *          type="string"
     *     ),
     *     @SWG\Parameter(
     *          name="type",
     *          in="formData",
     *          required=true,
     *          type="string"
     *     ),
     *     security={
     *          {"passport": {}},
     *     },
     *     @SWG\Response(response=200, ref="#/responses/typePerWeek")
     * )
     */

    public function listTypeValuesPerWeek(Request $request)
    {
        $types = config('custom.types');

        $validator = Validator::make($request->all(), [
            'type' => 'required|in:' . implode(',', $types),
        ]);

        if ($validator->fails()) {
            return json_response()->validationErrors([
                'fields' => $validator->errors(),
            ]);
        }

        $now = carbon::now();
        $year = $request->get("year", $now->year);
        $week = $request->get("week", $now->weekOfYear);
        $type = $request->get("type");

        // $data['year'] = $year;
        // $data['week'] = $week;

        // $type = "ehs-accidents";
        $data['data'] = TypeHelper::getTypeValuesPerWeek($type, $year, $week);
        $data['swiper_steps'] = TypeHelper::getTypePerWeekSteps($type, $year, $week);
        // $type = "ehs-observations";
        // $data['observations'] = TypeHelper::getTypeValuesPerWeek($type, $year, $week);

        return json_response()->success($data);
    }

    /**
     * List data Of EHS Daily
     *
     * @return \Illuminate\Http\Response
     *
     * @SWG\GET(
     *     path="/api/v2/ehs/daily",
     *     description="List data Of EHS Daily",
     *     operationId="list.ehs.daily",
     *     produces={"application/json"},
     *     tags={"EHS"},
     *     security={
     *     {"passport": {}},
     *   },
     *     @SWG\Response(response=200, ref="#/responses/ehsDaily")
     * )
     */

    public function getEhsData(ehsHelper $helper)
    {
        $now = Carbon::now();
        $date = $now->format('Y-m-d');
        $accidents = $helper->getTypeValueToday($date, 'ehs-accidents');
        $observations = $helper->getTypeValueToday($date, 'ehs-observations');
        $freeDays = $helper->calculateFreeDays();
        $average = $helper->currentMonthSixs();

        $data = collect([

            'accidents' => $accidents,
            'observations' => $observations,
            'freeDays' => $freeDays,
            'average' => $average,

        ]);

        return json_response()->success($data);
    }

    /**
     * List data Of Quality Daily
     *
     * @return \Illuminate\Http\Response
     *
     * @SWG\GET(
     *     path="/api/v2/quality/daily",
     *     description="List data Of Quality Daily",
     *     operationId="list.quality.daily",
     *     produces={"application/json"},
     *     tags={"Quality"},
     *     security={
     *     {"passport": {}},
     *   },
     *     @SWG\Response(response=200, ref="#/responses/qualityDaily")
     * )
     */

    public function getQualityDataDaily(Request $request, qualityHelper $helper)
    {
        $now = Carbon::now();
        $date = $now->format('Y-m-d');
        $notRun = $helper->getTypeValueToday($date, 'quality-not-run');
        $labError = $helper->getTypeValueToday($date, 'quality-lab-error');
        $Lims = $helper->getTypeValueToday($date, 'quality-lims');
        $defects = $helper->getTypeValueToday($date, 'quality-product-defects');
        $certificates = $helper->getTypeValueToday($date, 'quality-certificates');
        $others = $helper->getTypeValueToday($date, 'quality-others');

        $data = collect([

            'quality-not-run' => $notRun,
            'quality-lab-error' => $labError,
            'quality-lims' => $Lims,
            'quality-product-defects' => $defects,
            'quality-certificates' => $certificates,
            'quality-others' => $others,

        ]);

        return json_response()->success($data);
    }
}

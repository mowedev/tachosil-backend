<?php

namespace App\Http\Controllers\Api;

use App\Helpers\ConfigurationHelper;
use App\Helpers\ImportHelper;
use App\Http\Controllers\Controller;
use App\Models\Import;
use App\Models\Holiday;

class LogsController extends Controller
{
    //

    public function index(ConfigurationHelper $configurationsHelper)
    {
        $days = config('custom.logs_days');
        $startDate = \Carbon\Carbon::now()->subDays($days)->format("Y-m-d H:i:s");
        $endDate = \Carbon\Carbon::now()->format("Y-m-d H:i:s");
        $logs = Import::whereBetween('imported_time', [$startDate, $endDate])->get()->makeHidden(['status', 'id'])->toArray();
        $configurations = $configurationsHelper->getBoardConfigurations();
        $boardName = isset($configurations['general']['board_title']) ? $configurations['general']['board_title'] : "";
        $ftpPath = isset($configurations['general']['board_ftp']) ? $configurations['general']['board_ftp'] : ""; 

        foreach ($logs as $key => &$value) {

            $value['boardName']  = $boardName;
            $value['ftpUrl'] = $ftpPath;
        }
        return $logs;
    }

    public function getLogsByIds($ids)
    {
        if ($ids == []) {

            $logs = [];

        } else {

            $configurationsHelper = new ConfigurationHelper();
            $configurations = $configurationsHelper->getBoardConfigurations();
            $logs = Import::whereIn('id', $ids)->get()->makeHidden(['status'])->toArray();
            $boardName = isset($configurations['general']['board_title']) ? $configurations['general']['board_title'] : "";
            $ftpPath = isset($configurations['general']['board_ftp']) ? $configurations['general']['board_ftp'] : ""; 

            foreach ($logs as $key => &$value) {

                $value['boardName']  = $boardName;
                $value['ftpUrl'] = $ftpPath;

            }
        }

        return $logs;
    }

    public function getBoardDetails(ImportHelper $importHelper, ConfigurationHelper $configurationsHelper)
    {
        $configurations = $configurationsHelper->getBoardConfigurations();
        $details = [];

        $lastImport = Import::OrderBy('id', 'desc')->first();
        $ftpPath = isset($configurations['general']['board_ftp']) ? $configurations['general']['board_ftp'] : "";
        $boardName = isset($configurations['general']['board_title']) ? $configurations['general']['board_title'] : "";
        $boardType = isset($configurations['general']['board_type']) ? $configurations['general']['board_type'] : "";
        $boardUrl = isset($configurations['general']['board_url']) ? $configurations['general']['board_url'] : "";
        $importUrl = isset($configurations['general']['board_import_url']) ? $configurations['general']['board_import_url'] : "";
        $meetingTime = isset($configurations['general']['meeting_time']) ? $configurations['general']['meeting_time'] : "";

        $details['boardName'] = $boardName;
        $details['type'] = $boardType;
        $details['ftpUrl'] = $ftpPath;
        $details['url'] = $boardUrl;
        $details['importUrl'] = $importUrl;
        $details['meetingTime'] = $meetingTime;
        $details['systemImport'] = $lastImport['is_imported_by_system'];
        $details['importTime'] = $lastImport['imported_time'];
        $details['freeDays'] = ($lastImport['imported'] == 1) ? $importHelper->getNumberOfFreeDays() : $lastImport['message'];
        $details['user_id'] = ($lastImport['user_id']) ? $lastImport['user_id'] : null;

        return $details;
    }

    public function getLatestUnsolvedImport()
    {
        $latestImport = Import::orderBy('id', 'desc')->first();

        if ($latestImport) {

            $status = $latestImport->status;
            $isHidden = $latestImport->is_hidden;

            if ($status == 1 || $isHidden == 1) {
                return null;
            }

            $configurations = $configurationsHelper->getBoardConfigurations();
            $configurations = json_decode($configurations, true);
            $boardUrl = isset($configurations['general']['board_url']) ? $configurations['general']['board_url'] : "";

            $latestImport['url'] = $boardUrl;
            return $latestImport;

        } else {

            return null;
        }
    }

    public function hasUnsolvedImport()
    {
        $latestImport = Import::orderBy('id', 'desc')->first();

        if ($latestImport) {

            $status = $latestImport->status;

            if ($status == 1) {
                return 0;
            } else {
                return 1;
            }

        } else {
            return 0;
        }
    }

    public function getUploadsHistory()
    {
        $logs = Import::OrderBy('id', 'desc')->take(10)->get()->makeHidden(['status'])->toArray();
        return $logs;
    }

    public function removeUnsolvedImport($id)
    {
        $file = Import::find($id);
        //    dd(request()->all());
        if ($file) {
            $name = $file->name;
            Import::where('name', $name)->update(['is_hidden' => 1]);
        }
    }

    public function getHolidays()
    {
        $holidays = Holiday::all()->pluck('date');
        return $holidays;

    }
}

<?php

namespace App\Http\Controllers\Api;

use App\Helpers\ehsHelper;
use App\Http\Controllers\Controller;
use App\Models\SixS;
use Carbon\Carbon;
use Illuminate\Http\Request;
use Validator;

class SixSController extends Controller
{
    /**
     * Modify the sixS Values
     *
     * @return \Illuminate\Http\JsonResponse
     *
     * @SWG\Post(
     *     path="/api/v2/sixs/modify",
     *     description="Modify the values of six S of the current month",
     *     operationId="sixS.values.modify",
     *     produces={"application/json"},
     *          @SWG\Parameter(
     *          name="data",
     *          description="data",
     *          in="body",
     *          required=false,
     *          type="object",
     *          @SWG\Schema(
     *               type="object",
     *               @SWG\Property(
     *                    property="safety",
     *                    type="number",
     *               ),
     *               @SWG\Property(
     *                    property="order",
     *                    type="number",
     *               ),
     *               @SWG\Property(
     *                    property="documents",
     *                    type="number",
     *               ),
     *               @SWG\Property(
     *                    property="materials",
     *                    type="number",
     *               ),
     *               @SWG\Property(
     *                    property="clothing",
     *                    type="number",
     *               ),
     *               @SWG\Property(
     *                    property="security",
     *                    type="number",
     *               ),
     *               @SWG\Property(
     *                    property="discipline",
     *                    type="number",
     *               ),
     *          )
     *     ),
     *     security={
     *          {"passport": {}},
     *     },
     *     tags={"EHS"},
     *     @SWG\Response(response=200, description="6S Values Changed Successfully",ref="#/responses/modifyValuesOfSixS")
     * )
     */

    public function modifyValues(Request $request, ehsHelper $helper)
    {
        $validator = Validator::make($request->all(),
            [
                'safety' => 'numeric',
                'order' => 'numeric',
                'documents' => 'numeric',
                'materials' => 'numeric',
                'clothing' => 'numeric',
                'security' => 'numeric',
                'discipline' => 'numeric',
            ]
        );

        if ($validator->fails()) {
            return json_response()->validationErrors([
                'fields' => $validator->errors(),
            ]);
        }

        $date = new Carbon('first day of this month');

        $date = $date->format('Y-m-d');
        $input = $request->all();
        $input['date'] = $date;

        $sixS = SixS::where('date', $date)->first();

        if (!$sixS) {
            $sixS = SixS::create($input);
            event(new \App\Events\NewImportEvent);
            return json_response()->success($sixS);
        }

        $sixS->update($input);
        // $newSixS = SixS::where('date', $date)->first();
        event(new \App\Events\NewImportEvent);
        return json_response()->success($sixS);
    }

    /**
     * List Average for every month in the year
     *
     * @return \Illuminate\Http\Response
     *
     * (
     *     path="/api/v2/sixs/year",
     *     description="List Average for every month in the year",
     *     operationId="sixs.year.average",
     *     produces={"application/json"},
     *     Parameter(
     *          name="year",
     *          in="query",
     *          required=false,
     *          type="string"
     *     ),
     *     tags={"EHS"},
     *     security={
     *          {"passport": {}},
     *     },
     *     Response(response=200, ref="#/responses/sixsYearAverage")
     * )
     */

    // public function listValuesPerYear(ehsHelper $helper)
    // {
    //     $monthNames = get_month_names();
    //     $allSixsWithMonths = $helper->listSixsWithMonths();

    //     for ($i = 0; $i < 12; $i++) {
    //         if (isset(($allSixsWithMonths[$i + 1]))) {
    //             $avg = $helper->calculateAverage($allSixsWithMonths[$i + 1]);
    //         } else {
    //             if ($i + 1 > Carbon::now()->month) {
    //                 $avg = 0;
    //             } else {
    //                 $sixs_data = $helper->getLatestSixS($i + 1);
    //                 $avg = $helper->calculateAverage($sixs_data);
    //             }
    //         }

    //         $all[$i]['month'] = $i + 1;
    //         $all[$i]['name'] = $monthNames[$i + 1];
    //         $all[$i]['average'] = $avg;
    //         $all[$i]['target'] = config('custom.sixs_monthly_target');
    //     }

    //     return json_response()->success($all);
    // }

    /**
     * List average per quarter
     *
     * @return \Illuminate\Http\Response
     *
     * @SWG\Get(
     *     path="/api/v2/sixs/quarters/{year1}/{qu1}/{year2}/{qu2}",
     *     description="List average per quarter",
     *     operationId="sixs.quarter.average",
     *     produces={"application/json"},
     *     @SWG\Parameter(
     *          name="year1",
     *          in="path",
     *          required=true,
     *          type="string"
     *     ),
     *     @SWG\Parameter(
     *          name="qu1",
     *          in="path",
     *          required=true,
     *          type="string"
     *     ),
     *     @SWG\Parameter(
     *          name="year2",
     *          in="path",
     *          required=true,
     *          type="string"
     *     ),
     *     @SWG\Parameter(
     *          name="qu2",
     *          in="path",
     *          required=true,
     *          type="string"
     *     ),
     *     tags={"EHS"},
     *     security={
     *          {"passport": {}},
     *     },
     *     @SWG\Response(response=200, ref="#/responses/sixsYearAverage")
     * )
     */

    public function listPerQuarter($year1, $qu1, $year2, $qu2)
    {
        $data = ehsHelper::listSixsPerQuarters($year1, $qu1, $year2, $qu2);

        return json_response()->success($data);
    }

    /**
     * List Values of 6S current month
     *
     * @return \Illuminate\Http\Response
     *
     * @SWG\Get(
     *     path="/api/v2/sixs/month",
     *     description="Values of 6S current month ",
     *     operationId="list.sixs.month",
     *     produces={"application/json"},
     *     @SWG\Parameter(
     *          name="year",
     *          in="query",
     *          required=false,
     *          type="string"
     *     ),
     *     @SWG\Parameter(
     *          name="month",
     *          in="query",
     *          required=false,
     *          type="string"
     *     ),
     *     tags={"EHS"},
     *     security={
     *          {"passport": {}},
     *     },
     *     @SWG\Response(response=200, ref="#/responses/listSixsMonth")
     * )
     */

    public function listValuesOfCurrentMonth(ehsHelper $helper)
    {
        $allSixsWithMonths = $helper->listSixsWithMonths();

        $monthNumber = app('request')->input("month");
        $monthNumber = !is_null($monthNumber) ? $monthNumber : carbon::now()->month;

        if (isset($allSixsWithMonths[$monthNumber])) {
            $data = $allSixsWithMonths[$monthNumber];
            $data['average'] = $helper->calculateAverage($data);
        } else {
            $data = $helper->getLatestSixS($monthNumber);
            $data['average'] = $helper->calculateAverage($data);
        }

        // foreach ($data as $key => &$value) {
        //     $value = round($value);
        // }

        return json_response()->success($data);
    }
}

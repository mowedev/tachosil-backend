<?php

namespace App\Http\Controllers\Api;

use App\Http\Controllers\Controller;
use App\Models\Task;
use Carbon\Carbon;
use DB;
use Illuminate\Http\Request;

class ProductivityController extends Controller
{
    public function forecastHours(Request $request)
    {
        $now = Carbon::now();
        $week_start_date = $now->startOfWeek()->toDateString();
        $after_12_weeks = $now->addWeeks(12)->endOfWeek()->toDateString();

        /**
         * Active backlogs duration sum
         */
        $backlog_duration_sum = DB::table('tasks')->select('tasks.sample_number')->join('task_version', 'task_version.sample_number', '=', 'tasks.sample_number')
            ->whereNull('tasks.date_reviewed')
            ->where('task_version.state', 1)
            ->where('task_version.due_date', '<', $week_start_date)
            ->sum('duration');

        /**
         * To Do active tasks duration sum
         */
        $inProgress = DB::table("subtasks")->distinct()->whereNotNull("receive_date")->pluck("sample_number")->toArray();
        $planned_duration_sum = DB::table('tasks')->select(['tasks.sample_number', 'tasks.duration'])->join('task_version', 'task_version.sample_number', '=', 'tasks.sample_number')
            ->whereBetween('task_version.due_date', [$week_start_date, $after_12_weeks])
            ->whereNull('tasks.date_reviewed')
            ->where('task_version.state', 1)
            ->whereNotIn('tasks.sample_number', $inProgress)
            ->sum('duration');

        return json_response()->success([
            'backlog' => round($backlog_duration_sum),
            'forecast' => round($planned_duration_sum),
            'total' => round($backlog_duration_sum) + round($planned_duration_sum),
        ]);
    }
}

<?php

namespace App\Http\Controllers\Api;

use App\Helpers\ListHelper;
use App\Http\Controllers\Controller;
use App\Models\Duration;
use Carbon\Carbon;
use Excel;
use Illuminate\Http\Request;
use Validator;

class DurationController extends Controller
{
    public function index(Request $request)
    {
        $search_token = $request->get('search');

        $data = Duration::orderBy('product', 'ASC');
        if ($search_token) {
            $data->where('product', 'LIKE', '%' . $search_token . '%')
                ->orWhere('project', 'LIKE', '%' . $search_token . '%');
        }
        $data = $data->paginate(30)->toArray();

        foreach ($data['data'] as $key => $dur) {
            if (!$dur['product']) {
                $data['data'][$key]['product'] = $dur['project'];
            }
        }

        return json_response()->paginate($data);
        // $sub_query = DB::table('durations')->selectRaw('*, REPLACE(`product`, "-", "") as product_numeric')->toSql();
        // $sub_query = "($sub_query) as filtered_durations";
        // // dd($sub_query);
        // $query = DB::table(DB::raw($sub_query))
        // ->where('product_numeric', '<>', '')->whereNotNull('product_numeric');
        // if ($search_token) {
        //     $query->where('product_numeric', 'LIKE', '%' . $search_token . '%');
        // }
        // $data = $query->paginate(30)->toArray();
    }

    public function export()
    {
        $today = Carbon::today();
        Excel::create('durations.' . $today->toDateString(), function ($excel) {
            $excel->sheet('sheet 1', function ($sheet) {

                $headers = ['PROJECT', 'PRODUCT', 'LOGIN_DATE', 'DESCRIPTION', 'LOCATION', 'ANALYSIS', 'NAME', 'Aufwand (min)', 'Anzahl der Tage'];

                $durations = Duration::orderBy('product', 'ASC')->get()->toArray();
                $durations = array_map(function ($item) {
                    return [
                        optional($item['project']), $item['product'], $item['login_date'], $item['description'], $item['location'], $item['analysis'], $item['name'], round($item['total_duration'] * 60) /**Minutes */, $item['days'],
                    ];
                }, $durations);

                $data = array_merge([$headers], $durations);
                $sheet->fromArray($data, null, 'A1', false, false); // https://docs.laravel-excel.com/2.1/export/array.html
            });
        })->download('xlsx');
    }

    public function update(Request $request, ListHelper $list_helper, Duration $duration)
    {
        $input = $request->all();

        $rules = [
            'duration' => 'required|numeric|min:0',
            'days' => 'numeric|min:1',
        ];

        $validator = Validator::make($input, $rules);
        if ($validator->fails()) {
            return json_response()->error($validator->errors());
        }

        $product = $duration->product;
        $analysis = $duration->analysis;

        $days = $request->get('days', $duration->days);
        $total_duration = $request->get('duration', $duration->total_duration);
        $duration = $days > 0 ? ($total_duration / $days) : $total_duration;

        $list_helper->overrideDuration($product, $analysis, $duration, $total_duration, $days);

        $week = Carbon::now()->weekOfYear;
        event(new \App\Events\ProductivityUpdateEvent(null, $week));

        return json_response()->success("Duration updated successfully.");
    }
}

<?php

namespace App\Http\Controllers\Api;

use App\Helpers\CommanUtility;
use App\Helpers\ConfigurationHelper;
use App\Helpers\ehsHelper;
use App\Helpers\qualityHelper;
use App\Helpers\serviceHelper;
use App\Helpers\TaskHelper;
use App\Http\Controllers\Controller;
use Carbon\Carbon;
use Illuminate\Http\Request;

class CharsController extends Controller
{
    use CommanUtility;
    /**
     * Completed Tasks Percent
     *
     * @return \Illuminate\Http\Response
     *
     * @SWG\Get(
     *     path="/api/v2/guage/analytic",
     *     description="Get completed task in current day",
     *     operationId="chart.guage",
     *     produces={"application/json"},
     *     tags={"Charts"},
     *     security={
     *     {"passport": {}},
     *   },
     *     @SWG\Response(response=200, ref="#/responses/guageDay")
     * )
     */
    public function getGuageAnalytic(TaskHelper $helper)
    {
        $analytic["value"] = $helper->getCompletedTasksPercentInCurrentDay();

        return json_response()->success($analytic);
    }

    public function getGuageSap()
    {
        $analytic = ["value" => 75];

        return json_response()->success($analytic);
    }

    /**
     * Completed Tasks Percent for 1 year
     *
     * @return \Illuminate\Http\Response
     *
     * @SWG\Get(
     *     path="/api/v2/Schedule/Adherence",
     *     description="Get completed tasks in year",
     *     operationId="chart.year.percent",
     *     produces={"application/json"},
     *     @SWG\Parameter(
     *         name="year",
     *         in="query",
     *         required=false,
     *         type="string"
     *     ),
     *     tags={"Charts"},
     *     security={
     *     {"passport": {}},
     *   },
     *     @SWG\Response(response=200, ref="#/responses/scheduleAdherenece")
     * )
     */

    public function getScheduleAdherence(TaskHelper $helper)
    {
        $targets = $helper->getCompletedTasksPercent();

        return json_response()->success($targets);
    }

    /**
     * Completed Tasks Percent for weeks
     *
     * @return \Illuminate\Http\Response
     *
     * @SWG\Get(
     *     path="/api/v2/Schedule/Adherence/week",
     *     description="Get completed tasks for the past 12 week",
     *     operationId="chart.weeks.percent",
     *     produces={"application/json"},
     *     @SWG\Parameter(
     *         name="year",
     *         in="query",
     *         required=false,
     *         type="string"
     *     ),
     *     @SWG\Parameter(
     *         name="quarter",
     *         in="query",
     *         required=false,
     *         type="string"
     *     ),
     *     tags={"Charts"},
     *     security={
     *     {"passport": {}},
     *   },
     *     @SWG\Response(response=200, ref="#/responses/scheduleAdhereneceWeeks")
     * )
     */
    public function getScheduleAdherenceInWeeks(TaskHelper $helper)
    {
        $targets = $helper->getCompletedTasksPercentInWeeks();

        return json_response()->success($targets);
    }

    /**
     * Demand Rate Chart
     *
     * @return \Illuminate\Http\Response
     *
     * @SWG\Get(
     *     path="/api/v2/demand/rate",
     *     description="Demand Rate Chart",
     *     operationId="chart.demand.rate",
     *     produces={"application/json"},
     *     tags={"Charts"},
     *     security={
     *     {"passport": {}},
     *   },
     *     @SWG\Parameter(
     *         name="year",
     *         in="query",
     *         required=false,
     *         type="string"
     *     ),
     *     @SWG\Parameter(
     *         name="quarter",
     *         in="query",
     *         required=false,
     *         type="string"
     *     ),
     *     @SWG\Response(response=200, ref="#/responses/demandRate")
     * )
     */
    public function getDemandRate(TaskHelper $helper)
    {
        return $helper->demandRate();

        // $demands = [];

        // return json_response()->success($demands);
    }

    public function getDemandRatePerWeek(TaskHelper $helper)
    {
        return $helper->demandRatePerWeek();
    }
    public function getDemandRatePerMonth(TaskHelper $helper)
    {
        return $helper->demandRatePerMonth();
    }

    /**
     * Demand Rate Chart
     *
     * @return \Illuminate\Http\Response
     *
     * @SWG\Get(
     *     path="/api/v2/demand/rate/year",
     *     description="Demand Rate Chart with year only",
     *     operationId="chart.demand.rate.year",
     *     produces={"application/json"},
     *     tags={"Charts"},
     *     security={
     *          {"passport": {}},
     *     },
     *     @SWG\Parameter(
     *         name="year",
     *         in="query",
     *         required=false,
     *         type="string"
     *     ),
     *     @SWG\Response(response=200, ref="#/responses/demandRate")
     * )
     */
    public function getDemandRatePerYear(TaskHelper $helper)
    {
        return $helper->demandRatePerYear();
    }

    /**
     * Productivity Chart
     *
     * @return \Illuminate\Http\Response
     *
     * @SWG\Get(
     *     path="/api/v2/productivity",
     *     description="Get Productivity Chart",
     *     operationId="chart.Productivity",
     *     produces={"application/json"},
     *     @SWG\Parameter(
     *         name="year",
     *         in="query",
     *         required=false,
     *         type="string"
     *     ),
     *     @SWG\Parameter(
     *         name="quarter",
     *         in="query",
     *         required=false,
     *         type="string"
     *     ),
     *     tags={"Charts"},
     *     security={
     *     {"passport": {}},
     *   },
     *     @SWG\Response(response=200, ref="#/responses/productivity")
     * )
     */
    public function getProductivityChart(TaskHelper $helper)
    {

        return $helper->productivityChart();
        //return $helper->taskTotalDurationSingle(40);
    }

    /**
     * Productivity Chart
     *
     * @return \Illuminate\Http\Response
     *
     * @SWG\Get(
     *     path="/api/v2/productivity/year",
     *     description="Get Productivity Chart with year only",
     *     operationId="chart.Productivity.year",
     *     produces={"application/json"},
     *     @SWG\Parameter(
     *          name="year",
     *          in="query",
     *          required=false,
     *          type="string"
     *     ),
     *     tags={"Charts"},
     *     security={
     *          {"passport": {}},
     *     },
     *     @SWG\Response(response=200, ref="#/responses/productivity")
     * )
     */
    public function getProductivityChartPerYear(TaskHelper $helper)
    {
        return $helper->productivityChartPerYear();
    }

    /**
     * Dashboard Chart
     *
     * @return \Illuminate\Http\Response
     *
     * @SWG\Get(
     *     path="/api/v2/dashboard",
     *     description="Get Dashboard Chart",
     *     operationId="dashboard.chart",
     *     produces={"application/json"},
     *     @SWG\Parameter(
     *         name="date",
     *         in="query",
     *         required=false,
     *         type="string"
     *     ),
     *     tags={"Charts"},
     *     security={
     *     {"passport": {}},
     *   },
     *     @SWG\Response(response=200, ref="#/responses/dashboard")
     * )
     */
    public function getDashboard(TaskHelper $taskHelper, ehsHelper $ehsHelper, qualityHelper $qualityHelper, serviceHelper $serviceHelper, ConfigurationHelper $configurationHelper, Request $request)
    {
        $data = $this->prepareDashboardData($taskHelper, $ehsHelper, $qualityHelper, $serviceHelper, $configurationHelper, $request);
        return json_response()->success($data);
    }

    public function prepareDashboardData(TaskHelper $taskHelper, ehsHelper $ehsHelper, qualityHelper $qualityHelper, serviceHelper $serviceHelper, ConfigurationHelper $configurationHelper, Request $request)
    {
        $date = $request->date;

        $now = Carbon::now();
        $lastWorkingDay = $this->getLastWorkingDay($now);

        if ($date == null) {
            $date = $lastWorkingDay;
        }

        $now = Carbon::now()->toDateString();
        $previous_day_range = [$lastWorkingDay, $lastWorkingDay];
        $previous_week = Carbon::now()->subWeeks(1);
        $previous_week_range = [$previous_week->copy()->startOfWeek(), $previous_week->copy()->endOfWeek()];
        $productivity_calculation_mode = $configurationHelper->getProductivityCalculationMode();
        $productivity_dates_range = ($productivity_calculation_mode == "daily") ? $previous_day_range : $previous_week_range;

        $accidents = $ehsHelper->getTypeValueToday($now, 'ehs-accidents');
        $observations = $ehsHelper->getTypeValueToday($now, 'ehs-observations');
        $freeDays = $ehsHelper->calculateFreeDays();
        $average = $ehsHelper->currentMonthSixs();
        $notRun = $qualityHelper->getTypeValueToday($now, 'quality-not-run');
        $limsDoubleCheck = $qualityHelper->getTypeValueToday($now, 'quality-lims');
        $certificates = $qualityHelper->getTypeValueToday($now, 'quality-certificates');
        $labErrors = $qualityHelper->getTypeValueToday($now, 'quality-lab-error');
        $defects = $qualityHelper->getTypeValueToday($now, 'quality-product-defects');
        $allRoutines = $serviceHelper->getAllRoutine();
        $allSpecials = $serviceHelper->getAllSpecial();
        $capacity = $ehsHelper->getCapacityDashboard($productivity_dates_range);
        $plannedHours = $ehsHelper->getPlannedHoursByDueDate($date, $date);
        $plannedSamples = $ehsHelper->getPlannedSamples($date, $date);
        $doneHours = $ehsHelper->getDoneHoursByDueDateDashboard($productivity_dates_range);
        $adminTasksCapacity = $ehsHelper->getAdminTaskCapacityDashboard($productivity_dates_range);
        $adminTaskCapacityGroupedByCategory = $ehsHelper->getAdminTasksCapacityGroupedByCategory($productivity_dates_range);

        $doneSamples = $ehsHelper->getDoneSamples($date, $date);
        $lastImport = $taskHelper->getLastImport();
        // $adminTasksCapacities = $taskHelper->getAdminTaskcapacity();
        // $adminTasksCapacity = isset($adminTasksCapacities[$date])?$adminTasksCapacities[$date]:0;
        $sampleFullFilled = $serviceHelper->getSampleFullFilled($date);
        $capacityBlock = $capacity == 0 ? 1 : $capacity - $adminTasksCapacity;

        $nonTestTasksHours = isset($adminTaskCapacityGroupedByCategory['NonTestTask']) ? (float) $adminTaskCapacityGroupedByCategory['NonTestTask'] : 0;
        $meetingHours = isset($adminTaskCapacityGroupedByCategory['Meeting']) ? (float) $adminTaskCapacityGroupedByCategory['Meeting'] : 0;
        $trainingHours = isset($adminTaskCapacityGroupedByCategory['Training']) ? (float) $adminTaskCapacityGroupedByCategory['Training'] : 0;
        $adminTasksHours = isset($adminTaskCapacityGroupedByCategory['AdminTask']) ? (float) $adminTaskCapacityGroupedByCategory['AdminTask'] : 0;
        $productivity = round((($doneHours + $nonTestTasksHours + $meetingHours + $trainingHours + ($adminTasksHours * 0.8)) / ($capacityBlock + $nonTestTasksHours + $meetingHours + $trainingHours + $adminTasksHours)) * 100);

        $data = collect([
            'accidents' => $accidents,
            'observations' => $observations,
            'freeDays' => $freeDays,
            'sixs' => $average,
            'quality-not-run' => $notRun,
            'quality-complaints' => $defects,
            'quality-certificates' => $certificates,
            'quality-lims' => $limsDoubleCheck,
            'quality-oos' => $labErrors,
            'service-routine' => isset($allRoutines[$date]) ? $allRoutines[$date] : 0,
            'service-special' => isset($allSpecials[$date]) ? $allSpecials[$date] : 0,
            'capacity' => round((float) ($capacity - $adminTasksCapacity), 2),
            'plannedHours' => round($plannedHours, 1),
            'doneHours' => $doneHours,
            'plannedSamples' => $plannedSamples,
            'doneSamples' => $doneSamples,
            'samplesFulfilled' => $sampleFullFilled,
            'last_import_time' => $lastImport['time'] ? $this->timezoneObj($lastImport['time']) : null,
            'last_import_status' => $lastImport['status'],
            'productivity' => $productivity,
        ]);

        return $data;
    }
}

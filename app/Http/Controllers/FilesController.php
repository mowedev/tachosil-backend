<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\File;
use Session;
use Artisan;

class FilesController extends Controller
{
    //
    public function index()
    {
    	$files = File::OrderBy('id','desc')->get();
    	$table = "Files";

    	return view('files.index',compact('files','table'));
    }

    public function forceImport()
    {
		//exec('cd .. && php artisan task:import');
        
        define('ARTISAN_BINARY', base_path().'/artisan');
        call_in_background('task:import');

   		Session::flash('Forced','Import is in processing now ... ');
        return redirect('/files');
    }
}

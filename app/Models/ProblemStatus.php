<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class ProblemStatus extends Model
{
    //

    protected $hidden = ['created_at','updated_at'];

}

<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class DueDateLog extends Model
{
    protected $fillable = [
        'row_week', 'row_year', 'sample_number', 'login_date',
        'tv_due_date', 'tv_created_at', 'state', 'days',
    ];
}

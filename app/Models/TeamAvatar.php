<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class TeamAvatar extends Model
{
    protected $fillable = [
        'avatar'
    ];

    protected $appends = [
        'avatar_url'
    ];

    /**
     * Accessors & Mutators
     */
    public function getAvatarUrlAttribute()
    {
        return url("storage/avatars/{$this->attributes['avatar']}");
    }
}

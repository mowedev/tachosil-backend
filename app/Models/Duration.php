<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Duration extends Model
{
    protected $fillable = [
        'product', 'project', 'analysis', 'description', 'duration', 'days', 'total_duration',
        'login_date', 'location', 'name',
    ];
}

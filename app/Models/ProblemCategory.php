<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use App\Models\Problem;

class ProblemCategory extends Model
{
    protected $hidden = ['created_at','updated_at'];

    public function problem()
    {
    	return $this->hasMany(Problem::class, "category_id", "id");
    }

}

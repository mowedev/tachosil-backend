<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class RescheduleBacklogCount extends Model
{
    protected $fillable = ['date', 'type', 'count'];
    protected $hidden = ['created_at', 'updated_at'];




}

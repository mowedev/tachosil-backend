<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class TeamLeadership extends Model
{
    //
    protected $table = 'team_leaderships' ;
    protected $fillable = ['date','leadership_id','number_of_members'];

}

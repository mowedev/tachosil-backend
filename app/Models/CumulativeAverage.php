<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class CumulativeAverage extends Model
{
    protected $fillable = [
        'op_date', 'average', 'type'
    ];
}

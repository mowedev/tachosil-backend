<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class ImportErrorLog extends Model
{
    protected $table = "import_error_logs";
    protected $fillable = [
        'file_path', 'row', 'errors_json'
    ];
}

<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class BacklogLogs extends Model
{
    protected $fillable = [
        'batch', 'product', 'sample_number', 'project', 'description', 'due_date', 'date_reviewed', 'date_completed', 'login_date', 'receive_date', 'status', 'duration'
    ];
    protected $hidden = ['created_at', 'updated_at'];
}

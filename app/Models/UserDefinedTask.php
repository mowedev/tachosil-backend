<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class UserDefinedTask extends Model
{
    //
    protected $fillable =['description', 'due_date', 'sample_number', 'duration'];
    protected $hidden = ['created_at', 'updated_at'];
}

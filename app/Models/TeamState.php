<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use App\Models\State;

class TeamState extends Model
{
    //
    protected $hidden = ['created_at','updated_at','id'];

    protected $appends = ['state_name'];

    protected $fillable = ['date','state_id','number_of_members'];

	public function getStateNameAttribute()
	{

		$stateId = $this->attributes['state_id'];
		$name = State::where("id", $stateId)->value('name');
		return $name;
	}
}

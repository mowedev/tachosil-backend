<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class RescheduleBacklogSamplesCount extends Model
{
    protected $fillable = ['date', 'type', 'sample_number', 'count'];
    protected $hidden = ['created_at', 'updated_at'];
}

<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class HighlightEmoji extends Model
{
    protected $table = 'highlight_emojies';
    protected $fillable = [
        'highlight_id', 'emoji_id', 'count'
    ];
}

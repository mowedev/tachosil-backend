<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class TeamDailyStatus extends Model
{
    protected $fillable = [
        'date', 'state_id', 'index'
    ];

    /**
     * Relations
     */
    // public function member()
    // {
    //     return $this->belongsTo(TeamMember::class, 'member_id');
    // }
}

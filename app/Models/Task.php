<?php

namespace App\Models;

use App\Models\Subtask;
use App\Models\TaskVersion;
use Awobaz\Compoships\Compoships;
use Illuminate\Database\Eloquent\Model;

class Task extends Model
{
    use Compoships;

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $fillable = ['description', 'stage', 'sample_number', 'duration', 'days', 'batch', 'product', 'status', 'due_date', 'original_due_date', 'login_date', 'receive_date', 'date_completed', 'date_reviewed', 'is_user_defined'];

    protected $hidden = [
        'created_at', 'updated_at', 'prj', 'material_number',
        'stage', 'sample_logindate', 'sample_receivedate', 'test_date_completed',
        'test_reviewed', 'created_at', 'updated_at',
    ];
    public $timestamps = true;

    public function versions()
    {
        return $this->hasMany(TaskVersion::class, "sample_number", "sample_number");
    }

    public function subtasks()
    {
        return $this->hasMany(Subtask::class, "sample_number", "sample_number");
    }

    public function latestVersion()
    {
        return $this->versions()->where('state', 1)->orderBy('due_date', 'DESC');
    }

    public function scopeWithDuration($query)
    {
        $type = app('request')->input("type");
        $type = isset($type) ? $type : "calendar";

        if ($type == "list") {

            return $query->where('duration', '<>', 0);
            // return $query;
            
        } else {

            return $query->where('duration', '<>', 0);
        }

    }

    public function scopeWeekTasks($query, $dates)
    {
        $firstWeek = $dates['firstOfWeek'];
        $lastWeek = $dates['lastOfWeek'];

        return $query->whereBetween('due_date', [$firstWeek, $lastWeek]);
    }

    public function scopeWeekTasksByColoumn($query, $dates, $operator)
    {
        $firstWeek = $dates['firstOfWeek'];
        $lastWeek = $dates['lastOfWeek'];

        return $query->whereBetween($operator, [$firstWeek, $lastWeek]);
    }

    public function scopeNotCompletedTasks($query)
    {
        return $query->whereNull('date_reviewed');
    }

    public function scopeCompletedTasksByCompletedDate($query, $data)
    {
        return $query->whereIn('date_completed', $data);
    }

    public function scopeCompletedTasks($query)
    {
        return $query->whereNotNull("date_reviewed");
    }

    public function scopeNotCloned($query)
    {
        return $query->doesntHave("versions");
    }

    public function scopeCloned($query)
    {
        return $query->has("versions");
    }

    public function scopeFilterEstimatedTasks($query, $operator)
    {
        return $query->where("product", $operator, "");
    }

    public function scopeSortTask($query)
    {
        $query->orderBy('order');
    }

    public function scopeSortTaskByDate($query)
    {
        $query->orderBy('due_date');
    }

    public function scopeOldOpenTasks($query, $date)
    {
        $query->where("due_date", "<", $date)
            ->whereNull("date_reviewed")
            ->where("is_orignal", 1);
    }

    public function scopeGetFront($query)
    {
        return $query->where("is_front", "<>", "1");
    }

    public function scopeGetFurture($query, $operator)
    {
        return $query->where("duration", "$operator", "0");
    }

    public function scopeUserDefined($query, $operator)
    {
        return $query->where("is_user_defined", $operator);
    }

    public function scopeGetTaskList($query)
    {
        return $query //->where("is_user_defined",1)
        //->orWhere("duration",0)
        ->where("is_orignal", 1);
    }

    public function scopeIsOrignal($query, $value)
    {
        return $query->where("is_orignal", $value);
    }

    public function scopeExactDueDate($query, $date)
    {
        return $query->whereHas('versions', function ($q) use ($date) {
            $q->where('task_version.due_date', $date)->where('state', 1);
        });
    }
}

<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class TeamMember extends Model
{
    protected $fillable = [
        'avatar_id'
    ];

    protected $appends = [
        'avatar_url'
    ];

    /**
     * Accessors & Mutators
     */
    public function getAvatarUrlAttribute()
    {
        if (!$this->avatar) {
            return null;
        }
        $avatar = $this->avatar instanceof TeamAvatar ? $this->avatar->avatar : $this->avatar;
        return url("storage/avatars/{$avatar}");
    }

    /**
     * Relations
     */
    public function avatar()
    {
        return $this->belongsTo(TeamAvatar::class, 'avatar_id');
    }

}

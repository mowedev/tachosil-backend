<?php

namespace App\Models;

use App\Models\Task;
use Awobaz\Compoships\Compoships;
use Carbon\Carbon;
use Illuminate\Database\Eloquent\Model;

class TaskVersion extends Model
{
    use Compoships;

    protected $table = "task_version";

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        'updated_at',
    ];

    protected $fillable = ['sample_number', 'order', 'state', 'move_state', 'due_date'];
    public $timestamps = true;

    public function task()
    {
        return $this->belongsTo(Task::class, "sample_number", "sample_number");
    }

    public function scopeWeekTasksByColoumn($query, $dates, $field)
    {
        $firstWeek = $dates['firstOfWeek'];
        $lastWeek = $dates['lastOfWeek'];

        return $query->whereBetween($field, [$firstWeek, $lastWeek]);
    }

    public function scopeBacklog($query)
    {
        return $query->where('state', 2);
    }

    public function scopeNotHardMovedToBacklog($query)
    {
        return $query->where('is_hard_moved_to_backlog', 0);
    }

    public function scopeBacklogNotReschedule($query)
    {
        $today = Carbon::today()->format("Y-m-d");

        return $query->where('state', 2)
            ->orWhere(function ($q) use ($today) {
                $q->whereHas('task', function ($que) {
                    $que->whereNull('tasks.date_reviewed');
                })->where('state', 1)
                    ->where("due_date", '<', $today);
            });
    }

    public function scopeActive($query)
    {
        return $query->where('state', 1);
    }

    public function scopeNotCompletedParent($query)
    {
        return $query->whereHas('task', function ($q) {
            $q->whereNull('tasks.date_reviewed');
        });
    }

    public function scopeNotCompletedParentWithDuration($query)
    {
        return $query->whereHas('task', function ($q) {
            $q->whereNull('tasks.date_reviewed')->where('tasks.duration', '<>', 0);
        });
    }

    // public function scopeNotMissingData($query)
    // {
    //     return $query->whereHas('task', function ($q) {
    //         $q->where(function ($query) {
    //             $query->whereNotNull('analysis');
    //         });
    //     });
    // }

    public function scopeParentWithOutDuration($query)
    {
        return $query->whereHas('task', function ($q) {
            $q->where('tasks.duration', 0);
        });
    }

    public function scopeNotCompletedParentTemp($query)
    {
        $today = Carbon::today()->format("Y-m-d");

        return $query->whereHas('task', function ($q) use ($today) {
            $q->where(function ($que) use ($today) {
                $que->whereNull('tasks.date_reviewed')
                    ->orWhere('tasks.date_reviewed', $today);
            });
        });
    }

    public function scopeNotCompletedParentWithDurationTemp($query)
    {
        $today = Carbon::today()->format("Y-m-d");

        return $query->whereHas('task', function ($q) use ($today) {
            $q->where(function ($que) use ($today) {
                $que->whereNull('tasks.date_reviewed')
                    ->orWhere('tasks.date_reviewed', $today);
            })->where('tasks.duration', '<>', 0);
        });
    }

    public function scopePassedDueDates($query, $date)
    {
        return $query->where("due_date", "<", $date)
            ->orWhere('is_hard_moved_to_backlog', "=", 1);
    }

}

<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class SixS extends Model
{
    //
    protected $table = 'six_s' ;

    protected $fillable = ['date','safety','order', 'documents', 'materials' ,'clothing','security' ,'discipline'];

    protected $hidden = ['created_at','id'];

    protected $casts = [

        'safety' => 'float',
        'order' => 'float',
        'documents' => 'float',
        'materials' => 'float' ,
        'clothing' => 'float',
        'security' => 'float' ,
        'discipline' => 'float',
        'updated_at' => 'date:Y-m-d'

    ];



}	

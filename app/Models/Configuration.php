<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Configuration extends Model
{
    protected $hidden = ['created_at', 'updated_at'];
    protected $appends = ['section_name'];
    /**
     * Relations
     */
    public function section()
    {
        return $this->belongsTo(Section::class);
    }

    /**
     * Accessors & Mutators
     */
    public function getSectionNameAttribute()
    {
        return optional($this->section)->name;
    }

    public function getValueAttribute()
    {
        if (is_array(json_decode($this->attributes['value']))) {
            return json_decode($this->attributes['value']);
        }

        return $this->attributes['value'];
    }
}

<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use App\Models\Configuration;
use App\Helpers\ConfigurationHelper;

class Import extends Model
{
    protected $table = 'imported';

    protected $fillable = ['name', 'imported_time','status'];
    protected $appends = [
        'imported'
    ];
    public $timestamps = false;

    public function getImportedAttribute()
    {
        $status = $this->attributes['status'];
        $imported = ($status == 1)?1:0;
        return $imported;

    }
}

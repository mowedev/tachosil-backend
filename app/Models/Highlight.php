<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Carbon\Carbon;

class Highlight extends Model
{
    protected $fillable = ['title', 'description', 'due_date'];

    public function scopeSort($query, $sortBy, $sortOrder)
    {
        if ($sortBy == null || $sortOrder == null) {

            return $query->orderBy('created_at', 'desc');
        }

        return $query->orderBy($sortBy, $sortOrder);
    }

    public function getCreatedAtAttribute()
    {
        $created_at = $this->attributes['created_at'];
        $created_at = Carbon::parse($created_at);
        return $created_at->toDateString();
    }

    /**
     * Relations
     */
    public function emojies()
    {
        return  $this->belongsToMany(Emoji::class, 'highlight_emojies', 'highlight_id', 'emoji_id')->select('emojies.id', 'emojies.img')->withPivot('count');
    }
}

<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class AdminTask extends Model
{
    //
    protected $fillable = ['date','duration','category','description'];
    protected $hidden = ['created_at','updated_at'];

    public function scopeWeekTasks($query, $dates)
    {
    	$firstWeek = $dates['firstOfWeek'];
    	$lastWeek = $dates['lastOfWeek'];

    	return $query->whereBetween('date', [$firstWeek, $lastWeek]);
    }
}

<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Capacity extends Model
{
    protected $hidden = ['created_at', 'updated_at'];
    protected $casts = [
        'capacity' => "float",
    ];

    public function scopeWeekTasks($query, $dates)
    {
        $firstWeek = $dates['firstOfWeek'];
        $lastWeek = $dates['lastOfWeek'];

        return $query->whereBetween('date', [$firstWeek, $lastWeek]);
    }
}

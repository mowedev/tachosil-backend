<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use App\Models\ProblemCategory;
use Illuminate\Support\Facades\DB;

class Problem extends Model
{

    protected $fillable = ['category_id', 'status_id', 'description', 'solution', 'responsible', 'due_date'];

    protected $casts = [
        'created_at' => 'datetime:Y-m-d',
    ];

    protected $hidden = ['updated_at'];

    protected $appends = ["category", "status"];

    public function category()
    {
        return $this->belongsTo(ProblemCategory::class);
    }

    public function scopeFilterProblems($query, $q)
    {
        if($q === NULL){

            return $query;
        }

        return $query->whereIn('category_id', $q);
    }

    public function scopeSortProblems($query, $sortBy, $sortOrder)
    {
        // $query->orderBy('status_id','ASC');
        if(!$sortBy || !$sortOrder){
            return $query->orderBy('created_at','DESC');
        }
        if ($sortBy == "category_id") {
            return $query->select(['*', 'problem_categories.category', 'problems.created_at'])
            ->join('problem_categories', 'problem_categories.id', '=', 'problems.category_id')
            ->orderBy("category", $sortOrder);
        }
        return $query->orderBy($sortBy, $sortOrder);
    }

    public function getCategoryAttribute()
    {
        return DB::table("problem_categories")->where("id", $this->attributes['category_id'])->value("category");
    }

    public function getStatusAttribute()
    {
        return DB::table("problem_statuses")->where("id", $this->attributes['status_id'])->value("status");
    }
}

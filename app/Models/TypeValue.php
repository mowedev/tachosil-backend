<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class TypeValue extends Model
{
    //
    protected $hidden = ['created_at','updated_at','id'];

    protected $casts = [

    	'value' =>'float'

    ];

}

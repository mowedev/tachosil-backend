<?php

namespace App\Formatters;

/**
 * Contains formatting functions for tasks/productivity APIs
 * This class should not contain SQL queries or heavy logic.
 */

class AdminTaskFormatter
{
    public static function formatAdminTasks($adminTasks)
    {
        $adminTasksCategories = config('custom.admin_tasks_categories');
        $formatted = array_fill_keys(array_values($adminTasksCategories), 0);
        
        foreach ($adminTasksCategories as $category) {
    		foreach ($adminTasks as $task) {
                if ($task['category'] === $category) {
                    $formatted[$category] = (float) $task['duration'];
                }
    		}
		}

		return $formatted;
    }

    public static function getEmptyAdminTasks()
    {
        $adminTasksCategories = config('custom.admin_tasks_categories');
        $tasks = [];

        foreach ($adminTasksCategories as $oneCategory) {
            $tasks[$oneCategory] = 0;
        }

        return $tasks;
    }
}

<?php

namespace App\Formatters;

use Carbon\Carbon;

/**
 * Contains formatting functions for tasks/productivity APIs
 * This class should not contain SQL queries or heavy logic.
 */

class TaskFormatter
{
    /**
     * @param $input['pastTasks']
     * @param $input['futureTasks']
     * @param $input['backlog']
     * @param $input['adminTasks']
     * @param $input['capacity']
     * @param $input['adminCap']
     * @param $input['demandRates']
     * @param $input['start_date']
     * @param $input['end_date']
     * @param $input['request_type']
     * @param $input['doneHoursSumPerDate']
     */
    public static function calendarViewResponse($input)
    {
        extract($input);

        $allTasks = [];
        $numOfDays = $request_type == 'list' ? 6 : 5;

        for ($i = 0; $i < $numOfDays; $i++) {
            $cap = 32;
            if (isset($capacity[$start_date])) {
                $cap = isset($adminCap[$start_date]) ? ($capacity[$start_date] - $adminCap[$start_date]) : $capacity[$start_date];
            }

            $doneHours = isset($doneHoursSumPerDate[$start_date]) ? $doneHoursSumPerDate[$start_date] : 0;
            $adminHours = isset($adminCap[$start_date]) ? $adminCap[$start_date] : 0;

            $allTasks[$i]['day'] = $start_date;
            $allTasks[$i]['capacity'] = $cap;
            $allTasks[$i]['actual_capacity'] = isset($capacity[$start_date]) ? $capacity[$start_date] : 32;
            $allTasks[$i]['demand_rates'] = isset($demandRates[$start_date]) ? $demandRates[$start_date] : 0;

            if (isset($pastTasks[$start_date])) {
                $allTasks[$i]['tasks'] = $pastTasks[$start_date];
            } else {
                $allTasks[$i]['tasks'] = isset($futureTasks[$start_date]) ? $futureTasks[$start_date] : [];
            }

            $allTasks[$i]['backlog'] = isset($backlog[$start_date]) ? $backlog[$start_date] : [];
            $allTasks[$i]['admin_tasks'] = isset($adminTasks[$start_date]) ? AdminTaskFormatter::formatAdminTasks($adminTasks[$start_date]) : AdminTaskFormatter::getEmptyAdminTasks();

            $nonTestTasksHours = $allTasks[$i]['admin_tasks']['NonTestTask'];
            $meetingHours = $allTasks[$i]['admin_tasks']['Meeting'];
            $trainingHours = $allTasks[$i]['admin_tasks']['Training'];
            $adminTasksHours = $allTasks[$i]['admin_tasks']['AdminTask'];

            $start_date_carbon = Carbon::createFromFormat('Y-m-d', $start_date)->setTime(0, 0, 0);
            $today = Carbon::today();

            if ($start_date_carbon->lessThan($today)) {
                $allTasks[$i]['planned_tests'] = array_reduce($allTasks[$i]['tasks'], function ($carry, $item) {
                    if ($item['status'] == 1) { // Done
                        $carry += $item['duration'];
                    } else {
                        $carry += 0;
                    }
                    
                    return $carry;
                });
            } else {
                $allTasks[$i]['planned_tests'] = array_reduce($allTasks[$i]['tasks'], function ($carry, $item) {
                    // if ($item['status'] == 3) { // Planned
                    //     $carry += $item['duration'];
                    // } else {
                    //     $carry += 0;
                    // }

                    $carry += $item['duration'];
                    return $carry;
                });
            }
            
            if ($cap + $adminHours == 0) {
                $allTasks[$i]['productivity'] = null;
            // } elseif ($start_date_carbon->greaterThanOrEqualTo($today)) {
            } elseif ($start_date_carbon->greaterThan($today)) {
                $allTasks[$i]['productivity'] = 0;
                // $planned_hours = $allTasks[$i]['planned_tests'];
                // $allTasks[$i]['productivity'] = round((($planned_hours + $nonTestTasksHours + $meetingHours + $trainingHours + ($adminTasksHours * 0.8)) / ($cap + $nonTestTasksHours + $meetingHours + $trainingHours + $adminTasksHours)) * 100);
            } else {
                $allTasks[$i]['productivity'] = round((($doneHours + $nonTestTasksHours + $meetingHours + $trainingHours + ($adminTasksHours * 0.8)) / ($cap + $nonTestTasksHours + $meetingHours + $trainingHours + $adminTasksHours)) * 100);
            }

            $allTasks[$i]['admin_hours'] = (float) ($adminHours);

            $start_date = (new carbon($start_date))->addDays(1)->format("Y-m-d");
        }

        return $allTasks;
    }
}

<?php

namespace App\Helpers;

use App\Models\ImportErrorLog;
// use App\Models\MailingList;
// use App\Notifications\ImportFailureNotification;
use App\Mail\ImportStatusMail;
use Carbon\Carbon;
use Notification;
use Validator;
use Mail;

class LogingHelper
{
    protected $now;

    public function __construct($now = null)
    {
        $this->now = $now ? $now : Carbon::now();
    }

    public function report($errors, $valid_data = false)
    {
        $mailing_list = $this->getMailingList();
        // Notification::send($mailing_list, new ImportFailureNotification($errors, $this->now->toDateTimeString(), $valid_data));
        Mail::to($mailing_list)->send(new ImportStatusMail($errors, $this->now->toDateTimeString(), $valid_data));
        $errors = $this->formatErrors($errors);
        $op = $this->saveErrorLogs($errors);
    }

    public function formatErrors($errors)
    {
        $formatted_errors = array_map(function ($error) {
            return [
                'file_path' => $error['file_path'],
                'row' => $error['row'],
                'errors_json' => json_encode($error['row'] > 0 ? ['validation_errors' => $error['errors']] : ['errors' => $error['errors']]),
                'created_at' => $this->now->toDateTimeString(),
                'updated_at' => $this->now->toDateTimeString(),
            ];
        }, $errors);

        return $formatted_errors;
    }

    public function getMailingList()
    {
        // $mailing_list = MailingList::get();
        $mailing_list = config('custom.mailing_list', []);
        return $mailing_list;
    }

    public function saveErrorLogs($formatted_errors)
    {
        $op = ImportErrorLog::Insert($formatted_errors);
        return $op;
    }

    public function validate($row)
    {
        $validator = Validator::make($row, [
            'sample_number' => 'required',
            'sample_status' => 'required',
            'test_status' => 'required',
            'due_date' => 'required',
            'login_date' => 'required',
        ]);

        $errors = [];
        if ($validator->fails()) {
            $errors = $validator->errors()->toArray();
        }

        if (!empty($errors)) {
            return [
                'status' => false,
                'errors' => $errors,
            ];
        }

        return [
            'status' => true,
        ];
    }
}

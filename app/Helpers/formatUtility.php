<?php

namespace App\Helpers;

use App\Models\Leadership;
use Carbon\Carbon;
use DB;

trait formatUtility
{

    public function formatFromTasks($allTasks, $completed_tasks = FALSE)
    {
        $backlogedSamples = $this->getAllBacklogedSamples();

        foreach ($allTasks as $date => $tasks) {
            $carbon_date = Carbon::createFromFormat('Y-m-d', $date);
            foreach ($tasks as $index => $value) {
                $allTasks[$date][$index]['order'] = $value['latest_version'] ? $value['latest_version'][0]['order'] : 1;

                $allTasks[$date][$index]['due_date'] = $allTasks[$date][$index]['original_due_date'];

                $allTasks[$date][$index]['status'] = 1;
                unset($allTasks[$date][$index]['latest_version']);
                $allTasks[$date][$index]['was_backlog'] = in_array($value['sample_number'], $backlogedSamples) ? 1 : 0;

                $sub_tasks = $allTasks[$date][$index]['subtasks'];
                $sub_tasks_count = count($sub_tasks);
                $part = $allTasks[$date][$index]['part'];
                $days = $allTasks[$date][$index]['days'];
                $done_sub_tasks = array_reduce($sub_tasks, function ($carry, $item) use ($carbon_date, $part, $days) {

                    if ($item['date_reviewed']) {
                        $cdate_reviewed = Carbon::createFromFormat('Y-m-d', $item['date_reviewed']);
                        if ($part == $days) {
                            $carry += 1;
                        } else if ($carbon_date->greaterThanOrEqualTo($cdate_reviewed)) {
                            $carry += 1;
                        }
                    }

                    return $carry;
                });
                $done_sub_tasks = $done_sub_tasks ? $done_sub_tasks : 0;

                $allTasks[$date][$index]['progress'] = $sub_tasks_count ? "$done_sub_tasks/$sub_tasks_count" : '';
            }

            $allTasks[$date] = collect($allTasks[$date])->sortBy('order');
            $allTasks[$date] = $allTasks[$date]->values()->all();

            foreach ($tasks as $index => $value) {

                $allTasks[$date][$index]['orderIndex'] = $index + 1;

            }

            $index++;
        }

        return json_decode(json_encode($allTasks), true);
    }

    public function formatFromTaskVersions($allTasks, $orderByCreation = false, $due_dates = [])
    {
        $backlogedSamples = $this->getAllBacklogedSamples();
        $formattedTasks = [];

        foreach ($allTasks as $date => $tasks) {
            $carbon_date = Carbon::createFromFormat('Y-m-d', $date);
            foreach ($tasks as $index => $value) {
                $formattedTasks[$date][$index] = $value['task'];

                $sub_tasks = $formattedTasks[$date][$index]['subtasks'];
                $sub_tasks_count = count($sub_tasks);
                $part = $formattedTasks[$date][$index]['part'];
                $days = $formattedTasks[$date][$index]['days'];
                $done_sub_tasks = array_reduce($sub_tasks, function ($carry, $item) use ($carbon_date, $part, $days){

                    if ($item['date_reviewed']) {
                        $cdate_reviewed = Carbon::createFromFormat('Y-m-d', $item['date_reviewed']);
                        if ($part == $days) {
                            $carry += 1;
                        } else if ($carbon_date->greaterThanOrEqualTo($cdate_reviewed)) {
                            $carry += 1;
                        }
                    }

                    return $carry;
                });
                $done_sub_tasks = $done_sub_tasks ? $done_sub_tasks : 0;

                $formattedTasks[$date][$index]['progress'] = $sub_tasks_count ? "$done_sub_tasks/$sub_tasks_count" : '';
                $formattedTasks[$date][$index]['order'] = $value['order'];
                $formattedTasks[$date][$index]['part'] = $value['part'];

                $due = $value['task']['original_due_date'];

                if (isset($value['subtask_analysis'])) {
                    $formattedTasks[$date][$index]['analysis'] = $value['subtask_analysis'];
                }

                $formattedTasks[$date][$index]['due_date'] = $due;
                $formattedTasks[$date][$index]['created_at'] = $value['created_at'];
                $formattedTasks[$date][$index]['was_backlog'] = in_array($value['task']['sample_number'], $backlogedSamples) ? 1 : 0;
            }

            $formattedTasks[$date] = $orderByCreation ? collect($formattedTasks[$date])->sortBy('created_at') : collect($formattedTasks[$date])->sortBy('order');
            $formattedTasks[$date] = $formattedTasks[$date]->values()->all();

            foreach ($tasks as $index => $value) {
                $formattedTasks[$date][$index]['orderIndex'] = $index + 1;
            }
        }

        return json_decode(json_encode($formattedTasks), true);
    }

    public function formatDailyBacklog($backlogItems)
    {
        $formatted = [];

        foreach ($backlogItems as $key => $item) {

            $formatted[$key]['id'] = $item['task']['id'];
            $formatted[$key]['batch'] = $item['task']['batch'];
            $formatted[$key]['product'] = $item['task']['product'];
            $formatted[$key]['sample_number'] = $item['sample_number'];
            $formatted[$key]['project'] = $item['task']['project'];
            $formatted[$key]['description'] = $item['task']['description'];
            $formatted[$key]['due_date'] = $item['due_date'];
            $formatted[$key]['date_reviewed'] = $item['task']['date_reviewed'];
            $formatted[$key]['status'] = $item['task']['status'];
            $formatted[$key]['duration'] = $item['task']['duration'];

        }

        return $formatted;
    }

    public function listViewResponse($tasks, $week)
    {
        $allTasks = [];

        $day = (isset($week['firstOfWeek'])) ? $week['firstOfWeek'] : $week['past']['firstOfWeek'];
        $last = (isset($week['lastOfWeek'])) ? $week['lastOfWeek'] : $week['future']['lastOfWeek'];

        //$capacity = $this->getCapacity([$day, $last]);
        //$adminCap = $this->getAdminTaskcapacity();
        $demandRates = $this->getDemandRates();

        for ($i = 0; $i < 5; $i++) {

            $allTasks[$i]['day'] = $day;
            //$allTasks[$i]['capacity'] = $cap;
            $allTasks[$i]['demand_rates'] = $demandRates[$day];

            if (isset($tasks[$day])) {
                $allTasks[$i]['tasks'] = $tasks[$day];
            } else {
                $allTasks[$i]['tasks'] = [];
            }

            $day = (new carbon($day))->addDays(1)->format("Y-m-d");
        }

        return $allTasks;
    }

    public function formatSingleTask($task)
    {
        $formattedTasks = [];
        $task = isset($task[0]) ? $task[0] : $task;
        if (isset($task)) {
            $formattedTasks = $task;
            $formattedTasks['order'] = $task['latest_version'] ? $task['latest_version'][0]['order'] : 0;
            $formattedTasks['due_date'] = $task['latest_version'] ? $task['latest_version'][0]['due_date'] : NULL;
            $formattedTasks['sample_number'] = $formattedTasks['sample_number'];
            unset($formattedTasks['latest_version']);
        }

        return $formattedTasks;
    }

    public function formatFullBacklog($backlogs)
    {
        $formated = [];
        $index = 0;
        foreach ($backlogs as $date => $list) {
            foreach ($list as $key => $value) {
                $value['duration'] *= $value['days'];
                $formated[$index] = $value;
                $index++;
            }
        }

        return (array_values(collect($formated)->sortBy('due_date')->toArray()));
    }

    public function formatTeamChart($trends, $start_date, $end_date, $limit = null)
    {
        $result = [];
        $i = 0;
        $now = Carbon::now();
        $today = $now->format("Y-m-d");
        $start_date = (new Carbon($start_date));
        $formatted = $start_date->format("d");
        $start_date = $start_date->format("Y-m-d");

        $holidays = DB::table('holidays')->pluck('date')->toArray();

        while ($start_date <= $end_date) {
            if (in_array($start_date, $holidays)) {
                $start_date = (new Carbon($start_date))->addDays(1);
                $formatted = $start_date->format("d");
                $start_date = $start_date->format("Y-m-d");
                continue;
            }

            $result[$i]['day'] = (integer) $formatted;
            $result[$i]['date'] = $start_date;
            $result[$i]['number'] = $start_date > $today ? null : (double) ((isset($trends[$start_date]) && !is_null($trends[$start_date]) && ($start_date <= $today)) ? round($trends[$start_date], 1) : 0);

            $start_date = (new Carbon($start_date))->addDays(1);
            $formatted = $start_date->format("d");
            $start_date = $start_date->format("Y-m-d");
            $i++;
        }

        if ($limit && count($result) > $limit) {
            $result = array_slice($result, count($result) - $limit, $limit);
        }

        return $result;
    }

    public function formatLeadershipChart($leadership_data, $start_date, $end_date)
    {
        $result = [];
        $i = 0;
        $now = Carbon::now();
        $today = $now->format("Y-m-d");
        $start_date = (new Carbon($start_date));
        $formatted = $start_date->format("d");
        $start_date = $start_date->format("Y-m-d");

        // $leader_ships_config = ConfigurationHelper::config('team_leadership_names');
        $leader_ships_config = json_decode(Leadership::select(['id', 'name'])->get()->toJson());

        $leader_ships = [];
        foreach ($leader_ships_config as $leader_ship) {
            $leader_ships[$leader_ship->id] = $leader_ship->name;
        }

        $holidays = DB::table('holidays')->pluck('date')->toArray();

        while ($start_date <= $end_date) {
            if (in_array($start_date, $holidays)) {
                $start_date = (new Carbon($start_date))->addDays(1);
                $formatted = $start_date->format("d");
                $start_date = $start_date->format("Y-m-d");
                continue;
            }

            $result[$i]['day'] = (integer) $formatted;
            $result[$i]['date'] = $start_date;
            $sum = 0;

            foreach ($leader_ships_config as $leader_ship) {
                $result[$i]['leader'][$leader_ship->id] = 0;

                $abbrv = "";
                if (preg_match_all('/\b(\w)/', strtoupper($leader_ship->name), $m)) {
                    $abbrv = implode('', $m[1]); // $v is now SOQTU
                }

                $result[$i]['leader_name'][$leader_ship->id] = [
                    'id' => $leader_ship->id,
                    'name' => $abbrv,
                    'value' => 0,
                ];
            }

            if (is_array($leadership_data) && !empty($leadership_data) && array_key_exists($start_date, $leadership_data)) {
                foreach ($leadership_data[$start_date] as $leadership_entry) {
                    if (is_array($leadership_entry) && !empty($leadership_entry)) {
                        $sum += $leadership_entry['number_of_members'];

                        if (isset($leadership_entry['leadership_id']) && isset($leader_ships[$leadership_entry['leadership_id']])) {
                            $leader_ship_name = $leader_ships[$leadership_entry['leadership_id']];
                            $result[$i]['leader'][$leadership_entry['leadership_id']] = $leadership_entry['number_of_members'];
                            $result[$i]['leader_name'][$leadership_entry['leadership_id']]['value'] = $leadership_entry['number_of_members'];
                        }
                    }
                }
            }

            if (isset($result[$i]['leader_name'])) {
                $result[$i]['leader_name'] = array_values($result[$i]['leader_name']);
            }

            $result[$i]['count'] = $start_date > $today ? null : $sum;
            // $result[$i]['count'] = (double) ((isset($leadership_data[$start_date]) && !is_null($leadership_data[$start_date]) && ($start_date <= $today)) ? $leadership_data[$start_date] : 0);

            $start_date = (new Carbon($start_date))->addDays(1);
            $formatted = $start_date->format("d");
            $start_date = $start_date->format("Y-m-d");
            $i++;
        }

        return $result;
    }

    public function moodVsLeadershipChartFormat($mood_data, $leadership_data, $start_date, $end_date)
    {
        $result = [];
        $i = 0;
        $now = Carbon::now();
        $today = $now->format("Y-m-d");
        $start_date = (new Carbon($start_date));
        $formatted = $start_date->format("d");
        $start_date = $start_date->format("Y-m-d");

        while ($start_date <= $end_date) {
            $result[$i]['day'] = (integer) $formatted;
            $result[$i]['date'] = $start_date;
            $result[$i]['team'] = (double) ((isset($mood_data[$start_date]) && !is_null($mood_data[$start_date]) && ($start_date <= $today)) ? $mood_data[$start_date] : 0);
            $result[$i]['leadership'] = (double) ((isset($leadership_data[$start_date]) && !is_null($leadership_data[$start_date]) && ($start_date <= $today)) ? $leadership_data[$start_date] : 0);

            $start_date = (new Carbon($start_date))->addDays(1);
            $formatted = $start_date->format("d");
            $start_date = $start_date->format("Y-m-d");
            $i++;
        }

        return $result;
    }

    public function listFormatForExport($list)
    {
        $formatted = [];
        $counter = 0;
        $allMoved = $this->getAllMovedTasks();
        $originalDueDates = $this->getOriginalDueDates();
        $backlogedSamples = $this->getAllBacklogedSamples();

        foreach ($list as $key => $dayTasks) {

            foreach ($dayTasks['tasks'] as $index => $task) {

                $formatted[$counter]['id'] = $counter + 1;
                $formatted[$counter]['batch'] = $task['batch'];
                $formatted[$counter]['product'] = $task['product'];
                $formatted[$counter]['sample_number'] = $task['sample_number'];
                $formatted[$counter]['description'] = $task['description'];
                $formatted[$counter]['due_date'] = $task['due_date'];
                $formatted[$counter]['date_reviewed'] = $task['date_reviewed'];
                $formatted[$counter]['status'] = $task['status'];
                $formatted[$counter]['duration'] = $task['duration'];
                $formatted[$counter]['moved'] = $this->isMoved($task['sample_number'], $allMoved);
                $formatted[$counter]['original_date'] = isset($originalDueDates[$task['sample_number']]) ? $originalDueDates[$task['sample_number']] : null;
                $formatted[$counter]['is_user_defined'] = $task['is_user_defined'];
                $formatted[$counter]['was_backlog'] = in_array($task['sample_number'], $backlogedSamples) ? 1 : 0;

                $counter++;
            }
        }

        return $formatted;
    }

    public function backlogFormatForExport($backlogTasks)
    {
        $formatted = [];
        $counter = 0;
        $allMoved = $this->getAllMovedTasks();
        $originalDueDates = $this->getOriginalDueDates();
        $backlogedSamples = $this->getAllBacklogedSamples();

        foreach ($backlogTasks as $key => $task) {

            $formatted[$counter]['id'] = $counter + 1;
            $formatted[$counter]['batch'] = $task['batch'];
            $formatted[$counter]['product'] = $task['product'];
            $formatted[$counter]['sample_number'] = $task['sample_number'];
            $formatted[$counter]['description'] = $task['description'];
            $formatted[$counter]['due_date'] = $task['due_date'];
            $formatted[$counter]['date_reviewed'] = $task['date_reviewed'];
            $formatted[$counter]['status'] = $task['status'];
            $formatted[$counter]['duration'] = $task['duration'];
            $formatted[$counter]['moved'] = $this->isMoved($task['sample_number'], $allMoved);
            $formatted[$counter]['original_date'] = isset($originalDueDates[$task['sample_number']]) ? $originalDueDates[$task['sample_number']] : null;
            $formatted[$counter]['is_user_defined'] = $task['is_user_defined'];
            $formatted[$counter]['was_backlog'] = in_array($task['sample_number'], $backlogedSamples) ? 1 : 0;

            $counter++;
        }

        return $formatted;

    }

    public function formatDoneTasks($tasks)
    {
        $formatted = [];
        $counter = 0;
        $allMoved = $this->getAllMovedTasks();
        $originalDueDates = $this->getOriginalDueDates();
        $backlogedSamples = $this->getAllBacklogedSamples();

        foreach ($tasks as $key => $task) {

            $formatted[$counter]['id'] = $counter + 1;
            $formatted[$counter]['batch'] = $task['batch'];
            $formatted[$counter]['product'] = $task['product'];
            $formatted[$counter]['sample_number'] = $task['sample_number'];
            $formatted[$counter]['description'] = $task['description'];
            $formatted[$counter]['due_date'] = !empty($task['latest_version']) ? $task['latest_version'][0]['due_date'] : '';
            $formatted[$counter]['date_reviewed'] = $task['date_reviewed'];
            $formatted[$counter]['status'] = 1;
            $formatted[$counter]['duration'] = $task['duration'];
            $formatted[$counter]['moved'] = $this->isMoved($task['sample_number'], $allMoved);
            $formatted[$counter]['original_date'] = isset($originalDueDates[$task['sample_number']]) ? $originalDueDates[$task['sample_number']] : null;
            $formatted[$counter]['is_user_defined'] = $task['is_user_defined'];
            $formatted[$counter]['was_backlog'] = in_array($task['sample_number'], $backlogedSamples) ? 1 : 0;

            $counter++;
        }

        return $formatted;
    }

    public function formatPlannedTasks($tasks, $counter_number)
    {
        $formatted = [];
        $counter = $counter_number;
        $allMoved = $this->getAllMovedTasks();
        $originalDueDates = $this->getOriginalDueDates();
        $backlogedSamples = $this->getAllBacklogedSamples();

        foreach ($tasks as $key => $task) {

            $formatted[$counter]['id'] = $counter + 1;
            $formatted[$counter]['batch'] = $task['task']['batch'];
            $formatted[$counter]['product'] = $task['task']['product'];
            $formatted[$counter]['sample_number'] = $task['task']['sample_number'];
            $formatted[$counter]['description'] = $task['task']['description'];
            $formatted[$counter]['due_date'] = $task['due_date'];
            $formatted[$counter]['date_reviewed'] = $task['task']['date_reviewed'];
            $formatted[$counter]['status'] = 3;
            $formatted[$counter]['duration'] = $task['task']['duration'];
            $formatted[$counter]['moved'] = $this->isMoved($task['task']['sample_number'], $allMoved);
            $formatted[$counter]['original_date'] = isset($originalDueDates[$task['task']['sample_number']]) ? $originalDueDates[$task['task']['sample_number']] : null;
            $formatted[$counter]['is_user_defined'] = $task['task']['is_user_defined'];
            $formatted[$counter]['was_backlog'] = in_array($task['sample_number'], $backlogedSamples) ? 1 : 0;

            $counter++;
        }

        return $formatted;
    }
}

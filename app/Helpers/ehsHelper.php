<?php

namespace App\Helpers;

use App\Helpers\CommanUtility;
use App\Models\SixS;
use App\Models\TypeValue;
use Carbon\Carbon;
use DB;

class ehsHelper
{
    use CommanUtility;

    public function listSixsWithMonths()
    {
        $year = app('request')->input("year");
        $year = !is_null($year) ? $year : carbon::now()->year;

        $query = DB::select(DB::raw(

            "SELECT * , month(`date`) as month  from six_s where year(`date`) = $year "
        ));

        $sixs = json_decode(json_encode($query), true);
        $sixs = array_value_column($sixs, 'month', 'all');

        return $sixs;
    }

    public function getLatestSixS($month)
    {
        $year = app('request')->input("year");
        $year = !is_null($year) ? $year : carbon::now()->year;
        $day = 1;

        $date = Carbon::createFromDate($year, $month, 1)->format('Y-m-d');

        $sixS = SixS::Where('date', '<', $date)->OrderBy('date', 'desc')->first();

        if ($sixS == null) {

            return get_default_sixs($date);
        }

        return $sixS;
    }

    public function currentMonthSixs()
    {
        $monthNumber = Carbon::now()->month;
        $allSixsWithMonths = $this->listSixsWithMonths();
        $latestSixsData = $this->getLatestSixS($monthNumber);
        $latestAvg = $this->calculateAverage($latestSixsData);

        if (isset($allSixsWithMonths[$monthNumber])) {

            $avg = $this->calculateAverage($allSixsWithMonths[$monthNumber]);

        } else {

            $avg = $latestAvg;
        }

        return $avg;
    }

    public function calculateAverage($data)
    {
        if ($data) {

            $average = round(($data['safety'] + $data['order'] + $data['documents'] + $data['materials'] + $data['clothing'] + $data['security'] + $data['discipline']) / 7, 1);

        } else {

            $average = 0;

        }

        return $average;
    }

    public function calculateFreeDays()
    {
        $lastAccident = TypeValue::where('type', 'ehs-accidents')->where('value', '>', 0)->orderBy('date', 'desc')->limit(1)->first();

        $now = Carbon::now();

        if ($lastAccident) {

            $lastDate = Carbon::Parse($lastAccident->date);

            $freeDays = $lastDate->diffInDays($now);

        } else {

            $freeDays = 0;
        }

        return $freeDays;
    }

    public static function getQuarter($qu)
    {
        $quarters = [
            1 => [1, 3],
            2 => [4, 6],
            3 => [7, 9],
            4 => [10, 12],
        ];

        return $quarters[$qu];
    }

    public static function listSixsPerQuarters($year1, $qu1, $year2, $qu2)
    {
        $months = [
            self::getQuarter($qu1),
            self::getQuarter($qu2),
        ];

        $sixs = self::getSixsWithQuarters($year1, $year2, $months);
        $previous_row = self::getPreviousValidSixsEntry($year1, $months[0][0]);
        $data['data'] = self::formatSixs($sixs, $year1, $qu1, $year2, $qu2, $previous_row);
        $data['swiper_steps'] = self::sixsPerQuartersSteps();

        return $data;
    }

    public static function sixsPerQuartersSteps()
    {
        $start = self::getFirstValidSixsEntry();

        if (!$start) {
            return null;
        }

        // dd(ceil(1 / 3), ceil(2 / 3), ceil(3 / 3), ceil(4 / 3), ceil(7 / 3), ceil(10 / 3));
        $now = Carbon::now();
        $current_year = $now->year;
        $current_quarter = ceil($now->month / 3);
        $year_counter = $start['row_year'];
        $date_counter = ceil($start['row_month'] / 3);

        return get_swiper_steps($current_year, $current_quarter, $year_counter, $date_counter, 4, 'quarter');
    }

    public static function formatSixs($sixs, $year1, $qu1, $year2, $qu2, $previous_row = null)
    {
        $data = self::getTrendResponse($qu1, $qu2);

        foreach ($data as &$value) {
            $value['target'] = config('custom.sixs_monthly_target');
        }

        foreach ($sixs as $row) {
            if (array_key_exists($row['row_month'], $data)) {
                $formatted = self::formatSixEntry($row);
                $data[$row['row_month']]['data'] = $formatted;
                $data[$row['row_month']]['average'] = $formatted['average'];
            }
        }

        $now = Carbon::now();
        $current_month = $now->month;
        $current_year = $now->year;
        $data = array_values($data);

        for ($i = 0; $i < count($data); $i++) {
            if (!$data[$i]['data']) {
                if ($i == 0) {
                    if ($previous_row) {
                        $formatted = self::formatSixEntry($previous_row);
                        $data[$i]['data'] = $formatted;
                        $data[$i]['average'] = $formatted['average'];
                    }
                } elseif (($year1 < $current_year && $i <= 2)
                    || ($year2 < $current_year && $i <= 5)
                    || ($data[$i]['month'] <= $current_month)) {
                    // $data[$i]['month'] <= $current_month
                    // elseif($data[$i - 1]['month'] == $data[$i - 1]['data']['month']) {
                    $data[$i]['data'] = $data[$i - 1]['data'];
                    $data[$i]['average'] = $data[$i - 1]['average'];
                }
            }
        }

        return $data;
    }

    public static function formatSixEntry($row)
    {
        return [
            'date' => $row['date'],
            'safety' => $row['safety'],
            'order' => $row['order'],
            'documents' => $row['documents'],
            'materials' => $row['materials'],
            'clothing' => $row['clothing'],
            'security' => $row['security'],
            'discipline' => $row['discipline'],
            'average' => (double) $row['average'],
            'month' => $row['row_month'],
        ];
    }

    public static function getSixsWithQuarters($year1, $year2, $months)
    {
        $sixs = SixS::select('*')->selectRaw('MONTH(date) as row_month')
            ->selectRaw('YEAR(date) as row_year')
            ->selectRaw('ROUND((ifnull(`safety`, 0) + ifnull(`order`, 0) + ifnull(`documents`, 0)
        + ifnull(`materials`, 0) + ifnull(`clothing`, 0)
        + ifnull(`security`, 0) + ifnull(`discipline`, 0)) / 7, 1) as average')
            ->orderBy('date', 'ASC')
            ->havingRaw("
            (
                (row_year = {$year1})
                AND (row_month >= {$months[0][0]} AND row_month <= {$months[0][1]})
            ) OR
            (
                (row_year = {$year2})
                AND (row_month >= {$months[1][0]} AND row_month <= {$months[1][1]})
            )
        ")
        // ->toSql();
            ->get()->toArray();
        // dd($sixs);
        $sixs = array_value_column($sixs, 'row_month', 'all');

        return $sixs;
    }

    public static function getPreviousValidSixsEntry($year, $month)
    {
        $date = Carbon::createFromDate($year, $month, 1);

        $six = SixS::select('*')->selectRaw('MONTH(date) as row_month')
            ->selectRaw('YEAR(date) as row_year')
            ->selectRaw('ROUND((ifnull(`safety`, 0) + ifnull(`order`, 0) + ifnull(`documents`, 0)
        + ifnull(`materials`, 0) + ifnull(`clothing`, 0)
        + ifnull(`security`, 0) + ifnull(`discipline`, 0)) / 7, 2) as average')
            ->where(function ($query) use ($date) {
                $query->where('date', '<=', $date->toDateString());
            })
            ->orderBy('date', 'DESC')
        // ->havingRaw("
        //     (
        //         (row_year = {$year})
        //         AND (row_month = {$month})
        //     )
        // ")
            ->first();
        $six = $six ? $six->toArray() : null;
        return $six;
    }

    public static function getFirstValidSixsEntry()
    {

        $six = SixS::select('*')->selectRaw('MONTH(date) as row_month')
            ->selectRaw('YEAR(date) as row_year')
            ->selectRaw('ROUND((ifnull(`safety`, 0) + ifnull(`order`, 0) + ifnull(`documents`, 0)
        + ifnull(`materials`, 0) + ifnull(`clothing`, 0)
        + ifnull(`security`, 0) + ifnull(`discipline`, 0)) / 7, 2) as average')
            ->orderBy('date', 'ASC')
            ->first();
        $six = $six ? $six->toArray() : null;
        return $six;
    }

    public static function getTrendResponse($qu1, $qu2)
    {
        $months = [
            [
                1 => [
                    'month' => 1,
                    'name' => "Jan",
                    'data' => null,
                    'average' => 0,
                ],
                2 => [
                    'month' => 2,
                    'name' => "Feb",
                    'data' => null,
                    'average' => 0,
                ],
                3 => [
                    'month' => 3,
                    'name' => "Mar",
                    'data' => null,
                    'average' => 0,
                ],
            ],
            [
                4 => [
                    'month' => 4,
                    'name' => "Apr",
                    'data' => null,
                    'average' => 0,
                ],
                5 => [
                    'month' => 5,
                    'name' => "May",
                    'data' => null,
                    'average' => 0,
                ],
                6 => [
                    'month' => 6,
                    'name' => "Jun",
                    'data' => null,
                    'average' => 0,
                ],
            ],
            [
                7 => [
                    'month' => 7,
                    'name' => "Jul",
                    'data' => null,
                    'average' => 0,
                ],
                8 => [
                    'month' => 8,
                    'name' => "Aug",
                    'data' => null,
                    'average' => 0,
                ],
                9 => [
                    'month' => 9,
                    'name' => "Sep",
                    'data' => null,
                    'average' => 0,
                ],
            ],
            [
                10 => [
                    'month' => 10,
                    'name' => "Oct",
                    'data' => null,
                    'average' => 0,
                ],
                11 => [
                    'month' => 11,
                    'name' => "Nov",
                    'data' => null,
                    'average' => 0,
                ],
                12 => [
                    'month' => 12,
                    'name' => "Dec",
                    'data' => null,
                    'average' => 0,
                ],
            ],
        ];

        return $months[$qu1 - 1] + $months[$qu2 - 1];
    }
}

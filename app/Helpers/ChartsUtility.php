<?php

namespace App\Helpers;

use App\Models\AdminTask;
use App\Models\Capacity;
use App\Models\Configuration;
use App\Models\Section;
use Carbon\Carbon;
use Illuminate\Support\Facades\DB;
use App\Helpers\ConfigurationHelper;

trait ChartsUtility
{
    public function getTotalTasksPerMonth()
    {
        // $year = app('request')->input("year");
        // $year = !is_null($year) ? $year : Carbon::now()->year;

        $tasks = DB::select(DB::raw(
            "SELECT COUNT(*) as total , month(due_date) as month, CONCAT(year(date_reviewed),'-',month(date_reviewed)) as `year_month`
            from tasks INNER JOIN task_version on tasks.sample_number = task_version.sample_number
            where tasks.date_reviewed is not null
            GROUP BY `year_month`, `month`"
        ));

        $tasks = json_decode(json_encode($tasks), true);

        $tasks = array_column($tasks, "total", "year_month");

        return $tasks;
    }

    public function getTotalCompletedTasksOnTimePerMonth()
    {
        // $year = app('request')->input("year");
        // $year = !is_null($year) ? $year : Carbon::now()->year;
        $rescheduled_samples = DB::table('task_version')->whereIn('state', [2, 3])->pluck('sample_number')->toArray();

        $tasks = DB::table('tasks')
            ->selectRaw("count(*) total, month(date_reviewed) month")
            ->selectRaw("CONCAT(year(date_reviewed),'-',month(date_reviewed)) as `year_month`")
        // ->whereyear("date_reviewed", $year)
            ->whereNotIn('sample_number', $rescheduled_samples)
            ->groupBy("year_month")
            ->groupBy("month")
            ->get()
            ->toArray();

        $tasks = array_column($tasks, "total", "year_month");

        return $tasks;
    }

    public function getTotalCompletedTasksPerMonth()
    {
        // $year = app('request')->input("year");
        // $year = !is_null($year) ? $year : Carbon::now()->year;

        $tasks = DB::table('tasks')
            ->selectRaw("count(*) total, month(date_reviewed) month")
            ->selectRaw("CONCAT(year(date_reviewed),'-',month(date_reviewed)) as `year_month`")
        // ->whereyear("date_reviewed", $year)
            ->groupBy("year_month")
            ->groupBy("month")
            ->get()
            ->toArray();

        $tasks = json_decode(json_encode($tasks), true);

        $tasks = array_column($tasks, "total", "year_month");

        return $tasks;
    }

    public function getMonthAdherance()
    {
        $year = app('request')->input("year");
        $year = !is_null($year) ? $year : Carbon::now()->year;

        $monthAdherance = DB::select(DB::raw("SELECT finalAchive.month, finalAchive.year, finalAchive.year_month, finalAchive.name,
        finalAchive.total as achived_total, finalMissed.total as missed_total,
        IF (finalMissed.total IS NULL, 
            ROUND((finalAchive.total / finalAchive.total )*100, 2),
            ROUND((finalAchive.total / (finalAchive.total + finalMissed.total ))*100, 2)
        ) AS actual
        FROM
        (
            SELECT sum(c.achived) total, c.month, c.year, concat(c.year, '-', c.month) as `year_month`, c.name
            FROM
            (
              SELECT count(tasks.sample_number) achived, month(tasks.date_reviewed) month, year(tasks.date_reviewed) year, 
              DATE_FORMAT(tasks.date_reviewed, '%b') name FROM tasks
              GROUP BY tasks.date_reviewed
            ) AS c GROUP BY month, year, `year_month`, name
        ) finalAchive
        INNER JOIN
        (
            SELECT sum(mp.missed) total , mp.month, mp.year, concat(mp.year, '-', mp.month) as `year_month`, mp.name
            FROM
            (
               SELECT count(DISTINCT(backlog_logs.sample_number)) missed, month(backlog_logs.due_date) month, year(backlog_logs.due_date) year, 
               DATE_FORMAT(backlog_logs.due_date, '%b') name 
               FROM backlog_logs 
               GROUP BY backlog_logs.due_date
            ) as mp GROUP BY month, year, `year_month`, name
        ) finalMissed
        ON finalAchive.month = finalMissed.month AND finalAchive.year = finalMissed.year
        ORDER BY finalAchive.month"));
        // dd($monthAdherance);
        // $monthAdherance = json_decode(json_encode($monthAdherance), true);
        // $monthAdherance = array_value_column($monthAdherance, "month");

        return collect($monthAdherance);
    }

    public function getCompletedTasksPercent()
    {
        $now = Carbon::now();
        $year = request()->get('year', $now->year);
        $monthExistingAdherance = $this->getMonthAdherance();
        $monthNames = get_month_names();
        // dd($monthExistingAdherance);

        $first_task_entry = DB::table('tasks')->select('date_reviewed')
            ->whereNotNull('date_reviewed')
            ->orderBy('date_reviewed', 'ASC')->first();

        if ($first_task_entry) {
            $months_counter = Carbon::parse($first_task_entry->date_reviewed);
        } else {
            $months_counter = Carbon::now();
        }

        $swiper_steps = [];
        $year_counter = (integer) $months_counter->year;
        $current_year = Carbon::now()->year;
        while ($year_counter <= $current_year) {
            $swiper_steps[] = [
                'year' => $year_counter,
            ];

            if ($year_counter == $current_year) {
                break;
            }
            $year_counter++;
        }

        $data = [];
        $counter = 1;
        $average = 0;
        $sum = 0;
        $section = app('request')->header('section');

        

        for ($i = 1; $i <= 12; $i++) {
            $data[] = [
                'name' => $monthNames[$i],
                'month' => $i,
                'average' => null,
                'trend' => 0,
            ];
        }

        while (($months_counter->year == $now->year && $months_counter->month < $now->month) || $months_counter->year < $now->year) {
            $year_month = "{$months_counter->year}-{$months_counter->month}";
            $month = $months_counter->month;
            $entry = [];

            $adherance = $monthExistingAdherance->where('year_month', $year_month)->first();
            // dd($adherance);
            $entry["trend"] = ($adherance) ? round($adherance->actual) : 0;

            $entry["month"] = $month;
            $entry["name"] = $monthNames[$month];
        //    $entry["target"] = $adherance ? $adherance->target : $target;

            $sum += $entry["trend"];
            $average = ($sum / $counter);
            $entry["average"] = round($average);

            // echo "{{$year_month}} :  trend : {$entry["trend"]}     <--------------------->  sum: {$sum}  / count: {$counter} = average: {$average} <br>";
            if ($months_counter->year == $year) {
                // $data[] = $entry;
                $data[$month - 1] = $entry;
            }

            $counter++;
            $months_counter->addMonths(1);
        }

        return [
            'data' => $data,
            'swiper_steps' => array_reverse($swiper_steps),
        ];
    }

    public function getWeekAdherance()
    {
        $year = app('request')->input("year");
        $year = !is_null($year) ? $year : Carbon::now()->year;

        $weekAdherance = DB::select(DB::raw("SELECT
        finalAchive.week , concat('KW', finalAchive.week) name ,
        IF (finalMissed.total IS NULL, ROUND((finalAchive.total / finalAchive.total )*100, 2) , ROUND((finalAchive.total / (finalAchive.total + finalMissed.total ))*100, 2)) AS actual

        FROM
        (
        SELECT sum(c.achived) total , c.week, c.name
            FROM
            (
              SELECT count(tasks.sample_number) achived, week(tasks.date_reviewed, 1) week, DATE_FORMAT(tasks.date_reviewed, '%u') name FROM tasks
              WHERE year(tasks.date_reviewed) = '$year' GROUP by tasks.date_reviewed
            ) AS c GROUP BY week, name
        ) finalAchive
        LEFT JOIN
        (
            SELECT sum(mp.missed) total , mp.week, mp.name
            FROM
            (
               SELECT count(DISTINCT(backlog_logs.sample_number)) missed, week(backlog_logs.due_date) week, year(backlog_logs.due_date) year, 
               DATE_FORMAT(backlog_logs.due_date, '%b') name 
               FROM backlog_logs
               WHERE year(backlog_logs.due_date) = '$year'
               GROUP BY backlog_logs.due_date
            ) as mp GROUP BY week, name
        ) finalMissed
        ON finalAchive.week = finalMissed.week

        order by finalAchive.week"));

        $weekAdherance = json_decode(json_encode($weekAdherance), true);
        $weekAdherance = array_value_column($weekAdherance, "week");

        return $weekAdherance;
    }

    public function getTargets()
    {
        $targets =
            [
            1 => 90,
            2 => 95,
            3 => 97,
            4 => 98,
            5 => 99,
            6 => 85,
            7 => 75,
            8 => 91,
            9 => 93,
            10 => 94,
            11 => 90,
            12 => 90,
        ];

        return $targets;
    }

    public function getStartEndMonthsByQuarter($quarter)
    {
        $quarterData = [];
        $now = Carbon::now();
        $currentMonth = $now->month;

        switch ($quarter) {

            case '1':

                $quarterData['startMonth'] = 1;
                $quarterData['endMonth'] = 3;
                $quarterData['startWeek'] = 1;
                $quarterData['endWeek'] = 13;

                break;

            case '2':

                $quarterData['startMonth'] = 4;
                $quarterData['endMonth'] = 6;
                $quarterData['startWeek'] = 14;
                $quarterData['endWeek'] = 26;

                break;

            case '3':

                $quarterData['startMonth'] = 7;
                $quarterData['endMonth'] = 9;
                $quarterData['startWeek'] = 27;
                $quarterData['endWeek'] = 39;

                break;

            case '4':

                $quarterData['startMonth'] = 10;
                $quarterData['endMonth'] = 12;
                $quarterData['startWeek'] = 40;
                $quarterData['endWeek'] = 52;

                break;

            default:

                $quarterData = $this->getStartEndMonthsByQuarter(get_current_quarter($currentMonth));
                break;
        }

        return $quarterData;
    }

    public function getTotalTasksPerWeek($startMonth, $endMonth, $year)
    {
        /*$tasks = DB::table('tasks')
        ->selectRaw("count(*) total, week(date_reviewed) wk")
        ->whereRaw("month(date_reviewed) between $startMonth and $endMonth")
        ->whereyear('date_reviewed', $year)
        ->groupBy("wk")
        ->get()
        ->toArray();*/

        $year = app('request')->input("year");
        $year = !is_null($year) ? $year : Carbon::now()->year;

        $tasks = DB::select(DB::raw("SELECT COUNT(*) as total , week(due_date) as wk from tasks INNER JOIN task_version on tasks.sample_number = task_version.sample_number where year(due_date) = '$year' and  tasks.date_reviewed is not null and month(due_date) between '$startMonth' and '$endMonth'  GROUP BY wk "));

        $tasks = json_decode(json_encode($tasks), true);

        $tasks = array_column($tasks, "total", "wk");

        return $tasks;
    }

    public function getTotalCompletedTasksOnTimePerWeek($startMonth, $endMonth, $year)
    {
        $rescheduled_samples = DB::table('task_version')->whereIn('state', [2, 3])->pluck('sample_number')->toArray();

        $tasks = DB::table('tasks')
            ->selectRaw("count(*) total, week(date_reviewed) wk")
            ->whereRaw("month(date_reviewed) between $startMonth and $endMonth")
            ->whereyear('date_reviewed', $year)
            ->whereNotIn('sample_number', $rescheduled_samples)
            ->groupBy("wk")
            ->get()
            ->toArray();

        $tasks = array_column($tasks, "total", "wk");
        return $tasks;
    }

    public function getTotalCompletedTasksPerWeek($startMonth, $endMonth, $year)
    {
        $year = app('request')->input("year");
        $year = !is_null($year) ? $year : Carbon::now()->year;

        $tasks = DB::table('tasks')
            ->selectRaw("count(*) total, week(date_reviewed) wk")
            ->whereRaw("month(date_reviewed) between $startMonth and $endMonth")
            ->whereyear("date_reviewed", $year)
            ->groupBy("wk")
            ->get()
            ->toArray();

        $tasks = json_decode(json_encode($tasks), true);

        $tasks = array_column($tasks, "total", "wk");

        return $tasks;
    }

    public function getCompletedTasksPercentInWeeks()
    {
        $weekAdherance = $this->getWeekAdherance();
        $year = app('request')->input("year");
        $year = !is_null($year) ? $year : Carbon::now()->year;

        $quarter = app('request')->input("quarter");
        $quarter = !is_null($quarter) ? $quarter : carbon::now()->quarter;

        $quarterData = $this->getStartEndMonthsByQuarter($quarter);
        $all = [];

        $counter = 0;

        $currentWeek = Carbon::now()->weekOfYear;
        //$target = $this->getTarget('productivity', 'adherence_targetline');

        for ($i = $quarterData['startWeek']; $i <= $quarterData['endWeek']; $i++) {
            if (isset($weekAdherance[$i])) {
                $all[$counter]["week"] = $weekAdherance[$i]["week"];
                $all[$counter]["name"] = $weekAdherance[$i]["name"];
                $all[$counter]["actual"] = $this->handleWeeklyChartsValue($weekAdherance[$i]["actual"], $i, $year);  /*($i < $currentWeek) ? round($weekAdherance[$i]["actual"]) : 0;*/
            //    $all[$counter]["target"] = $target;
            } else {
                $all[$counter]["week"] = $i;
                $all[$counter]["name"] = "KW" . $i;
                $all[$counter]["actual"] = 0;
               // $all[$counter]["target"] = $target;
            }

            $counter++;
        }

        $first_data = DB::table('tasks')->select('date_reviewed')
            ->selectRaw('YEAR(`date_reviewed`) as row_year')
            ->selectRaw('WEEK(`date_reviewed`, 3) as row_week')
            ->whereNotNull('date_reviewed')
            ->orderBy('date_reviewed', 'ASC')->first();

        // if ($first_data) {
        //     $counter = Carbon::parse($first_data->date_reviewed);
        // } else {
        //     $counter = Carbon::now();
        // }
        // $now = Carbon::now();

        $swiper_steps = [];

        if ($first_data) {
            if ($first_data->row_week >= 1 && $first_data->row_week <= 13) {
                $counter_quarter = 1;
            } elseif ($first_data->row_week >= 14 && $first_data->row_week <= 26) {
                $counter_quarter = 2;
            } elseif ($first_data->row_week >= 27 && $first_data->row_week <= 39) {
                $counter_quarter = 3;
            } else {
                $counter_quarter = 4;
            }

            $this_week = (integer) Carbon::now()->format('W');
            if ($this_week >= 1 && $this_week <= 13) {
                $quarter_limit = 1;
            } elseif ($this_week >= 14 && $this_week <= 26) {
                $quarter_limit = 2;
            } elseif ($this_week >= 27 && $this_week <= 39) {
                $quarter_limit = 3;
            } else {
                $quarter_limit = 4;
            }

            $first_year = $first_data->row_year;
            while ($first_year <= Carbon::now()->year) {
                $limit = $first_year == Carbon::now()->year ? $quarter_limit : 4;
                for ($i = $counter_quarter; $i <= $limit; $i++) {
                    $swiper_steps[] = [
                        'year' => $first_year,
                        'quarter' => $i,
                    ];
                }

                $first_year++;
                $counter_quarter = 1;
            }
        } else {
            $this_week = (integer) Carbon::now()->format('W');
            if ($this_week >= 1 && $this_week <= 13) {
                $counter_quarter = 1;
            } elseif ($this_week >= 14 && $this_week <= 26) {
                $counter_quarter = 2;
            } elseif ($this_week >= 27 && $this_week <= 39) {
                $counter_quarter = 3;
            } else {
                $counter_quarter = 4;
            }

            $swiper_steps[] = [
                'year' => $year,
                'quarter' => $counter_quarter,
            ];
        }

        return [
            'data' => $all,
            'swiper_steps' => array_reverse($swiper_steps),
        ];
    }

    public function getTotalTasksInCurrentDay($today)
    {
        $task = DB::table('task_version')
            ->selectRaw("count(*) total, due_date")
            ->where("due_date", $today)
            ->groupBy("due_date")
            ->first();

        return !is_null($task) ? $task->total : 0;
    }

    public function getTotalCompletedTasksOnTimeInCurrentDay($today)
    {
        $task = DB::table('tasks')
            ->selectRaw("count(*) total, date_reviewed")
            ->where("date_reviewed", $today)
            ->groupBy("date_reviewed")
            ->first();

        return !is_null($task) ? $task->total : 0;
    }

    public function getCompletedTasksPercentInCurrentDay()
    {
        $today = Carbon::today()->format("Y-m-d");

        $total = $this->getTotalTasksInCurrentDay($today);
        $completed = $this->getTotalCompletedTasksOnTimeInCurrentDay($today);

        $percent = 0;

        if ($total != 0) {
            $percent = round(($completed / $total) * 100, 2);
        }

        return $percent;
    }

    public function demandRate()
    {
        $year = app('request')->input("year");
        $year = !is_null($year) ? $year : Carbon::now()->year;

        $quarter = app('request')->input("quarter");
        $quarter = !is_null($quarter) ? $quarter : carbon::now()->quarter;

        $quarterData = $this->getStartEndMonthsByQuarter($quarter);
        $weeks = $this->taskTotalDurationWeeks($quarterData['startWeek'], $quarterData['endWeek'], $year);
        $currentWeek = Carbon::now()->weekOfYear;

        $result = [];
      //  $target = $this->getTarget('productivity', 'levelled_demand_rate_targetline');

        for ($i = $quarterData['startWeek']; $i <= $quarterData['endWeek']; $i++) {

            $value = $this->handleDemandRatesWeeklyChartValue($weeks, $i, $year);
            //$value = isset($weeks[$i]) && ($i < $currentWeek) ? $weeks[$i] / 95 : 0;
            $value *= 100;
            $value = round($value, 2);

            if ($value == 0 && $year == Carbon::now()->year) {
                if ($quarter == Carbon::now()->quarter && $i >= Carbon::now()->format('W')) {
                    $value = null;
                }
            }

            array_push($result, ["week" => $i, "value" => $value/*, 'target' => $target*/]);
        }

        $first_data = $this->firstTaskTotalDuration();
        $swiper_steps = [];

        if ($first_data) {
            if ($first_data->row_week >= 1 && $first_data->row_week <= 13) {
                $counter_quarter = 1;
            } elseif ($first_data->row_week >= 14 && $first_data->row_week <= 26) {
                $counter_quarter = 2;
            } elseif ($first_data->row_week >= 27 && $first_data->row_week <= 39) {
                $counter_quarter = 3;
            } else {
                $counter_quarter = 4;
            }

            $this_week = (integer) Carbon::now()->format('W');
            if ($this_week >= 1 && $this_week <= 13) {
                $quarter_limit = 1;
            } elseif ($this_week >= 14 && $this_week <= 26) {
                $quarter_limit = 2;
            } elseif ($this_week >= 27 && $this_week <= 39) {
                $quarter_limit = 3;
            } else {
                $quarter_limit = 4;
            }

            $first_year = $first_data->row_year;
            while ($first_year <= Carbon::now()->year) {
                $limit = $first_year == Carbon::now()->year ? $quarter_limit : 4;
                for ($i = $counter_quarter; $i <= $limit; $i++) {
                    $swiper_steps[] = [
                        'year' => $first_year,
                        'quarter' => $i,
                    ];
                }

                $first_year++;
                $counter_quarter = 1;
            }
        } else {
            $this_week = (integer) Carbon::now()->format('W');
            if ($this_week >= 1 && $this_week <= 13) {
                $counter_quarter = 1;
            } elseif ($this_week >= 14 && $this_week <= 26) {
                $counter_quarter = 2;
            } elseif ($this_week >= 27 && $this_week <= 39) {
                $counter_quarter = 3;
            } else {
                $counter_quarter = 4;
            }

            $swiper_steps[] = [
                'year' => $year,
                'quarter' => $counter_quarter,
            ];
        }

        return json_response()->success([
            'data' => $result,
            'swiper_steps' => array_reverse($swiper_steps),
        ]);
    }

    public function demandRatePerWeek()
    {
        $year = request()->input("year", Carbon::now()->year);
        $week = request()->input("week", carbon::now()->format('W'));

        $rows = $this->taskTotalDurationPerWeek($year, $week);
        $now = Carbon::now();
        $then = $now->setISODate($year, $week);
        $first_day_of_week = $then->startOfWeek();
        $target = $this->getTarget('productivity', 'levelled_demand_rate_targetline');

        $formatted = [];
        for ($i = 1; $i <= 7; $i++) {
            $row = $rows->where('row_day_of_week', $i)->first();
            if ($row) {
                $formatted[] = [
                    'index' => $row->row_day_of_week,
                    'value' => (double) $row->value,
                    'year' => $row->row_year,
                    'month' => $row->row_month,
                    'week' => $row->row_week,
                    'day' => $row->row_day,
                    'date' => $row->date_reviewed,
                    'target' => $target,
                ];
            } else {
                $formatted[] = [
                    'index' => $i,
                    'value' => 0,
                    'year' => $first_day_of_week->year,
                    'month' => $first_day_of_week->month,
                    'week' => (integer) $first_day_of_week->format('W'),
                    'day' => $first_day_of_week->day,
                    'date' => $first_day_of_week->toDateString(),
                    'target' => $target,
                ];
            }
            $first_day_of_week->addDays(1);
        }

        $first_data = $this->firstTaskTotalDuration();
        $swiper_steps = get_swiper_steps($year, $week, $first_data->row_year, $first_data->row_week, 52, 'week');

        return json_response()->success([
            'data' => $formatted,
            'swiper_steps' => $swiper_steps,
        ]);
    }

    public function demandRatePerMonth()
    {
        $year = request()->input("year", Carbon::now()->year);
        $month = request()->input("month", carbon::now()->month);
        // dd($year);
        $rows = $this->taskTotalDurationPerMonth($year, $month);

        $first_day_of_month = Carbon::create($year, $month, 1);
        $last_day_of_month = $first_day_of_month->copy()->endOfMonth();
        // dd($first_day_of_month, $last_day_of_month);
        $formatted = [];
        $i = 1;

        while ($first_day_of_month->lessThanOrEqualTo($last_day_of_month)) {
            $row = $rows->where('row_day', $first_day_of_month->day)->first();
            if ($row) {
                $formatted[$i - 1] = [
                    'index' => $i,
                    'value' => (double) $row->value,
                    'year' => $row->row_year,
                    'month' => $row->row_month,
                    'week' => $row->row_week,
                    'day' => $row->row_day,
                    'date' => $row->date_reviewed,
                ];
            } else {
                $formatted[$i - 1] = [
                    'index' => $i,
                    'value' => 0,
                    'year' => $first_day_of_month->year,
                    'month' => $first_day_of_month->month,
                    'week' => (integer) $first_day_of_month->format('W'),
                    'day' => $first_day_of_month->day,
                    'date' => $first_day_of_month->toDateString(),
                ];
            }
            $i++;
            $first_day_of_month->addDays(1);
        }

        $first_data = $this->firstTaskTotalDuration();
        $swiper_steps = get_swiper_steps($year, $month, $first_data->row_year, $first_data->row_month, 12, 'month');

        return json_response()->success([
            'data' => $formatted,
            'swiper_steps' => $swiper_steps,
        ]);
    }

    public function demandRatePerYear()
    {
        $year = request()->input("year", Carbon::now()->year);

        $rows = $this->taskTotalDurationPerYear($year);
        //dd($rows);
        $months_names = get_month_names();
       // $target = $this->getTarget('productivity', 'levelled_demand_rate_targetline');
        $formatted = [];

        for ($i = 1; $i <= 12; $i++) {
            $row = $rows->where('row_month', $i)->first();

            if ($row) {
                $formatted[$i - 1] = [
                    'name' => $months_names[$i],
                    'month' => $i,
                    'value' => round($row->value),
                  //  'target' => $target,
                ];
            } else {
                $formatted[$i - 1] = [
                    'name' => $months_names[$i],
                    'month' => $i,
                    'value' => 0,
                  //  'target' => $target,
                ];
            }
        }

        foreach ($formatted as &$item) {
            if ($item['value'] == 0 && $year == Carbon::now()->year) {
                $item['value'] = null;
            }
        }

        $first_data = $this->firstTaskTotalDuration();
        $swiper_steps = [];

        if ($first_data) {
            $first_year = $first_data->row_year;
            while ($first_year <= Carbon::now()->year) {
                $swiper_steps[] = [
                    'year' => $first_year,
                ];
                $first_year++;
            }
        } else {
            $swiper_steps[] = [
                'year' => $year,
            ];
        }

        return json_response()->success([
            'data' => $formatted,
            'swiper_steps' => array_reverse($swiper_steps),
        ]);
    }

    public function productivityChart()
    {
        $year = app('request')->input("year");
        $year = !is_null($year) ? $year : Carbon::now()->year;

        $quarter = app('request')->input("quarter");
        $quarter = !is_null($quarter) ? $quarter : carbon::now()->quarter;

        $quarterData = $this->getStartEndMonthsByQuarter($quarter);
        $weeks = $this->taskTotalDurationWeeks($quarterData['startWeek'], $quarterData['endWeek'], $year);
        $capacities = $this->capaictyTotalDurationGroupedByWeek($year);
        $task_admin = $this->taskAdminTotalDuration($year);
        $admin_grouped = $this->getAdminTasksCapacityGroupedByCategoryAndWeek();
        $currentWeek = Carbon::now()->weekOfYear;
        $currentYear = Carbon::now()->year;

     //   $target = $this->getTarget('productivity', 'work_admin_balance_targetline');
        $result = [];

        for ($i = $quarterData['startWeek']; $i <= $quarterData['endWeek']; $i++) {

            $adminTask = isset($admin_grouped[$i]['AdminTask']) ? $admin_grouped[$i]['AdminTask'] : 0;
            $meeting = isset($admin_grouped[$i]['Meeting']) ? $admin_grouped[$i]['Meeting'] : 0;
            $nonTestTask = isset($admin_grouped[$i]['NonTestTask']) ? $admin_grouped[$i]['NonTestTask'] : 0;
            $training = isset($admin_grouped[$i]['Training']) ? $admin_grouped[$i]['Training'] : 0;
            $totalAdmin = isset($task_admin[$i]) ? $task_admin[$i] : 0;

            $capacity = isset($capacities[$i]) ? $capacities[$i] : 1;
            $doneDurations = isset($weeks[$i]) ? $weeks[$i] : 0;
            $capacityBlock = $capacity - $totalAdmin;

            if($capacityBlock + $nonTestTask+ $meeting + $training + $adminTask == 0){

                $cal = 0;

            }else{

                $cal = ($doneDurations + $nonTestTask + $meeting + $training + ($adminTask * 0.8)) / ($capacityBlock + $nonTestTask + $meeting + $training + $adminTask);
                $cal *= 100;
                $cal = round($cal);
            }
            
            if(($year > $currentYear) || ($year == $currentYear && $i >= $currentWeek)){
                array_push($result, ["week" => $i, "value" => 0/*, 'target' => $target*/]);
            }else{
                array_push($result, ["week" => $i, "value" => $cal/*, 'target' => $target*/]);
            }

/*            if ($i < $currentWeek) {
                array_push($result, ["week" => $i, "value" => $cal, 'target' => $target]);
            } else {
                array_push($result, ["week" => $i, "value" => 0, 'target' => $target]);
            }*/
        }

        $first_data = $this->firstTaskAdminTotalDuration();
        $swiper_steps = [];

        if ($first_data) {
            if ($first_data->row_week >= 1 && $first_data->row_week <= 13) {
                $counter_quarter = 1;
            } elseif ($first_data->row_week >= 14 && $first_data->row_week <= 26) {
                $counter_quarter = 2;
            } elseif ($first_data->row_week >= 27 && $first_data->row_week <= 39) {
                $counter_quarter = 3;
            } else {
                $counter_quarter = 4;
            }

            $this_week = (integer) Carbon::now()->format('W');
            if ($this_week >= 1 && $this_week <= 13) {
                $quarter_limit = 1;
            } elseif ($this_week >= 14 && $this_week <= 26) {
                $quarter_limit = 2;
            } elseif ($this_week >= 27 && $this_week <= 39) {
                $quarter_limit = 3;
            } else {
                $quarter_limit = 4;
            }

            $first_year = $first_data->row_year;
            while ($first_year <= Carbon::now()->year) {
                $limit = $first_year == Carbon::now()->year ? $quarter_limit : 4;
                for ($i = $counter_quarter; $i <= $limit; $i++) {
                    $swiper_steps[] = [
                        'year' => $first_year,
                        'quarter' => $i,
                    ];
                }

                $first_year++;
                $counter_quarter = 1;
            }
        } else {
            $this_week = (integer) Carbon::now()->format('W');
            if ($this_week >= 1 && $this_week <= 13) {
                $counter_quarter = 1;
            } elseif ($this_week >= 14 && $this_week <= 26) {
                $counter_quarter = 2;
            } elseif ($this_week >= 27 && $this_week <= 39) {
                $counter_quarter = 3;
            } else {
                $counter_quarter = 4;
            }

            $swiper_steps[] = [
                'year' => $year,
                'quarter' => $counter_quarter,
            ];
        }

        return json_response()->success([
            'data' => $result,
            'swiper_steps' => array_reverse($swiper_steps),
        ]);
    }

    public function productivityChartPerYear()
    {
        $year = request()->input("year", Carbon::now()->year);
        $admin_grouped = $this->getAdminTasksCapacityGroupedByCategoryAndMonth($year);
        $capacities = $this->capaictyTotalDurationGroupedByMonth($year);
        $doneHours = $this->taskTotalDurationGroupedByMonth($year);
        $currentMonth = Carbon::now()->month;
        $months_names = get_month_names();
   //     $target = $this->getTarget('productivity', 'work_admin_balance_targetline');
        $formatted = [];

        for ($i = 1; $i <= 12; $i++) {

            $adminTask = isset($admin_grouped[$i]['AdminTask']) ? $admin_grouped[$i]['AdminTask'] : 0;
            $meeting = isset($admin_grouped[$i]['Meeting']) ? $admin_grouped[$i]['Meeting'] : 0;

            $nonTestTask = isset($admin_grouped[$i]['NonTestTask']) ? $admin_grouped[$i]['NonTestTask'] : 0;

            $training = isset($admin_grouped[$i]['Training']) ? $admin_grouped[$i]['Training'] : 0;
            $totalAdmin = $adminTask + $meeting + $nonTestTask + $training;

            $capacity = isset($capacities[$i]) ? $capacities[$i] : 1;

            $doneDurations = isset($doneHours[$i]) ? $doneHours[$i] : 0;

            $capacityBlock = $capacity - $totalAdmin;
                                                //dd($capacityBlock);

            $cal = ($doneDurations + $nonTestTask + $meeting + $training + ($adminTask * 0.8)) / ($capacityBlock + $nonTestTask + $meeting + $training + $adminTask);
            $cal *= 100;
            $cal = round($cal);

            $formatted[$i - 1] = [
                'name' => $months_names[$i],
                'month' => $i,
                'value' => $this->handleMonthlyChartsValue($cal, $i, $year),
             //   'target' => $target,
            ];
        }

        $first_data = $this->firstTaskAdminTotalDuration();
        $swiper_steps = [];

        if ($first_data) {
            $first_year = $first_data->row_year;
            while ($first_year <= Carbon::now()->year) {
                $swiper_steps[] = [
                    'year' => $first_year,
                ];
                $first_year++;
            }
        } else {
            $swiper_steps[] = [
                'year' => $year,
            ];
        }

        return json_response()->success([
            'data' => $formatted,
            'swiper_steps' => array_reverse($swiper_steps),
        ]);
    }

    public function getWeeks($numWeeks)
    {
        $weeks = [];

        $now = Carbon::now();
        $current_week = "$now->weekOfYear";

        for ($i = $current_week - $numWeeks + 1; $i <= $current_week; $i++) {
            $week = $this->getWeekDates($i);
            $week["week_num"] = $i;
            //array_push($week , 'week_num'=>$i);
            array_push($weeks, $week);
        }

        return $weeks;
    }

    public function taskAdminTotalDuration($year)
    {
        $task_admin = AdminTask::selectRaw("sum(duration) total_duration, week(date, 1) week")
            ->whereyear('date', $year)
            ->groupBy("week")
            ->get()
            ->toArray();

        $task_admin = json_decode(json_encode($task_admin), true);

        $task_admin = array_column($task_admin, 'total_duration', 'week');

        return $task_admin;
    }

    public function getAdminTasksCapacityGroupedByCategoryAndWeek($year = null)
    {
        $year = $year ? $year : Carbon::now()->year;
        $adminTasksCapacities = DB::select(DB::raw("SELECT `category`, SUM(`duration`) as `total`, WEEK(`date`, 3) as `week`, YEAR(`date`) as `year` from admin_tasks WHERE YEAR(`date`) = $year group by `category`, `week`, `year`"));
        // $adminTasksCapacities = DB::select(DB::raw("SELECT category, SUM(`duration`) as total, week(`date`,1) as week from admin_tasks group by week,category"));
        $adminTasksCapacities = json_decode(json_encode($adminTasksCapacities), true);
        $adminTasksCapacities = collect($adminTasksCapacities);
        $adminTasksCapacities = $adminTasksCapacities->groupBy('week')->toArray();
        $formatted = [];

        foreach ($adminTasksCapacities as $week => $data) {
            foreach ($data as $key => $value) {

                $category = $value['category'];
                $total = $value['total'];
                $formatted[$week][$category] = $total;
            }
        }

        return $formatted;
    }

    public function getAdminTasksCapacityGroupedByCategoryAndMonth($year)
    {
        $adminTasksCapacities = DB::select(DB::raw("SELECT category, SUM(`duration`) as total, month(`date`) as month from admin_tasks where year(`date`) = $year group by month,category"));
        $adminTasksCapacities = json_decode(json_encode($adminTasksCapacities), true);
        $adminTasksCapacities = collect($adminTasksCapacities);
        $adminTasksCapacities = $adminTasksCapacities->groupBy('month')->toArray();
        $formatted = [];

        foreach ($adminTasksCapacities as $month => $data) {
            foreach ($data as $key => $value) {

                $category = $value['category'];
                $total = $value['total'];
                $formatted[$month][$category] = $total;
            }
        }

        return $formatted;
    }

    public function taskAdminTotalDurationPerYear($year)
    {
        $task_admin = AdminTask::selectRaw("sum(duration) value, MIN(date) as `date`, MONTH(`date`) as row_month")
            ->whereyear('date', $year)
            ->orderBy('row_month')
            ->groupBy("row_month")
            ->get();

        return $task_admin;
    }

    public function firstTaskAdminTotalDuration()
    {
        $task_admin = AdminTask::selectRaw("sum(duration) value, `date`, MONTH(`date`) as row_month, YEAR(`date`) as row_year, WEEK(`date`, 3) as row_week")
            ->groupBy("date")
            ->orderBy('date')
            ->first();

        return $task_admin;
    }

    public function capaictyTotalDurationGroupedByWeek($year)
    {
        $holydays = DB::table('holidays')->pluck('date')->toArray();
        $capacity = Capacity::selectRaw("sum(capacity) total, week(date,1) week")
            ->whereNotIn('date', $holydays)
            ->whereyear('date', $year)
            ->groupBy("week")
            ->get()
            ->toArray();

        $capacity = json_decode(json_encode($capacity), true);

        $capacity = array_column($capacity, 'total', 'week');

        return $capacity;
    }

    public function capaictyTotalDurationGroupedByMonth($year)
    {
        $capacity = Capacity::selectRaw("sum(capacity) total, month(date) month")
            ->whereyear('date', $year)
            ->groupBy("month")
            ->get()
            ->toArray();

        $capacity = json_decode(json_encode($capacity), true);

        $capacity = array_column($capacity, 'total', 'month');

        return $capacity;
    }

    public function taskTotalDurationWeeks($startWeek, $endWeek, $year)
    {
        $durations = DB::table('tasks')
            ->join('task_version', 'task_version.sample_number', '=', 'tasks.sample_number')
            ->selectRaw('due_date, WEEK(due_date, 3) as `week`, MAX(tasks.duration) as duration, task_version.sample_number')
            ->whereNotNull('date_reviewed')
            ->whereYear('due_date', $year)
            ->where('state', 1)
            ->whereRaw("WEEK(due_date, 3) >= $startWeek AND WEEK(due_date, 3) <= $endWeek")
            ->groupBy('due_date')
            ->groupBy('task_version.sample_number')
            ->get();
        $result = [];
        foreach ($durations as $row) {
            $result[$row->week] = isset($result[$row->week]) ? $result[$row->week] + $row->duration : $row->duration;
        }

        return $result;
    }

    public function taskTotalDurationGroupedByMonth($year)
    {
        $durations = DB::select(DB::raw("SELECT SUM(duration) as total , month(date_reviewed) as month from
         tasks WHERE(date_reviewed is not null AND date_reviewed != '' ) AND year(date_reviewed) = '$year' GROUP BY month"));

        $durations = json_decode(json_encode($durations), true);

        $durations = array_column($durations, "total", "month");

        return $durations;
    }

    public function taskTotalDuration()
    {
        $sql = DB::table("tasks")
            ->select('date_reviewed')
            ->selectRaw('SUM(duration) as value')
            ->selectRaw('week(date_reviewed, 3) as row_week')
            ->selectRaw('YEAR(date_reviewed) row_year')
            ->selectRaw('MONTH(date_reviewed) row_month')
            ->selectRaw('(WEEKDAY(date_reviewed) + 1) row_day_of_week')
            ->selectRaw('DAY(date_reviewed) row_day')
            ->whereNotNull('date_reviewed')
            ->where('date_reviewed', '<>', '')
        //  ->whereNotIn('date', $holydays)
            ->groupBy('date_reviewed');

        return $sql;
    }

    public function taskTotalDurationPerWeek($year, $week)
    {
        $rows = $this->taskTotalDuration()
            ->whereRaw("YEAR(date_reviewed) = '$year'")
            ->whereRaw("week(date_reviewed, 3) = '$week'")
            ->orderBy('date_reviewed')
            ->get();

        return $rows;
    }

    public function taskTotalDurationPerMonth($year, $month)
    {
        $rows = $this->taskTotalDuration()
            ->whereRaw("YEAR(date_reviewed) = '$year'")
            ->whereRaw("MONTH(date_reviewed) = '$month'")
            ->orderBy('date_reviewed')
            ->get();

        return $rows;
    }

    public function taskTotalDurationPerYear($year)
    {
        $rows = DB::table("tasks")
            ->selectRaw('MIN(date_reviewed) as date_reviewed')
            ->selectRaw('SUM(duration) as value')
            ->selectRaw('MONTH(date_reviewed) row_month')
            ->whereNotNull('date_reviewed')
            ->where('date_reviewed', '<>', '')
            ->whereRaw("YEAR(date_reviewed) = '$year'")
            ->orderBy('date_reviewed')
            ->groupBy('row_month')
            ->get();

        return $rows;
    }

    public function firstTaskTotalDuration()
    {
        $row = $this->taskTotalDuration()
            ->orderBy('date_reviewed')
            ->first();

        return $row;
    }

    public function getTarget($section_name, $config_key)
    {
        $section_id = Section::where('name', $section_name)->value('id');
        $target = Configuration::where('section_id', $section_id)->where('key', $config_key)->value('value');
        $target = (int) $target;
        return $target;
    }
    public function handleMonthlyChartsValue($value, $monthNumber, $year)
    {
        $currentMonth = Carbon::now()->month;
        $currentYear = Carbon::now()->year;

        if (($year > $currentYear) || ($year == $currentYear && $monthNumber >= $currentMonth)){

            $value  = 0 ;

        }
        return $value;
    }

    public function handleWeeklyChartsValue($value, $weekNumber, $year)
    {
        $currentWeek = Carbon::now()->weekOfYear;
        $currentYear = Carbon::now()->year;

        if (($year > $currentYear) || ($year == $currentYear && $weekNumber >= $currentWeek)){

            $value  = 0 ;

        }
        return round($value);
    }

    public function handleDemandRatesWeeklyChartValue($data, $weekNumber, $year){

        $currentWeek = Carbon::now()->weekOfYear;
        $currentYear = Carbon::now()->year;


        if (($year > $currentYear) || ($year == $currentYear && $weekNumber >= $currentWeek) || (!isset($data[$weekNumber]))){

            $value  = 0 ;

        }else{

            $value = $data[$weekNumber] / 95 ;
        }

        return $value;
    }


}

<?php

namespace App\Helpers;

use App\Helpers\CommanUtility;
use App\Helpers\DatesUtility;
use App\Models\File;
use App\Models\Import;
use App\Models\RescheduleBacklogCount;
use App\Models\RescheduleBacklogSamplesCount;
use App\Models\Subtask;
use App\Models\Task;
use App\Models\TaskVersion;
use Carbon\Carbon;
use Excel;
use Illuminate\Support\Facades\DB;

class ImportHelper
{
    use CommanUtility, DatesUtility;

    protected $errors;
    protected $now;
    protected $logger;

    public function __construct()
    {
        $this->errors = [];
        $this->now = Carbon::now();
        $this->logger = new LogingHelper($this->now);
    }

    public function importDurationFromExcel($request)
    {
        $path = $request->file->getRealPath();

        $data = Excel::selectSheetsByIndex(0)->load($path, function ($reader) {
            $reader->get();
        });

        $data = $data->toArray();

        $data = $this->formatImportedDurations($data);

        DB::table('durations')->truncate();
        DB::table('durations')->insert($data);

        return true;
    }

    private function formatImportedDurations($data)
    {
        return array_map(function ($row) {
            return [
                'project' => isset($row['project']) ? $row['project'] : '',
                'product' => $row['product'],
                'analysis' => $row['analysis'],
                'description' => $row['description'],
                'total_duration' => $row['aufwand_min'] / 60,
                'duration' => $row['anzahl_der_tage'] > 0 ? ($row['aufwand_min'] / $row['anzahl_der_tage']) / 60 : $row['aufwand_min'] / 60, // in hours
                'days' => $row['anzahl_der_tage'],
                'name' => $row['name'],
                'login_date' => $row['login_date'],
                'location' => $row['location'],

                'created_at' => $this->now->toDateTimeString(),
                'updated_at' => $this->now->toDateTimeString(),
            ];
        }, $data);
    }

    public function getUniqueTasks($tasks)
    {
        $uniqueTasks = [];
        $samples_tasks = collect($tasks)->groupBy('sample_number');
        $products_durations = $this->getDurationGroupProduct();
        $projects_durations = $this->getDurationGroupProject();

        foreach ($samples_tasks as $sample => $tasks_by_sample) {
            $found = false;

            foreach ($tasks_by_sample as $task) {
                if ($found) {
                    continue;
                }

                if ($task['product'] == '' && $task['project'] == '') {
                    $task['duration'] = 0;
                    $task['days'] = 1;
                    array_push($uniqueTasks, $task);
                    $found = true;
                    continue;
                }

                if ($task['project'] && isset($projects_durations[$task['project']])) {
                    $durations = $projects_durations[$task['project']];
                    $find_duration_with_analysis = $durations->firstWhere('analysis', $task['analysis']);

                    if ($find_duration_with_analysis) {
                        $task['duration'] = $find_duration_with_analysis->duration;
                        $task['days'] = $find_duration_with_analysis->days;
                        // $task['part'] = $find_duration_with_analysis->days;
                        array_push($uniqueTasks, $task);
                        $found = true;
                        continue;
                    }
                }

                if ($task['product'] && isset($products_durations[$task['product']])) {
                    $durations = $products_durations[$task['product']];
                    $find_duration_with_analysis = $durations->firstWhere('analysis', $task['analysis']);

                    if ($find_duration_with_analysis) {
                        $task['duration'] = $find_duration_with_analysis->duration;
                        $task['days'] = $find_duration_with_analysis->days;
                        // $task['part'] = $find_duration_with_analysis->days;
                        array_push($uniqueTasks, $task);
                        $found = true;
                        continue;
                    }
                }
            }

            if (!$found) {
                $task = $tasks_by_sample->first();
                $task['duration'] = 0;
                $task['days'] = 1;
                array_push($uniqueTasks, $task);
                continue;
            }
        }
        // $test = collect($uniqueTasks);
        // dd($test->where('duration', 0), $test->where('duration', '<>', 0));

        return $uniqueTasks;
    }

    public function getDurationGroupProject()
    {
        $durations = DB::table("durations")
            ->selectRaw("`project`, `analysis`, `duration`, `days`")
            ->get();
        return $durations->groupBy('project');
    }

    public function getDurationGroupProduct()
    {
        $durations = DB::table("durations")
            ->selectRaw("`product`, `analysis`, `duration`, `days`")
            ->get();
        return $durations->groupBy('product');
        // $durations = DB::table("durations")
        //     ->selectRaw("CONCAT(`product`, '-', `analysis`) as identifier, `duration`, `days`")
        //     ->get();

        // return $durations->groupBy('identifier')->toArray();
    }

    public function insertTasksXml($request, $is_request)
    {
        $path = "";
        $insertImported = [];
        $maxId = File::OrderBy('id', 'desc')->value('id');
        $maxId = ($maxId) ? $maxId : 0;

        if ($is_request) {

            $file = $request->file('file');
            $path = $file->getRealPath();
            $name = $file->getClientOriginalName();
            
            $file->storeAs("public/xmls", $name);
            // $uploaded_path = storage_path("app/public/xmls/$name");

            $import = $this->importXml($path, $name);

            $user_id = app('request')->input("user_id");
            $insertImported[0]["id"] = $maxId + 1;
            $insertImported[0]["name"] = $name;
            $insertImported[0]['imported_time'] = Carbon::now()->format("Y-m-d H:i:s");
            $insertImported[0]['status'] = $import['status'];
            $insertImported[0]['message'] = $import['message'];
            $insertImported[0]['is_imported_by_system'] = 0;
            $insertImported[0]['user_id'] = isset($user_id) ? $user_id : null;

            DB::table('imported')->insert($insertImported);

        } else {

            $dir = env('XML_PATH', "/var/www/html/xml-tachosil");
            $counter = 0;

            foreach ($request as $value) {

                $path = $dir . "/" . $value;
                echo "Importing: $path \n";

                // validate file type
                $import = $this->importXml($path);
                $insertImported[$counter]["id"] = $maxId + 1;
                $insertImported[$counter]["name"] = $value;
                $insertImported[$counter]['imported_time'] = Carbon::now()->format("Y-m-d H:i:s");
                $insertImported[$counter]['status'] = $import['status'];
                $insertImported[$counter]['message'] = $import['message'];
                $counter++;
                $maxId++;
            }

            DB::table('imported')->insert($insertImported);
        }

        if (!empty($this->errors)) {
            $operation_status = array_reduce($insertImported, function ($carry, $item) {
                $carry = $carry || $item['status'];
                return $carry;
            });

            $valid_date = (!empty($this->errors) && !empty($insertImported) && $operation_status) || (empty($this->errors) && empty($insertImported));
            $this->logger->report($this->errors, $valid_date);
        }

        //return empty($this->errors) ? true : false;
        return $insertImported;
    }

    public function importXml($request, $file_name = null)
    {
        $path = $request;

        try {

            $xmlObjectList = simplexml_load_file($path);
            $data = [];
            $index = 0;

            foreach ($xmlObjectList as $key => $xmlObject) {
                $temp["sample_number"] = (string) $xmlObject->SAMPLE_NUMBER; // required
                $temp["batch"] = (string) $xmlObject->BATCH_NAME;
                $temp["description"] = (string) $xmlObject->DESCRIPTION;
                $temp["product"] = (string) $xmlObject->PRODUCT;
                $temp["stage"] = (string) $xmlObject->STAGE;
                $temp["sample_status"] = (string) $xmlObject->SAMPLE_STATUS; // required
                $temp["test_status"] = (string) $xmlObject->TEST_STATUS; // required
                $temp["reported_name"] = (string) $xmlObject->REPORTED_NAME;
                $temp["analysis"] = (string) $xmlObject->ANALYSIS;
                $temp["project"] = (string) $xmlObject->PROJECT;

                $duedate = (string) $xmlObject->SAMPLE_DUE_DATE;
                $temp["due_date"] = $duedate ? (new Carbon($duedate))->format("Y-m-d") : null; // required

                $logindate = (string) $xmlObject->SAMPLE_LOGIN_DATE; // required
                $temp["login_date"] = ($logindate == "") ? null : (new Carbon($logindate))->format("Y-m-d");

                $receivedate = (string) $xmlObject->SAMPLE_RECEIVE_DATE;
                $temp["receive_date"] = ($receivedate == "") ? null : (new Carbon($receivedate))->format("Y-m-d");

                $completedate = (string) $xmlObject->TEST_DATE_COMPLETED;
                $temp["date_completed"] = ($completedate == "") ? null : (new Carbon($completedate))->format("Y-m-d");

                $reviewdate = (string) $xmlObject->TEST_DATE_REVIEWED;
                $temp["date_reviewed"] = ($reviewdate == "") ? null : (new Carbon($reviewdate))->format("Y-m-d");
                $temp['part'] = 1;

                $status = $this->logger->validate($temp);
                $index++;

                if (!$status['status']) {
                    $this->errors[] = [
                        'file_path' => $file_name ? $file_name : $request,
                        'row' => $index,
                        'errors' => $status['errors'],
                        'date' => $this->now->toDateTimeString(),
                    ];
                    continue;
                }

                $data[$index] = $temp;
            }

            if (!empty($this->errors) && empty($data)) {
                return ['status' => false, 'message' => 'Empty file/No valid data.'];
            } else if (empty($data)) {
                return ['status' => false, 'message' => 'Empty file.'];
            }

            $separated = $this->separateTasksSubTasks($data);
            $subTasks = $separated['subtask'];

            $uninqueTasks = $this->getUniqueTasks($separated['tasks']);

            $this->logic($uninqueTasks, $subTasks);
        } catch (\Exception $e) {

            // dd($e);

            $this->errors[] = [
                'file_path' => $file_name ? $file_name : $request,
                'row' => null,
                'errors' => [
                    'error' => "File reading error ! something is wrong with this file format or content.",
                    // 'error' => $e->getMessage() . " ----> line : " . $e->getLine() . " File: " . $e->getFile() . "\n",
                ],
                'date' => $this->now->toDateTimeString(),
            ];

            return ['status' => false, 'message' => 'File reading error '];
        }

        return ['status' => true, 'message' => ''];
    }

    public function getNewXmlFile()
    {
        $dir = env('XML_PATH', "/var/www/html/xml-tachosil");

        $dirFiles = scandir($dir);
        $dirFiles = array_diff($dirFiles, array('.', '..'));
        $imported = DB::table("imported")
            ->whereIn("name", $dirFiles)
            ->pluck("name")
            ->toArray();

        $ids = [];

        $toImport = array_diff($dirFiles, $imported);

        if (!empty($toImport)) {

            $files = $this->insertTasksXml($toImport, false);
            foreach ($files as $key => $value) {
                array_push($ids, $value['id']);
            }

            return $ids;
        }

        return [];
    }

    public function separateTasksSubTasks($allImported)
    {

        $excludedForTasks = [
            "sample_status", "test_status", "reported_name",
        ];

        $includeForSubtask = [
            "sample_status", "test_status", "reported_name",
            "analysis", "sample_number", 'due_date', 'date_reviewed', 'login_date', 'date_completed', "receive_date",
        ];

        $nullFields = ['login_date', 'date_completed', 'date_reviewed'];

        $allTasks = [];

        $allSubTasks = [];

        foreach ($allImported as $key => $task) {

            foreach ($task as $fieldName => $value) {

                if (!in_array($fieldName, $excludedForTasks)) {

                    if (in_array($fieldName, $nullFields)) {

                        $allTasks[$key][$fieldName] = null;

                    } else {

                        $allTasks[$key][$fieldName] = $value;
                    }
                }

                if (in_array($fieldName, $includeForSubtask)) {
                    $allSubTasks[$key][$fieldName] = $value;
                }
            }
        }

        $separated["tasks"] = $allTasks;
        $separated["subtask"] = $allSubTasks;

        return $separated;
    }

    public function getSamplesFromDB($importedSamples)
    {
        $existing = DB::table("task_version")
            ->select("sample_number", "due_date")
            ->where("state", 1)
            ->where("move_state", "<>", 2)
            ->whereIn("sample_number", $importedSamples)
            ->get()
            ->toArray();

        $existing = json_decode(json_encode($existing), true);

        return $existing;
    }

    public function getRescheduleAndBacklog($subtasksDueDates, $minTaskVersionsDueDates, $maxTaskVersionsDueDates, $tasks)
    {
        $result = [
            "same" => [],
            "backlog" => [],
            "reschedule" => [],
            "changed" => [],
            "statusChanged" => [], // To remove task versions of these since task version of planned, in progress and done are too different
        ];

        $hardMoved = $this->getHardMovedFromBacklog();

        $today = (new Carbon())->format("Y-m-d");

        foreach ($subtasksDueDates as $sub_task) {
            $sample_number = $sub_task->sample_number;
            
            if (isset($minTaskVersionsDueDates[$sample_number])) {

                $same_duedate_if_planned = !isset($sub_task->receive_date) && !isset($sub_task->date_reviewed) && $sub_task->due_date == $maxTaskVersionsDueDates[$sample_number];
                $same_duedate_if_in_progress = (isset($sub_task->receive_date) && $sub_task->receive_date && $sub_task->receive_date == $minTaskVersionsDueDates[$sample_number]);
                $same_duedate_if_done = (isset($sub_task->date_reviewed) && $sub_task->date_reviewed && $sub_task->date_reviewed == $maxTaskVersionsDueDates[$sample_number]);

                if ($same_duedate_if_planned || $same_duedate_if_in_progress || $same_duedate_if_done) {
                    array_push($result["same"], $sample_number);
                } else {

                    /**
                     * IF due date is less than today
                     * And
                     * Task version < that due date (Not extended and not moved from backlog)
                     */
                    $task = isset($tasks[$sample_number]) ? $tasks[$sample_number][0] : null;
                    if ($sub_task->due_date < $today && $maxTaskVersionsDueDates[$sample_number] < $sub_task->due_date) {
                        array_push($result["backlog"], $sample_number);
                    } else if ($task && $task->original_due_date == $sub_task->due_date) { // Would mean status change, due date is the same but $same_duedate_if_planned AND $same_duedate_if_in_progress And $same_duedate_if_done didn't apply
                        array_push($result["statusChanged"], $sample_number);
                    } else {
                        array_push($result["reschedule"], $sample_number);
                    }                   

                    array_push($result["changed"], $sample_number);
                }

            } else if (!in_array($sample_number, $hardMoved)) {
                array_push($result["changed"], $sample_number);
            }
        }
        
        return $result;
    }

    public function logic($xmlUniqueTasks, $subTask)
    {
        $xmlSamples = array_column($xmlUniqueTasks, "sample_number");
        $xmlBatches = array_column($xmlUniqueTasks, "batch");

        $tasks_before_update = $this->getTasks($xmlSamples);
        $this->removeExistingSamples($xmlSamples);
        $this->clearExistingUserDefined($xmlBatches);
        $this->removeMovedTasks();

        $newSamplesData = array_advanced_filter($xmlUniqueTasks, $xmlSamples, "sample_number");
        $newsubTasksData = array_advanced_filter($subTask, $xmlSamples, "sample_number");
        $formattedTasks = $this->formatTasks($newSamplesData);

        $this->insertNewSamples($formattedTasks, $newsubTasksData, []);

        $this->updateReviewedDate();
        $this->updateCompletedDate();
        $this->updateLoginDate();

        $subtasksMinDueDates = $this->getSubtasksWithMinDueDate();
        $taskVersionsMinDueDates = $this->getTaskVersionsWithMinDueDate();
        $taskVersionsMaxDueDates = $this->getTaskVersionsWithMaxDueDate();

        $changedTasks = $this->getRescheduleAndBacklog($subtasksMinDueDates, $taskVersionsMinDueDates, $taskVersionsMaxDueDates, $tasks_before_update);
        $changedTaskVersions = array_advanced_filter($xmlUniqueTasks, $changedTasks["changed"], "sample_number");

        $this->updateTaskVersionState($changedTasks['backlog'], $changedTasks['reschedule']);

        DB::table('task_version')->where('state', 1)->whereIn('sample_number', $changedTasks['statusChanged'])->delete();
        $formattedChangedTasksVersions = $this->formatTasksVersion($changedTaskVersions);
        $hardMoved = $this->checkHardMovedDueDate();
        $this->handleHardMovedTasks($hardMoved);

        $this->insertNewSamples([], [], $formattedChangedTasksVersions);
        $this->deleteCancelledTasks();
        $this->deleteCancelledSubtasks();
        $this->deleteNonExistentSamples();
        $this->removeDuplicateBacklog();
        $this->updateReviewedDate();
        $this->addExtendedTaskVersion($subtasksMinDueDates);

        return json_response()->success("Data Imported Successfully");

    }

    private function addExtendedTaskVersion($subtasksMinDueDates)
    {
        $today = Carbon::today();
        $in_progress_tasks = TaskVersion::join('tasks', 'tasks.sample_number', '=', 'task_version.sample_number')
            ->selectRaw("`task_version`.`sample_number`")
            ->selectRaw("MAX(`tasks`.`duration`) as `duration`")
            ->selectRaw("MAX(`tasks`.`product`) as `product`")
            ->selectRaw("MAX(`tasks`.`analysis`) as `analysis`")
            ->selectRaw("MAX(`tasks`.`days`) as `days`, MAX(`tasks`.`date_reviewed`) as `date_reviewed`, MAX(`tasks`.`receive_date`) as `receive_date`")
            ->selectRaw("MAX(`task_version`.`part`) as `part`, MAX(`task_version`.`due_date`) as `due_date`")
            ->where('state', 1)
            ->where('days', '>', 1)
            ->where('due_date', '<', $today)
            ->whereNull('date_reviewed')
            ->whereNotNull('receive_date')
            ->groupBy('sample_number')
            ->get();

        $durations = DB::table('durations')->get();

        $dueDatesOrder = $this->getDueDateOrder();
        // $subtasksMinDueDates = $this->getSubtasksWithMinDueDate();
        $min_dates = [];
        foreach ($subtasksMinDueDates as $subtasksMinDueDate) {
            $min_dates[$subtasksMinDueDate->sample_number] = $subtasksMinDueDate->due_date;
        }
        $extended_tasks = [];

        foreach ($in_progress_tasks as $task) {
            $final_due_date = Carbon::createFromFormat('Y-m-d', isset($min_dates[$task->sample_number]) ? $min_dates[$task->sample_number] : $task->due_date);
            $latest_due_date = Carbon::createFromFormat('Y-m-d', $task->due_date);
            if ($today->gt($latest_due_date)) {
                $next_date = $latest_due_date->copy()->addDays(1);
                while (DatesUtility::dateIsHoliday($next_date->toDateString())) {
                    $next_date->addDays(1);
                }

                $part = $task->days + 1;

                $limit = $final_due_date->gt($today) ? $today : $final_due_date;
                while ($limit->gte($next_date)) {
                    $due = $next_date->toDateString();
                    if (isset($dueDatesOrder[$due])) {
                        $dueDatesOrder[$due] = $dueDatesOrder[$due] + 1;
                    } else {
                        $dueDatesOrder[$due] = 1;
                    }

                    $extended_tasks[] = [
                        'sample_number' => $task->sample_number,
                        'due_date' => $due,
                        'order' => $dueDatesOrder[$due],
                        'part' => $part,
                        'state' => 1,
                        'move_state' => 2,
                        'created_at' => $today->toDateTimeString(),
                    ];

                    $next_date->addDays(1);
                    while (DatesUtility::dateIsHoliday($next_date->toDateString())) {
                        $next_date->addDays(1);
                    }
                    $part++;
                }

                $task_duration = $durations->where('product', $task->product)
                    ->where('analysis', $task->analysis)->first();

                if ($task_duration) {
                    $parts = ($part - 1) > 1 ? ($part - 1) : 1;
                    DB::table('tasks')->where('sample_number', $task->sample_number)->update([
                        'duration' => $task_duration->total_duration / $parts,
                        'days' => $parts
                    ]);
                } else {
                    $parts = ($part - 1) > 1 ? ($part - 1) : 1;
                    DB::table('tasks')->where('sample_number', $task->sample_number)->update([
                        'duration' => ($task->duration * $task->days) / $parts,
                        'days' => $parts
                    ]);
                }
            }
        }

        DB::table('task_version')->insert($extended_tasks);
    }

    public function deleteCancelledSubtasks()
    {
        Subtask::where('test_status', 'x')->delete();
    }

    public function deleteCancelledTasks()
    {
        $samples = Subtask::select('sample_number')->where('sample_status', 'X')->get()->toArray();
        $samples = array_column($samples, 'sample_number');

        Task::whereIn('sample_number', $samples)->delete();
        TaskVersion::whereIn('sample_number', $samples)->delete();
        Subtask::whereIn('sample_number', $samples)->delete();
    }

    public function checkHardMovedDueDate()
    {
        $query = DB::select(DB::raw(

            "SELECT task_version.sample_number , due_date , date_reviewed FROM task_version inner join tasks on task_version.sample_number = tasks.sample_number where task_version.state = 1 and is_hard_moved_from_backlog = 1 "

        ));

        $tasks = json_decode(json_encode($query), true);
        return $tasks;
    }

    public function handleHardMovedTasks($tasks)
    {
        $samples = [];
        $now = Carbon::now()->format("Y-m-d");

        foreach ($tasks as $index => $value) {

            if (($now > $value['due_date']) && $value['date_reviewed'] == null) {
                array_push($samples, $value["sample_number"]);

            }
        }

        TaskVersion::whereIn('sample_number', $samples)->where('state', 1)->update(['is_hard_moved_from_backlog' => 0]);
    }

    public function clearExistingUserDefined($xmlBatches)
    {
        $userDefinedSamples = DB::Table('tasks')->where('is_user_defined', 1)->whereIn('batch', $xmlBatches)->pluck('sample_number');

        Task::whereIn('sample_number', $userDefinedSamples)->delete();
        TaskVersion::whereIn('sample_number', $userDefinedSamples)->delete();
    }

    public function updateReviewedDate()
    {
        DB::select(DB::raw(

            "UPDATE tasks inner join (SELECT t.sample_number , max(t.date_reviewed) as date_reviewed from (

                    SELECT sample_number, date_reviewed from subtasks where sample_number not in (

                        SELECT sample_number from subtasks where date_reviewed is null
                    )

            ) as t group by t.sample_number ) as full ON tasks.sample_number = full.sample_number  SET tasks.date_reviewed = full.date_reviewed"

        ));
    }

    public function updateCompletedDate()
    {
        DB::select(DB::raw(

            "UPDATE tasks inner join (SELECT t.sample_number , max(t.date_completed) as date_completed from (

                    SELECT sample_number, date_completed from subtasks where sample_number not in (

                        SELECT sample_number from subtasks where date_completed is null
                    )

            ) as t group by t.sample_number ) as full ON tasks.sample_number = full.sample_number SET tasks.date_completed = full.date_completed"

        ));
    }

    public function updateLoginDate()
    {
        DB::select(DB::raw(

            "UPDATE tasks inner join (SELECT sample_number , min(login_date) as login_date from subtasks group by sample_number ) as full ON tasks.sample_number = full.sample_number SET tasks.login_date = full.login_date"
        ));
    }

    public function getTasks($sample_numbers)
    {
        $tasks = DB::table('tasks')->whereIn('sample_number', $sample_numbers)->get()->groupBy('sample_number');
        return $tasks;
    }

    public function getSubtasksWithMinDueDate()
    {
        $query = DB::select(DB::raw(
            "SELECT sample_number , min(due_date) as due_date, MIN(`receive_date`) as `receive_date`, MAX(`date_reviewed`) as `date_reviewed` from subtasks group by sample_number"
        ));

        return $query;
        // dd($query);
        // $tasks = json_decode(json_encode($query), true);
        // $tasks = array_column($tasks, 'due_date', 'sample_number');
        // return $tasks;
    }

    public function getTaskVersionsWithMinDueDate()
    {
        $query = DB::select(DB::raw(

            "SELECT sample_number , min(due_date) as due_date  from task_version where state = 1 and is_hard_moved_from_backlog = 0  group by sample_number"
        ));

        $tasks = json_decode(json_encode($query), true);
        $tasks = array_column($tasks, 'due_date', 'sample_number');
        return $tasks;
    }

    public function getTaskVersionsWithMaxDueDate ()
    {
        $query = DB::select(DB::raw(
            "SELECT sample_number , MAX(due_date) as due_date  from task_version where state = 1 and is_hard_moved_from_backlog = 0  group by sample_number"
        ));

        $tasks = json_decode(json_encode($query), true);
        $tasks = array_column($tasks, 'due_date', 'sample_number');
        return $tasks;
    }

    public function getHardMovedFromBacklog()
    {
        $query = DB::select(DB::raw(

            "SELECT sample_number FROM task_version where state = 1 and is_hard_moved_from_backlog = 1 "
        ));

        $tasks = json_decode(json_encode($query), true);
        $tasks = array_column($tasks, 'sample_number');
        return $tasks;
    }

    public function deleteNonExistentSamples()
    {
        $tasks_sample_numbers = DB::table('tasks')->select('sample_number')->get()->toArray();
        $tasks_sample_numbers = json_decode(json_encode($tasks_sample_numbers), true);
        $tasks_sample_numbers = array_column($tasks_sample_numbers, 'sample_number');

        $subtasks_sample_numbers = DB::table('subtasks')->select('sample_number')->get()->toArray();
        $subtasks_sample_numbers = json_decode(json_encode($subtasks_sample_numbers), true);
        $subtasks_sample_numbers = array_column($subtasks_sample_numbers, 'sample_number');

        DB::table('task_version')->whereNotIn('sample_number', $tasks_sample_numbers)->delete();
        DB::table('task_version')->whereNotIn('sample_number', $subtasks_sample_numbers)->delete();
        DB::table('tasks')->where('is_user_defined', 0)->whereNotIn('sample_number', $subtasks_sample_numbers)->delete(); // Delete non exsiting normal tasks
        DB::table('tasks')->where('is_user_defined', 1)->whereIn('sample_number', $subtasks_sample_numbers)->delete(); // Delete old user defined tasks that are now added
    }

    public function removeExistingSamples($sample_numbers)
    {
        Task::whereIn('sample_number', $sample_numbers)->delete();
        Subtask::whereIn('sample_number', $sample_numbers)->delete();
    }

    public function removeMovedTasks()
    {
        TaskVersion::where('move_state', 2)->delete();
        TaskVersion::where('move_state', 1)->update([
            'state' => 1,
            'move_state' => 0,
        ]);
    }

    public function formatTasks($tasks)
    {
        $now = Carbon::now();
        foreach ($tasks as $key => &$task) {
            $task['original_due_date'] = $task['due_date'];
            $task['created_at'] = $now->toDateTimeString();
        }
        $tasks = array_exclude($tasks, ['due_date']);

        return $tasks;
    }

    public function formatTasksVersion($tasks)
    {
        $dueDatesOrder = $this->getDueDateOrder();
        $today = Carbon::today();
        $data = [];

        $tasks_sample_numbers = collect($tasks)->pluck('sample_number')->toArray();
        $updated_tasks = Task::whereIn('sample_number', $tasks_sample_numbers)->whereNotNull('date_reviewed')->get()->groupBy('sample_number')->toArray(); // tasks with updated date_reviewed
        foreach ($tasks as $task) {
            $days = $task['days'];

            $is_done_condition = isset($updated_tasks[$task['sample_number']]) && count($updated_tasks[$task['sample_number']]) > 0 && $updated_tasks[$task['sample_number']][0]['date_reviewed'];
            if (!is_null($task['receive_date']) && !$is_done_condition) {
                $temp_due_date = Carbon::createFromFormat('Y-m-d', $task['receive_date']);
                /**
                 * Add days so task_version.part = 1 starts on the $task['receive_date']
                 */
                for ($i = 1; $i < $days; $i++) {
                    $temp_due_date->addDays(1);
                    while (DatesUtility::dateIsHoliday($temp_due_date->toDateString())) {
                        $temp_due_date->addDays(1);
                    }
                }
            } elseif ($is_done_condition) {
                $date_reviewed = $updated_tasks[$task['sample_number']][0]['date_reviewed'];
                $temp_due_date = Carbon::createFromFormat('Y-m-d', $date_reviewed);
            } else {
                $temp_due_date = Carbon::createFromFormat('Y-m-d', $task['due_date']);
            }

            $task_temp = [];
            for ($i = 0; $i < $days; $i++) {
                $task_temp = [
                    'sample_number' => $task['sample_number'],
                    'due_date' => $task['due_date'],
                ];

                $due = $temp_due_date->copy()->toDateString();
                if (isset($dueDatesOrder[$due])) {
                    $dueDatesOrder[$due] = $dueDatesOrder[$due] + 1;
                } else {
                    $dueDatesOrder[$due] = 1;
                }

                $task_temp['due_date'] = $due;
                $task_temp['order'] = $dueDatesOrder[$due];
                $task_temp['state'] = 1;
                $task_temp['part'] = $days - $i;
                $task_temp['move_state'] = 0;
                $task_temp['created_at'] = $today->format("Y-m-d");
                $data[] = $task_temp;

                $temp_due_date = $temp_due_date->subDays(1);
                while (DatesUtility::dateIsHoliday($temp_due_date->toDateString())) {
                    $temp_due_date->subDays(1);
                }
            }
        }

        return $data;
    }

    public function getDueDateOrder()
    {
        $dueDatesOrder = DB::table("task_version")
            ->selectRaw("due_date, max(`order`) lastorder")
            ->groupBy("due_date")
            ->get()
            ->toArray();

        $dueDatesOrder = json_decode(json_encode($dueDatesOrder), true);

        $dueDatesOrder = array_column($dueDatesOrder, 'lastorder', 'due_date');

        return $dueDatesOrder;
    }

    public function insertNewSamples($tasks, $subtasks, $versions)
    {
        $tasks = DB::table('tasks')->insert($tasks);
        $subtasks = DB::table('subtasks')->insert($subtasks);
        $versions = DB::table('task_version')->insert($versions);
    }

    public function updateTaskVersionState($backlog, $reschedule)
    {
        $now = Carbon::now();
        $today = $now->format("Y-m-d");

        $backlog_count = count($backlog);
        $reschedule_count = count($reschedule);

        DB::table("task_version")
            ->whereIn("sample_number", $backlog)
            ->where("state", 1)
            ->update(["state" => 2]);

        DB::table("task_version")
            ->whereIn("sample_number", $reschedule)
            ->where("state", 1)
            ->update(["state" => 3]);

        $backlogRow = RescheduleBacklogCount::firstOrNew(['date' => $today, 'type' => 'backlog']);
        $backlogRow->count = ($backlogRow->count + $backlog_count);
        $backlogRow->save();

        $rescheduleRow = RescheduleBacklogCount::firstOrNew(['date' => $today, 'type' => 'reschedule']);
        $rescheduleRow->count = ($rescheduleRow->count + $reschedule_count);
        $rescheduleRow->save();

        $backlog_samples = RescheduleBacklogSamplesCount::where('date', $today)
            ->whereIn('sample_number', $backlog)
            ->where('type', 'backlog')->get();
        $reschedule_samples = RescheduleBacklogSamplesCount::where('date', $today)
            ->whereIn('sample_number', $backlog)
            ->where('type', 'reschedule')->get();

        foreach ($backlog as $sample_number) {
            $backlog_sample = $backlog_samples->where('sample_number', $sample_number)->first();
            if ($backlog_sample) {
                $backlog_sample->count += 1;
                $backlog_sample->save();
            } else {
                RescheduleBacklogSamplesCount::create([
                    'date' => $today,
                    'sample_number' => $sample_number,
                    'type' => 'backlog',
                    'count' => 1,
                ]);
            }
        }
        foreach ($reschedule as $sample_number) {
            $reschedule_sample = $reschedule_samples->where('sample_number', $sample_number)->first();
            if ($reschedule_sample) {
                $reschedule_sample->count += 1;
                $reschedule_sample->save();
            } else {
                RescheduleBacklogSamplesCount::create([
                    'date' => $today,
                    'sample_number' => $sample_number,
                    'type' => 'reschedule',
                    'count' => 1,
                ]);
            }
        }
    }

    public function updateTasksSubTasks($tasks, $subtasks, $commonBatches)
    {

        DB::table("temp_tasks")->truncate();
        DB::table("temp_subtasks")->truncate();

        $commonTasks = array_advanced_filter($tasks, $commonBatches, "batch");
        $commonTasks = array_simplify($commonTasks, ['batch', 'date_reviewed', 'date_completed', 'due_date']);

        DB::table("temp_tasks")->insert($commonTasks);

        $commonSubTasks = array_advanced_filter($subtasks, $commonBatches, "batch");
        //       dd($commonSubTasks);

        DB::table("temp_subtasks")->insert($commonSubTasks);

        $query = "UPDATE `tasks` INNER JOIN temp_tasks on temp_tasks.batch = tasks.batch SET tasks.date_completed=temp_tasks.date_completed,tasks.date_reviewed=temp_tasks.date_reviewed ";
        $update_task_table = DB::select(DB::raw($query));

        $subTaskquery = "UPDATE `subtasks` INNER JOIN temp_subtasks on temp_subtasks.batch = subtasks.batch AND temp_subtasks.analysis = subtasks.analysis SET subtasks.reported_name = temp_subtasks.reported_name, subtasks.sample_status = temp_subtasks.sample_status, subtasks.test_status = temp_subtasks.test_status";
        $update_subtask_table = DB::select(DB::raw($subTaskquery));
    }

    public function assignTaskVersionToTask()
    {
        $query = "UPDATE `task_version` INNER JOIN tasks on task_version.batch = tasks.batch SET task_version.task_id = tasks.id WHERE task_version.task_id is null ";
        $update_task_version_table = DB::select(DB::raw($query));
    }

    public function getBacklogProducts()
    {
        $today = (new Carbon())->format("Y-m-d");

        $backlog = DB::select(DB::raw("insert into task_version (`order`, due_date, state, move_state, created_at, updated_at, sample_number)
         select 1 as `order`, task_version.due_date , 2 as state, '0' as move_state, null as created_at, null as updated_at ,task_version.sample_number from
         tasks inner join task_version on tasks.sample_number = task_version.sample_number
        where task_version.due_date between '$today' and '$today' and task_version.state = 1 and
         tasks.date_reviewed is null"));
    }

    public function removeDuplicateBacklog()
    {
        $duplicate = DB::select(DB::raw("select max(id) as ids from task_version where state = 2 group by
         sample_number, due_date, state having count(*) > 1"));

        $duplicate = json_decode(json_encode($duplicate), true);
        $duplicate = array_column($duplicate, "ids");

        DB::table("task_version")->whereIn("id", $duplicate)->delete();
    }

    public function getNumberOfFreeDays()
    {
        $freeDays = 0;
        $today = Carbon::today();
        $lastFailureImport = Import::where('status', 0)->orderBy('id', 'desc')->first();
        $lastFailureDate = Carbon::parse($lastFailureImport['imported_time']);

        if (!$lastFailureImport) {

            return $freeDays;

        } else {

            $freeDays = $today->diffInDays($lastFailureDate);
            return $freeDays;
        }
    }

    public function getImportError($filePath)
    {
        dd($filePath);
    }
}

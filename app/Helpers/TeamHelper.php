<?php

namespace App\Helpers;

use App\Models\Highlight;
use App\Models\TeamState;
use App\Models\TeamLeadership;
use App\Models\Leadership;
use Carbon\Carbon;
use DB;

class TeamHelper
{
    public function getUsedUsersNumber($date, $status)
    {
        $useedUsers = DB::table("team_states")
            ->selectRaw("sum(number_of_members) total")
            ->where("date", $date)
            ->where("state_id", $status)
            ->groupBy('date')
            ->first();

        if (!is_null($useedUsers)) {
            return $useedUsers->total;
        }

        return 0;

    }

    public function getUsedLeadersNumbers($date, $status)
    {
        $useedUsers = DB::table("team_leaderships")
            ->selectRaw("sum(number_of_members) total")
            ->where("date", $date)
            ->where("leadership_id", $status)
            ->groupBy('date')
            ->first();

        if (!is_null($useedUsers)) {
            return $useedUsers->total;
        }

        return 0;

    }

    public function insertOrUpdateTeamState($teamState, $date, $stateID, $increment)
    {
        if (is_null($teamState)) {

            $newTeamState = TeamState::create(
                [
                    'date' => $date,
                    'state_id' => $stateID,
                    'number_of_members' => ($increment < 0) ? 0 : $increment,
                ]
            );

            return $newTeamState;

        } else {

            $number = $teamState->number_of_members;
            $new = $number + $increment;
            $teamState->number_of_members = $new < 0 ? 0 : $new;
            $teamState->save();

            return $teamState;
        }

    }

    public function insertOrUpdateLeadershipState($teamState, $date, $stateID, $increment)
    {
        if (is_null($teamState)) {

            $newTeamState = TeamLeadership::create(
                [
                    'date' => $date,
                    'leadership_id' => $stateID,
                    'number_of_members' => ($increment < 0) ? 0 : $increment,
                ]
            );

            return $newTeamState;

        } else {

            $number = $teamState->number_of_members;
            $new = $number + $increment;
            $teamState->number_of_members = $new < 0 ? 0 : $new;
            $teamState->save();

            return $teamState;
        }

    }

    public function getLatestHighlight()
    {
        $highlight = Highlight::OrderBy('id', 'desc')->first();
        return $highlight;
    }

    public static function getTrends($start_date, $end_date)
    {
        $holydays = DB::table('holidays')->pluck('date')->toArray();
        for ($i = 0; $i < count($holydays); $i++) {
            $holydays[$i] = "'{$holydays[$i]}'";
        }
        $holydays = implode(',', $holydays);

        $trends = DB::select(DB::raw("select stat.`date`, Round((sum(stat.val)/COUNT(stat.`id`)),2) as number
                from
                (
                SELECT `date`,
                id,
                state_id,
                `index`,
                if(state_id = 1,
                    5,
                    if(state_id = 2,
                        3,
                        if(state_id = 3,
                            1,
                            0
                        )
                    )
                )  val
                FROM `team_daily_statuses` where state_id in (1,2,3) and (`date` BETWEEN '$start_date' and '$end_date')
                and `date` NOT IN ($holydays)
                ) as stat
                group by stat.`date`
			"));

        $trends = json_decode(json_encode($trends), true);
        $trends = array_column($trends, 'number', 'date');

        return $trends;
    }

    public static function getFirstTrend($type = "month")
    {
        $year = date('Y');
        $month = date('m');
        $week = date('W');

        // $now = Carbon::now();
        // $week = (integer) $now->format('W');
        // if ($now->format('w') == 0 || $now->format('w') == 6) { // if it's a weekend 0 (for Sunday) through 6 (for Saturday)
        //     $week += 1;
        // }

        $trends = DB::select(DB::raw("select stat.`date`, YEAR(stat.`date`) `year`, MONTH(stat.`date`) `month`, WEEK(stat.`date`, 3) `week`, Round((sum(stat.val)/COUNT(stat.`id`)),2) as number
                from
                (
                SELECT `date`,
                id,
                state_id,
                `index`,
                if(state_id = 1,
                    5,
                    if(state_id = 2,
                        3,
                        if(state_id = 3,
                            1,
                            0
                        )
                    )
                )  val
                FROM `team_daily_statuses` where state_id in (1,2,3)
                ) as stat
                group by stat.`date` ORDER BY stat.`date` ASC LIMIT 1
            "));
        if (empty($trends)) {
            if ($type == 'week') {
                return get_swiper_steps($year, $week, null, null, 52, 'week');
            }
            return get_swiper_steps($year, $month, null, null, 12, 'month');
        }
        $start = $trends[0];
        if ($type == 'week') {
            return get_swiper_steps($year, $week, $start->year, $start->week, 52, 'week');
        }
        return get_swiper_steps($year, $month, $start->year, $start->month, 12, 'month');
    }

    public static function getLeadershipChartGroupedData($start_date, $end_date)
    {
        $holydays = DB::table('holidays')->pluck('date')->toArray();

        $data = DB::table('team_states')->where('state_id', '>', 3)
            ->selectRaw('date, SUM(`number_of_members`) as `number`')
            ->whereBetween('date', [$start_date, $end_date])
            ->whereNotIn('date', $holydays)
            ->groupBy('date')
            ->get();

        $data = json_decode(json_encode($data), true);
        $data = array_column($data, 'number', 'date');

        return $data;
    }

    public static function getLeadershipChartData($start_date, $end_date)
    {
        $holydays = DB::table('holidays')->pluck('date')->toArray();

      /*  $data = DB::table('team_states')->where('state_id', '>', 3)
            ->selectRaw('date, state_id, number_of_members')
            ->whereBetween('date', [$start_date, $end_date])
            ->whereNotIn('date', $holydays)
            ->get();*/

        $leaderships = Leadership::pluck('id')->toArray();

        $data = DB::table('team_leaderships')->whereIn('leadership_id', $leaderships)
            ->selectRaw('date, leadership_id, number_of_members')
            ->whereBetween('date', [$start_date, $end_date])
            ->whereNotIn('date', $holydays)
            ->get();

        $data = $data->groupBy('date');
        $data = json_decode(json_encode($data), true);

        return $data;
    }

    public static function getLeadershipSteps()
    {
        $year = date('Y');
        $month = date('m');
        $week = date('W');
        // $now = Carbon::now();
        // $week = (integer) $now->format('W');
        // if ($now->format('w') == 0 || $now->format('w') == 6) { // if it's a weekend 0 (for Sunday) through 6 (for Saturday)
        //     $week += 1;
        // }

        $first_entry = DB::table('team_states')->where('state_id', '>', 3)
            ->select('date')
            ->selectRaw('YEAR(`date`) as year')
            ->selectRaw('WEEK(`date`, 3) as week')
            ->groupBy('date')
            ->orderBy('date', 'ASC')
            ->first();

        if (!$first_entry) {
            return get_swiper_steps($year, $week, null, null, 52, 'week');
        }
        return get_swiper_steps($year, $week, $first_entry->year, $first_entry->week, 52, 'week');
    }
}

<?php

namespace App\Helpers;

use Carbon\Carbon;
use App\Models\SixS;
use App\Models\TypeValue;
use DB;
use App\Helpers\CommanUtility;


class qualityHelper
{

	use CommanUtility;


	public function getNotRunThisMonth($monthName)
	{
		$year = carbon::now()->year ;
        $notRun = DB::select( DB::raw("SELECT SUM(value) as total FROM `type_values` where `type` = 'quality-not-run' and MONTHNAME(`date`) = '$monthName' and YEAR(`date`) = '$year'"));

        $notRun = array_column($notRun, 'total');

        $total = $notRun[0];

        if($total == NULL ){

        	$total = 0;

        }

        return $total ;
	}



	public function getLimsThisMonth($monthName)
	{
		$year = carbon::now()->year ;
        $lims = DB::select( DB::raw("SELECT SUM(value) as total FROM `type_values` where `type` = 'quality-lims' and MONTHNAME(`date`) = '$monthName' and YEAR(`date`) = '$year'"));

        $lims = array_column($lims, 'total');

        $total = $lims[0];

        if($total == NULL ){

        	$total = 0;

        }

        return $total ;
	}

	public function calculateQualityTrend($tasks, $notRun, $lims)
	{
		if($tasks != 0 ){

			$diffrence  = ($tasks - $notRun - $lims );

			if($diffrence <= 0) {

				$diffrence = 0 ;
			}

			$trend = $diffrence / $tasks * 100 ;
			$trend = round($trend,2);

			if($trend <= 0){

				$trend = 0 ;

			}

		}else {

			$trend = 0;
		}

		return $trend ;

	}
}
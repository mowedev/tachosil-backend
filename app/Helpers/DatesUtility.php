<?php

namespace App\Helpers;

use Carbon\Carbon;

trait DatesUtility
{
    public static $holidays;

	public function getWeekDates($weekIndex)
    {
        $date = Carbon::today();
        $today = $date->format("Y-m-d");
        $currentYear = app('request')->input("year");
        $currentYear = !is_null($currentYear)?$currentYear:$date->format("Y");
       // dd($currentYear);
        $firstOfWeek = $date->setISODate($currentYear, $weekIndex)
                            ->format('Y-m-d');
        $lastOfWeek = $date->addDays(6)->format('Y-m-d');

        $dates["firstOfWeek"] = $firstOfWeek;
        $dates["lastOfWeek"] = $lastOfWeek;
        $dates["today"] = $today;
        return $dates;
    }

    public function splitWeekDaysToPastAndFuture($weekIndex)
    {
        $week = $this->getWeekDates($weekIndex);
        $yesterday = Carbon::yesterday()->format("Y-m-d");
        $today = Carbon::today()->format("Y-m-d");

        $splited["past"]["firstOfWeek"] = $week["firstOfWeek"];
        $splited["past"]["lastOfWeek"] = $yesterday;
        $splited["future"]["firstOfWeek"] = $today;
        $splited["future"]["lastOfWeek"] = $week["lastOfWeek"];

        return $splited;
    }

    public function isPastWeek($weekIndex)
    {
        $week = $this->getWeekDates($weekIndex);
        if ($week['lastOfWeek'] < $week['today']) {
            return true;
        }

        return false;
    }

    public function isFutureWeek($weekIndex)
    {
        $week = $this->getWeekDates($weekIndex);

        if ($week['firstOfWeek'] > $week['today']) {
            return true;
        }

        return false;
    }

    public static function dateIsHoliday($date) {
        $holidays = self::$holidays ? self::$holidays : \App\Models\Holiday::get()->pluck('date')->toArray();

        if (in_array($date, $holidays)) {
            return true;
        }
        return false;        
    }
}

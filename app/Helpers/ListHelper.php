<?php

namespace App\Helpers;

use App\Helpers\AdminTasksUtility;
use App\Helpers\ChartsUtility;
use App\Helpers\CommanUtility;
use App\Helpers\DatesUtility;
use App\Helpers\formatUtility;
use App\Models\Duration;
use App\Models\Task;
use App\Models\TaskVersion;
use Carbon\Carbon;
use Illuminate\Support\Facades\DB;

class ListHelper
{
    use DatesUtility, ChartsUtility, AdminTasksUtility, CommanUtility, formatUtility;

    public function getTaskUserDefined($weekIndex)
    {
        $week = $this->getWeekDates($weekIndex);

        $status = $this->getTasksStatus();

        $taskVersions = TaskVersion::with('task.subtasks')
            ->WeekTasksByColoumn($week, 'due_date')
            ->NotCompletedParent()
            ->Active()
            ->get()
            ->groupBy('due_date')
            ->toArray();

        $tasks = Task::with(['subtasks', 'latestVersion'])
            ->CompletedTasks()
            ->WeekTasksByColoumn($week, 'date_reviewed')
            ->get()
            ->groupBy('date_reviewed')
            ->toArray();

        $dueDateTasks = $this->formatFromTaskVersions($taskVersions);
        $reviewedDateTasks = $this->formatFromTasks($tasks);

        $dueDateTasks = $this->updateTasksStatus($dueDateTasks, $status);
        $reviewedDateTasks = $this->updateTasksStatus($reviewedDateTasks, $status);

        $result = $this->listTaskResponse($dueDateTasks, $reviewedDateTasks, $week);
        return json_response()->success($result);
    }

    public function addTaskByUser($request)
    {
        $random_string = "00" . rand(10000, 100000000);

        $product = $request->product;
        $analysis = $request->analysis;
        $duration = $request->duration;
        $due_date = $request->due_date;
        $days = $request->get('days', 1);
        // $days = $days === 0 ? 1 : $days;

        $now = Carbon::now();
        $due_date_carbon = Carbon::createFromFormat('Y-m-d', $due_date);
        $tasks_data = [];
        $task_versions_data = [];

        $temp_due_date = $due_date_carbon->copy();

        $taskData = $request->except(['due_date']);
        $taskData['original_due_date'] = $due_date;
        $taskData['days'] = $days;
        $taskData['part'] = 1;
        $taskData['is_user_defined'] = 1;
        $taskData['sample_number'] = $random_string;
        $taskData['created_at'] = $now->toDateTimeString();
        $tasks_data[] = $taskData;

        for ($i = 0; $i < $days; $i++) {
            $taskVersionData['due_date'] = $temp_due_date->copy()->toDateString();
            $taskVersionData['part'] = $days - $i;
            $taskVersionData['order'] = $this->getLastOrderForDay($temp_due_date->toDateString());
            $taskVersionData['sample_number'] = $random_string;
            $taskVersionData['state'] = 1;
            $taskVersionData['move_state'] = 0;
            $taskVersionData['created_at'] = $now->toDateTimeString();
            $task_versions_data[] = $taskVersionData;

            $temp_due_date->subDays(1);
            while (DatesUtility::dateIsHoliday($temp_due_date->toDateString())) {
                $temp_due_date->subDays(1);
            }
        }

        if ($days === 0) {
            $taskVersionData['due_date'] = $temp_due_date->copy()->toDateString();
            $taskVersionData['part'] = 1;
            $taskVersionData['order'] = $this->getLastOrderForDay($temp_due_date->toDateString());
            $taskVersionData['sample_number'] = $random_string;
            $taskVersionData['state'] = 1;
            $taskVersionData['move_state'] = 0;
            $taskVersionData['created_at'] = $now->toDateTimeString();
            $task_versions_data[] = $taskVersionData;
        }

        DB::beginTransaction();
        try {
            if ($product && $analysis && $duration) {
                $this->overrideDuration($product, $analysis, $duration, ($duration * $days), $days);
            }

            $tasks = Task::insert($tasks_data);
            if ($task_versions_data) {
                $versions = TaskVersion::insert($task_versions_data);
            }
        } catch (\Exception $e) {
            DB::rollBack();
            dd($e);
            return [];
        }
        DB::commit();

        $insertedTasks = Task::with("latestVersion")
            ->where('sample_number', $random_string)
            ->get()
            ->toArray();

        return array_map(function ($item) {
            return $this->formatSingleTask($item);
        }, $insertedTasks);
    }

    public function overrideDuration($product, $analysis, $duration, $total_duration, $days = 1, $for_missing_data_or_backlog = false, $task = null, $project = null)
    {
        if ($product) {
            $dur = Duration::where('product', $product)
                ->where('analysis', $analysis)
                ->first();
        } else {
            $dur = Duration::where('project', $project)
                ->where('analysis', $analysis)
                ->first();
        }

        if ($dur) {
            $old_days = $dur->days;
        } else {
            $old_days = 1;
        }

        $update['duration'] = $duration;

        if ($days) {
            $update['days'] = $days;
        }

        $now = Carbon::now();
        $week_start_date = $now->startOfWeek()->toDateString();

        if ($for_missing_data_or_backlog && $task->duration == 0) { // Missing data situation

            $this->handleMissingTasks($product, $project, $analysis, $days, $now, $week_start_date, $update);

        } else {
            if ($days > $old_days) {
                $this->addTaskVersionParts($product, $analysis, $old_days, $days, $now, $week_start_date, $for_missing_data_or_backlog);
            } elseif ($days < $old_days) {
                $this->subTaskVersionParts($product, $analysis, $old_days, $days, $now, $week_start_date, $for_missing_data_or_backlog);
            }

            if ($task) {
                $task->update($update);
            } elseif ($product && $analysis) {
                Task::where('product', $product)
                    ->where('analysis', $analysis)
                    ->whereNull('receive_date')
                    ->whereNull('date_completed')
                    ->whereNull('date_reviewed')
                    ->update($update);
            }
        }

        if (!$dur && ($product || $project) && $analysis && $duration && $days) {
            Duration::create([
                'product' => $product,
                'project' => $project,
                'analysis' => $analysis,
                'duration' => $duration,
                'days' => $days,
                'total_duration' => $total_duration,
            ]);
        } elseif ($dur) {
            $update['total_duration'] = $total_duration;
            $dur->update($update);
        }
    }

    private function handleMissingTasks($product, $project, $analysis, $days, $now, $week_start_date, $update)
    {
        $missing_data_samples = [];

        if ($product) {
            $missing_data_samples = Task::select(['tasks.sample_number'])
                ->selectRaw("0 as `part`, MAX(`tasks`.`date_reviewed`) as date_reviewed")
                ->selectRaw("MIN(`subtasks`.`due_date`) as due_date")
                ->join('subtasks', 'subtasks.sample_number', '=', 'tasks.sample_number')
                ->where(function ($query) use ($product, $analysis) {
                    $query->where(function ($query) use ($product, $analysis) {
                        $query->where('tasks.product', $product)
                            ->where('tasks.analysis', $analysis);
                    })
                        ->orWhere(function ($query) use ($product, $analysis) {
                            $query->where('tasks.product', $product)
                                ->where('subtasks.analysis', $analysis);
                        });
                })
                ->where('duration', 0)
                ->groupBy('tasks.sample_number')
                ->get();
        } elseif ($project) {
            $missing_data_samples = Task::select(['tasks.sample_number'])
                ->selectRaw("0 as `part`, MAX(`tasks`.`date_reviewed`) as date_reviewed")
                ->selectRaw("MIN(`subtasks`.`due_date`) as due_date")
                ->join('subtasks', 'subtasks.sample_number', '=', 'tasks.sample_number')
                ->where(function ($query) use ($project, $analysis) {
                    $query->where(function ($query) use ($project, $analysis) {
                        $query->where('tasks.project', $project)
                            ->where('tasks.analysis', $analysis);
                    })
                        ->orWhere(function ($query) use ($project, $analysis) {
                            $query->where('tasks.project', $project)
                                ->where('subtasks.analysis', $analysis);
                        });
                })
                ->where('duration', 0)
                ->groupBy('tasks.sample_number')
                ->get();
        }

        $task_versions = array_map(function ($item) use ($days) {
            $due_date = isset($item['date_reviewed']) && $item['date_reviewed'] ? $item['date_reviewed'] : $item['due_date'];
            $due = Carbon::parse($due_date);

            for ($i = 0; $i < $days; $i++) {
                $due->subDays(1);

                while (DatesUtility::dateIsHoliday($due->toDateString())) {
                    $due->subDays(1);
                }
            }

            $array_row = [
                'sample_number' => $item['sample_number'],
                'part' => $item['part'],
                'due_date' => $due->toDateString(),
            ];
            return (object) $array_row;
        }, $missing_data_samples->toArray());

        // dd($task_versions);
        $samples = $missing_data_samples->pluck('sample_number')->toArray();
        TaskVersion::where('state', 1)->whereIn('sample_number', $samples)
            ->delete();
        $this->addParts($task_versions, [], 0, $days, $now, true);

        Task::whereIn('sample_number', $samples)
            ->update($update);
    }

    private function addTaskVersionParts($product, $analysis, $old_days, $days, $now, $week_start_date, $for_missing_data_or_backlog)
    {
        $samples = Task::select('sample_number', 'days')->where('product', $product)
            ->where('analysis', $analysis)
            ->pluck('sample_number')->toArray();

        if ($for_missing_data_or_backlog) {
            $task_versions = TaskVersion::selectRaw('MAX(part) as part, MAX(due_date) as due_date, sample_number')
                ->whereIn('sample_number', $samples)
                ->where('state', 1)
                ->groupBy('sample_number')
                ->get();
        } else {
            $task_versions = TaskVersion::selectRaw('MAX(part) as part, MAX(due_date) as due_date, sample_number')
            // ->where('due_date', '>=', $week_start_date)
                ->NotCompletedParentWithDuration()
                ->whereHas('task', function ($query) {
                    $query->whereNull('tasks.receive_date')->where('tasks.duration', '<>', 0);
                })
                ->whereIn('sample_number', $samples)
                ->where('state', 1)
                ->groupBy('sample_number')
                ->get();
        }

        $orders = DB::table('task_version')
            ->selectRaw('due_date, MAX(`order`) as max_order')
            ->whereIn('sample_number', $samples)
            ->groupBy('due_date')
            ->get()
            ->groupBy('due_date')->toArray();

        $this->addParts($task_versions, $orders, $old_days, $days, $now);
    }

    public function addParts($task_versions, $orders, $old_days, $days, $now, $done_samples = false)
    {
        $task_versions_data = [];
        foreach ($task_versions as $task_version) {
            $due = Carbon::createFromFormat('Y-m-d', $task_version->due_date);
            for ($i = 0; $i < $days - $old_days; $i++) {
                $due->addDays(1);
                while (DatesUtility::dateIsHoliday($due->toDateString())) {
                    $due->addDays(1);
                }

                $order = 1;
                if (isset($orders[$due->toDateString()])) {
                    $order = $orders[$due->toDateString()][0]->max_order + 1;
                }

                $task_versions_data[] = [
                    'sample_number' => $task_version->sample_number,
                    'due_date' => $due->toDateString(),
                    'part' => $task_version->part + $i + 1,
                    'state' => 1,
                    'order' => $order,
                    'move_state' => $done_samples ? 0 : 2,
                    'created_at' => $now->toDateTimeString(),
                ];
            }
        }

        DB::beginTransaction();
        try {
            TaskVersion::insert($task_versions_data);
        } catch (\Exception $e) {
            DB::rollBack();
            dd($e);
        }
        DB::commit();
    }

    private function subTaskVersionParts($product, $analysis, $old_days, $days, $now, $week_start_date, $for_missing_data_or_backlog)
    {
        $samples = Task::select('sample_number', 'days')->where('product', $product)
            ->where('analysis', $analysis)
            ->pluck('sample_number')->toArray();
        $parts = $old_days - $days;

        if ($for_missing_data_or_backlog) {
            $task_versions = TaskVersion::whereIn('sample_number', $samples)
                ->orderBy('state', 'ASC')
                ->orderBy('part', 'ASC')
                ->get()
                ->groupBy('sample_number');
        } else {
            $task_versions = TaskVersion::whereIn('sample_number', $samples)
            // ->where('due_date', '>=', $week_start_date)
                ->NotCompletedParentWithDuration()
                ->whereHas('task', function ($query) {
                    $query->whereNull('tasks.receive_date')->where('tasks.duration', '<>', 0);
                })
                ->orderBy('state', 'ASC')
                ->orderBy('part', 'ASC')
                ->get()
                ->groupBy('sample_number');
        }

        $this->subParts($task_versions, $parts);
    }

    private function subParts($task_versions, $parts)
    {
        $task_versions_data = [];
        $task_versions_to_delete = [];
        DB::beginTransaction();
        try {
            foreach ($task_versions as $versions) {
                $version_parts = $versions->groupBy('part')->take($parts);
                // dd($version_parts->toArray(), $version_parts->take($parts)->toArray());

                foreach ($version_parts as $v_part) {
                    if (count($v_part) > 1) {
                        if ($v_part->where('move_state', 2)
                            ->where('state', 1)
                            ->first()) {
                            $task_versions_to_delete[] = $v_part->where('move_state', 2)
                                ->where('state', 1)
                                ->first()->id;
                        }

                        continue;
                    }

                    if ($v_part[0]->state == 1) {
                        if ($v_part[0]->move_state == 2) {
                            $task_versions_to_delete[] = $v_part[0]->id;
                        } else {
                            DB::table('task_version')->where('id', $v_part[0]->id)->update([
                                'state' => 4,
                                'move_state' => 1,
                            ]);
                        }
                    }
                }
            }

            // TaskVersion::insert($task_versions_data);
            DB::table('task_version')->whereIn('id', $task_versions_to_delete)->delete();
        } catch (\Exception $e) {
            DB::rollBack();
            dd($e);
        }
        DB::commit();
    }

    public function getDuration($samNum)
    {
        $task = Task::where("sample_number", $samNum)->first();

        if (is_null($task)) {
            return null;
        }

        $duration = Duration::where("product", $task->product)->select("duration")->where("duration", "<>", 0)->first();
        if (is_null($duration)) {
            return null;
        }

        return $duration->duration;
    }

    public function updateUserDefinedTask($id, $request)
    {
        $task = Task::find($id);
        $taskVersion = TaskVersion::where('sample_number', $task->sample_number)
            ->where('state', 1)
            ->orderBy('part', 'DESC')
            ->first();

        $analysis = $task->analysis;
        $newDuration = $request->duration;
        $product = $request->product;
        $duedate = $request->due_date;
        $days = $request->days;

        /**
         * Move
         */
        $part = $request->get('part');
        $task_version_to_move = $part ? TaskVersion::where('sample_number', $task->sample_number)
            ->where('part', $part)
            ->first() : $taskVersion;

        if ($task_version_to_move && $task_version_to_move->due_date !== $duedate) {
            (new TaskHelper())->move([
                'task_id' => $id,
                'duedate' => $duedate,
                'part' => $task_version_to_move->part,
                'order' => $request->order,
            ]);
        }

        /**
         * Update days/duration
         */
        if (!is_null($task)) {
            if ($task->is_user_defined == 0) {
                if ($product && $analysis && $newDuration) {
                    $this->overrideDuration($product, $analysis, $newDuration, ($newDuration * $days), $days);
                }
            } else {
                if ($product && $analysis && $duration) {
                    $this->overrideDuration($product, $analysis, $newDuration, ($newDuration * $days), $days);
                } else {
                    $old_days = $task->days;
                    $product = $task->product;
                    $analysis = $task->analysis;
                    $now = Carbon::now();
                    $week_start_date = $now->copy()->startOfWeek()->toDateString();

                    $old_due_date = Carbon::createFromFormat('Y-m-d', $taskVersion->due_date);
                    // $max_due_date = $old_due_date->copy()->addDays($days);
                    $min_due_date = $old_due_date->copy()->subDays($days);
                    $old_days = $old_days === 0 ? 1 : $old_days;

                    if ($days > $old_days) {
                        $min_task_version = TaskVersion::selectRaw('MIN(part) as part, MIN(due_date) as due_date, sample_number')
                            ->where('sample_number', $task->sample_number)
                            ->where('state', 1)
                            ->groupBy('sample_number')
                            ->first();
                        $orders = DB::table('task_version')
                            ->selectRaw('due_date, MAX(`order`) as max_order')
                            ->whereBetween('due_date', [$min_due_date, $old_due_date])
                            ->groupBy('due_date')
                            ->get()
                            ->groupBy('due_date')->toArray();
                        $due = Carbon::createFromFormat('Y-m-d', $min_task_version->due_date);
                        for ($i = 0; $i < $days - $old_days; $i++) {
                            $due->subDays(1);
                            while (DatesUtility::dateIsHoliday($due->toDateString())) {
                                $due->subDays(1);
                            }
                            $order = 1;
                            if (isset($orders[$due->toDateString()])) {
                                $order = $orders[$due->toDateString()][0]->max_order + 1;
                            }
                            $task_versions_data[] = [
                                'sample_number' => $task->sample_number,
                                'due_date' => $due->toDateString(),
                                'part' => $taskVersion->part + $i + 1,
                                'state' => 1,
                                'order' => $order,
                                'move_state' => 0,
                                'created_at' => $now->toDateTimeString(),
                            ];
                        }
                        DB::beginTransaction();
                        try {
                            TaskVersion::insert($task_versions_data);
                        } catch (\Exception $e) {
                            DB::rollBack();
                            dd($e);
                        }
                        DB::commit();
                    } elseif ($days < $old_days) {
                        $parts = $old_days - $days;
                        $task_versions = TaskVersion::where('sample_number', $task->sample_number)
                            ->orderBy('state', 'ASC')
                            ->orderBy('part', 'ASC')
                            ->get()
                            ->groupBy('sample_number');
                        // dd($parts, $task_versions);
                        $this->subParts($task_versions, $parts);
                    }
                }
                // $order = ($taskVersion->due_date != $duedate) ? $this->getLastOrderForDay($duedate) : $taskVersion->order;
                // $taskVersion->update(["due_date" => $duedate, "order" => $order]);
            }

            Task::where("id", $id)->update($request->only([
                'description', 'batch', 'duration', 'days', 'status'
            ]));
        }

        $updatedTask = Task::with("latestVersion")
            ->where("id", $id)
            ->get()
            ->toArray();

        return $this->formatSingleTask($updatedTask);
    }

    public function updateProductDuration($duration, $product)
    {
        $temp_product = Duration::where("product", $product)->first();
        if (is_null($temp_product)) {
            DB::table('durations')->insert(
                ['product' => $product, 'duration' => $duration]
            );

            //Duration::insert(["product" => "$product", "duration" => "$duration"]);
        } else {
            Duration::where("product", $product)->update(["duration" => $duration]);
        }

        Task::where("product", $product)->where("duration", 0)->update(["duration" => $duration]);
    }

    public function listTaskResponse($dueDateTasks, $reviewedDateTasks, $week)
    {
        $allTasks = [];

        $day = $week['firstOfWeek'];
        $last = $week['lastOfWeek'];

        $capacity = $this->getCapacity([$day, $last]);
        $tasks = [];

        for ($i = 0; $i < 5; $i++) {

            $cap = 32;
            $dueTasks = isset($dueDateTasks[$day]) ? $dueDateTasks[$day] : [];
            $revTasks = isset($reviewedDateTasks[$day]) ? $reviewedDateTasks[$day] : [];

            $tasks[$day] = array_merge($dueTasks, $revTasks);

            $allTasks[$i]['date'] = $day;
            //$allTasks[$i]['tasks'] = isset($tasks[$day])? $tasks[$day]:[];
            $allTasks[$i]['tasks'] = isset($tasks[$day]) ? $tasks[$day] : [];
            $day_name = new Carbon($day);
            $allTasks[$i]['day'] = $day_name->format('l');

            $day = (new carbon($day))->addDays(1)->format("Y-m-d");

        }

        return $allTasks;
    }

    // move state = 2
    // original -> state = 4
}

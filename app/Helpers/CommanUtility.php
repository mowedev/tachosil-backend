<?php

namespace App\Helpers;

use App\Models\Holiday;
use App\Models\Import;
use App\Models\TypeValue;
use Carbon\Carbon;
use Illuminate\Support\Facades\DB;

trait CommanUtility
{
    public function getNotCompletedTasksIds()
    {
        $allNotCompletedIds = DB::table('subtasks')
            ->select("sample_number")
            ->where(function ($query) {
                $query->whereNotIn("sample_status", ["A", "C"])
                    ->orWhereNotIn("test_status", ["A", "C"]);
            })
            ->distinct()
            ->get()
            ->toArray();
        $allNotCompletedIds = json_decode(json_encode($allNotCompletedIds), true);

        $allNotCompletedIds = array_column($allNotCompletedIds, "sample_number");

        return $allNotCompletedIds;
    }

    public function getCapacity($dateRange)
    {
        $capacity = DB::table("capacities")
            ->select('date', 'capacity')
            ->whereBetween('date', $dateRange)
            ->get()
            ->toArray();

        $capacity = json_decode(json_encode($capacity), true);

        $capacity = array_column($capacity, "capacity", 'date');

        return $capacity;
    }

    public function getDemandRates()
    {
        $demand_rates = DB::table("demand_rates")
            ->select('date', 'demand_rate')
            ->get()
            ->toArray();

        $demand_rates = json_decode(json_encode($demand_rates), true);

        $demand_rates = array_column($demand_rates, "demand_rate", 'date');

        return $demand_rates;
    }

    public function getTasksStatus()
    {
        $tasksStatus = DB::table('subtasks')
            ->selectRaw("sample_number, concat(sample_status, test_status) as full_status")
            ->groupBy("sample_number", "full_status")
            ->get()
            ->toArray();

        $tasksStatus = json_decode(json_encode($tasksStatus), true);

        $tasksStatus = array_column($tasksStatus, "full_status", "sample_number");

        return $tasksStatus;
    }

    public function getLastOrderForDay($day)
    {
        $order = DB::table("task_version")
            ->selectRaw("max(`order`) lastorder")
            ->where("due_date", $day)
            ->first();

        return (is_null($order->lastorder)) ? 1 : ($order->lastorder) + 1;
    }

    public function getMaxOrderForDay($day)
    {
        $order = DB::table("task_version")
            ->selectRaw("max(`order`) maxorder")
            ->where("due_date", $day)
            ->first();

        return (is_null($order->maxorder)) ? 0 : ($order->maxorder);
    }

    public function getMinOrderForDay($day)
    {
        $order = DB::table("task_version")
            ->selectRaw("min(`order`) minorder")
            ->where("due_date", $day)
            ->first();

        return (is_null($order->minorder)) ? 0 : ($order->minorder);
    }

    public function adjustTasks()
    {
        $startDate = "2018-10-01";

        for ($i = 0; $i < 40; $i++) {
            //$this->getMissedTasks($startDate);
            $startDate = (new Carbon($startDate))->addDays(1)->format("Y-m-d");
        }
        dd($startDate);
        return true;
    }

    public function getDueDateOrderSingleDate($due_date)
    {
        $dueDatesOrder = DB::table("tasks")
            ->where("due_date", $due_date)
            ->selectRaw("due_date, max(`order`) lastorder")
            ->groupBy("due_date")
            ->get()
            ->toArray();

        $dueDatesOrder = json_decode(json_encode($dueDatesOrder), true);

        $dueDatesOrder = array_column($dueDatesOrder, 'lastorder', 'due_date');

        return $dueDatesOrder;
    }

    public function updateTasksStatus($tasks, $status = [])
    {
        $inProgress = DB::table("subtasks")->distinct()->whereNotNull("receive_date")->pluck("sample_number")->toArray();

        foreach ($tasks as $date => $tasks_in_date) {
            foreach ($tasks_in_date as $index => $task) {
                // dd($tasks, $tasks_in_date, $task);
                if ($task) {
                    if (array_key_exists('is_user_defined', $task) && $task['is_user_defined']) {
                        $tasks[$date][$index]['status'] = intval($task['status']) ;
                    } elseif (!is_null($task['date_reviewed'])) {
                        $tasks[$date][$index]['status'] = 1;
                    } elseif (in_array($task["sample_number"], $inProgress)) {
                        $tasks[$date][$index]['status'] = 2;
                    } else {
                        $tasks[$date][$index]['status'] = 3;
                    }
                }
            }
        }

        return $tasks;
    }

    /*Status numbers*/

    // done = 1
    // in progress = 2
    // plannign = 3

    public function getTypeValueToday($date, $type)
    {
        $type = TypeValue::where('date', $date)->where('type', $type)->value('value');

        if ($type == null) {

            $type = 0;

        }

        return $type;
    }

    public function getPlannedHoursByDueDate($startDate, $endDate)
    {
        $durations = DB::select(DB::raw("SELECT SUM(tasks.duration) as total from tasks INNER JOIN task_version on tasks.sample_number = task_version.sample_number where (task_version.due_date BETWEEN '$startDate' AND '$endDate') AND task_version.state = 1
        "));

        $durations = json_decode(json_encode($durations), true);

        $durations = array_column($durations, "total");

        if ($durations[0] == null) {

            $durations[0] = 0;

        }

        return $durations[0];
    }

    public function getDoneHoursByDueDate($startDate, $endDate)
    {
        $durations = DB::select(DB::raw("SELECT SUM(duration) as total from tasks where date_reviewed between '$startDate' and '$endDate' "));

        $durations = json_decode(json_encode($durations), true);

        $durations = array_column($durations, "total");

        if ($durations[0] == null) {

            $durations[0] = 0;

        }

        return $durations[0];
    }

    public function getPlannedSamples($startDate, $endDate)
    {
        $samples = DB::select(DB::raw("SELECT COUNT(*) as total from tasks INNER JOIN task_version on tasks.sample_number = task_version.sample_number where (task_version.due_date BETWEEN '$startDate' AND '$endDate') AND task_version.state = 1
        "));

        $samples = json_decode(json_encode($samples), true);

        $samples = array_column($samples, "total");

        if ($samples[0] == null) {

            $samples[0] = 0;

        }

        return $samples[0];
    }

    public function getPlannedSamplesGrouped()
    {
        $samples = DB::select(
            DB::raw(
                "SELECT SUM(tasks.duration) as total , task_version.due_date as planned_date
                FROM tasks INNER JOIN task_version on tasks.sample_number = task_version.sample_number
                WHERE task_version.state = 1 GROUP by planned_date"
            )
        );

        $samples = json_decode(json_encode($samples), true);

        $samples = array_column($samples, "total", "planned_date");

        return $samples;
    }

    public function getDoneSamples($startDate, $endDate)
    {
        $samples = DB::select(DB::raw("SELECT COUNT(*) as total from tasks where date_reviewed between '$startDate' and '$endDate' "));

        $samples = json_decode(json_encode($samples), true);

        $samples = array_column($samples, "total");

        if ($samples[0] == null) {

            $samples[0] = 0;

        }

        return $samples[0];
    }

    public function getPlannedDoneDate($date)
    {
        if ($date->isMonday()) {

            $newDate = $date->previous(Carbon::FRIDAY);

        } else {

            $newDate = $date->subDays(1);
        }

        $newDateString = $newDate->format("Y-m-d");

        return $newDateString;
    }

    public function getLastWorkingDay($date)
    {
        do {
            $dateString = $this->getPlannedDoneDate($date);
        } while ($this->isHoliday($dateString));

        return $dateString;
    }

    public function isHoliday($date)
    {
        $holidays = Holiday::pluck('date')->toArray();

        return in_array($date, $holidays);
    }

    public function getLastImport()
    {
        $last_import = Import::OrderBy('imported_time', 'desc')->first();
        if (!$last_import) {
            return [
                'time' => '',
                'status' => '',
            ];
        }
        $last_import_time = $last_import->imported_time;
        $last_import_status = $last_import->status;

        $data['time'] = (new Carbon($last_import_time))->format('d.m.Y, g:i A');
        $data['status'] = $last_import_status;

        return $data;
    }

    public static function timezoneObj($date)
    {
        $carbon = Carbon::parse($date);
        $time = (array) $carbon->getTimezone();
        $time['timestamp'] = $carbon->getTimestamp();
        return $time;
    }

    public function getCapacityDashboard($dateRange)
    {
        $holydays = DB::table('holidays')->pluck('date')->toArray();
        $capacities = DB::table("capacities")
            ->select('date', 'capacity')
            ->whereBetween('date', $dateRange)
            ->whereNotIn('date', $holydays)
            ->get()
            ->toArray();

        $capacity = 0;
        foreach ($capacities as $item) {
            $capacity += $item->capacity;
        }
        // dd($capacities, $capacity);
        return $capacity;
    }

    public function getDoneHoursByDueDateDashboard($between)
    {
        $start = is_string($between[0]) ? $between[0] : $between[0]->toDateString();
        $end = is_string($between[1]) ? $between[1] : $between[1]->toDateString();
        $durations = DB::table('tasks')
            ->join('task_version', 'task_version.sample_number', '=', 'tasks.sample_number')
            ->select('due_date')
            ->selectRaw('MAX(tasks.duration) as duration, task_version.sample_number, task_version.part')
            ->whereNotNull('date_reviewed')
            ->where('state', 1)
            ->whereBetween('due_date', [$start, $end])
            ->groupBy('due_date')
            ->groupBy('task_version.sample_number')
            ->groupBy('task_version.part')
            ->get();

        $duration = 0;
        foreach ($durations as $row) {
            $duration += $row->duration;
        }

        return $duration;
    }

    public function getAdminTaskCapacityDashboard($between)
    {
        // $holydays = DB::table('holidays')->pluck('date')->toArray();
        $adminTasksCapacities = DB::table("admin_tasks")
            ->select(['duration', 'date'])
        // ->whereNotIn('date', $holydays)
            ->whereBetween('date', $between)
            ->get();

        $adminTasksCapacity = 0;
        foreach ($adminTasksCapacities as $item) {
            $adminTasksCapacity += $item->duration;
        }

        return $adminTasksCapacity;
    }

    public function getAdminTasksCapacityGroupedByCategory($between)
    {
        $startDate = $between[0];
        $endDate = $between[1];

        $adminTasksCapacities = DB::select(DB::raw("SELECT category, SUM(`duration`) as total from admin_tasks where `date` between '$startDate' and '$endDate' group by category "));

        $adminTasksCapacities = json_decode(json_encode($adminTasksCapacities), true);
        $adminTasksCapacities = array_column($adminTasksCapacities, "total", 'category');

        return $adminTasksCapacities;
    }
}

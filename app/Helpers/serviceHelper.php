<?php

namespace App\Helpers;

use App\Helpers\CommanUtility;
use App\Models\RescheduleBacklogCount;
use App\Models\RescheduleBacklogSamplesCount;
use App\Models\TypeValue;
use Carbon\Carbon;
use DB;
use Datetime;

class serviceHelper
{
    use CommanUtility;

    public function getDuedatesIntervalQuery($start = null, $end = null, $samples_on_condition_1 = null, $samples_on_condition_2 = null)
    {
        if (is_null($samples_on_condition_1)) {
            $samples_on_condition_1 = DB::table('task_version')->whereIn('state', [2, 3])
                ->pluck('sample_number')->toArray();
        }
        if (is_null($samples_on_condition_2)) {
            $samples_on_condition_2 = DB::table('task_version as original')
                ->join('task_version as updated', 'original.sample_number', '=', 'updated.sample_number')
                ->where('updated.state', 1)
                ->where('updated.id', '>', 'original.id')
                ->where('original.state', 3)
                ->where('updated.due_date', '<', 'original.due_date')
                ->pluck('original.sample_number')->toArray();
        }

        $query = "SELECT WEEK(tv.due_date, 3) row_week, YEAR(tv.due_date) row_year,
            tasks.sample_number, tasks.login_date, tv.due_date, DATE_FORMAT(tv.created_at, '%Y-%m-%d') as created_at, state,
            Datediff(tv.due_date, login_date) - Holidays_in(login_date, tv.due_date) AS days
			FROM       tasks
			INNER JOIN task_version tv
			ON         tasks.sample_number = tv.sample_number
            WHERE      ";
        if ($start && $end) {
            $query .= " tv.created_at BETWEEN '$start' AND '$end'";
        }
        $query .= " AND state = 1 AND tv.created_at IS NOT NULL";

        if ($samples_on_condition_1 || $samples_on_condition_2) {
            $query .= " AND (";
            if ($samples_on_condition_1) {
                $query .= " (
                    tasks.sample_number NOT IN (" . implode(',', $samples_on_condition_1) . ")
                )";
            }

            if ($samples_on_condition_1 || $samples_on_condition_2) {
                $query .= " OR ";
            }

            if ($samples_on_condition_2) {
                $query .= " (
                    tv.sample_number IN (" . implode(',', $samples_on_condition_2) . ")
                )";
            }
            $query .= ")";
        }

        return $query;
    }

    public function getDueDatesQueryForToday()
    {
        $now = Carbon::now();

        $PlannedDoneDate = $this->getLastWorkingDay($now);

        $query = $this->getDuedatesIntervalQuery($PlannedDoneDate, $PlannedDoneDate);

        $data = DB::select(DB::raw($query));

        $data = json_decode(json_encode($data), true);

        return $data;
    }

    public function getDueDatesByInterval($startDate, $endDate, $groupByDate = null, $withSteps = false)
    {
        $today = Carbon::today();
        $samples_on_condition_1 = DB::table('task_version')->whereIn('state', [2, 3])
            ->pluck('sample_number')->toArray();
        $samples_on_condition_2 = DB::table('task_version as original')
            ->join('task_version as updated', 'original.sample_number', '=', 'updated.sample_number')
            ->where('updated.state', 1)
            ->where('updated.id', '>', 'original.id')
            ->where('original.state', 3)
            ->where('updated.due_date', '<', 'original.due_date')
            ->pluck('original.sample_number')->toArray();

        $startDateCarbon = is_string($startDate) ? Carbon::createFromFormat('Y-m-d', $startDate)->setTime(0, 0, 0) : $startDate;
        $endDateCarbon = is_string($endDate) ? Carbon::createFromFormat('Y-m-d', $endDate)->setTime(0, 0, 0) : $endDate;

        if ($startDateCarbon->lessThanOrEqualTo($today) && $endDateCarbon->greaterThanOrEqualTo($today)) {
            $query = $this->getDuedatesIntervalQuery($startDate, $endDate, $samples_on_condition_1, $samples_on_condition_2);

            $dates = [];

            /**
             * Get calculated for todays day only
             */
            $due_dates_days = DB::select(DB::raw($query));
            foreach ($due_dates_days as $date) {
                if (Carbon::createFromFormat('Y-m-d', $date->created_at)->setTime(0, 0, 0)->greaterThanOrEqualTo($today)) {
                    $dates[] = $date;
                }
            }

            /**
             * Get stored logs otherwise
             */
            $due_dates_logs = DB::table('due_date_logs')
                ->select(['*'])
                ->selectRaw('tv_created_at as created_at')
                ->whereBetween('tv_created_at', [$startDate, $endDate])
                ->get();
            foreach ($due_dates_logs as $date) {
                if (Carbon::createFromFormat('Y-m-d', $date->created_at)->setTime(0, 0, 0)->lessThan($today)) {
                    $dates[] = $date;
                }
            }
        } else {
            $dates = DB::table('due_date_logs')
                ->select(['*'])
                ->selectRaw('tv_created_at as created_at')
                ->whereBetween('tv_created_at', [$startDate, $endDate])
                ->get();
        }

        if ($groupByDate) {
            $dates_grouped_by = collect($dates)->groupBy('created_at');
            $data_for_date = isset($dates_grouped_by[$groupByDate]) ? $dates_grouped_by[$groupByDate] : [];
            $dates = $data_for_date;
        }

        $dates = json_decode(json_encode($dates), true);

        $dueDatesGroups = [
            'two' => [0, 1, 2],
            'five' => [3, 4, 5],
            'ten' => [6, 7],
        ];

        foreach ($dueDatesGroups as $groupKey => $groupDates) {
            $duedates = $this->getDueDatesData($dates, $groupDates);
            $duedatesGrouped = collect($duedates)->groupBy('created_at')->toArray();
            $result[$groupKey] = $duedatesGrouped;
        }

        if ($withSteps) {
            return [
                'data' => $result,
                'swiper_steps' => $this->getDueDatesOfWeekSteps($samples_on_condition_1, $samples_on_condition_2),
            ];
        }

        return $result;
    }

    public function getDueDatesData($dates, $days)
    {
        $filtered = array_filter($dates, function ($date) use ($days) {

            return in_array($date['days'], $days);

        });

        return $filtered;
    }

    public function getDueDatesOfWeekSteps($samples_on_condition_1, $samples_on_condition_2)
    {
        $start = DB::table('tasks')->join('task_version as tv', 'tasks.sample_number', '=', 'tv.sample_number')
            ->selectRaw("WEEK(tv.due_date, 3) row_week, YEAR(tv.due_date) row_year, tasks.sample_number, tasks.login_date, tv.due_date, DATE_FORMAT(tv.created_at, '%Y-%m-%d') as created_at, state")
            ->whereNotIn('tasks.sample_number', $samples_on_condition_1)
            ->whereNotIn('tv.sample_number', $samples_on_condition_2)
            ->where('state', 1)
            ->whereNotNull('tv.created_at')
            ->orderBy('tv.created_at', 'ASC')->first();

        if (!$start || empty($start)) {
            return null;
        }
        // $start = $start[0];

        // Carbon::setWeekStartsAt(Carbon::SUNDAY);
        $now = Carbon::now();
        $current_year = $now->year;
        $current_week = (integer) $now->format('W');
        // if ($now->format('w') == 0) {
        //     $current_week += 1;
        // }

        return get_swiper_steps($current_year, $current_week, $start->row_year, $start->row_week, 52, 'week');
    }

    public function getScheduledTasksCount()
    {
        $samples = RescheduleBacklogCount::where('type', 'reschedule')->get()->toArray();
        $samples = array_column($samples, 'count', 'date');

        return $samples;
    }

    public function getBacklogTasksCount()
    {
        $samples = RescheduleBacklogCount::where('type', 'backlog')->get()->toArray();
        $samples = array_column($samples, 'count', 'date');

        return $samples;
    }

    public function getOwnSample($date)
    {
        $own = TypeValue::where('type', 'service-own-sample')->where('date', $date)->value('value');

        if ($own == null) {
            $own = 0;
        }

        return $own;
    }

    public function getSpecialSamplesByDate($date)
    {
        $rows = DB::table("backlog_logs")
            ->where(function ($query) {
                $query->where('product', "")
                    ->orWhereNull('product');
            })
            ->where('due_date', $date->toDateString())
            ->orderBy('due_date', 'ASC')
            ->paginate(6);

        $rows = json_decode(json_encode($rows), true);
        $items = $rows['data'];
        $rows['data'] = $this->updateTasksStatus([$items])[0];

        return $rows;
    }

    public function getSpecialSamplesOfWeek($year, $week)
    {
        $holydays = DB::table('holidays')->pluck('date')->toArray();
        $rows = DB::table('backlog_logs')
            ->select('due_date')
            ->selectRaw('YEAR(due_date) row_year')
            ->selectRaw('MONTH(due_date) row_month')
            ->selectRaw('DAYOFWEEK(due_date) row_day_of_week')
            ->selectRaw('WEEK(due_date, 3) row_week')
            ->selectRaw('DAY(due_date) row_day')
            ->selectRaw('count(*) as total')
            ->where(function ($query) {
                $query->where('product', "")
                    ->orWhereNull('product');
            })
            ->whereNotIn('due_date', $holydays)
            ->orderBy('due_date', 'ASC')
            ->having('row_year', $year)
            ->having('row_week', $week)
            ->groupBy('due_date')
            ->get();

        $now = Carbon::now();
        $then = $now->setISODate($year, $week);
        $first_day_of_week = $then->startOfWeek();

        $formatted = [];

        for ($i = 1; $i <= 7; $i++) {
            $row = $rows->where('row_day_of_week', $i + 1)->first();
            if ($row) {
                $formatted[] = [
                    'index' => $row->row_day_of_week - 1,
                    'value' => $row->total,
                    'year' => $row->row_year,
                    'month' => $row->row_month,
                    'week' => $row->row_week,
                    'day' => $row->row_day,
                    'due_date' => $row->due_date,
                ];
            } else {
                $formatted[] = [
                    'index' => $i,
                    'value' => 0,
                    'year' => $first_day_of_week->year,
                    'month' => $first_day_of_week->month,
                    'week' => (integer) $first_day_of_week->format('W'),
                    'day' => $first_day_of_week->day,
                    'due_date' => $first_day_of_week->toDateString(),
                ];
            }
            $first_day_of_week->addDays(1);
        }

        return $formatted;
    }

    public function getSpecialSamplesOfWeekSteps($year, $week)
    {
        $holydays = DB::table('holidays')->pluck('date')->toArray();
        $start = DB::table('backlog_logs')
            ->select('due_date')
            ->selectRaw('YEAR(due_date) row_year')
            ->selectRaw('MONTH(due_date) row_month')
            ->selectRaw('DAYOFWEEK(due_date) row_day_of_week')
            ->selectRaw('WEEK(due_date, 3) row_week')
            ->selectRaw('DAY(due_date) row_day')
            ->selectRaw('count(*) as total')
            ->where(function ($query) {
                $query->where('product', "")
                    ->orWhereNull('product');
            })
            ->whereNotIn('due_date', $holydays)
            ->orderBy('due_date', 'ASC')
            ->groupBy('due_date')
            ->first();

        if (!$start) {
            return null;
        }

        // Carbon::setWeekStartsAt(Carbon::SUNDAY);
        $now = Carbon::now();
        $current_year = $now->year;
        $current_week = (integer) $now->format('W');
        // if ($now->format('w') == 0) {
        //     $current_week += 1;
        // }

        return get_swiper_steps($current_year, $current_week, $start->row_year, $start->row_week, 52, 'week');
    }

    public function getRoutineSamplesByDate($date)
    {
        $rows = DB::table('backlog_logs')
            ->whereNotNull('product')
            ->where('product', '<>', '')
            ->where('due_date', $date->toDateString())
            ->orderBy('due_date', 'ASC')
            ->paginate(6);
        $rows = json_decode(json_encode($rows), true);

        return $rows;
    }

    public function getRoutineSamplesOfWeek($year, $week)
    {
        $holydays = DB::table('holidays')->pluck('date')->toArray();
        $rows = DB::table('backlog_logs')
            ->select('due_date')
            ->selectRaw('YEAR(due_date) row_year')
            ->selectRaw('MONTH(due_date) row_month')
            ->selectRaw('DAYOFWEEK(due_date) row_day_of_week')
            ->selectRaw('WEEK(due_date, 3) row_week')
            ->selectRaw('DAY(due_date) row_day')
            ->selectRaw('count(*) as total')
            ->whereNotNull('product')
            ->where('product', '<>', '')
            ->whereNotIn('due_date', $holydays)
            ->orderBy('due_date', 'ASC')
            ->having('row_year', $year)
            ->having('row_week', $week)
            ->groupBy('due_date')
            ->get();

        $now = Carbon::now();
        $then = $now->setISODate($year, $week);
        $first_day_of_week = $then->startOfWeek();

        $formatted = [];

        for ($i = 1; $i <= 7; $i++) {
            $row = $rows->where('row_day_of_week', $i + 1)->first();
            if ($row) {
                $formatted[] = [
                    'index' => $row->row_day_of_week - 1,
                    'value' => $row->total,
                    'year' => $row->row_year,
                    'month' => $row->row_month,
                    'week' => $row->row_week,
                    'day' => $row->row_day,
                    'due_date' => $row->due_date,
                ];
            } else {
                $formatted[] = [
                    'index' => $i,
                    'value' => 0,
                    'year' => $first_day_of_week->year,
                    'month' => $first_day_of_week->month,
                    'week' => (integer) $first_day_of_week->format('W'),
                    'day' => $first_day_of_week->day,
                    'due_date' => $first_day_of_week->toDateString(),
                ];
            }
            $first_day_of_week->addDays(1);
        }

        return $formatted;
    }

    public function getRoutineSamplesOfWeekSteps($year, $week)
    {
        $holydays = DB::table('holidays')->pluck('date')->toArray();
        $start = DB::table('backlog_logs')
            ->select('due_date')
            ->selectRaw('YEAR(due_date) row_year')
            ->selectRaw('MONTH(due_date) row_month')
            ->selectRaw('DAYOFWEEK(due_date) row_day_of_week')
            ->selectRaw('WEEK(due_date, 3) row_week')
            ->selectRaw('DAY(due_date) row_day')
            ->selectRaw('count(*) as total')
            ->whereNotNull('product')
            ->where('product', '<>', '')
            ->whereNotIn('due_date', $holydays)
            ->orderBy('due_date', 'ASC')
            ->groupBy('due_date')
            ->first();

        if (!$start) {
            return null;
        }

        // Carbon::setWeekStartsAt(Carbon::SUNDAY);
        $now = Carbon::now();
        $current_year = $now->year;
        $current_week = (integer) $now->format('W');
        // if ($now->format('w') == 0) {
        //     $current_week += 1;
        // }
        $date = new DateTime;
        $date->setISODate($current_year-1, 53);
        $weeksInYear = ($date->format("W") === "53" ? 53 : 52);
        return get_swiper_steps($current_year, $current_week, $start->row_year, $start->row_week, $weeksInYear, 'week');
//        return get_swiper_steps($current_year, $current_week, $start->row_year, $start->row_week, 52, 'week');
    }

    /**
     * Return all specials grouped by due date
     */
    public function getAllSpecial()
    {
        $specials = DB::table('backlog_logs')->join('tasks', 'backlog_logs.sample_number', '=', 'tasks.sample_number')
            ->select('due_date')
            ->selectRaw('count(*) as total')
            ->where(function ($query) {
                $query->whereNull('backlog_logs.product')
                    ->orWhere('backlog_logs.product', '');
            })
            ->groupBy('backlog_logs.due_date')
            ->get()->toArray();

        $specials = array_column($specials, 'total', 'due_date');

        return $specials;
    }

    /**
     * Return special by date
     */
    public function getSpecialByDate($date)
    {
        $special = DB::table('backlog_logs')->join('tasks', 'backlog_logs.sample_number', '=', 'tasks.sample_number')
            ->select('due_date')
            ->selectRaw('count(*) as total')
            ->where(function ($query) {
                $query->whereNull('backlog_logs.product')
                    ->orWhere('backlog_logs.product', '');
            })
            ->where('backlog_logs.due_date', $date)
            ->groupBy('backlog_logs.due_date')
            ->first();

        return $special;
    }

    /**
     * Return all routines grouped by due date
     */
    public function getAllRoutine()
    {
        $routines = DB::table('backlog_logs')->join('tasks', 'backlog_logs.sample_number', '=', 'tasks.sample_number')
            ->select('due_date')
            ->selectRaw('count(*) as total')
            ->whereNotNull('backlog_logs.product')
            ->where('backlog_logs.product', '<>', '')
            ->groupBy('backlog_logs.due_date')
            ->get()->toArray();

        $routines = array_column($routines, 'total', 'due_date');
        return $routines;
    }

    /**
     * Return routine by date
     */
    public function getRoutineByDate($date)
    {
        $routine = DB::table('backlog_logs')->join('tasks', 'backlog_logs.sample_number', '=', 'tasks.sample_number')
            ->select('due_date')
            ->selectRaw('count(*) as total')
            ->whereNotNull('backlog_logs.product')
            ->where('backlog_logs.product', '<>', '')
            ->where('backlog_logs.due_date', $date)
            ->groupBy('backlog_logs.due_date')
            ->first();

        return $routine;
    }

    public function getServicesValue($all, $date)
    {
        $now = Carbon::now();
        $nowString = $now->format("Y-m-d");
        $dateString = $date->format("Y-m-d");

        if (isset($all[$dateString]) && $dateString < $nowString) {
            return $all[$dateString];
        } else {
            return 0;
        }
    }

    public function getRescheduledValue($allRescheduled, $allBacklog, $date)
    {
        $now = Carbon::now();

        $nowString = $now->format("Y-m-d");
        $dateString = $date->format("Y-m-d");

        if (isset($allRescheduled[$dateString]) && ($dateString < $nowString)) {
            return $allRescheduled[$dateString]; // + $allBacklog[$dateString];
        } else {
            return 0;
        }
    }

    public function getRescheduledSamplesOfWeek($year, $week)
    {
        $scheduledSamples = $this->getScheduledTasksCount();
        $backlogedSamples = $this->getBacklogTasksCount();
        // dd($scheduledSamples, $backlogedSamples);
        $all = [];
        $now = Carbon::now();
        $then = $now->setISODate($year, $week);
        $first_day_of_week = $then->startOfWeek();
        $holydays = DB::table('holidays')->pluck('date')->toArray();

        $formatted = [];

        for ($i = 1; $i <= 7; $i++) {
            if (in_array($first_day_of_week->toDateString(), $holydays)) {
                $formatted[] = [
                    'index' => $i,
                    'value' => 0,
                    'year' => $first_day_of_week->year,
                    'month' => $first_day_of_week->month,
                    'week' => (integer) $first_day_of_week->format('W'),
                    'day' => $first_day_of_week->day,
                    'date' => $first_day_of_week->toDateString(),
                ];
            } else {
                $formatted[] = [
                    'index' => $i,
                    'value' => $this->getRescheduledValue($scheduledSamples, $backlogedSamples, $first_day_of_week),
                    'year' => $first_day_of_week->year,
                    'month' => $first_day_of_week->month,
                    'week' => (integer) $first_day_of_week->format('W'),
                    'day' => $first_day_of_week->day,
                    'date' => $first_day_of_week->toDateString(),
                ];
            }

            $first_day_of_week->addDay();
        }

        return $formatted;
    }

    public function getRescheduledSamplesOfWeekSteps($year, $week)
    {
        $holydays = DB::table('holidays')->pluck('date')->toArray();
        $start = RescheduleBacklogCount::select(['date', 'type'])
            ->selectRaw('YEAR(date) row_year')
            ->selectRaw('MONTH(date) row_month')
            ->selectRaw('DAYOFWEEK(date) row_day_of_week')
            ->selectRaw('WEEK(date, 3) row_week')
            ->selectRaw('DAY(date) row_day')
            ->whereNotIn('date', $holydays)
            ->where('type', 'reschedule')->orWhere('type', 'backlog')->orderBy('date', 'ASC')->first();

        if (!$start) {
            return null;
        }

        // Carbon::setWeekStartsAt(Carbon::SUNDAY);
        $now = Carbon::now();
        $current_year = $now->year;
        $current_week = (integer) $now->format('W');
        // if ($now->format('w') == 0) {
        //     $current_week += 1;
        // }

        return get_swiper_steps($current_year, $current_week, $start->row_year, $start->row_week, 52, 'week');
    }

    public function getSampleFullFilled($day)
    {
        $missed = DB::select(DB::raw("SELECT count(DISTINCT(tasks.sample_number)) missed FROM tasks
		LEFT JOIN backlog_logs ON tasks.sample_number = backlog_logs.sample_number WHERE
        backlog_logs.due_date = '$day'"));
        // $missed = DB::select(DB::raw("SELECT count(DISTINCT(tasks.sample_number)) missed FROM tasks
        // LEFT JOIN task_version ON tasks.sample_number = task_version.sample_number WHERE
        //  task_version.state = 2 AND  task_version.due_date = '$day'"));

        $missed = json_decode(json_encode($missed), true);

        $missedCount = 0;
        if (isset($missed[0]) && isset($missed[0]['missed'])) {
            $missedCount = $missed[0]['missed'];
        }

        $achived = DB::select(DB::raw(" SELECT count(tasks.sample_number) achived FROM tasks
		WHERE  tasks.date_reviewed = '$day' "));

        $achived = json_decode(json_encode($achived), true);

        $achivedCount = 0;
        if (isset($achived[0]) && isset($achived[0]['achived'])) {
            $achivedCount = $achived[0]['achived'];
        }

        $supposedToAchive = $achivedCount + $missedCount;
        $fullFilled = 100;

        if ($supposedToAchive != 0) {

            $fullFilled = ceil(($achivedCount / $supposedToAchive) * 100);
        }

        return $fullFilled;
    }

    public function getDueDatesByDate($startDate, $endDate, $groupByDate, $type)
    {
        $query = $this->getDuedatesIntervalQuery($startDate, $endDate);

        $dates = DB::select(DB::raw($query));

        $dates_grouped_by = collect($dates)->groupBy('created_at');
        $data_for_date = isset($dates_grouped_by[$groupByDate]) ? $dates_grouped_by[$groupByDate] : [];

        $dates = json_decode(json_encode($data_for_date), true);

        $dueDatesGroups = [
            'two' => [0, 1, 2],
            'five' => [3, 4, 5],
            'ten' => [6, 7],
        ];

        $duedates = array_values($this->getDueDatesData($dates, $dueDatesGroups[$type]));

        return collect($duedates);
    }

    public function getRescheduleSamplesByDate($date)
    {
        $rows = RescheduleBacklogSamplesCount::selectRaw('tasks.sample_number, MIN(subtasks.due_date) as due_date, MAX(batch) as batch, MAX(product) as product')
            ->selectRaw('MAX(project) as project, MAX(description) as description, MAX(duration) as duration')
            ->selectRaw('MAX(tasks.date_reviewed) as date_reviewed, MAX(tasks.status) as status, MAX(reschedule_backlog_samples_counts.count) as count')
            ->distinct('tasks.sample_number')
            ->join('tasks', 'reschedule_backlog_samples_counts.sample_number', '=', 'tasks.sample_number')
            ->join('subtasks', 'reschedule_backlog_samples_counts.sample_number', '=', 'subtasks.sample_number')
            ->where('date', $date->toDateString())
            ->where('reschedule_backlog_samples_counts.type', 'reschedule')
            ->orderBy('due_date', 'ASC')
            ->groupBy('subtasks.sample_number')
        // ->get();
        // ->toSql();
            ->paginate(6);
        // dd($rows, $date->toDateString());
        $rows = json_decode(json_encode($rows), true);
        $items = $rows['data'];
        $rows['data'] = $this->updateTasksStatus([$items])[0];

        return $rows;
    }
}

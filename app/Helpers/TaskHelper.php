<?php

namespace App\Helpers;

use App\Formatters\TaskFormatter;
use App\Helpers\AdminTasksUtility;
use App\Helpers\ChartsUtility;
use App\Helpers\CommanUtility;
use App\Helpers\DatesUtility;
use App\Helpers\formatUtility;
use App\Models\Subtask;
use App\Models\Task;
use App\Models\TaskVersion;
use Carbon\Carbon;
use Illuminate\Support\Facades\DB;
use File;

class TaskHelper
{
    use DatesUtility, ChartsUtility, AdminTasksUtility, CommanUtility, formatUtility;

    public function getTasksOfPastWeeks($week)
    {
        /**
         * Completed tasks
         */

        $firstWeek = $week['firstOfWeek'];
        $lastWeek = $week['lastOfWeek'];
        $completed_tasks = Task::with(['subtasks', 'latestVersion'])
            ->select(['task_version.sample_number', 'task_version.part', 'task_version.due_date'])
            ->selectRaw('MAX(tasks.id) as id, MAX(tasks.duration) as duration, MAX(tasks.batch) as batch, MAX(tasks.product) as product, MAX(tasks.analysis) as analysis')
            ->selectRaw('MAX(tasks.days) as days, MAX(tasks.description) as description, MAX(tasks.stage) as stage, MAX(tasks.status) as status, MAX(tasks.project) as project')
            ->selectRaw('MAX(tasks.original_due_date) as original_due_date, MAX(tasks.login_date) as login_date, MAX(tasks.receive_date) as receive_date, MAX(tasks.date_completed) as date_completed, MAX(tasks.date_reviewed) as date_reviewed, MAX(tasks.is_user_defined) as is_user_defined')
            ->join('task_version', 'task_version.sample_number', '=', 'tasks.sample_number')
            ->whereBetween('task_version.due_date', [$firstWeek, $lastWeek])
            ->whereNotNull('date_reviewed')
            ->where('task_version.state', 1)
            ->where('duration', '>', 0)
            ->groupBy('task_version.due_date')
            ->groupBy('task_version.sample_number')
            ->groupBy('task_version.part')
            ->get()
            ->groupBy('due_date')
            ->toArray();

        /**
         * In complete parts
         */
        $today = Carbon::today();

        $sample_numbers = Subtask::selectRaw("sample_number, MAX(`due_date`) as `due_date`")
            ->where('due_date', '>=', $today->toDateString())
            ->groupBy('sample_number')
            ->pluck('sample_number')->toArray();

        $moved_from_back_log = TaskVersion::where('state', 5)
            ->selectRaw("sample_number")
            ->groupBy('sample_number')
            ->pluck('sample_number')->toArray();

        // dd($sample_numbers);
        $in_complete_tasks_with_parts = TaskVersion::WeekTasksByColoumn($week, 'due_date')
            ->with('task.subtasks')
            ->where(function ($query) use ($sample_numbers, $moved_from_back_log) {
                $query->whereIn('sample_number', $sample_numbers)
                    ->orWhereIn('sample_number', $moved_from_back_log);
            })
            ->NotHardMovedToBacklog()
            ->NotCompletedParentWithDuration()
        // ->NotMissingData()
            ->Active()
            ->get();
        $in_complete_samples = array_unique($in_complete_tasks_with_parts->pluck('sample_number')->toArray());

        // $due_dates = $this->getMaxTaskDuedates($in_complete_samples);

        $in_complete_tasks_with_parts = $in_complete_tasks_with_parts->groupBy('due_date')
            ->toArray();

        $formatted_completed_tasks = $this->formatFromTasks($completed_tasks, true);
        $formatted_in_complete_tasks_with_parts = $this->formatFromTaskVersions($in_complete_tasks_with_parts, false);
        $first_day_of_week = Carbon::createFromFormat('Y-m-d', $week['firstOfWeek']);
        $last_day_of_week = Carbon::createFromFormat('Y-m-d', $week['lastOfWeek']);

        $formated = [];
        while ($first_day_of_week->lessThanOrEqualTo($last_day_of_week)) {

            $day = $first_day_of_week->toDateString();
            if (isset($formatted_completed_tasks[$day]) && isset($formatted_in_complete_tasks_with_parts[$day])) {
                $formated[$day] = array_merge($formatted_completed_tasks[$day], $formatted_in_complete_tasks_with_parts[$day]);
            } elseif (isset($formatted_completed_tasks[$day])) {
                $formated[$day] = $formatted_completed_tasks[$day];
            } elseif (isset($formatted_in_complete_tasks_with_parts[$day])) {
                $formated[$day] = $formatted_in_complete_tasks_with_parts[$day];
            }

            $first_day_of_week->addDays(1);
        }

        // $formated = array_merge($this->formatFromTasks($completed_tasks), $this->formatFromTaskVersions($in_complete_tasks_with_parts, false, $due_dates));

        return $formated;
    }

    private function getMaxTaskDuedates($in_complete_samples = [])
    {
        $due_dates = TaskVersion::select(['sample_number'])
            ->selectRaw('MAX(due_date) as due_date')
            ->where('state', 1)
            ->whereIn('sample_number', $in_complete_samples)
            ->groupBy('sample_number')
            ->get()
            ->groupBy('sample_number')->toArray();
        // $due_dates = Subtask::select(['sample_number'])
        //     ->selectRaw('MAX(due_date) as due_date')
        //     ->whereIn('sample_number', $in_complete_samples)
        //     ->groupBy('sample_number')
        //     ->get()
        //     ->groupBy('sample_number')->toArray();

        $user_made_samples_due_dates = TaskVersion::selectRaw('task_version.sample_number, MAX(task_version.due_date) as due_date')
            ->join('tasks', 'tasks.sample_number', '=', 'task_version.sample_number')
            ->where('state', 1)
            ->where("is_user_defined", 1)
            ->groupBy('task_version.sample_number')
            ->get()
            ->groupBy('sample_number')->toArray();

        foreach ($user_made_samples_due_dates as $sample => $due) {
            $due_dates[$sample][0] = [
                'sample_number' => $sample,
                'due_date' => $due[0]['due_date'],
            ];
        }

        return $due_dates;
    }

    public function getDoneTasks($start, $end)
    {
        $tasks = Task::with(['subtasks', 'latestVersion'])
            ->CompletedTasks()
            ->whereBetween('date_reviewed', [$start, $end])
            ->WithDuration()
            ->get()
            ->toArray();

        $tasks = $this->formatDoneTasks($tasks);

        return $tasks;
    }

    public function getPlannedTasks($start, $end, $counter_number)
    {
        $tasks = TaskVersion::with('task.subtasks')
            ->whereBetween('due_date', [$start, $end])
            ->NotCompletedParent()
            ->Active()
            ->get()
            ->toArray();

        $tasks = $this->formatPlannedTasks($tasks, $counter_number);

        return $tasks;
    }

    public function getBacklogOfPastWeeks($week)
    {
        $today = Carbon::today()->format("Y-m-d");
        $status = $this->getTasksStatus();

        /**
         * Because task version would start from received_date we'll use max(due date) from subtasks
         */
        $missed_due_sample_numbers = Subtask::select(['sample_number'])
            ->selectRaw('MAX(due_date) as due_date')
            ->where('due_date', '<', $today)
            ->groupBy('sample_number')
            ->pluck('sample_number')
            ->toArray();

        $missed_due_sample_numbers = $missed_due_sample_numbers ? implode(',', $missed_due_sample_numbers) : "''";

        $tasks = TaskVersion::with('task.subtasks')
            ->selectRaw('MAX(`task_version`.`id`) id, MAX(`task_version`.`order`) `order`, MAX(`task_version`.`part`) `part`, MAX(`task_version`.`due_date`) `due_date`')
            ->selectRaw('MAX(`task_version`.`state`) state, MAX(`task_version`.`move_state`) move_state, MAX(`task_version`.`created_at`) created_at, MAX(`task_version`.`updated_at`) updated_at')
            ->selectRaw('`task_version`.`sample_number`, MAX(tasks.original_due_date) as original_due_date, MAX(`task_version`.`is_hard_moved_from_backlog`) is_hard_moved_from_backlog, MAX(`task_version`.`is_hard_moved_to_backlog`) is_hard_moved_to_backlog, MAX(`tasks`.`description`) description')
            ->join('tasks', 'task_version.sample_number', '=', 'tasks.sample_number')
            ->havingRaw("(task_version.sample_number IN ($missed_due_sample_numbers) AND original_due_date < '$today' AND original_due_date >= '" . $week['firstOfWeek'] . "' AND original_due_date <= '" . $week['lastOfWeek'] . "')")
            ->orHavingRaw("(is_hard_moved_to_backlog = 1)")
            ->whereNull('tasks.date_reviewed')
            ->whereNull('tasks.date_completed')
            ->where('task_version.state', 1)
            ->groupBy('sample_number')
            ->get()
            ->groupBy('original_due_date')
            ->toArray();

        return $this->formatFromTaskVersions($tasks);
    }

    public function pastWeeksData($week, $type = 'calendar')
    {
        $status = $this->getTasksStatus();

        if ($type == "list") {
            $adminTasks = [];
            $backlog = [];
        } else {
            $backlog = $this->getBacklogOfPastWeeks($week);
            $backlog = $this->updateTasksStatus($backlog, $status);
            $adminTasks = $this->getAdminTasks($week);
        }

        $tasks = $this->updateTasksStatus($this->getTasksOfPastWeeks($week), $status);

        $between = [$week['firstOfWeek'], $week['lastOfWeek']];
        $capacity = CapacityHelper::getCapacity($between);
        $adminCap = AdminTaskHelper::getAdminTaskcapacity();
        $demandRates = DemanRateHelper::getDemandRates();

        $input = [
            'pastTasks' => $tasks,
            'futureTasks' => [],
            'backlog' => $backlog,
            'adminTasks' => $adminTasks,
            'capacity' => $capacity,
            'adminCap' => $adminCap,
            'demandRates' => $demandRates,
            'start_date' => $between[0],
            'end_date' => $between[1],
            'request_type' => $type,
            'doneHoursSumPerDate' => $this->getDoneHoursSumForEachDueDateDashboard($between),
        ];

        return TaskFormatter::calendarViewResponse($input);
    }

    public function getDoneHoursSumForEachDueDateDashboard($between)
    {
        $durations = DB::table('tasks')
            ->join('task_version', 'task_version.sample_number', '=', 'tasks.sample_number')
            ->select('due_date')
            ->selectRaw('MAX(tasks.duration) as duration, task_version.sample_number, task_version.part')
            ->whereNotNull('date_reviewed')
            ->where('state', 1)
            ->whereBetween('due_date', $between)
            ->groupBy('due_date')
            ->groupBy('task_version.sample_number')
            ->groupBy('task_version.part')
            ->get()->groupBy('due_date');

        $res = [];
        foreach ($durations as $date => $rows) {
            $dur = $rows->sum('duration');
            $res[$date] = $dur;
        }

        return $res;
    }

    public function getTasksOfFutureWeeks($week)
    {
        $type = app('request')->input("type");
        $type = isset($type) ? $type : "calendar";

        $tasks = TaskVersion::WeekTasksByColoumn($week, 'due_date')
            ->with('task.subtasks')
            ->NotHardMovedToBacklog()
            ->NotCompletedParentWithDuration()
        // ->NotMissingData()
            ->Active()
            ->get();

        $sample_numbers = array_unique($tasks->pluck('sample_number')->toArray());
        // $due_dates = $this->getMaxTaskDuedates($sample_numbers);

        $tasks = $tasks->groupBy('due_date')
            ->toArray();

        return $formated = $this->formatFromTaskVersions($tasks, false);
    }

    public function getTasksOfFutureWeeksTemp($week)
    {
        $today = Carbon::today()->format("Y-m-d");

        $type = app('request')->input("type");
        $type = isset($type) ? $type : "calendar";

        $tasks = TaskVersion::WeekTasksByColoumn($week, 'due_date')
            ->with('task.subtasks')
            ->NotHardMovedToBacklog()
            ->NotCompletedParentWithDuration()
        // ->NotMissingData()
            ->Active()
            ->get();

        $completed_tasks = Task::with(['subtasks', 'latestVersion'])
            ->select(['task_version.sample_number', 'task_version.part', 'task_version.due_date'])
            ->selectRaw('MAX(tasks.id) as id, MAX(tasks.duration) as duration, MAX(tasks.batch) as batch, MAX(tasks.product) as product, MAX(tasks.analysis) as analysis')
            ->selectRaw('MAX(tasks.days) as days, MAX(tasks.description) as description, MAX(tasks.stage) as stage, MAX(tasks.status) as status, MAX(tasks.project) as project')
            ->selectRaw('MAX(tasks.original_due_date) as original_due_date, MAX(tasks.login_date) as login_date, MAX(tasks.receive_date) as receive_date, MAX(tasks.date_completed) as date_completed, MAX(tasks.date_reviewed) as date_reviewed, MAX(tasks.is_user_defined) as is_user_defined')
            ->join('task_version', 'task_version.sample_number', '=', 'tasks.sample_number')
            ->whereBetween('task_version.due_date', [$week['firstOfWeek'], $week['lastOfWeek']])
            ->whereNotNull('date_reviewed')
            ->where('task_version.state', 1)
            ->where('duration', '>', 0)
            ->groupBy('task_version.due_date')
            ->groupBy('task_version.sample_number')
            ->groupBy('task_version.part')
            ->get()
            ->groupBy('due_date')
            ->toArray();
        $completed_tasks = $this->formatFromTasks($completed_tasks, true);
        // $currentTasks = $this->getCurrentDayCompletedTasks();

        $sample_numbers = array_unique($tasks->pluck('sample_number')->toArray());
        // $due_dates = $this->getMaxTaskDuedates($sample_numbers);

        $tasks = $tasks->groupBy('due_date')
            ->toArray();

        $formated = $this->formatFromTaskVersions($tasks, false);

        if (isset($completed_tasks[$today])) {
            $formated[$today] = isset($formated[$today]) ? array_merge($formated[$today], $completed_tasks[$today]) : $completed_tasks[$today];
        }

        return $formated;
    }

    public function getCurrentDayCompletedTasks()
    {

        $today = Carbon::today()->format("Y-m-d");

        $week['firstOfWeek'] = $today;
        $week['lastOfWeek'] = $today;

        $tasks = Task::with(['subtasks', 'latestVersion'])
            ->CompletedTasks()
            ->WeekTasksByColoumn($week, 'date_reviewed')
            ->WithDuration()
            ->get()
            ->groupBy('date_reviewed')
            ->toArray();

        return $this->formatFromTasks($tasks);
    }

    public function futureWeeksData($week, $type)
    {
        if ($type == "list") {
            $adminTasks = [];
        } else {
            $adminTasks = $this->getAdminTasks($week);
        }

        $status = $this->getTasksStatus();

        $today = $week['today'];
        $futureTasks = $this->getTasksOfFutureWeeks($week);
        $futureTasks = $this->updateTasksStatus($futureTasks, $status);
        $currentTasks = $this->getCurrentDayCompletedTasks();

        if (isset($currentTasks[$today])) {
            $futureTasks[$today] = isset($futureTasks[$today]) ? array_merge($futureTasks[$today], $currentTasks[$today]) : $currentTasks[$today];
        }

        $between = [$week['firstOfWeek'], $week['lastOfWeek']];
        $capacity = CapacityHelper::getCapacity($between);
        $adminCap = AdminTaskHelper::getAdminTaskcapacity();
        $demandRates = DemanRateHelper::getDemandRates();

        $input = [
            'pastTasks' => [],
            'futureTasks' => $futureTasks,
            'backlog' => [],
            'adminTasks' => $adminTasks,
            'capacity' => $capacity,
            'adminCap' => $adminCap,
            'demandRates' => $demandRates,
            'start_date' => $between[0],
            'end_date' => $between[1],
            'request_type' => $type,
            'doneHoursSumPerDate' => $this->getDoneHoursSumForEachDueDateDashboard($between),
        ];

        return TaskFormatter::calendarViewResponse($input);
    }

    public function futureAndPastWeekData($weekIndex, $type)
    {
        $week = $this->splitWeekDaysToPastAndFuture($weekIndex);
        $status = $this->getTasksStatus();

        if ($type == "list") {
            $adminTasks = [];
            $pastBacklog = [];
        } else {
            $adminWeek = [
                "firstOfWeek" => $week["past"]["firstOfWeek"],
                "lastOfWeek" => $week["future"]["lastOfWeek"],
            ];

            $adminTasks = $this->getAdminTasks($adminWeek);
            $pastBacklog = $this->updateTasksStatus(
                $this->getBacklogOfPastWeeks($week["past"]),
                $status
            );
        }

        $pastTasks = $this->updateTasksStatus(
            $this->getTasksOfPastWeeks($week["past"]),
            $status
        );

        $futureTasks = $this->updateTasksStatus(
            $this->getTasksOfFutureWeeksTemp($week["future"]),
            $status
        );

        $between = [$week['past']['firstOfWeek'], $week['future']['lastOfWeek']];
        $capacity = CapacityHelper::getCapacity($between);
        $adminCap = AdminTaskHelper::getAdminTaskcapacity();
        $demandRates = DemanRateHelper::getDemandRates();

        $input = [
            'pastTasks' => $pastTasks,
            'futureTasks' => $futureTasks,
            'backlog' => $pastBacklog,
            'adminTasks' => $adminTasks,
            'capacity' => $capacity,
            'adminCap' => $adminCap,
            'demandRates' => $demandRates,
            'start_date' => $between[0],
            'end_date' => $between[1],
            'request_type' => $type,
            'doneHoursSumPerDate' => $this->getDoneHoursSumForEachDueDateDashboard($between),
        ];

        return TaskFormatter::calendarViewResponse($input);
    }

    public function handleSubTask($subtasks, $task_id)
    {
        foreach ($subtasks as $key => $value) {
            unset($subtasks[$key]["id"]);
            $subtasks[$key]["task_id"] = $task_id;

        }

        return $subtasks;
    }

    public function getAllTasksGroupByMonth()
    {
        $year = app('request')->input("year");
        $year = !is_null($year) ? $year : carbon::now()->year;

        $tasks = DB::table("task_version")
            ->selectRaw("MONTHNAME(`due_date`) as months , count(*) as total")
            ->whereyear('due_date', $year)
            ->groupBy('months')
            ->get()->toArray();

        $tasks = array_column($tasks, 'total', 'months');
        return $tasks;
    }

    public function move($data)
    {
        $dates = [];
        $taskID = $data['task_id'];
        $dueDate = $data['duedate'];
        $part = $data['part'];
        $order = isset($data['order']) ? $data['order'] : null;
        $carbon_due_date = Carbon::createFromFormat('Y-m-d', $dueDate);
        $now = Carbon::now();

        $task = Task::find($taskID);
        $task_parts = TaskVersion::where('sample_number', $task->sample_number)
            ->where('part', '>=', $part)
            ->where('state', 1)
            ->orderBy('part', 'ASC')->get();
        // dd($task, $task_parts);

        $delete_moved_task_versions = [];
        DB::beginTransaction();
        try {
            $newVersions = [];
            // dd($task_parts);
            foreach ($task_parts as $task_version) {
                $sample_number = $task_version->sample_number;

                $TaskVersionData = $task_version;
                $id = $TaskVersionData->id;
                $oldDueDate = $TaskVersionData->due_date;

                while (DatesUtility::dateIsHoliday($carbon_due_date->toDateString())) {
                    $carbon_due_date->addDays(1);
                }

                $dueDate = $carbon_due_date->toDateString();
                if (empty($dates)) {
                    $dates['old'] = $oldDueDate;
                    $dates['new'] = $dueDate;
                }

                $newVersionData = [
                    'sample_number' => $sample_number,
                    'due_date' => $dueDate,
                    'part' => $task_version->part,
                    'state' => 1,
                    'move_state' => 2,
                    'created_at' => $now->toDateTimeString(),
                ];

                if ($order) {
                    $maxOrder = $this->getMaxOrderForDay($dueDate);
                    $minOrder = $this->getMinOrderForDay($dueDate);

                    if ((($minOrder <= $order) && ($order <= $maxOrder)) || (($minOrder == $maxOrder) && ($minOrder == 1) && ($order == 1))) {
                        DB::statement('update task_version set `order` = `order`+1 where due_date = "' . $dueDate . '" and `order` between ' . $order . ' and ' . $maxOrder);
                        DB::statement('update task_version set `order` = ' . $order . ' where id = ' . $id);
                    } else if ((($minOrder == $maxOrder) && ($minOrder == 0)) || ($order > $maxOrder)) {
                        DB::statement('update task_version set `order` = ' . $order . ' where id = ' . $id);
                    } else {
                        return json_response()->forbidden();
                    }

                    $newVersionData['order'] = $order;
                } else {
                    $newVersionData['order'] = $this->getLastOrderForDay($dueDate);
                }

                if ($TaskVersionData->state == 1 && $TaskVersionData->move_state == 2) {
                    $delete_moved_task_versions[] = $TaskVersionData->id;
                } else {
                    $TaskVersionData->update([
                        'state' => 4,
                        'move_state' => 1,
                    ]);
                }

                $newVersions[] = $newVersionData;
                $carbon_due_date->addDays(1);
            }
            // foreach ($newVersions as &$version) {
            //     $version['due_date'] = $dueDate;
            // }

            TaskVersion::whereIn('id', $delete_moved_task_versions)->delete();
            $newVersion = TaskVersion::insert($newVersions);
        } catch (\Exception $e) {
            DB::rollBack();
            dd($e);
        }
        DB::commit();

        return $dates;
    }

    public function moveAllTimeBacklog($data)
    {
        try {

            $dates = [];
            $taskID = $data['task_id'];
            $dueDate = $data['duedate'];

            $maxOrder = $this->getMaxOrderForDay($dueDate);
            $minOrder = $this->getMinOrderForDay($dueDate);
            $order = isset($data['order']) ? $data['order'] : $maxOrder + 1;

            $task = Task::where('id', $taskID)->first();
            $sample_number = $task->sample_number;

            $durations = DB::table('durations')->get();
            $TaskVersionData = TaskVersion::where('sample_number', $sample_number)
                ->where('state', 1)
                ->orderBy('due_date', 'DESC')
                ->first();

            $oldDueDate = $TaskVersionData->due_date;
            $old_due_date = Carbon::createFromFormat('Y-m-d', $oldDueDate);
            $new_due_date = Carbon::createFromFormat('Y-m-d', $dueDate);

            if ($task->days > 1 && $new_due_date->gt($old_due_date)) {
                $now = Carbon::now();
                $part = $TaskVersionData->part + 1;
                $due_dates_order = DB::table("task_version")
                    ->selectRaw("due_date, max(`order`) last_order")
                    ->whereBetween('due_date', [$oldDueDate, $dueDate])
                    ->groupBy("due_date")
                    ->pluck('last_order', 'due_date')
                    ->toArray();

                $extension_task_versions = [];

                $next_date = $old_due_date->copy()->addDays(1);
                while (DatesUtility::dateIsHoliday($next_date->toDateString())) {
                    $next_date->addDays(1);
                }

                while ($new_due_date->gte($next_date)) {
                    $due = $next_date->toDateString();
                    if (isset($dueDatesOrder[$due])) {
                        $dueDatesOrder[$due] = $dueDatesOrder[$due] + 1;
                    } else {
                        $dueDatesOrder[$due] = 1;
                    }

                    $extension_task_versions[] = [
                        'sample_number' => $sample_number,
                        'due_date' => $due,
                        'order' => $due_dates_order[$due],
                        'part' => $part,
                        'state' => 1,
                        'move_state' => 2,
                        'created_at' => $now->toDateTimeString(),
                    ];

                    $next_date->addDays(1);
                    while (DatesUtility::dateIsHoliday($next_date->toDateString())) {
                        $next_date->addDays(1);
                    }
                    $part++;
                }

                DB::table('task_version')->insert($extension_task_versions);
                $newTaskVersion = $TaskVersionData->replicate();
                $newTaskVersion->state = 5;
                $newTaskVersion->save();

                $task_duration = $durations->where('product', $task->product)
                    ->where('analysis', $task->analysis)->first();

                if ($task_duration) {
                    $parts = ($part - 1) > 1 ? ($part - 1) : 1;
                    $task->update([
                        'duration' => $task_duration->total_duration / $parts,
                        'days' => $parts,
                    ]);
                } else {
                    $parts = ($part - 1) > 1 ? ($part - 1) : 1;
                    $task->update([
                        'duration' => ($task->duration * $task->days) / $parts,
                        'days' => $parts,
                    ]);
                }
            } else {
                /**
                 ************************************************************************
                 ************************ Ask Mostafa about this ************************
                 ************************************************************************
                 * Used in formatUtility::formatFromTaskVersions
                 * to set was_backlog value
                 */
                //$TaskVersionData->is_hard_moved_from_backlog = 1;
                // dd($TaskVersionData);
                $TaskVersionData->state = 5;
                $TaskVersionData->save();

                $newTaskVersion = $TaskVersionData->replicate();
                $newTaskVersion->state = 1;
                $newTaskVersion->due_date = $dueDate;
                $newTaskVersion->is_hard_moved_from_backlog = 1;
                $newTaskVersion->save();
            }

            // $id = $newTaskVersion->id;

            if ((($minOrder <= $order) && ($order <= $maxOrder)) || (($minOrder == $maxOrder) && ($minOrder == 1) && ($order == 1))) {
                DB::statement('update task_version set `order` = `order`+1 where due_date = "' . $dueDate . '" and `order` between ' . $order . ' and ' . $maxOrder);
                DB::statement('update task_version set `order` = ' . $order . ' where sample_number = ' . $sample_number);
            } else if ((($minOrder == $maxOrder) && ($minOrder == 0)) || ($order > $maxOrder)) {
                DB::statement('update task_version set `order` = ' . $order . ' where sample_number = ' . $sample_number);
            } else {
                return json_response()->forbidden();
            }

            $dates['old'] = $oldDueDate;
            $dates['new'] = $dueDate;

            return $dates;

        } catch (ModelNotFoundException $e) {
            return json_response()->notFound();
        }
    }

    public function getFullTimeBacklog()
    {
        $today = Carbon::today()->format("Y-m-d");
        $status = $this->getTasksStatus();

        /**
         * Because task version would start from received_date we'll use max(due date) from subtasks
         */
        $missed_due_sample_numbers = Subtask::select(['sample_number'])
            ->selectRaw('MAX(due_date) as due_date')
            ->where('due_date', '<', $today)
            ->groupBy('sample_number')
            ->pluck('sample_number')
            ->toArray();

        $missed_due_sample_numbers = $missed_due_sample_numbers ? implode(',', $missed_due_sample_numbers) : "''";

        $tasks = TaskVersion::with('task.subtasks')
            ->selectRaw('MAX(`task_version`.`id`) id, MAX(`task_version`.`order`) `order`, MAX(`task_version`.`part`) `part`, MAX(`task_version`.`due_date`) `due_date`')
            ->selectRaw('MAX(`task_version`.`state`) state, MAX(`task_version`.`move_state`) move_state, MAX(`task_version`.`created_at`) created_at, MAX(`task_version`.`updated_at`) updated_at')
            ->selectRaw('`task_version`.`sample_number`, MAX(tasks.original_due_date) as original_due_date, MAX(`task_version`.`is_hard_moved_from_backlog`) is_hard_moved_from_backlog, MAX(`task_version`.`is_hard_moved_to_backlog`) is_hard_moved_to_backlog, MAX(`tasks`.`description`) description')
            ->join('tasks', 'task_version.sample_number', '=', 'tasks.sample_number')
            ->havingRaw("(task_version.sample_number IN ($missed_due_sample_numbers) AND original_due_date < '$today')")
            ->orHavingRaw("(is_hard_moved_to_backlog = 1)")
            ->whereNull('tasks.date_reviewed')
            ->whereNull('tasks.date_completed')
            ->where('task_version.state', 1)
            ->groupBy('sample_number')
            ->orderBy('original_due_date')
            ->orderBy('description')
            ->get()
            ->groupBy('due_date')
            ->toArray();

        return $this->formatFullBacklog($this->updateTasksStatus($this->formatFromTaskVersions($tasks, true), $status));
    }

    public function getAllMissingSamples()
    {
        /**
         * Display all analyses per sample_number
         */
        $tasks = TaskVersion::with('task.subtasks')
            ->selectRaw('MAX(`task_version`.`id`) id, MAX(`task_version`.`order`) `order`, MAX(`task_version`.`part`) `part`, MAX(`task_version`.`due_date`) `due_date`')
            ->selectRaw('MAX(`task_version`.`state`) state, MAX(`task_version`.`move_state`) move_state, MAX(`task_version`.`created_at`) created_at, MAX(`task_version`.`updated_at`) updated_at')
            ->selectRaw('`task_version`.`sample_number`, MAX(`task_version`.`is_hard_moved_from_backlog`) is_hard_moved_from_backlog, MAX(`task_version`.`is_hard_moved_to_backlog`) is_hard_moved_to_backlog')
            ->selectRaw('subtasks.analysis as subtask_analysis')
            ->join('tasks', 'task_version.sample_number', '=', 'tasks.sample_number')
            ->join('subtasks', 'subtasks.sample_number', '=', 'tasks.sample_number')
            ->where('tasks.duration', 0) // ->ParentWithOutDuration()
            ->where('task_version.state', 1) // ->Active()
            ->orderBy('due_date', 'ASC')
            ->groupBy('sample_number')
            ->groupBy('subtasks.analysis')
            ->orderBy('sample_number')
            ->get()
            ->groupBy('due_date')
            ->toArray();
        // dd($tasks->toArray());

        return $this->formatFullBacklog($this->updateTasksStatus($this->formatFromTaskVersions($tasks, true)));
    }

    public function writeCsv($tasks, $backlogTasks)
    {
        $name = 'list_' . date('Y-m-d') . rand() . '.csv';
        $path = public_path('csvs/');

        if(!File::isDirectory($path)){
            File::makeDirectory($path, 0777, true, true);
        }

        $file_pointer = fopen($path . $name, 'w+');
        
        fputcsv($file_pointer, array('ID', 'BATCH', 'PRODUCT', 'SAMPLE_NUMBER', 'DESCRIPTION', 'DUE_DATE', 'DATE_REVIEWED', 'STATUS', 'DURATION', 'CHANGED', 'ORIGINAL_DUE_DATE', 'USER_DEFINED', 'WAS_BACKLOG'));

        foreach ($tasks as $index => $task) {

            fputcsv($file_pointer, $task);
        }

        fputcsv($file_pointer, array('', '', '', '', '', '', '', '', '', '', '', ''));
        fputcsv($file_pointer, array('BACKLOG', '', '', '', '', '', '', '', '', '', '', ''));
        fputcsv($file_pointer, array('', '', '', '', '', '', '', '', '', '', '', ''));

        fputcsv($file_pointer, array('ID', 'BATCH', 'PRODUCT', 'SAMPLE_NUMBER', 'DESCRIPTION', 'DUE_DATE', 'DATE_REVIEWED', 'STATUS', 'DURATION', 'CHANGED', 'ORIGINAL_DUE_DATE', 'USER_DEFINED', 'WAS_BACKLOG'));

        foreach ($backlogTasks as $index => $backlogTask) {

            fputcsv($file_pointer, $backlogTask);
        }

        fclose($file_pointer);

        $file = env('APP_URL') . "/csvs/" . $name;

        return $file;
    }

    public function isMoved($taskID, $allMoved)
    {
        if (in_array($taskID, $allMoved)) {
            return 1;
        }
        return 0;
    }

    public function getAllMovedTasks()
    {
        //$tasks = DB::table('task_version')->select('task_id')->where('move_state',2)->where('task_id','<>',null)->get();
        $tasks = DB::table('task_version')->select('sample_number')->where('move_state', 2)->get();
        $tasks = json_decode(json_encode($tasks), true);
        $tasks = array_column($tasks, 'sample_number');

        return $tasks;
    }

    public function getOriginalDueDates()
    {
        //$tasks = DB::table('task_version')->select('task_id','due_date')->where('state',4)->where('task_id','<>',null)->get();
        $tasks = DB::table('task_version')->select('sample_number', 'due_date')->where('state', 4)->get();
        $tasks = json_decode(json_encode($tasks), true);
        $tasks = array_column($tasks, 'due_date', 'sample_number');

        return $tasks;
    }

    public function getAllBacklogedSamples()
    {
        $tasks = DB::table('task_version')
            ->select('sample_number')
            ->where('state', 5)
            ->get();
        $tasks = json_decode(json_encode($tasks), true);
        $tasks = array_column($tasks, 'sample_number');

        return $tasks;
    }

    public function getDurationFromAdminTask($data, $category)
    {
        if ($data['category'] == $category) {
            return (float) $data['duration'];
        }
    }
}

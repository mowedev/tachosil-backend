<?php

namespace App\Helpers;

use Illuminate\Support\Facades\DB;

class AdminTaskHelper
{
    public static function getAdminTaskcapacity()
    {
        $admin = DB::table("admin_tasks")
            ->selectRaw("sum(duration) total, `date`")
            ->groupBy("date")
            ->get()
            ->toArray();

        $admin = json_decode(json_encode($admin), true);
        $admin = array_column($admin, "total", "date");

        return $admin;
    }
}

<?php

namespace App\Helpers;

use DB;

class CapacityHelper
{
    public static function getCapacity($dateRange)
    {
        $capacity = DB::table("capacities")
            ->select('date', 'capacity')
            ->whereBetween('date', $dateRange)
            ->get()
            ->toArray();

        $capacity = json_decode(json_encode($capacity), true);
        $capacity = array_column($capacity, "capacity", 'date');

        return $capacity;
    }
}

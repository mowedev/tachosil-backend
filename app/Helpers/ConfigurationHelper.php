<?php

namespace App\Helpers;

use App\Models\Configuration;
use App\Models\Leadership;
use App\Models\ProblemStatus;
use App\Models\Section;
use Carbon\Carbon;
use DB;

class ConfigurationHelper
{
    public static function config($key, $default = null)
    {
        $configuration = Configuration::where('key', $key)->first();
        return $configuration ? $configuration->value : $default;
    }

    public function updateConfigurations($data)
    {
        $capacity = isset($data['capacity']) ? $data['capacity'] : [];
        $challengesStatuses = isset($data['challenges']) ? $data['challenges'] : [];
        /*   $leaderships = isset($data['team']) ? $data['team'] : [];
        $productivity = isset($data['productivity']) ? collect($data['productivity'])->except(['capacity'])->toArray() : [];
        $data['productivity'] = $productivity;
        $data = collect($data)->except(['team', 'challenges'])->toArray();
        $sections = $this->getAllSections();

        foreach ($data as $key => $value) {

        foreach ($value as $configKey => $configValue) {

        if (isset($sections[$key])) {

        $sectionId = $sections[$key];

        $configValue = ($configKey == "tachometer_color_ranges") ? json_encode($configValue) : $configValue;

        Configuration::updateOrCreate(
        ['section_id' => $sectionId, 'key' => $configKey],
        ['value' => $configValue]
        );
        }
        }
        }
         */
        $this->updateChallengesStatuses($challengesStatuses);
        // $this->updateLeadershipsNames($leaderships);
        $this->updateCapacity($capacity);
    }

    public function getAllSections()
    {
        $sections = Section::all()->makeHidden(['created_at', 'updated_at'])->toArray();
        $sections = array_column($sections, 'id', 'name');
        return $sections;
    }

    public function updateChallengesStatuses($challengesStatuses)
    {
        foreach ($challengesStatuses as $key => $value) {

            $statusId = $value['id'];
            $statusName = $value['status'];
            $statusColor = $value['color'];
            ProblemStatus::where('id', $statusId)->update(['status' => $statusName, 'color' => $statusColor]);
        }
    }

    public function updateLeadershipsNames($leaderships)
    {
        foreach ($leaderships as $index => $value) {

            $leadershipId = isset($value['id']) ? $value['id'] : null;
            $leadershipName = $value['name'];

            if ($leadershipId) {
                Leadership::where('id', $leadershipId)->update(['name' => $leadershipName]);

            } else {
                Leadership::create(['name' => $leadershipName]);
            }
        }
    }

    public function ListLeaderships()
    {
        $leaderships = Leadership::whereNotNull('name')->where('name', '!=', '')->get()->makeHidden(['created_at', 'updated_at'])->toArray();
        return $leaderships;
    }

    public function listChallengesStatuses()
    {
        $challengesStatuses = ProblemStatus::all()->makeHidden(['created_at', 'updated_at'])->toArray();
        return $challengesStatuses;
    }

    public function listConfigurationsGroupedBySection()
    {
        $configurations = Configuration::with('section')->get()->groupBy('section.name')->toArray();

        foreach ($configurations as $section => $sectionData) {

            $sectionData = array_column($sectionData, 'value', 'key');
            foreach ($sectionData as $key => $value) {

                if ($key == "automatic_lock") {
                    if ($value == 0) {
                        $automaticLock = false;
                    } else {
                        $automaticLock = true;
                    }

                    $sectionData[$key] = $automaticLock;
                }
            }
            $configurations[$section] = $sectionData;
        }
        return $configurations;
    }

    public function getTeamConfigurations()
    {
        $leaderships = $this->ListLeaderships();
        return $leaderships;
    }

    public function getChallengesConfigurations()
    {
        $challengesStatuses = $this->listChallengesStatuses();
        return $challengesStatuses;
    }

    public function getSectionConfigurations($sectionId)
    {
        $configurations = Configuration
            ::where('section_id', $sectionId)
            ->get()
            ->toArray();

        $configurations = array_column($configurations, 'value', 'key');
        return $configurations;
    }

    public function updateCapacity($capacities)
    {
        /* $membersIds = array_column($capacities, 'id');
        $this->removeNotExistingTeamMembers($membersIds);

        foreach ($capacities as $index => $value) {

        $userId = isset($value['id']) ? $value['id'] : null;
        $userName = isset($value['name']) ? $value['name'] : null;
        $capacity_details = isset($value['capacity_details']) ? json_encode($value['capacity_details']) : null;

        if ($userId) {
        TeamMember::where('id', $userId)->update(['name' => $userName, 'capacity_details' => $capacity_details]);

        } else {
        TeamMember::create(['name' => $userName, 'capacity_details' => $capacity_details]);
        }
        }

        $teamCapacities = DB::table('team_members')->select('capacity_details')->get();
        $teamCapacities = json_decode(json_encode($teamCapacities), true);
        $teamCapacities = array_column($teamCapacities, 'capacity_details');
        $daysCapacities = [
        "Monday" => 0,
        "Tuesday" => 0,
        "Wednesday" => 0,
        "Thursday" => 0,
        "Friday" => 0,
        ];

        foreach ($teamCapacities as $key => $object) {

        $formattedObject = ($object) ? json_decode($object, true) : [];
        foreach ($formattedObject as $day => $capacity) {

        $daysCapacities[$day] += $capacity;

        }
        }
         */
        $year = Carbon::now()->year;
        $todayDate = Carbon::today();
        $endDate = (new Carbon("last day of December $year"));
        $startDate = (new Carbon($todayDate));

        for ($i = 0; $startDate <= $endDate; $i++) {

            $date = $startDate->format("Y-m-d");
            $data[$i]['date'] = $date;
            $i++;
            $startDate->addDays(1);
        }

        DB::table('capacities')->whereBetween('date', [$todayDate->format("Y-m-d"), $endDate->format("Y-m-d")])->delete();
        DB::table('capacities')->insert($data);

        foreach ($capacities as $day => $capacity) {
            DB::statement("update capacities set capacity = $capacity where DAYNAME(`date`) = '$day' and date between '$todayDate' and '$endDate'");
        }
    }

    public function getTeamCapacities()
    {
        $teamCapacities = DB::table('team_members')->select(['id', 'name', 'capacity_details'])->get();
        $teamCapacities = json_decode(json_encode($teamCapacities), true);

        foreach ($teamCapacities as $index => &$teamMember) {
            $teamMember['capacity_details'] = json_decode($teamMember['capacity_details'], true);
        }
        return $teamCapacities;
    }

    public function getConfigurationsKeys()
    {
        return [

            'capacity',
            'challenges',
        ];
    }

    public function getConfigurationsRules()
    {
        return [

            'capacity' => 'array',
            'challenges' => 'array',
        ];
    }

    public function removeNotExistingTeamMembers($ids)
    {
        DB::table('team_members')->whereNotIn('id', $ids)->delete();
    }

    public function getBoardConfigurations()
    {
        $instaceId = config('custom.instance_id');
        $configurationsServiceUrl = config('custom.configurations_service');

        $ch = curl_init();
        curl_setopt($ch, CURLOPT_URL, $configurationsServiceUrl . '/configurations/get');
        curl_setopt($ch, CURLOPT_POST, 1);
        curl_setopt($ch, CURLOPT_POSTFIELDS, "id=$instaceId");
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
        $configurations = curl_exec($ch);
        $configurations = json_decode($configurations, true);

        curl_close($ch);

        return $configurations;
    }

    public function getProductivityCalculationMode()
    {
        $configurations = $this->getBoardConfigurations();
        $mode = isset($configurations['dashboard']['tachometer_calculation_mode']) ? $configurations['dashboard']['tachometer_calculation_mode'] : 'weekly';

        return $mode;
    }

}

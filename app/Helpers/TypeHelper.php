<?php

namespace App\Helpers;

use Carbon\Carbon;
use DB;

class TypeHelper
{
    public static function getTypePerWeekSteps($type, $year, $week)
    {
        $start = self::getTypeSwiperLimitPerWeek($type);

        Carbon::setWeekStartsAt(Carbon::SUNDAY);
        $now = Carbon::now();
        $current_year = $now->year;
        $current_week = (integer) $now->format('W');
        // if ($now->format('w') == 0) {
        //     $current_week += 1;
        // }

        if (!$start) {
            return get_swiper_steps($current_year, $current_week, null, null, 52, 'week');
        }
        
        return get_swiper_steps($current_year, $current_week, $start['year'], $start['week'], 52, 'week');
    }

    public static function getTypeSwiperLimitPerWeek($type)
    {
        $row = DB::table("type_values")
            ->select('date')
            ->selectRaw('YEAR(date) row_year')
            ->selectRaw('MONTH(date) row_month')
            ->selectRaw('DAYOFWEEK(date) row_day_of_week')
            ->selectRaw('WEEK(date, 6) row_week')
            ->selectRaw('DAY(date) row_day, value')
            ->where('type', $type)
            ->orderBy('date')
            ->first();
        // ->toArray();

        if (!$row) {
            return null;
        }
        return [
            'index' => $row->row_day_of_week,
            'value' => $row->value,
            'year' => $row->row_year,
            'month' => $row->row_month,
            'week' => $row->row_week,
            'day' => $row->row_day,
            'date' => $row->date,
        ];
    }

    public static function getTypeValuesPerWeek($type, $year, $week)
    {
        $holydays = DB::table('holidays')->pluck('date')->toArray();
        $TypeValues = DB::table("type_values")
            ->select('date')
            ->selectRaw('YEAR(date) row_year')
            ->selectRaw('MONTH(date) row_month')
            ->selectRaw('(WEEKDAY(date) + 1) row_day_of_week')
            ->selectRaw('WEEK(date, 3) row_week')
            ->selectRaw('DAY(date) row_day, value')
            ->where('type', $type)
            ->whereNotIn('date', $holydays)
            ->having('row_year', $year)
            ->having('row_week', $week)
            ->orderBy('date')
            ->get();
        // ->toArray();
        // dd($TypeValues);
        // Carbon::setWeekStartsAt(Carbon::SUNDAY);
        $now = Carbon::now();
        $then = $now->setISODate($year, $week);
        $first_day_of_week = $then->startOfWeek();

        $TypeValues = self::formatTypeValueChartData($TypeValues, $first_day_of_week);
        // $daysExists = array_column($TypeValues, "value", "num");
        return $TypeValues;
    }

    public static function formatTypeValueChartData($rows, $first_day_of_week)
    {
// dd($rows, $first_day_of_week);
        $formatted = [];

        for ($i = 1; $i <= 7; $i++) {
            $row = $rows->where('row_day_of_week', $i)->first();
            if ($row) {
                $formatted[] = [
                    'index' => $row->row_day_of_week - 1,
                    'value' => (double) $row->value,
                    'year' => $row->row_year,
                    'month' => $row->row_month,
                    'week' => $row->row_week,
                    'day' => $row->row_day,
                    'date' => $row->date,
                ];
            } else {
                $formatted[] = [
                    'index' => $i,
                    'value' => 0,
                    'year' => $first_day_of_week->year,
                    'month' => $first_day_of_week->month,
                    'week' => (integer) $first_day_of_week->format('W'),
                    'day' => $first_day_of_week->day,
                    'date' => $first_day_of_week->toDateString(),
                ];
            }
            $first_day_of_week->addDays(1);
        }

        // foreach ($rows as $row) {
        //     $formatted[] = [
        //         'index' => $row->row_day_of_week,
        //         'value' => $row->value,
        //         'year' => $row->row_year,
        //         'month' => $row->row_month,
        //         'week' => $row->row_week,
        //         'day' => $row->row_day,
        //         'date' => $row->date,
        //     ];
        // }

        return $formatted;
    }
}

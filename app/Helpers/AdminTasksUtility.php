<?php

namespace App\Helpers;

use App\Models\AdminTask;
use Illuminate\Support\Facades\DB;

trait AdminTasksUtility
{
    public function getAdminTasks($week)
    {
        $adminTasks = AdminTask::WeekTasks($week)
            ->get()
            ->groupBy('date')
            ->toArray();

        return $adminTasks;
    }

    public function getAdminTaskcapacity()
    {
        $admin = DB::table("admin_tasks")
            ->selectRaw("sum(duration) total, `date`")
            ->groupBy("date")
            ->get()
            ->toArray();

        $admin = json_decode(json_encode($admin), true);
        $admin = array_column($admin, "total", "date");

        return $admin;
    }
}

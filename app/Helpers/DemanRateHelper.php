<?php

namespace App\Helpers;

use DB;

class DemanRateHelper
{
    public static function getDemandRates()
    {
        $demand_rates = DB::table("demand_rates")
            ->select('date', 'demand_rate')
            ->get()
            ->toArray();

        $demand_rates = json_decode(json_encode($demand_rates), true);
        $demand_rates = array_column($demand_rates, "demand_rate", 'date');

        return $demand_rates;
    }
}
<?php

namespace App\Helpers;

use Carbon\Carbon;
use DB;

class CumulativeAverageHelper
{
    use ChartsUtility;

    public static function getCumulativeAverage($type, $tasks)
    {
        $now = Carbon::now();
        $year = (integer) request()->get('year', $now->year);
        $currentMonth = $now->month;
        // $tasks = self::getAllTasksGroupByMonth();
        // $latest_average_record = CumulativeAverage::where('type', 'quality')->orderBy('id', 'DESC')->first();
        $tasks_first_record = $tasks->first();
        $average = 0;
        $months_counter = Carbon::create($now->year, $now->month, 1);

        // if ($latest_average_record) {
        //     $average = $latest_average_record->average;
        //     $months_counter = Carbon::create($latest_average_record->year, $latest_average_record->month, 1);
        // } else
        if ($tasks_first_record) {
            $months_counter = Carbon::create($tasks_first_record->year, $tasks_first_record->month, 1);
        }

        $counter = 1;
        $sum = 0;

        $data = [];
        $targets = (new self)->getTargets();
        $months_names = get_month_names();

        for ($i = 1; $i <= 12; $i++) {
            $data[] = [
                'name' => $months_names[$i],
                'month' => $i,
                'average' => 0,
                'trend' => 0,
              //  'target' => 90,
                // 'target' => $targets[$i],
            ];
        }

        while ($months_counter->lessThanOrEqualTo($now)) {
            $task = $tasks->where('month', $months_counter->month)
                ->where('year', $months_counter->year)
                ->first();
            $total = $task ? $task->total : 0;

            $notRun = self::getNotRunThisMonth($months_counter->year, $months_counter->month);
            $lims = self::getLimsThisMonth($months_counter->year, $months_counter->month);
            $trend = self::calculateQualityTrend($total, $notRun, $lims);
            // dd($trend);
            $sum += $trend;
            $average = ($sum / $counter);

            if ($months_counter->year === $year) {
                $data[$months_counter->month - 1]['name'] = $months_counter->format('M');
                $data[$months_counter->month - 1]['average'] = $average;
                $data[$months_counter->month - 1]['trend'] = $trend;
            }
            // echo "$trend    $sum    $counter    $average <br>";
            $counter++;
            $months_counter->addMonths(1);
        }

        // $starting_month = $year == Carbon::now()->year ? $currentMonth : 1;
        // for ($i = $starting_month; $i <= 12; $i++) {
        //     if ($data[$i - 1]['average'] == 0) {
        //         $data[$i - 1]['average'] = (isset($data[$i - 2]) && $data[$i - 2]) ? $data[$i - 2]['average'] : $data[$starting_month - 1]['average'];
        //     }
        // }

        // die();
        // dd($months_counter->month);
        $swiper_steps = [];
        if (!$tasks->isEmpty()) {
            $year_counter = (integer) $tasks->first()->year;
            $current_year = Carbon::now()->year;
            while ($year_counter <= $current_year) {
                $swiper_steps[] = [
                    'year' => $year_counter,
                ];

                if ($year_counter == $current_year) {
                    break;
                }
                $year_counter++;
            }
        }

        return [
            'data' => $data,
            'swiper_steps' => array_reverse($swiper_steps),
        ];
    }

    public static function getAllTasksGroupByMonth()
    {
        $tasks = DB::table("task_version")
            ->selectRaw("0 as trend, 0 as average, YEAR(`due_date`) as year ,MONTH(`due_date`) as month ,MONTHNAME(`due_date`) as month_name , count(*) as total")
            ->groupBy('year')
            ->groupBy('month')
            ->groupBy('month_name')
            ->orderBy('year')
            ->orderBy('month')
            ->get();

        return $tasks;
    }

    public static function getNotRunThisMonth($year, $month)
    {
        $notRun = DB::select(DB::raw("SELECT SUM(value) as total FROM `type_values` where `type` = 'quality-not-run' and month(`date`) = '$month' and YEAR(`date`) = '$year'"));

        $notRun = array_column($notRun, 'total');

        $total = $notRun[0];

        if ($total == null) {

            $total = 0;

        }

        return $total;
    }

    public static function getLimsThisMonth($year, $month)
    {
        $lims = DB::select(DB::raw("SELECT SUM(value) as total FROM `type_values` where `type` = 'quality-lims' and month(`date`) = '$month' and YEAR(`date`) = '$year'"));

        $lims = array_column($lims, 'total');

        $total = $lims[0];

        if ($total == null) {

            $total = 0;

        }

        return $total;
    }

    public static function calculateQualityTrend($tasks, $notRun, $lims)
    {
        if ($tasks != 0) {

            $diffrence = ($tasks - $notRun - $lims);

            if ($diffrence <= 0) {

                $diffrence = 0;
            }

            $trend = $diffrence / $tasks * 100;
            $trend = round($trend, 2);

            if ($trend <= 0) {

                $trend = 0;

            }

        } else {

            $trend = 0;
        }

        return $trend;

    }
    // public static function calculateAveragePerMonth($date, $counter)
    // {
    //     $year = $date->year;
    //     $month = $date->month;

    //     $tasks = DB::table("task_version")
    //         ->selectRaw("MONTHNAME(`due_date`) as month , count(*) as total")
    //         ->whereyear('due_date', $year)
    //         ->groupBy('months')
    //         ->get()->toArray();

    // }

    // public static function saveAverage($data)
    // {
    //     $cumulative_average = CumulativeAverage::where('year', $data['year'])
    //         ->where('month', $data['month'])
    //         ->where('type', $data['type'])->first();
    //     if ($cumulative_average) {
    //         return $cumulative_average->update($data);
    //     }

    //     return CumulativeAverage::create($data);
    // }

}

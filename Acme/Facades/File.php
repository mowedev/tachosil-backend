<?php

namespace Acme\Facades;

use Illuminate\Support\Facades\Facade;

/**
 * @see \Acme\Http\File
 */
class File extends Facade
{
    /**
     * Get the registered name of the component.
     *
     * @return string
     */
    protected static function getFacadeAccessor()
    {
        return 'acme.file';
    }
}

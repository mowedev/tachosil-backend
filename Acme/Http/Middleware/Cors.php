<?php

namespace Acme\Http\Middleware;

use Closure;

class Cors
{
	/**
	 * Handle an incoming request.
	 * 
	 * Allow requests from all origins
	 * 
	 * @param  \Illuminate\Http\Request  $request
	 * @param  \Closure $next
	 * @return mixed
	 */
	public function handle($request, Closure $next)
	{
		$response = $next($request);

		$response->header('Access-Control-Allow-Methods', 'HEAD, GET, POST, PUT, PATCH, DELETE');
		$response->header('Access-Control-Allow-Headers', $request->header('Access-Control-Request-Headers'));
		$response->header('Access-Control-Allow-Origin', '*');
		$response->header('Access-Control-Max-Age', '86400');

		return $response;
	}

    // public function terminate($request, $response)
    // {
    // 	//
    // }
}

<?php

namespace Acme\Http;

use Illuminate\Http\Response as BaseResponse;
use Illuminate\Http\UploadedFile;
use Illuminate\Support\Facades\Storage;
// use Intervention\Image\ImageManager;
use Intervention\Image\Facades\Image;
use Smalot\PdfParser\Parser as PdfParser;

use App\Models\File as FileModel;

/**
 * File Class
 *
 * @category Libraries
 * @author The MOWE Solutions Dev Team
 */
class File extends UploadedFile
{
    /**
     * The file instance
     * 
     * @var string|\Illuminate\Http\UploadedFile
     */
    protected $file;

    /**
     * Document file extensions list
     * 
     * @var array
     * @see https://www.file-extensions.org/filetype/extension/name/document-files
     */
    protected $documentExtensions = [
        // Adobe extensions
        'pdf', '_pdf', 'pdf_',
        'indd', 'idml', 'ind', 'idd', 'indb',
        // Microsoft Office extensions
        'docx', 'xlsx', 'pptx', 'sldx', 'ppsx',
        'doc', 'xls', 'ppt', 'sldm', 'ppsm',
        'pub',
        // OpenDocument extensions
        'odt', 'ods', 'odp', 'odc', 'odg',
        // Popular extensions
        'txt', 'csv', 'epub', 'iwa',
        // QuarkXPress extensions
        'xdoc', 'zave', 'qct', 'xtg',
    ];

    /**
     * Create a new class instance.
     *
     * @return void
     */
    public function __construct($file = null)
    {
        $this->file = $file;
    }

    /**
     * Sets the file
     * 
     * @param string|\Illuminate\Http\UploadedFile $file
     */
    public function setFile($file = null)
    {
        $this->file = $file;

        return $this;
    }

    /**
     * Checks the extension of a file is a document extension or not.
     * 
     * @param  string|\Illuminate\Http\UploadedFile  $file
     * @return boolean
     */
    public function isDocument($file = null)
    {
        $this->file = $file ? $file : $this->file;

        if (is_string($this->file)) {
            $extension = $this->file;
        } elseif ($this->file instanceof UploadedFile) {
            $extension = $this->file->extension();
        } else {
            return false;
        }

        return in_array($extension, $this->documentExtensions);
    }

    /**
     * Checks the MIME type of a file is an image MIME type or not.
     * 
     * @param  string|\Illuminate\Http\UploadedFile  $file
     * @return boolean
     */
    public function isImage($file = null)
    {
        return $this->mimeTypeGroup($file) === 'image';
    }

    /**
     * Checks the MIME type of a file is a video MIME type or not.
     * 
     * @param  string|\Illuminate\Http\UploadedFile  $file
     * @return boolean
     */
    public function isVideo($file = null)
    {
        return $this->mimeTypeGroup($file) === 'video';
    }

    public function isAudio($file = null)
    {
        return $this->mimeTypeGroup($file) === 'audio';
    }

    /**
     * Returns the MIME type group of a file.
     * 
     * @param  string|\Illuminate\Http\UploadedFile  $file
     * @return string|null
     */
    protected function mimeTypeGroup($file = null)
    {
        $this->file = $file ? $file : $this->file;

        if (is_string($this->file)) {
            return substr($this->file, 0, 5);
        } elseif ($this->file instanceof UploadedFile) {
            return substr($this->file->getMimeType(), 0, 5);
        }

        return null;
    }

    public function url(FileModel $file)
    {
        $directory = $file->directory ? $file->directory.'/' : '';

        return Storage::disk($file->disk)->url(
            "{$directory}{$file->filename}.{$file->extension}"
        );
    }

    public function thumb(FileModel $file)
    {
        $directory = 'thumbs/';

        return Storage::disk($file->disk)->url(
            "{$directory}{$file->filename}.jpg"
        );
    }

    /**
     * Returns uploaded file directory based on file extension or MIME type.
     * 
     * @param  \Illuminate\Http\UploadedFile $file
     * @return string
     */
    public function guessDirectory(UploadedFile $file)
    {
        if ($this->isDocument($file->extension())) {
            return 'documents';
        } elseif ($this->isImage($file->getMimeType())) {
            return 'photos';
        } elseif ($this->isVideo($file->getMimeType())) {
            return 'videos';
        } elseif ($this->isAudio($file->getMimeType())) {
            return 'audio';
        }

        return '';
    }

    /**
     * Uploads a given file to a specific disk & directory
     * 
     * @param  \App\Models\File $file
     * @return array
     */
    public function upload(FileModel $file, $exif)
    {
        $metadata = null;

        //dd(app('request')->input('image_variants'));
        // resize image and prevent possible upsizing
        if ($this->isImage()) {
            $img = Image::make($this->file);         
            $exif = ($img->exif('Orientation') == null) ? $exif : $img->exif('Orientation');
            $angle = get_angle_from_exif($exif);

            $encoded = $img->resize(1024, null, function ($constraint) {
                $constraint->aspectRatio();
                $constraint->upsize();
            });

            if ($angle != -1) {
                if (image_needs_flip($exif)) {
                    $encoded = $encoded->rotate($angle)->flip('h')->encode();
                } else {
                    $encoded = $encoded->rotate($angle)->encode();
                }
                
            } else {
               if (image_needs_flip($exif)) {
                $encoded = $encoded->flip('h')->encode();
            } else {
                $encoded = $encoded->encode();
            }
        }
    } else {
        if ($this->isDocument()) {
            $metadata = $this->pdfMetadata($this->file->getRealPath());
        }

        $encoded = file_get_contents($this->file->getRealPath(), true);
    }

        // Store file
    $path = Storage::disk($file->disk)->put(
        "{$file->directory}/{$file->filename}.{$file->extension}",
        $encoded
    );

        //check if is_image and save 
        //different sizes for the image
    if ($this->isImage()) {
            // now image variants will be static 
            // later we may change it
        $imageVariants = [
            'img1' => ['width' => 480],
            'img2' => ['width' => 640],
            'img3' => ['width' => 960],
        ];
            //call the method that will save the image variants
        $this->saveImageVariants($imageVariants, $file);
    }

    return $path ? compact('path', 'metadata') : false;
}

    /**
     * Returns PDF file meta details
     * 
     * @param  string $path
     * @return array
     */
    public function pdfMetadata($path)
    {
        try {
            $parser = new PdfParser;
            $pdf = $parser->parseFile($path);
            $meta = $pdf->getDetails();
        } catch (\Exception $e) {
            $meta = [];
        }

        return [
            'title' => isset($meta['Title']) ? $meta['Title'] : '',
            'author' => isset($meta['Author']) ? $meta['Author'] : '',
            'subject' => isset($meta['Subject']) ? $meta['Subject'] : '',
            'keywords' => isset($meta['Keywords']) ? $meta['Keywords'] : '',
            'producer' => isset($meta['Producer']) ? $meta['Producer'] : '',
            'created_at' => isset($meta['CreationDate']) ? $meta['CreationDate'] : '',
            'updated_at' => isset($meta['ModDate']) ? $meta['ModDate'] : '',
        ];
    }

    public function delete($file)
    {
        if (is_array($file)) {
            return array_map([$this, 'delete'], $file);
        }

        if (! $file instanceof FileModel) {
            return false;
        }

        $directory = $file->directory ? $file->directory.'/' : '';

        return Storage::disk($file->disk)->delete(
            "{$directory}{$file->filename}.{$file->extension}"
        );
    }

    /**
     * @param  array  $imageVariants
     * @return void
     */
    public function saveImageVariants(array $imageVariants, FileModel $file)
    {
        foreach ($imageVariants as $image => $values) {
         $img = Image::make($this->file);
         $encoded = (string) $img->resize($values['width'], null, function ($constraint) {
            $constraint->aspectRatio();
            $constraint->upsize();
        })->encode();

            // Store file
         $path = Storage::disk($file->disk)->put(
            "{$file->directory}/{$file->filename}_{$values['width']}.{$file->extension}",
            $encoded
        );
     }
 }

    // public function path($file = null)
    // {
    //     $this->file = $file ? $file : $this->file;

    //     if ($this->file instanceof UploadedFile) {
    //         return $this->file->getRealPath();
    //     } elseif ($this->file instanceof FileModel) {
    //         return $this->buildFilePathFromModel($this->file);
    //     }

    //     return null;
    // }

    // protected function buildFilePathFromModel(FileModel $file)
    // {
    //     $directory = $file->directory ? $file->directory.'/' : '';

    //     return storage_path("
    //         app/
    //     ");

    //     return Storage::disk($file->disk)->url(
    //         "{$directory}{$file->filename}.{$file->extension}"
    //     );
    // }

}
<?php

if (!function_exists('line')) {
    /**
     * Check if string endwith substring.
     *
     * @param  string  $bigString
     * @param  string  $substring
     * @return bool
     */
    function endsWith($bigString, $subString)
    {
        $len = strlen($subString);

        if ($len == 0) {
            return true;
        }

        return (substr($bigString, -$len) === $subString);
    }
}

if (!function_exists('line')) {
    /**
     * Translate the given message.
     *
     * @param  string  $id
     * @param  array   $parameters
     * @param  string  $locale
     * @return string
     */
    function line($id = null, $parameters = [], $locale = null)
    {
        if (is_null($id)) {
            return app('translator');
        }

        return app('translator')->trans($id, $parameters, $locale);
    }
}

if (!function_exists('config_path')) {
    /**
     * Get the configuration path.
     *
     * @param  string $path
     * @return string
     */
    function config_path($path = '')
    {
        return app()->basePath() . '/config' . ($path ? '/' . $path : $path);
    }
}

if (!function_exists('public_path')) {
    /**
     * Get the path to the public folder.
     *
     * @param  string  $path
     * @return string
     */
    function public_path($path = '')
    {
        return app()->basePath() . '/public' . ($path ? DIRECTORY_SEPARATOR . $path : $path);
    }
}

if (!function_exists('app_url')) {
    /**
     * Returns platform name from a given URL
     *
     * @param  string $url
     * @return string
     */
    function app_url($uri = null)
    {
        return rtrim(config('custom.app_url'), '/') . ($uri ? '/' . $uri : $uri);
    }
}

if (!function_exists('request_with')) {
    /**
     * Checks the 'with' query parameter
     *
     * @param  string $with
     * @return boolean
     */
    function request_with($with)
    {
        // static $_with_collection;

        // if (! isset($_with_collection)) {
        if (!app('request')->has('with')) {
            return false;
        }

        $_with_collection = collect(explode(',', app('request')->input('with')));
        // }

        return $_with_collection->contains($with);
    }
}

if (!function_exists('per_page')) {
    /**
     * Returns number of items to display in page
     *
     * @return integer
     */
    function per_page()
    {
        static $_items_per_page;

        if (isset($_items_per_page)) {
            return $_items_per_page;
        }

        return $_items_per_page = app('request')->has('limit') ? (int) app('request')->input('limit') : config('custom.paging.per_page');
    }
}

if (!function_exists('request_token')) {
    /**
     * Fetches the request token from the header/input array
     *
     * @return integer
     */
    function request_token()
    {
        static $_request_token;

        if (isset($_request_token)) {
            return $_request_token;
        }

        $request = app('request');

        return $_request_token = $request->header('Authorization') ? str_replace('Bearer ', '', (string) $request->header('Authorization')) : $request->input('token');
    }
}

if (!function_exists('json_response')) {
    /**
     * Returns an instance of \Acme\Http\Response
     *
     * @return \Acme\Http\Response
     */
    function json_response(array $options = [])
    {
        return new \Acme\Http\Response($options);
    }
}

if (!function_exists('file_helper')) {
    /**
     * Returns an instance of \Acme\Http\File
     *
     * @return \Acme\Http\File
     */
    function file_helper($file = null)
    {
        return \Acme\Facades\File::setFile($file);
        // return app('acme.file')->setFile($file);
    }
}

if (!function_exists('hashid_to_int')) {
    /**
     * Returns an integer from Hashids string
     *
     * @return int
     */
    function hashid_to_int($id)
    {
        try {
            return filter_var($id, FILTER_VALIDATE_INT) ? $id : \Hashids::decode($id)[0];
        } catch (Exception $e) {
            return null;
        }
    }
}

if (!function_exists('check_url_start')) {
    function check_url_start($url)
    {
        if ((stripos($url, 'http')) === false) {
            return $url = 'http://' . $url;
        } else {
            return $url;
        }

    }
}

if (!function_exists('current_user')) {
    function current_user()
    {
        $user = JWTAuth::user();

        if ($user instanceof \Illuminate\Database\Eloquent\Collection) {
            $user = $user->first();
        }
        return $user;
    }
}

/**
 * @param  int $exif
 * @return int $angle
 */
if (!function_exists('get_angle_from_exif')) {

    function get_angle_from_exif($exif)
    {
        $angle = -1;

        switch ($exif) {
            case 3:
                $angle = 180;
                break;
            case 4:
                $angle = 180;
                break;
            case 5:
                $angle = -90;
                break;
            case 6:
                $angle = -90;
                break;
            case 7:
                $angle = 90;
                break;
            case 8:
                $angle = 90;
                break;
            default:
                $angle = -1;
        }

        return $angle;
    }
}

/**
 * @param  int $exif
 * @return bool
 */
if (!function_exists('image_needs_flip')) {

    function image_needs_flip($exif)
    {
        if ($exif == 2 || $exif == 4 || $exif == 5 || $exif == 7) {
            return true;
        } else {
            return false;
        }
        // $encoded = (string) $img->resize(1024, null, function ($constraint) {
        //             $constraint->aspectRatio();
        //             $constraint->upsize();
        //         })->rotate($angle)->encode();
    }
}

if (!function_exists('array_value_column')) {

    function array_value_column($arr, $colKey, $colVal = "all")
    {
        $formated = [];

        foreach ($arr as $key => $value) {
            if ($colVal == "all") {
                $formated[$value[$colKey]] = $value;
            } else {
                $formated[$value[$colKey]] = $value[$colVal];
            }

        }

        return $formated;
    }
}

if (!function_exists('get_tasks_status')) {

    function get_tasks_status($status)
    {
        $finalStatus = 3;

        switch ($status) {
            case 'AA':
                $finalStatus = 1;
                break;

            case 'AC':
                $finalStatus = 1;
                break;

            case 'CA':
                $finalStatus = 1;
                break;

            case 'PI':
                $finalStatus = 2;
                break;

            default:
                $finalStatus = 3;
                break;
        }

        return $finalStatus;
    }
}

if (!function_exists('get_current_quarter')) {

    function get_current_quarter($currentMonth)
    {
        if (in_array($currentMonth, [1, 2, 3])) {

            $quarter = 1;

        } elseif (in_array($currentMonth, [4, 5, 6])) {

            $quarter = 2;

        } elseif (in_array($currentMonth, [7, 8, 9])) {

            $quarter = 3;

        } else {

            $quarter = 4;

        }

        return $quarter;
    }
}

if (!function_exists('get_german_day')) {

    function get_german_day($day)
    {
        $days = [
            "Monday" => "Montag",
            "Tuesday" => "Dienstag",
            "Wednesday" => "Mittwoch",
            "Thursday" => "Donnerstag",
            "Friday" => "Freitag",
        ];

        return isset($days[$day]) ? $days[$day] : "NA";
    }
}

if (!function_exists('get_month_names')) {

    function get_month_names()
    {
        $months =
            [
            1 => "Jan",
            2 => "Feb",
            3 => "Mar",
            4 => "Apr",
            5 => "May",
            6 => "Jun",
            7 => "Jul",
            8 => "Aug",
            9 => "Sep",
            10 => "Oct",
            11 => "Nov",
            12 => "Dec",
        ];

        return $months;
    }
}

if (!function_exists('get_month_full_names')) {

    function get_month_full_names()
    {
        $months =
            [
            1 => "January",
            2 => "February",
            3 => "March",
            4 => "April",
            5 => "May",
            6 => "June",
            7 => "July",
            8 => "August",
            9 => "September",
            10 => "October",
            11 => "November",
            12 => "December",
        ];

        return $months;
    }
}

if (!function_exists('array_level_transform')) {

    function array_level_transform($arr)
    {
        $result = [];
        foreach ($arr as $key => $value) {
            foreach ($value as $level => $row) {
                $result[$key][] = $level;
            }
        }

        return $result;
    }
}

if (!function_exists('array_advanced_filter')) {

    function array_advanced_filter($tofilter, $lookup, $field)
    {
        $newTasks = array_filter($tofilter, function ($task) use ($lookup, $field) {

            if (in_array($task[$field], $lookup)) {
                return true;
            } else {
                return false;
            }
        });

        return $newTasks;
    }
}

if (!function_exists('array_simplify')) {

    function array_simplify($source, $tohave)
    {
        $simplified = [];

        foreach ($source as $key => $task) {
            foreach ($task as $fieldName => $value) {

                if (in_array($fieldName, $tohave)) {
                    $simplified[$key][$fieldName] = $value;
                }
            }
        }

        return $simplified;

    }
}

if (!function_exists('array_exclude')) {

    function array_exclude($source, $toExclude)
    {
        $excluded = [];

        foreach ($source as $key => $task) {
            //dd($task);
            foreach ($task as $fieldName => $value) {

                if (!in_array($fieldName, $toExclude)) {
                    $excluded[$key][$fieldName] = $value;
                }
            }
        }

        return $excluded;

    }
}

if (!function_exists('get_default_sixs')) {

    function get_default_sixs($date)
    {
        $sixs = [
            "safety" => 0,
            "order" => 0,
            "documents" => 0,
            "materials" => 0,
            "clothing" => 0,
            "security" => 0,
            "discipline" => 0,
            "date" => $date,
        ];

        return $sixs;

    }
}

if (!function_exists('init_types_month')) {

    function init_types_month($count, $exists)
    {
        $val = [];

        for ($i = 0; $i < $count; $i++) {
            $val[$i]["index"] = $i + 1;
            $val[$i]["value"] = isset($exists[$i + 1]) ? (float) $exists[$i + 1] : 0;
        }

        return $val;

    }
}

if (!function_exists('get_problem_categories')) {

    function get_problem_categories($categories)
    {
        $list = [];

        if (!is_null($categories)) {
            $list = explode(",", $categories);
        }

        return $list;
    }

}

if (!function_exists('get_swiper_steps')) {
    function get_swiper_steps($current_year, $current_date, $year_counter, $date_counter, $date_limit, $type)
    {
        if (!$year_counter) {
            $year_counter = \Carbon\Carbon::now()->year;
        }
        if (!$date_counter) {
            switch ($type) {
                case 'month':
                    $date_counter = \Carbon\Carbon::now()->month;
                    break;
                case 'week':
                    $date_counter = \Carbon\Carbon::now()->format('W');
                    break;
            }
        }
        $steps = [];

        while ($year_counter <= $current_year) {
            $steps[] = [
                'year' => $year_counter,
                $type => $date_counter,
            ];

            if ($year_counter == $current_year && $date_counter == $current_date) {
                break;
            }
            $date_counter++;
            if ($date_counter > $date_limit) {
                $year_counter++;
                $date_counter = 1;
            }
        }

        return !empty($steps) ? array_reverse($steps) : [
            [
                'year' => $year_counter,
                $type => $date_counter,
            ]
        ];
    }
}

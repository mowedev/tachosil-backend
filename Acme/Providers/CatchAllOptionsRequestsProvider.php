<?php

namespace Acme\Providers;

use Illuminate\Support\ServiceProvider;

/**
 * If the incoming request is an OPTIONS request
 * we will register a handler for the requested route
 */
class CatchAllOptionsRequestsProvider extends ServiceProvider
{
    /**
     * Register any application services.
     *
     * @return void
     */
	public function register()
	{
		$request = app('request')->capture();

		if ($request->isMethod('OPTIONS')) {
			app()->options($request->path(), ['middleware' => 'throttle', function() {
				return response('', 200);
			}]);
		}
	}
}

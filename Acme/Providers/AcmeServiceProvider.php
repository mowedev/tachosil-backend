<?php

namespace Acme\Providers;

use Illuminate\Support\ServiceProvider;

use Illuminate\Http\Request;

class AcmeServiceProvider extends ServiceProvider
{
    public function boot(Request $request)
    {
        // Set locale
        if ($request->has('lang')) {
            $locales = explode(',', config('custom.app_locales'));
            if (in_array($request->input('lang'), $locales)) {
                app('translator')->setLocale($request->input('lang'));
            }
        }
    }

    /**
     * Register any application services.
     *
     * @return void
     */
    public function register()
    {
        $this->app->register(\Acme\Providers\FileServiceProvider::class);
        $this->app->register(\Acme\Providers\ImageServiceProvider::class);

        if ($this->app->runningInConsole()) {
            $this->commands([
                \Acme\Console\StorageLinkCommand::class,
                \Acme\Console\ViewClearCommand::class,
            ]);
        }
    }
}

<?php

return [

	"tasks_inserted" => "Tasks and Sub Tasks has been inserted Successfully",
	"duration_inserted" => "Duration has been inserted Successfully",
	"not_week" => "Invalid Week Index used",
	"imported" => "Tasks imported successfully",
	"task_duration_updated" => "Task duration updated Successfully.",
	"task_order_updated" => "Tasks orders updated Successfully.",
	"tasks_cloned" => "Tasks Cloned Successfully.",
	"tasks_updated" => "Task has been updated Successfully",
	"task_same_due" => "Task has Same Due Date",
	"task_old_due" => "Task cann't be moved due to old Due Date",
	"task_is_done" => "Task cann't be moved , Task have date reviewed",
	"task_is_hidden" => "Task cann't be moved , Task have new Due Date",
	"task_duration_invalid" => "Task cann't be inserted , Task duration need to be valid ",
	"task_inserted" => "Tasks has been inserted",
	"task_deleted" => "Tasks has been deleted",
	"task_not_found" => "cann't find task",
	"task_not_user_defined" => "Task is not user defined",
	"task_exist" => "Task is exist",
	"failure_email_sent" => "Failure Email has been sent successfully",
	"capicity_updated" => "Capacity updated successfully",
	"demandrate_updated" => "Demand Rate updated successfully",

];
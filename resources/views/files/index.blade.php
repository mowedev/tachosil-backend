@extends('layouts.admin.admin')
@section('content')
    <div class="row">
        <div class="col-sm-12">
            <div class="card-box">
                <h4 class="m-t-0 header-title"><b>{{$table}} Table</b></h4>
                <p class="text-muted font-13">

                    <a href="{{url('/import/force')}}" class="btn btn-success" role="button">Force Import</a><br><br>

                </p>

                @if((Session::has('Forced')))
                    <div class="alert alert-success">{{ Session::get('Forced') }}
                        <i class="fa fa-check"
                           aria-hidden="true"></i>
                    </div>
                @endif

                <table data-toggle="table"
                       data-search="true"
                       data-show-refresh="false"
                       data-show-toggle="false"
                       data-show-columns="true"
                       data-sort-name="id"
                       data-page-list="[5, 10, 20]"
                       data-page-size="10"
                       data-pagination="true" data-show-pagination-switch="true" class="table-bordered ">
                    <thead>
                    <tr>
                        {{-- <th data-field="id" data-sortable="true" data-formatter="invoiceFormatter">Order ID</th>--}}
                        <th data-field="number" data-align="center" data-sortable="true">ID</th>
                        <th data-field="name" data-align="center" data-sortable="true">File Name</th>
                        <th data-field="imported_time" data-align="center" data-sortable="true">Time</th>
                        <th data-field="status" data-align="center" data-sortable="true">Status</th>

                    </tr>
                    </thead>

                    <tbody>
                    @foreach($files as $file)
                        <tr>
                            <td>{{$file->id}}</td>
                            <td>{{$file->name}}</td>
                            <td>{{$file->imported_time}}</td>
                            <td>@if($file->status == 0) {{"Failed"}} @else {{"Success"}} @endif</td>
                        </tr>
                    @endforeach

                    </tbody>
                </table>
            </div>
        </div>
    </div>
@stop

@component('mail::message')
{{-- Greeting --}}
# @lang('Hello!')

{{-- Intro Lines --}}
@foreach ($introLines as $line)
{{ $line }}

@endforeach

@foreach ($errors as $error)
<ul>
    <li>
        File name : {{ $error['file_path'] }}
    </li>
    @if ($error['row'])
    <li>
        Row : {{ $error['row'] }}
    </li>
    @endif
    <li>
        Date : {{ $date }} ({{ config('app.timezone', 'UTC') }})
    </li>
    <li>
        <ul style="list-style: none;">
            @foreach ($error['errors'] as $col => $ers)
            <li>
                {{ is_array($ers) && count($ers) > 0 ? $ers[0] : $ers }}
            </li>
            @endforeach
        </ul>
    </li>
</ul>
@endforeach

@lang('Regards'),<br>{{ config('app.name') }}
@endcomponent

<!DOCTYPE html>

<html>
<head>
	<title>Import Failed</title>
</head>

<body>

	The import process of the file which name is {{$file}} failed because of invalid file format, Time : {{Carbon\Carbon::now()->format('Y-m-d h-i-s')}}. 

</body>

</html>
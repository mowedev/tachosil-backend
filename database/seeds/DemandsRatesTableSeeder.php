<?php

use Carbon\Carbon;
use Illuminate\Database\Seeder;

class DemandsRatesTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $year2019 = $this->generateSeedingData("2019-01-01");
        $year2020 = $this->generateSeedingData("2020-01-01");
        $year2021 = $this->generateSeedingData("2021-01-01");
        $year2022 = $this->generateSeedingData("2022-01-01");
        $data = array_merge($year2019, $year2020, $year2021, $year2022);

        DB::table('demand_rates')->insert($data);
        // DB::statement("update demand_rates set demand_rate = 15 where DAYNAME(`date`) = 'Friday'");
    }

    private function generateSeedingData($start)
    {
        $start = new Carbon($start);
        $days_in_year = 365 + $start->format('L');

        for ($i = 0; $i < $days_in_year; $i++) {
            $start->addDays(1);
            $data[$i]['date'] = $start->format("Y-m-d");

            if ($start->format('l') == "Friday") {
                $data[$i]['demand_rate'] = 20;
            } else {
                $data[$i]['demand_rate'] = 28;
            }
        }

        return $data;
    }
}

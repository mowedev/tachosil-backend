<?php

use Illuminate\Database\Seeder;

class DatabaseSeeder extends Seeder
{
    /**
     * Seed the application's database.
     *
     * @return void
     */
    public function run()
    {
        $this->call(EmojiTableSeeder::class);
        $this->call(StatesTableSeeder::class);
        $this->call(TeamsTableSeeder::class);
        $this->call(CapacitiesTableSeeder::class);
        $this->call(DemandsRatesTableSeeder::class);
        $this->call(ProblemCategoriesTableSeeder::class);
        $this->call(ProblemStatusesTableSeeder::class);
        $this->call(SectionsTableSeeder::class);
        $this->call(ConfigurationsTableSeeder::class);
        $this->call(HolidaysTableSeeder::class);
        $this->call(TeamMemberSeeder::class);
        $this->call(TeamStatesTableSeeder::class);
    }
}

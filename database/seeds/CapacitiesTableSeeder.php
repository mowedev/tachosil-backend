<?php

use Illuminate\Database\Seeder;
use Carbon\Carbon;

class CapacitiesTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        // $start = "2019-01-01";
        // $data[0]['date'] = $start;
        // $data[0]['capacity'] = 36;

        // for ($i=1; $i < 365; $i++) { 
        // 	$start = (new Carbon($start))->addDays(1)->format("Y-m-d");
        // 	$data[$i]['date'] = $start;
        //     $data[$i]['capacity'] = 36;
        // }

        $year2019 = $this->generateSeedingData("2019-01-01");
        $year2020 = $this->generateSeedingData("2020-01-01");
        $year2021 = $this->generateSeedingData("2021-01-01");
        $year2022 = $this->generateSeedingData("2022-01-01");
        $data = array_merge($year2019, $year2020, $year2021, $year2022);
        
    	DB::table('capacities')->insert($data);
        // DB::statement("update capacities set capacity = 30 where DAYNAME(`date`) = 'Friday'");
        // DB::statement("update capacities set capacity = 47 where DAYNAME(`date`) = 'Monday'");
        // DB::statement("update capacities set capacity = 46.5 where DAYNAME(`date`) in ('Tuesday', 'Wednesday', 'Thursday')");
    }

    private function generateSeedingData($start)
    {
        $start = new Carbon($start);
        $days_in_year = 365 + $start->format('L');

        for ($i = 0; $i < $days_in_year; $i++) {
            $start->addDays(1);
            $data[$i]['date'] = $start->format("Y-m-d");
            
            switch ($start->format('l')) {
                case 'Monday':
                    $data[$i]['capacity'] = 47;
                    break;
                case 'Tuesday':
                    $data[$i]['capacity'] = 46;
                    break;
                case 'Wednesday':
                    $data[$i]['capacity'] = 46;
                    break;
                case 'Thursday':
                    $data[$i]['capacity'] = 44;
                    break;
                case 'Friday':
                    $data[$i]['capacity'] = 30;
                    break;
                
                default:
                    $data[$i]['capacity'] = 36;
                    break;
            }
        }

        return $data;
    }
}

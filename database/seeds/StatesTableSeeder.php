<?php

use Illuminate\Database\Seeder;

class StatesTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        //
        $data = [
    		[ 
                'id' => 1,
    			'name'=>'Sunny'
    		],
    		[
                'id' => 2,
    			'name'=>'Cloudy'
    		],
    		[ 
                'id' => 3,
    			'name'=>'Raining'
    		],
    		[
                'id' => 4,
    			'name'=>'Meeting-L1'
    		],
            [ 
                'id' => 5,
                'name'=>'Meeting-L2'
            ],
            [ 
                'id' => 6,
                'name'=>'Meeting-L3'
            ],

    	];
        
    	DB::table('states')->insert($data);
    }
}

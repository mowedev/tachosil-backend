<?php

use Illuminate\Database\Seeder;

class TeamMemberSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {

        $data = [
    		[
                'id' => 1,
    			'avatar_id' => null
            ],
            [
                'id' => 2,
    			'avatar_id' => null
            ],
            [
                'id' => 3,
    			'avatar_id' => null
            ],
            [
                'id' => 4,
    			'avatar_id' => null
            ],
            [
                'id' => 5,
    			'avatar_id' => null
            ],
            [
                'id' => 6,
    			'avatar_id' => null
            ],
            [
                'id' => 7,
    			'avatar_id' => null
            ],
            [
                'id' => 8,
    			'avatar_id' => null
            ],
            [
                'id' => 9,
    			'avatar_id' => null
            ],
            [
                'id' => 10,
    			'avatar_id' => null
            ],
    	];

        DB::table('team_members')->truncate();
    	DB::table('team_members')->insert($data);
    }
}

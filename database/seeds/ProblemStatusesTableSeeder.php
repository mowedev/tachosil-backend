<?php

use Illuminate\Database\Seeder;

class ProblemStatusesTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $data = [

            [
                'id' => 1,
                'status' => 'Not Started'
            ],
    		[ 
                'id' => 2,
    			'status'=>'To Do'
    		],
    		[
                'id' => 3,
    			'status'=>' In Progress'
    		],
    		[
                'id' => 4,
    			'status'=>'In Review'
    		],
    		[
                'id' => 5,
    			'status'=>'Done'
    		]

    	];
        
    	DB::table('problem_statuses')->insert($data);
    }
}

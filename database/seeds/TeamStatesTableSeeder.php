<?php

use Illuminate\Database\Seeder;

class TeamStatesTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        //
        $data = [
    		[ 
    			'date'=> '2019-10-31',
    			'state_id' => 1,
    			'number_of_members' => 3
    		],
    		[
    			'date'=> '2019-10-31',
    			'state_id' => 2,
    			'number_of_members' => 4
    		],
    		[ 
    			'date'=> '2019-10-31',
    			'state_id' => 3,
    			'number_of_members' => 2

    		],
    		[
    			'date'=> '2019-10-31',
    			'state_id' => 4,
    			'number_of_members' => 5
    		],
    		[ 
    			'date'=> '2019-10-31',
    			'state_id' => 5,
    			'number_of_members' => 3
    		],

    		[ 
    			'date'=> '2019-10-30',
    			'state_id' => 1,
    			'number_of_members' => 3
    		],
    		[
    			'date'=> '2019-10-30',
    			'state_id' => 2,
    			'number_of_members' => 4
    		],
    		[ 
    			'date'=> '2019-10-30',
    			'state_id' => 3,
    			'number_of_members' => 2

    		],
    		[
    			'date'=> '2019-10-30',
    			'state_id' => 4,
    			'number_of_members' => 5
    		],
    		[ 
    			'date'=> '2019-10-30',
    			'state_id' => 5,
    			'number_of_members' => 3
    		],

    		[ 
    			'date'=> '2019-10-29',
    			'state_id' => 1,
    			'number_of_members' => 3
    		],
    		[
    			'date'=> '2019-10-29',
    			'state_id' => 2,
    			'number_of_members' => 4
    		],
    		[ 
    			'date'=> '2019-10-29',
    			'state_id' => 3,
    			'number_of_members' => 2

    		],
    		[
    			'date'=> '2019-10-29',
    			'state_id' => 4,
    			'number_of_members' => 5
    		],
    		[ 
    			'date'=> '2019-10-29',
    			'state_id' => 5,
    			'number_of_members' => 3
    		],

    	];
    	DB::table('team_states')->insert($data);
    }
}

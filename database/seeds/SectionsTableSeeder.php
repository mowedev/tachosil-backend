<?php

use Illuminate\Database\Seeder;

class SectionsTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('sections')->truncate();

		$data = [
            [
                'id' => 1,
                'name' => 'Dashboard'
            ],
    		[ 
                'id' => 2,
    			'name'=>'EHS'
    		],
    		[
                'id' => 3,
    			'name'=>'Quality'
    		],
    		[
                'id' => 4,
    			'name'=>'Service'
    		],
    		[
                'id' => 5,
    			'name'=>'Productivity'
    		],
    		[
    			'id' => 6,
    			'name'=>'Team'
    		],
    		[
    			'id' => 7,
    			'name'=>'Challenges'
    		],
    		[
    			'id' => 8,
    			'name'=>'General'
    		],


    	];
        
    	DB::table('sections')->insert($data);
    }
}

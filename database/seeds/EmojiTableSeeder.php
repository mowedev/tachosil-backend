<?php

use Illuminate\Database\Seeder;

class EmojiTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        if (DB::table('emojies')->count() > 0) {
            return;
        }

        $data = [
            [
                'id' => 1,
                'img' => 'Love'
            ],
            [
                'id' => 2,
                'img' => 'Happy'
            ],
            [
                'id' => 3,
                'img' => 'Yummy'
            ],
            [
                'id' => 4,
                'img' => 'Party'
            ],
            [
                'id' => 5,
                'img' => 'Baby'
            ],
            [
                'id' => 6,
                'img' => 'Hello'
            ],
    	];

    	DB::table('emojies')->insert($data);
    }
}

<?php

use Illuminate\Database\Seeder;

class ProblemCategoriesTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        //
        $data = [

    		[ 
                'id' => 1,
    			'category'=>'ehs'
    		],
    		[
                'id' => 2,
    			'category'=>'quality'
    		],
    		[
                'id' => 3,
    			'category'=>'service'
    		],
    		[
                'id' => 4,
    			'category'=>'productivity'
    		],    		
    		[
                'id' => 5,
    			'category'=>'team'
    		],
    		
    	];
        
    	DB::table('problem_categories')->insert($data);
    }
}

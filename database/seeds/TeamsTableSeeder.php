<?php

use Illuminate\Database\Seeder;
use Carbon\Carbon;

class TeamsTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $start = "2019-01-01";
        $data[0]['date'] = $start;
        $data[0]['no_of_employees'] = 6;
        $data[0]['no_of_managers'] = 10;
        
        for ($i=1; $i < 365; $i++) { 
            $start = (new Carbon($start))->addDays(1)->format("Y-m-d");
            $data[$i]['date'] = $start;
            $data[$i]['no_of_employees'] = 6;
            $data[$i]['no_of_managers'] = 10;
        }
    	DB::table('teams')->insert($data);

        DB::statement("update teams set no_of_employees = 4 where DAYNAME(`date`) = 'Friday'");
    }
}

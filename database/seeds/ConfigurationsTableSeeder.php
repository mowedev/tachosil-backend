<?php

use Illuminate\Database\Seeder;
use App\Models\TeamMember;

class ConfigurationsTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('configurations')->truncate();

        $data = [
            [
                'section_id' => 5,
                'key' => 'color_ranges',
                'value' => '',
            ],
            [
                'section_id' => 5,
                'key' => 'capacity_increments',
                'value' => '',
            ],
            [
                'section_id' => 6,
                'key' => 'team_member_count',
                'value' => TeamMember::count() ? TeamMember::count() : 10,
            ],
            [
                'section_id' => 6,
                'key' => 'team_leadership_names',
                'value' => '[{"id":4,"name":"Head Of Department"},{"id":5,"name":"Business Unit Manager"},{"id":6,"name":"Site Leader Team"}]',
            ],
            [
                'section_id' => 8,
                'key' => 'general_daily_meeting_time',
                'value' => '9:00 AM',
            ],
            [
                'section_id' => 8,
                'key' => 'general_board_title',
                'value' => 'Tachosil',
            ],
            [
                'section_id' => 8,
                'key' => 'general_lock_code',
                'value' => '11111',
            ],
            [
                'section_id' => 8,
                'key' => 'general_lock_time',
                'value' => 10 * 60,
            ],
            [
                'section_id' => 8,
                'key' => 'general_front_version',
                'value' => "0.2.13",
            ],
            [
                'section_id' => 8,
                'key' => 'general_backend_version',
                'value' => "1.0.0",
            ],
            [
                'section_id' => 5,
                'key' => 'productivity_statistics_targetline',
                'value' => '90',
            ],
            [
                'section_id' => 5,
                'key' => 'productivity_dashboard_mode',
                'value' => 'week',
            ],
            [
                'section_id' => 3,
                'key' => 'quality_statistics_targetline',
                'value' => '90',
            ],
            [
                'section_id' => 4,
                'key' => 'service_statistics_targetline',
                'value' => '90',
            ],
            [
                'section_id' => 7,
                'key' => 'challenges_statuses',
                'value' => '[{"name":"ToDo","color":"#ea5532"},{"name":"In Progress","color":"#fd9642"},{"name":"In Review","color":"#eba800"},{"name":"Done","color":"#abb436"}]',
            ],
            [
                'section_id' => 8,
                'key' => 'general_board_type',
                'value' => 'Timeline',
            ],
            [
                'section_id' => 8,
                'key' => 'general_board_ftp',
                'value' => '/var/www/html/xml-tachosil',
            ],
            [
                'section_id' => 8,
                'key' => 'general_board_url',
                'value' => 'http://takeda-redesign-dev.moweex-dev.xyz/',
            ],
            [
                'section_id' => 8,
                'key' => 'general_board_import_url',
                'value' => 'http://takeda-redesign-backend-dev.moweex-dev.xyz/api/v2/task/xml/import',
            ],
        ];

        DB::table('configurations')->insert($data);
    }
}

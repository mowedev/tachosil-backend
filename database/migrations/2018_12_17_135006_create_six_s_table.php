<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateSixSTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('six_s', function (Blueprint $table) {
            $table->increments('id');
            $table->date('date')->nullable();
            $table->decimal('safety',5,1)->nullable();
            $table->decimal('order',5,1)->nullable();
            $table->decimal('documents',5,1)->nullable();
            $table->decimal('materials',5,1)->nullable();
            $table->decimal('clothing',5,1)->nullable();
            $table->decimal('security',5,1)->nullable();
            $table->decimal('discipline',5,1)->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('six_s');
    }
}

<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateDueDateLogsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('due_date_logs', function (Blueprint $table) {
            $table->increments('id');

            $table->integer('row_week'); 
            $table->integer('row_year'); 
            $table->string('sample_number'); 
            $table->date('login_date'); 
            $table->date('tv_due_date'); 
            $table->date('tv_created_at'); 
            $table->tinyInteger('state'); 
            $table->integer('days'); 

            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('due_date_logs');
    }
}

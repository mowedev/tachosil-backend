<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class RenameIsHardMovedInTableTaskVersion extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        //
        Schema::table('task_version', function (Blueprint $table) {
            $table->renameColumn('is_hard_moved', 'is_hard_moved_from_backlog');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
     //   $table->dropColumn('is_hard_moved_from_backlog');

    }
}

<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateCountHolidaysFunction extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        DB::unprepared('
            DROP FUNCTION IF EXISTS `holidays_in`;
            CREATE FUNCTION holidays_in(begin DATE, end DATE) returns INTEGER DETERMINISTIC
            BEGIN
                declare off_days int;
                SELECT COUNT(*) INTO off_days FROM holidays WHERE (holidays.date <= end AND holidays.date >= begin);
                return off_days;
            END
        ');
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        DB::unprepared(' DROP function `holidays_in` ');
    }
}

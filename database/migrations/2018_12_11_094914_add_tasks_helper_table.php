<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddTasksHelperTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('tasks_helper', function (Blueprint $table) {
            $table->integer('sample_number');
            //$table->float('duration')->nullable();
            $table->integer('batch')->nullable();
            $table->string('product')->nullable();
            $table->text('description');
            $table->string('stage');
            //$table->string('status');
            $table->date('due_date')->nullable();
            $table->string('project')->nullable();
            $table->date('login_date')->nullable();
            $table->date('receive_date')->nullable();
            //$table->boolean('is_orignal')->nullable();
            $table->string('reported_name');
            $table->string('analysis');
            $table->string('sample_status');
            $table->string('test_status');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('tasks_helper');
    }
}

<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateTaskVersionTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('task_version', function (Blueprint $table) {
            $table->increments('id');
            //$table->integer('task_id');
            $table->string('sample_number');
            $table->integer('order');
            $table->date('due_date');
            $table->integer('state');
            $table->integer('move_state');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('task_version');
    }
}

<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateBacklogLogsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('backlog_logs', function (Blueprint $table) {
            $table->increments('id');

            $table->string('batch')->nullable();
            $table->string('product')->nullable();
            $table->string('sample_number')->nullable();
            $table->string('project')->nullable();
            $table->string('description')->nullable();

            $table->date('due_date');
            $table->date('date_reviewed')->nullable();
            $table->date('date_completed')->nullable();
            $table->date('login_date')->nullable();
            $table->date('receive_date')->nullable();

            $table->string('status')->nullable();
            $table->double('duration', 10, 2)->default(0);

            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('backlog_logs');
    }
}

<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateUserDefinedTasksTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('user_defined_tasks', function (Blueprint $table) {

            $table->increments('id');
            $table->string('due_date')->nullable();
            $table->text('description')->nullable();
            $table->string('batch')->nullable();
            $table->string('sample_number')->nullable();
            $table->float('duration', 8, 2)->default(0);
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('user_defined_tasks');
    }
}

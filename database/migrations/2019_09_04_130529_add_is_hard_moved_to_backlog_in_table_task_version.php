<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddIsHardMovedToBacklogInTableTaskVersion extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        //
        Schema::table('task_version', function (Blueprint $table) {
            //

           $table->boolean('is_hard_moved_to_backlog')->default(0);

        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
/*        Schema::table('subtasks', function (Blueprint $table) {
            //
            $table->dropColumn('is_hard_moved_to_backlog');

        });
*/
    }
}

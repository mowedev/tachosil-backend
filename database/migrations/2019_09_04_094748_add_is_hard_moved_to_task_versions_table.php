<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddIsHardMovedToTaskVersionsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('task_version', function (Blueprint $table) {
            //

           $table->boolean('is_hard_moved')->default(0);

        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
/*        Schema::table('task_version', function (Blueprint $table) {
            //
            $table->dropColumn('is_hard_moved');

        });*/
    }
}

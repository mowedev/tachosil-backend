<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateTempTasksTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('temp_tasks', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('sample_number');
            $table->date('due_date')->nullable();
            $table->date("date_completed")->nullable();
            $table->date("date_reviewed")->nullable();

        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('temp_tasks');
    }
}

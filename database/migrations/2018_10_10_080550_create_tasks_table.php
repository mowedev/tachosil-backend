<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateTasksTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('tasks', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('order');
            $table->integer('sample_number');
            $table->float('duration')->nullable();
            $table->integer('batch')->nullable();
            $table->string('product')->nullable();
            //$table->string('material_number')->nullable();
            $table->text('description');
            $table->string('stage');
            $table->string('status');
            //$table->datetime('sample_logindate');
            //$table->datetime('sample_receivedate');
            //$table->datetime('test_date_completed');
            //$table->datetime('test_reviewed');
            $table->date('due_date');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('tasks');
    }
}

<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateTempSubtasksTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('temp_subtasks', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('sample_number')->unsigned();
            $table->string('reported_name');
            $table->string('analysis');
            $table->string('sample_status');
            $table->string('test_status');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('temp_subtasks');
    }
}

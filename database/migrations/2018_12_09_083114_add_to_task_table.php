<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddToTaskTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('tasks', function (Blueprint $table) {
            $table->date('login_date')->nullable();
            $table->date('receive_date')->nullable();
            $table->date('date_completed')->nullable();
            $table->date('date_reviewed')->nullable();
            $table->boolean('is_orignal')->nullable();
            //$table->string('test_status')->nullable();
            //$table->string('reported_name')->nullable();
            //$table->string('analysis')->nullable();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('task', function (Blueprint $table) {
            //
        });
    }
}

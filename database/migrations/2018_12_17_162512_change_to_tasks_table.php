<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class ChangeToTasksTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('tasks', function (Blueprint $table) {
            $table->string('order')->nullable()->change();
            $table->string('sample_number')->nullable()->change();
            $table->string('description')->nullable()->change();
            $table->string('stage')->nullable()->change();
            $table->string('status')->nullable()->change();
            $table->string('due_date')->nullable()->change();
            $table->string('parent_id')->nullable()->change();
            $table->string('is_front')->nullable()->change();
            
            $table->integer('is_user_defined')->default(0)->nullable();

        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('tasks', function (Blueprint $table) {
            //
        });
    }
}

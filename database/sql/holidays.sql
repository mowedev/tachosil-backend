DELIMITER $$
--
-- Functions
--
CREATE  FUNCTION `holidays_in` (`begin` DATE, `end` DATE) RETURNS INT(11) BEGIN
                declare off_days int;
                SELECT COUNT(*) INTO off_days FROM holidays WHERE (holidays.date <= end AND holidays.date >= begin);
                return off_days;
            END$$

DELIMITER ;
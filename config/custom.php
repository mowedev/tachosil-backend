<?php

return [

    'types' => [
        'ehs-accidents', 'ehs-observations',
        'quality-not-run', 'quality-lab-error', 'quality-lims', 'quality-product-defects', 'quality-certificates', 'quality-others',
        'service-own-sample',
    ],
    'sixs_monthly_target' => 4.6,

    'paging' => [
        'per_page' => 15,
    ],

    'problems' => [
        'page' => 6,
    ],
    'highlights' => [
        'page' => 8,
    ],

    'code' => 11111,

    'mailing_list' => ['mustafa@mowesolutions.com', 'sobhy.wagih@mowesolutions.com', 'ibrahim.hamdy@mowesolutions.com', 'aya.tork@mowesolutions.com'],

    'meeting_time' => "08:30:00",

    'number_of_employees' => 10,
    'number_of_managers' => 99,
    'charts_target' => 90,
    'admin_tasks_categories' => ['AdminTask', 'Training', 'Meeting', 'NonTestTask'],
    'logs_days' => 14,
    'backend_url' => env('APP_URL') . '/api/v2',
    'instance_id' => env('INSTANCE_ID'),
    'configurations_service' => env('CONFIGURATIONS_SERVICE_URL'),
];

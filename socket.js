var app = require('express')();
var cors = require('cors');
var bodyParser = require('body-parser');
const axios = require('axios');


app.use(cors());
app.use(bodyParser.json({
    limit: '10mb'
})); // for parsing application/json
app.use(bodyParser.urlencoded({
    limit: '10mb',
    extended: true,
    parameterLimit: 1000000
}));

app.post('/', (req, res) => {
    let data = req.body;
    console.log('emitting dashboard');
    console.log(data);
    io.emit('pulse', data);
    res.json({
        'status': 'success'
    });
});

app.get('/', (req, res) => {
    res.json({
        'status': 'success'
    });
});

app.post('/productivity', (req, res) => {
    let data = req.body;
    console.log('emitting productivity');
    io.emit('productivity', data);
    res.json({
        'status': 'success'
    });
});

app.post('/logs', (req, res) => {

    let data = req.body;
    console.log('sending logs');

    axios.post('http://location-backend-testing.d2esadb293.us-east-2.elasticbeanstalk.com/logs/board/update', {
    data,
    boardName : "Packmittelboard"
    })
    .then(function (response) {
        console.log(response.data);
    })
    .catch(function (error) {
        console.log(error);
    });

   // io.emit('logs', data);

    res.json({
        'status': 'success'
    });
});


app.get('/test', function (req, res) {
    res.sendFile(__dirname + '/index.html');
});

const server = require("http").createServer(app);
const io = require("socket.io")(server);

io.on("connection", (socket) => {
    console.log(`Client connectied [id=${socket.id}]`);
    socket.on("disconnect", () => {
        console.info(`Client gone [id=${socket.id}]`);
    });

    socket.on("productivity", (data) => {
        console.log(data);
    });

    socket.on("logs", (data) => {
        console.log(data);
    });

    socket.on("pulse", (data) => {
        console.log(data);
    });
});

server.listen(3000, () => {
    console.log("listening on 3000");
});
